/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdarg.h>
#include <stdlib.h>
#include <net_message_debug_dist.h>
#include <Tools/component_tools.h>

#include <device.h>
#include <Components/bus.h>
#include "basic_tools.h"

#define MAXIMUM_SIZE_OF_COMMAND 128

CREATE_MISSING(receive)
CREATE_MISSING(receive_on_channel)
CREATE_MISSING(receive_block)
CREATE_MISSING(send_command)
CREATE_MISSING(send_command_block)
CREATE_MISSING(send_and_receive)
CREATE_MISSING(send_command_on_channel);
CREATE_MISSING(there_is_data_to_receive)
CREATE_MISSING(lock)
CREATE_MISSING(unlock)
CREATE_MISSING(check_recv_header)
CREATE_MISSING(get_values)
CREATE_MISSING(set_values)

void bus_bufferize(type_bus *bus, const char * command, int length_of_command);
void bus_bufferizef(type_bus *this, const char *format, ...);
int bus_sendf_command(type_bus *this, const char *format, ...);
void bus_flush(type_bus *bus);

void bus_add_channel(type_bus *bus, const char *id, type_receive_callback receive_callback, void *user_data)
{
  type_channel_callback *channel;
  MANY_REALLOCATIONS(&(bus->channels), bus->channels_nb+1);
  channel = &bus->channels[bus->channels_nb];
  channel->id = id;
  channel->receive_callback = receive_callback;
  channel->user_data = user_data;
  bus->channels_nb++;
}

void init(type_bus *bus)
{
  bus->channels = NULL;
  bus->channels_nb = 0;
  bus->initialization = xml_try_to_get_string(bus->node, "initialization");
  bus->finalization = xml_try_to_get_string(bus->node, "finalization");

  BUFFER_INIT(&bus->receive_buffer);
  BUFFER_LOCK(&bus->receive_buffer);

  BUFFER_INIT(&bus->send_buffer);

  bus->bufferize = bus_bufferize;
  bus->bufferizef = bus_bufferizef;
  bus->sendf_command = bus_sendf_command;
  bus->flush = bus_flush;

  LINK_DRIVER(bus, send_command);
  LINK_DRIVER(bus, send_command_block);
  LINK_DRIVER(bus, send_command_on_channel);

  LINK_DRIVER(bus, there_is_data_to_receive);
  LINK_DRIVER(bus, receive);
  LINK_DRIVER(bus, receive_on_channel);
  LINK_DRIVER(bus, receive_block);
  LINK_DRIVER(bus, send_and_receive);
  LINK_DRIVER(bus, lock);
  LINK_DRIVER(bus, unlock);
  LINK_DRIVER(bus, check_recv_header);

  LINK_DRIVER(bus, get_values);
  LINK_DRIVER(bus, set_values);

}

void start(type_bus *this)
{
  if (this->initialization != NULL) this->send_command(this, this->initialization, strlen(this->initialization));
}

void stop(type_bus *this)
{
  if (this->finalization != NULL) this->send_command(this, this->finalization, strlen(this->finalization));
}

void bus_bufferize(type_bus *bus, const char * command, int command_size)
{
  buffer_append(&(bus->send_buffer), command, command_size);
}

void bus_bufferizef(type_bus *this, const char *format, ...)
{
  char command[MAXIMUM_SIZE_OF_COMMAND];

  va_list arguments;

  va_start(arguments, format);
  vsprintf(command, format, arguments);
  va_end(arguments);

  this->bufferize(this, command, strlen(command));
}

int bus_sendf_command(type_bus *this, const char *format, ...)
{
  char command[MAXIMUM_SIZE_OF_COMMAND];

  va_list arguments;

  va_start(arguments, format);
  vsprintf(command, format, arguments);
  va_end(arguments);

  return this->send_command(this, command, strlen(command));
}

void bus_flush(type_bus *bus)
{
  bus->send_command(bus, bus->send_buffer.data, bus->send_buffer.position);
  bus->send_buffer.position = 0;
}
