/*
 * publish.c
 *
 *  Created on: 30 juil. 2014
 *      Author: sylvain
 */

#include <net_message_debug_dist.h>
#include <Tools/component_tools.h>


#include <Components/publish.h>

CREATE_MISSING(channel_create)
CREATE_MISSING(channel_is_connected)
CREATE_MISSING(channel_write)
CREATE_MISSING(channel_set_negociation_message)
CREATE_MISSING(channel_register_receive_callback)
CREATE_MISSING(destroy)

void init(Publish *this)
{
  LINK_DRIVER(this, channel_create)
  LINK_DRIVER(this, channel_is_connected)
  LINK_DRIVER(this, channel_write)
  LINK_DRIVER(this, channel_set_negociation_message)
  LINK_DRIVER(this, channel_register_receive_callback)
  LINK_DRIVER(this, destroy)
}

void start(Publish *this)
{
   (void)this;
}

void stop(Publish *this)
{
   (void)this;
}
