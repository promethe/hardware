/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file 
 \brief
 

 Author: Alexandre Pitti
 */

#include <net_message_debug_dist.h>
#include <Components/motor.h>

#define SIZE_BUFF 256
#define MAXIMUM_SIZE_OF_COMMAND 128

typedef struct kinova_motor {

  /* Attention a l'ordre des includes ! */

#include <Components/generic_fields.h>
#include <Components/motor_fields.h>

  const char *name;
  float previous_set_position;
  int refresh;
  int position;
  int refresh_torque;
  float torque;
  int passif_mode;

/* #include <Components/generic_fields.h>
  Device *device;
  Node *node;
  type_bus *bus;

*/
/* #include <Components/motor_fields.h>

  float position_0; // angle min
  float position_1; // angle max
  float positional_gain; // gain
  float speed_1;

  // virtual functions
  float (*get_position)(type_motor *this); 
  void (*get_torque)(type_motor *this);
  void (*set_position)(type_motor *this, float normalized_position);
  void (*set_speed)(type_motor *this, float normalized_speed);
  void (*set_torque)(type_motor *this, float normalized_torque);  

*/
  
} type_kinova_motor;

type_kinova_motor *constructor()
{
  type_kinova_motor *motor = ALLOCATION(type_kinova_motor);
  memset(motor, 0, sizeof(type_kinova_motor));
  return motor;
}

void init(type_kinova_motor* motor)
{
  /*int mode = 0;
  int mode_present;*/ /*Commente pour cause de warning, decommenter si besoin */
  motor->name = xml_get_string(motor->node, "name");
}

void stop(type_kinova_motor *motor)
{
  (void) motor;
}

// ok
float get_normalized_position(type_kinova_motor *motor)
{
  return (motor->position - motor->position_0) / ((float) (motor->position_1) - motor->position_0);
}

// ok
void get_position(type_kinova_motor* motor, float* position)
{
  int i, ret;
  int val[6];
  type_kinova_motor *mymotor = NULL;
  char buffer[SIZE_BUFF];
  float result;

  if (motor->refresh == 0)
  {
    memset(val, 0, sizeof(val));
    motor->bus->send_and_receive(motor->bus, "Proprio_groupe", 14, buffer, SIZE_BUFF);
    
    
    
    ret = sscanf(buffer, "motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d ", val, val + 1, val + 2, val + 3, val + 4, val + 5);
    if (ret != 6)
    {
      dprints("In get position\n");
      dprints("msg : %s\n", buffer);
      EXIT_ON_ERROR("motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d \n", val[0], val[1], val[2], val[3], val[4], val[5]);

    }
    
    
    //printf("DEBUUUGG = motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d \n",val[0],val[1],val[2],val[3],val[4],val[5]);


    for (i = 0; i < 6; i++)
    {
      mymotor = (type_kinova_motor*) motor->device->components[i];
      mymotor->position = val[i];
      mymotor->refresh = 1;
    }
  }
  motor->refresh = 0;


	
	result = get_normalized_position(motor);
	dprints("VALUE = %f\n",result);

  *position= result;
}

// ok
float get_absolute_position(type_kinova_motor *motor, float normalised_position)
{
  return motor->position_0 + motor->positional_gain * normalised_position;
}


//ok
void set_position(type_kinova_motor *motor, float* normalised_position)
{
  int absolute_position;
  char command[MAXIMUM_SIZE_OF_COMMAND]; /* new Alex */

  if (*normalised_position < 0) *normalised_position = 0;
  if (*normalised_position > 1) *normalised_position = 1;

  absolute_position = get_absolute_position(motor, *normalised_position);

  dprints("Motor %s, absolute_position %d\n", motor->name, absolute_position);

  if (fabs(motor->previous_set_position - absolute_position) > 0.01)

  dprints("\n\nPos %s #%d# from %s %d\n\n\n", motor->name, absolute_position, __FUNCTION__, __LINE__);

  /* ALEX 2013 motor->bus->bufferizef(motor->bus, "Pos %s #%d# ", motor->name, absolute_position);
  motor->bus->flush(motor->bus);*/
  /* NEW 2013 */
  sprintf(command, "Pos %s #%d# ", motor->name, absolute_position);
  motor->bus->send_command(motor->bus, command, strlen(command));

  motor->previous_set_position = absolute_position;
}

//ok
void set_speed(type_kinova_motor *parent, float *normalised_speed)
{
  int vitesse = 0;
  char commande[255];

  if (*normalised_speed > 1) *normalised_speed = 1;
  else if (*normalised_speed < -1) *normalised_speed = -1;

  memset(commande, 0, 255);

  vitesse = (int) (*normalised_speed * parent->speed_1);
  /*vitesse = (int)parent->speed_1;*/
  /* snprintf(commande, 255, "val_joint_vitesse%s:%s = %d\n", this->name, this->name, vitesse);
   parent->bus->transmit_receive(parent->bus,commande,reponse); */

  parent->bus->bufferizef(parent->bus, "Vit %d ", vitesse);

}

/**
 * Donne si le bras est en train d'etre manipule ou pas
 **/
//ok
void get_torque(type_kinova_motor *motor, float* torque)
{

  int i, ret;
  int val[6];
  type_kinova_motor *mymotor = NULL;
  char buffer[SIZE_BUFF];

  if (motor->refresh_torque == 0)
  {
    memset(val, 0, sizeof(val));
    motor->bus->send_and_receive(motor->bus, "get_passif_states", 14, buffer, SIZE_BUFF); /* rq : length are in fact not used in katana_bus...*/
    ret = sscanf(buffer, "Passif > motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d ", val, val + 1, val + 2, val + 3, val + 4, val + 5);
    if (ret != 6)
    {
      dprints("In get torque\n");
      dprints("msg : %s\n", buffer);
      EXIT_ON_ERROR("motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d \n", val[0], val[1], val[2], val[3], val[4], val[5]);

    }

    for (i = 0; i < 6; i++)
    {
      mymotor = (type_kinova_motor*) motor->device->components[i];
      mymotor->torque = (float) (val[i]);
      mymotor->refresh_torque = 1;
    }
  }
  else
  {
    motor->refresh_torque = 0;
  }

  *torque= motor->torque;
}

// ok
void set_torque(type_kinova_motor *motor, float* value)
{
  char buff[SIZE_BUFF];

  switch (motor->passif_mode)
  {
  case 0: /*switch passive mode*/
    if (*value > 0.5) motor->bus->send_and_receive(motor->bus, "passif_switch", 13, buff, SIZE_BUFF);
    break;
  case 1: /*select passive mode*/
    if (*value < 0.5) /* if low torque then activate passif mode*/
    motor->bus->send_and_receive(motor->bus, "passif_switch #1#", 17, buff, SIZE_BUFF);
    else motor->bus->send_command(motor->bus, "passif_switch #0#", 17);
    break;
  }
}
