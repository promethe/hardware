/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file 
\brief


Author: Sebastien Razakarivony
Created:
Modified:
- author: Antoine de Rengerve
- description: divers ajouts et corrections
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
- Containts functions needed for a motor of the katana arm

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: bus
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet.

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <net_message_debug_dist.h>
#include <Components/motor.h>

#define SIZE_BUFF 256

typedef struct Katana_motor{

	/* Attention a l'ordre des includes ! */

	#include <Components/generic_fields.h>
	#include <Components/motor_fields.h>

    const char *name;
    float previous_set_position;
    int refresh;
  int refresh_torque;
    int position;  
    float torque;
	float pos3D[3];
	int refresh_pos3D;
    int rotation;
    int passif_mode;
} Katana_motor;

Katana_motor *constructor()	{
   Katana_motor *motor=ALLOCATION(Katana_motor);
   memset(motor,0,sizeof(Katana_motor));
   return motor;
}

void init(Katana_motor* this)
{
  int mode=0;
  int mode_present;
  this->name = xml_get_string(this->node, "name");
  mode_present = xml_try_to_get_int(this->node,"mode",&mode);
  if(!mode_present) {
    PRINT_WARNING("WARNING: Default mode for katana_motor set torque (change current mode)\n");
  }
  if(mode <0 || mode > 1)
    EXIT_ON_ERROR("Unknown passive mode for katana motor %s !\n", this->name);
  this->passif_mode=mode;
  this->refresh = 0;
  if (this->device->number_of_components != 6)
    perror("Not the right number of neurons !\n");
}


void stop (Katana_motor *this)
{
   (void)this;
}

void parse(type_motor **motors,char * reponse);
int get_info(Katana_motor *parent,char * argument,char *valeur);

float get_normalized_position(Katana_motor *this)
{
  return ((float)(this->position)-this->position_0) /this->positional_gain;
}

void get_position(Katana_motor *this, float *position)
{
  int i,ret;
  int val[6];
  Katana_motor *motor=NULL;
  char buffer[SIZE_BUFF];
  
  
  if (this->refresh==0)
    {
      memset(val, 0, sizeof(val));
      this->bus->send_and_receive(this->bus,"Proprio_groupe",14,buffer,SIZE_BUFF);
      ret = sscanf(buffer,"motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d ",val,val+1,val+2,val+3,val+4,val+5);
      if(ret != 6) {
	dprints("In get position\n");
	dprints("msg : %s\n",buffer);
	EXIT_ON_ERROR("motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d \n",val[0],val[1],val[2],val[3],val[4],val[5]);
      }
/*       printf("motor0: %s ;motor1: %s ;motor2: %s ;motor3: %s ;motor4: %s ;motor5: %s \n",val[0],val[1],val[2],val[3],val[4],val[5]); */
      
      for (i=0; i<6; i++)
        {
	  motor = (Katana_motor*)this->device->components[i];
	  motor->position = val[i];
	  motor->refresh = 1;
        }
    }
  this->refresh = 0;
  
  *position=get_normalized_position(this);
}

/*
void parse(type_motor **motors,char * reponse)
{
    char *val,*type;
    int i = 0;

    type = strtok(reponse,":");

    for ( i = 0 ; i< 6; i++)
    {
        Katana_motor *motor = (Katana_motor*)&motors[i];
        val = strtok(reponse,":");
        while(strcmp(motor->name,val)!=0)
        {
        val  = strtok(NULL,";");
        val  = strtok(NULL,":");
        }
        val = strtok(NULL,";");

    get_info(motors[i],type,val);
    }
}*/

float get_absolute_position(Katana_motor *motor, float normalised_position)
{
	return motor->position_0 + motor->positional_gain * normalised_position;
}



void get_3D_pos_of_joint( Katana_motor *this, float** pos3D_of_joint)
{
   int i,ret;
   float pos3D[15];
   Katana_motor *motor=NULL;
   char buffer[SIZE_BUFF];

   if (this->refresh_pos3D==0)
   {
      memset(pos3D, 0, sizeof(pos3D));
      this->bus->send_and_receive(this->bus,"get3DPosOfJoint",14,buffer,SIZE_BUFF); /* rq : length are in fact not used in katana_bus...*/
      ret = sscanf(buffer,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", &pos3D[0], &pos3D[1], &pos3D[2], &pos3D[3], &pos3D[4], &pos3D[5],
            &pos3D[6], &pos3D[7], &pos3D[8], &pos3D[9], &pos3D[10], &pos3D[11], &pos3D[12], &pos3D[13], &pos3D[14]);
      if(ret != 15) {
         EXIT_ON_ERROR("Error in get_3D_pos_of_joint\n");
     }

      for (i=0; i<5; i++)
      {
         motor = (Katana_motor*)this->device->components[i];
         motor->pos3D[0] = (float)(pos3D[3*i]);
         motor->pos3D[1] = (float)(pos3D[3*i+1]);
         motor->pos3D[2] = (float)(pos3D[3*i+2]);
         motor->refresh_pos3D = 1;
      }
   }
   else
   {
      this->refresh_pos3D = 0;
   }

   *pos3D_of_joint = this->pos3D;
}

void set_position(Katana_motor *this, float *normalised_position)
{
    int absolute_position;

    if (*normalised_position<0)
      *normalised_position = 0;
    if (*normalised_position>1)
      *normalised_position =1;

    absolute_position = get_absolute_position(this, *normalised_position);

    dprints("Motor %s, absolute_position %d\n",this->name,absolute_position);

    if (fabs(this->previous_set_position -absolute_position) > 0.01)
    
	dprints("\n\nPos %s #%d# from %s %d\n\n\n", this->name, absolute_position, __FUNCTION__, __LINE__);	


    this->bus->bufferizef(this->bus,"Pos %s #%d# ", this->name, absolute_position );
    this->bus->flush(this->bus);

    this->previous_set_position = absolute_position;
}


/*
float get_position(Motor *parent)
{
	char commande[255];
    Katana_motor *this = NULL;

    this =  (Katana_motor*)parent->data ;

    if (this->refresh)
    {
        snprintf(commande,255,"proprio_%s:%s,\n",this->name,this->name);
        parent->bus->prepare_formated_command(parent->bus,"proprio_%s:%s,\n",this->name,this->name);
        parent->bus->flush(parent->bus);
        this->refresh = 1;
    }
return this->position;
}
*/

void set_speed(Katana_motor *parent, float *normalised_speed)
{
    int vitesse=0;
    char commande[255];

    if (*normalised_speed > 1)
       *normalised_speed = 1;
    else if (*normalised_speed < -1)
       *normalised_speed = -1;

    memset(commande, 0, 255);

    vitesse = (int)(*normalised_speed * parent->speed_1);
    /*vitesse = (int)parent->speed_1;*/
    /* snprintf(commande, 255, "val_joint_vitesse%s:%s = %d\n", this->name, this->name, vitesse);
    parent->bus->transmit_receive(parent->bus,commande,reponse); */

	parent->bus->bufferizef(parent->bus,"Vit %d ", vitesse );

}

void set_activation(Katana_motor *parent, float normalised_activation1, float normalised_activation2)
{
	char commande[255];

	if (normalised_activation1 > 1.)
		normalised_activation1 = 1.;
	else if (normalised_activation1 < 0.)
		normalised_activation1 = 0.;

	if (normalised_activation2 > 1.)
		normalised_activation2 = 1.;
	else if (normalised_activation2 < 0.)
		normalised_activation2 = 0.;

	memset(commande, 0, 255);

	/*vitesse = (int)parent->speed_1;*/
	/* snprintf(commande, 255, "val_joint_vitesse%s:%s = %d\n", this->name, this->name, vitesse);
    parent->bus->transmit_receive(parent->bus,commande,reponse); */

	parent->bus->bufferizef(parent->bus,"Act %f Act %f ", normalised_activation1, normalised_activation2 );

}

/**
 * Donne si le bras est en train d'etre manipule ou pas
 **/
void get_torque(Katana_motor *this, float *torque)
{

  int i,ret;
  int val[6];
  Katana_motor *motor=NULL;
  char buffer[SIZE_BUFF];
  
  
  if (this->refresh_torque==0)
    {
      memset(val, 0, sizeof(val));
      this->bus->send_and_receive(this->bus,"get_passif_states",14,buffer,SIZE_BUFF); /* rq : length are in fact not used in katana_bus...*/
      ret = sscanf(buffer,"Passif > motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d ",val,val+1,val+2,val+3,val+4,val+5);
      if(ret != 6) {
	dprints("In get torque\n");
	dprints("msg : %s\n",buffer);
	EXIT_ON_ERROR("motor0: %d ;motor1: %d ;motor2: %d ;motor3: %d ;motor4: %d ;motor5: %d \n",val[0],val[1],val[2],val[3],val[4],val[5]);
      }
      
      for (i=0; i<6; i++)
        {
	  motor = (Katana_motor*)this->device->components[i];
	  motor->torque = (float)(val[i]);
	  motor->refresh_torque = 1;
        }
    }
  else
    {
      this->refresh_torque = 0;
    }
  
  *torque= this->torque;
}



void set_torque(Katana_motor *this, float *value)
{
  /**
   * Convention : entre -1/-0.5 : switching du mode passif
   *              entre -0.5/0 : pas de switching du mode passif
   *		  entre 0/0.5 : mode passif mis a false
   *		  entre 0.5/1 : mode passif active
   **/

  char buff[SIZE_BUFF];

  switch(this->passif_mode) {
  case 0: /*switch passive mode*/
    if ( *value >0.5 ) this->bus->send_and_receive(this->bus,"passif_switch",13,buff,SIZE_BUFF);
    break;
  case 1: /*select passive mode*/
    if(*value <0.5) /* if low torque then activate passif mode*/
      this->bus->send_and_receive(this->bus,"passif_switch #1#",17,buff,SIZE_BUFF);
    else 
      this->bus->send_command(this->bus,"passif_switch #0#",17);
    break;
  }
}
