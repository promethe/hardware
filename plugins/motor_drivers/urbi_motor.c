/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <math.h>
#include <unistd.h>

#include <net_message_debug_dist.h>
#include <Components/motor.h>
#include <Components/bus.h>

typedef struct urbi_motor
{
	/* Attention a l'ordre des includes ! */
	#include <Components/generic_fields.h>
	#include <Components/motor_fields.h>

	const char *name;
	float previous_set_position, previous_set_speed;
	float current_torque;

}Urbi_motor;


Urbi_motor *constructor()	{
	return ALLOCATION(Urbi_motor);
}

static float get_absolute_position(Urbi_motor *motor, float normalised_position)
{
	return motor->position_0 + motor->positional_gain * normalised_position;
}

static float get_absolute_speed(Urbi_motor *motor, float normalised_speed)
{
	return  motor->positional_gain * normalised_speed;
}

static float get_normalized_position(Urbi_motor *motor, float absolute_position)
{
	return (absolute_position - motor->position_0) /motor->positional_gain;
}

void init(Urbi_motor *this)
{
	this->name = xml_get_string(this->node, "name");
	this->previous_set_position = -100;
	this->previous_set_speed = 0;
	this->current_torque = 0.0;
}

void get_position(Urbi_motor *this, float* position)
{
	float absolute_position;
	int id;

   	this->bus->lock(this->bus);
	this->bus->sendf_command(this->bus, "%s.val;", this->name);
	this->bus->receive(this->bus, (this->bus->receive_buffer).data, SIZE_OF_RECEIVE_BUFFER);
	this->bus->unlock(this->bus);

	sscanf((this->bus->receive_buffer).data, "[%d] %f", &id,  &absolute_position);

	*position = get_normalized_position(this, absolute_position);
}


void set_position(Urbi_motor *this, float *normalised_position)
{
    float absolute_position;

	if ((*normalised_position < 0) || (*normalised_position > 1))
	{
		printf("Warning: value %f outside the range of positions [0,1] for motor '%s' !\n", *normalised_position, this->name);
		if (*normalised_position < 0) *normalised_position = 0;
		else *normalised_position = 1.0;
	}

   absolute_position = get_absolute_position(this, *normalised_position);

   if (fabs(this->previous_set_position - absolute_position)> 0.001)
   this->bus->bufferizef(this->bus, "%s.val = %f,\n", this->name, absolute_position);

   this->previous_set_position = absolute_position;
}


void set_speed(Urbi_motor *this, float *normalised_speed)
{
   float absolute_speed;

	if ((*normalised_speed < -1) || (*normalised_speed > 1))
	{
		PRINT_WARNING("Value %f outside the range of speeds [-1 ,1] for motor '%s' !\n", *normalised_speed, this->name);
		if (*normalised_speed < -1) *normalised_speed = -1.0;
		else *normalised_speed = 1.0;
	}

    if (fabs(this->previous_set_speed - *normalised_speed)>0.001)
    {
      absolute_speed = get_absolute_speed(this, *normalised_speed);

		this->bus->bufferizef(this->bus, "%s.val = %s.val + %f, \n", this->name,  this->name, absolute_speed);
    }

    this->previous_set_speed = *normalised_speed;
}


void set_torque(Urbi_motor *this, float *normalized_torque)
{
	/** Value between in [0, 1] */
	if (fabs(this->current_torque - *normalized_torque)>0.001)
	{
        if(*normalized_torque < 0 || *normalized_torque>1) {
            PRINT_WARNING("Normalized torque %f for motor '%s' outside the range [0,1].", *normalized_torque, this->name);
            if (*normalized_torque < 0) *normalized_torque = 0.0;
            else *normalized_torque = 1.0;
        }
        this->bus->sendf_command(this->bus, "%s.load = %f;\n", this->name, *normalized_torque);
        this->current_torque = *normalized_torque;
	}
}

