/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 * Description : 
 * -Position feedback avalaible : this flag defines if
 * the current position can be read from the servo motor or not.  If
 * flag is not set, the position cannot be read. get_position thus
 * gives the last position command given to the motor.
 *
 * -Vmax : normalised max velocity : V=1 is equivalent to asking to
 * move to limit range in one operation.
 */


#define DEBUG 
#include <net_message_debug_dist.h>
#include <Components/motor.h>

typedef struct servo_motor
{
      /* Attention a l'ordre des includes ! */
#include <Components/generic_fields.h>
#include <Components/motor_fields.h>
      const char *name;
      float position_d;
      int A_pos_fdback; /* depending on the servo motor, a position
			 * feedback may be available or not */
      float pos_init;
      float Vmax;
      const char *controller_type; /* ssc2,ssc12,ssc32 */
      int ssc_num;
}Servo_motor;


Servo_motor *constructor()	{
   return ALLOCATION(Servo_motor);
}

float get_normalized_position(Servo_motor *this, float position)
{  
  return ((float)(position)-this->position_0) /this->positional_gain;
}
float get_absolute_position(Servo_motor *motor, float normalised_position)
{
   return motor->position_0 + motor->positional_gain * normalised_position;
}

void set_position(Servo_motor *this, float *normalised_position)
{
   char buff[255];
   float position;
   int taille_buff=0;
   printf("position envoyee : %f \n", *normalised_position);

   /* calcul de la position absolue */
   if (*normalised_position < 0.)
      *normalised_position = 0.;
   if (*normalised_position > 1.)
      *normalised_position = 1.;
   
   position = get_absolute_position(this, *normalised_position);
   
    
   /* update of desired position */
   this->position_d = position;
   
   /* ecriture de la position dans le buffer pour l'envoyer vers le
    * port serie */
   

   if ((strcmp(this->controller_type, "ssc2") == 0))
   {
      buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
      buff[1] = (char) this->ssc_num; /*!< numero du servo [0-254] */
      buff[2] = (char) position;  /*!< position du servo [0-254] */
      buff[3] = '\0';
      taille_buff = strlen(buff);
      printf("position envoyee : %f \n", position);

   }
   else if (strcmp(this->controller_type, "ssc12") == 0)
   {
      EXIT_ON_ERROR("(%s) : set_position with ssc12 is not validated yet (check code)\n",this->name);
/*       if (speed == -1) */
/*   vitesse = serv->Vdefault; */
/*       else */
/*   vitesse = (int) ((float) speed * serv->Vmult); */
/*       if (vitesse > serv->Vmax) */
/*   vitesse = serv->Vmax; */
/*       if ((vitesse < 1) && (speed != 0) && (serv->Vdefault != 0)) */
/*   vitesse = 1; */

/*       dprints("VITESSE: %d\n", vitesse); */
/*       dprints("commande d'envoi: %X | %X | %X\n", 255, */
/*       vitesse * 16 + this->ssc_num, position); */

/*       buff[0] = (char) 255;   *!< marqueur, toujours a 255 */
/*       buff[1] = (char) (vitesse * 16 + this->ssc_num);    !< numero du servo [0-254] */
/*       buff[2] = (char) position;  *!< position du servo [0-254] */
/*       buff[3] = '\0'; */
   }
   else if (strcmp(this->controller_type, "ssc32") == 0)
   {
      sprintf(buff,"#%d P%d S%d\r",this->ssc_num,(int) (500+8*position),2500/* vitesse qu'il faudra modifier*/);
      dprints("%s : %s\n",this->name, buff);
      taille_buff = strlen(buff);
   }
  else if (strcmp(this->controller_type, "pololu") == 0)
  {
    //printf("pololu reconnu \n ");
    //scanf("%d",&a);
    buff[0] = (char) 0x84;   //mode 
    buff[1] = (char) this->ssc_num;// // Device Id
    buff[2] = (char) (((int)position) & 0x7F);
    buff[3] = (char)  (((int)position) >> 7 & 0x7F); 
    //buff[7] = '\0';
    taille_buff = 4;
  }
   else
   {
      EXIT_ON_ERROR("(%s) : type de materiel (support des servos) non reconnu (joint_servo_command)\nTEST: %s / %d\n", this->name,this->controller_type, this->ssc_num);
   }
    

   /* envoi vers le port serie */
   this->bus->send_command(this->bus,buff, taille_buff);//strlen(buff));
}


void init(Servo_motor *this)
{
   int A_pos=0;
   float pos_ini=0.5;
   int ret;
   float Vmax, tmp_position;
   this->name = xml_try_to_get_string(this->node, "name");
   printf("name = %s \n",this->name);
   this->controller_type = xml_get_string(this->node, "controller_type"); /* ssc2, ssc12,ssc32 */
   printf("controller_type = %s \n", this->controller_type);
   this->ssc_num = xml_get_int(this->node,"ssc_num"); /* num du servo */
   printf("ssc_num = %d \n",this->ssc_num );
   ret = xml_try_to_get_int(this->node,"A_pos_feedback",&A_pos);
   if(!ret) {
      EXIT_ON_ERROR("Missing flag (Position feedback available <A_pos_feedback>) for servo motor %s !\n", this->name);
   }
   this->A_pos_fdback = A_pos;
   if(!xml_try_to_get_float(this->node, "pos_init", &pos_ini)) {
      EXIT_ON_ERROR("Missing 'pos_init' for servo motor %s !\n", this->name);
   }
   if(!xml_try_to_get_float(this->node, "Vmax", &Vmax)) {
      EXIT_ON_ERROR("Missing 'Vmax' for servo motor %s !\n", this->name);
   }
   if(Vmax > 1) Vmax=1;
   this->Vmax = Vmax;
   this->pos_init = pos_ini;
   this->position_d = pos_ini;
   tmp_position = get_normalized_position(this, pos_ini);
   set_position(this, &tmp_position);
}

void stop (Servo_motor *this)
{
   (void)this;
}



void get_position(Servo_motor *this, float *positions)
{
//   float position=-1;
/*TO DO en fonction de A_pos_fdback : requete au servo controleur ou utilisation de la valeur mise a jour par set_position*/

   if(this->A_pos_fdback) {
      EXIT_ON_ERROR("(%s) : Option getting position from servo is not implemented yet!",this->name);
   }
   else
      *positions=get_normalized_position(this, this->position_d);
}



/*TO DO : prevoir une fonction qui prend en compte une commande en
 * vitesse en meme temps que la commande en position */


void set_speed(Servo_motor *this, float *normalised_speed)
{
/* simulation of speed command by adding a variable increment to the
 * current position */
   float position;
   /* get current position */
   get_position(this, &position);

   /* bounding speed */
   if(*normalised_speed > this->Vmax)
      *normalised_speed = this->Vmax;
   if(*normalised_speed < -this->Vmax)
      *normalised_speed = -this->Vmax;

   /* new position */
   position= position+(*normalised_speed);
   set_position(this, &position);

   /* TO DO : servo motors with real speed commands : to be
    * implemented */
}

void get_torque(Servo_motor *this)
{
   /* not implemented */
   (void)this;
}

void set_torque(Servo_motor *this, float normalized_torque)
{
   /* not implemented */
   (void)this;
   (void)normalized_torque;
}

