/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <math.h>
#include <unistd.h>

#include <net_message_debug_dist.h>
#include <Components/motor.h>
#include <Components/bus.h>

/* #define VERBOSE */


typedef struct urbi_motor
{
  /* Attention a l'ordre des includes ! */
#include <Components/generic_fields.h>
#include <Components/motor_fields.h>
  
  float previous_set_position, previous_set_speed;
  float current_torque;
  int position;
  int pos_simu;
}Webots_motor;

void set_position(Webots_motor *this, float* normalised_position);


Webots_motor *constructor()	{
	return ALLOCATION(Webots_motor);
}

static float get_absolute_position(Webots_motor *motor, float normalised_position)
{
  return motor->position_0 + motor->positional_gain * normalised_position;
}

/* unused *
static float get_absolute_speed(Webots_motor *motor, float normalised_speed) 
{ 
	return  motor->positional_gain * normalised_speed; 
}*/ 

static float get_normalized_position(Webots_motor *motor, float absolute_position)
{
	return (absolute_position - motor->position_0) /motor->positional_gain;
}

void init(Webots_motor *this)
{
  char val[10];
  char commande[20];
  char *reponse=NULL;
  float retour;
  int length=-1, ret=-1;
  int initPresent;
  int posSimu_present=0;
  int posSimu=0;

  float init=-1.;

  this->previous_set_position = -100;
  this->previous_set_speed = 0;
  this->current_torque = 0.0;
  
  
  initPresent = xml_try_to_get_float(this->node,"position_init",&init);
#ifdef VERBOSE
  printf("Init is %f <%d>\n",init,initPresent);
#endif
  posSimu_present = xml_try_to_get_int(this->node,"position_simu",&posSimu);
  this->pos_simu = posSimu;
#ifdef VERBOSE
  printf("Pos Simu is %d <%d>\n",posSimu,posSimu_present);
#else
  (void)posSimu;
  (void)posSimu_present;
#endif

  this->bus->send_command(this->bus,"Alive",5);
  length=this->bus->receive(this->bus,(this->bus->receive_buffer).data,SIZE_OF_RECEIVE_BUFFER-1);
  (this->bus->receive_buffer).data[length]=0;
  reponse=this->bus->check_recv_header(this->bus, (this->bus->receive_buffer).data);

#ifdef VERBOSE
	  printf("Buff received : #%s#\n", reponse);
#endif

  memset(val, 0, sizeof(val));
  ret=sscanf(reponse,"%s %s", commande,val);
  if(ret!=2) {
    EXIT_ON_ERROR("In (%s) : Incorrect answer received for init : %s\n",this->device->id,reponse);
  }
  /*  printf("reponse: #%s#\n",val);*/
  if(strcmp(val,"true")!=0) {
    EXIT_ON_ERROR("Device not ready for %s\n",this->device->id);
  }

  if(initPresent) {
    retour=get_normalized_position(this, init);
    set_position(this, &(retour));
  }

}

void get_position(Webots_motor *this, float* position)
{
  float normalised_position;
  char commande[20];
  char val[10];
  char * reponse=NULL;
  int length=-1, ret=-1;
  
  if(this->pos_simu) { /** position simulee */
    this->position=this->previous_set_position;
  }
  else {
    this->bus->send_command(this->bus,"Proprio",7);
    length=this->bus->receive(this->bus,(this->bus->receive_buffer).data,(SIZE_OF_RECEIVE_BUFFER)-1);
    this->bus->receive_buffer.data[length]=0;
    reponse = this->bus->check_recv_header(this->bus, (this->bus->receive_buffer).data);

#ifdef VERBOSE
    printf("Buff received : #%s#\n", reponse);
#endif

    memset(val, 0, sizeof(val));
    ret=sscanf(reponse,"%s %s",commande,val);  
    if(ret!=2) {
      EXIT_ON_ERROR("In (%s) : Incorrect answer received for get_position : %s\n",this->device->id,reponse);
    }

    /*printf("reponse: #%s %s#\n",commande,val);*/
    this->position=atoi(val);
  }

/*   printf("position : %d\n",(int)this->position); */
  normalised_position = get_normalized_position(this, (float)(this->position));
/*   kprints("normalized pos : %f\n", normalised_position); */
  *position = normalised_position;
}


void set_position(Webots_motor *this, float* normalised_position)
{
  float absolute_position;
  char buff[50];
  char *reponse=NULL;
  int length=-1;

/*   cprints("normalised position: %f\n",*normalised_position); */

  if ((*normalised_position < 0) || (*normalised_position > 1))
    {
      printf("Warning: value %f outside the range of positions [0,1] for motor '%s' !\n", *normalised_position, this->device->id);
      if (*normalised_position < 0) *normalised_position = 0;
      else *normalised_position = 1.0;
    }
  
   absolute_position = get_absolute_position(this, *normalised_position);

/*    cprints("absolute position : %f\n",absolute_position); */

   if (fabs(this->previous_set_position-absolute_position)>0.0000001) {
     sprintf(buff,"Servo %3d", (int)absolute_position);
     this->bus->send_command(this->bus,buff,10);
     length=this->bus->receive(this->bus,this->bus->receive_buffer.data,(SIZE_OF_RECEIVE_BUFFER)-1);
     this->bus->receive_buffer.data[length]=0;
     reponse = this->bus->check_recv_header(this->bus, this->bus->receive_buffer.data);
#ifdef VERBOSE
	  printf("Buff received : #%s#\n", reponse);
#else
	  (void)reponse;
#endif
   }
   this->previous_set_position = absolute_position;
}


void set_speed(Webots_motor *this, float *normalised_speed)
{
   (void)this;
   (void) normalised_speed;
   EXIT_ON_ERROR("%s not implemented for %s\n",__FUNCTION__,this->device->id);
}


void set_torque(Webots_motor *this, float *normalized_torque)
{
   (void)this;
   (void)normalized_torque;
   EXIT_ON_ERROR("%s not implemented for %s\n",__FUNCTION__,this->device->id);
}


void stop(Webots_motor *this)
{
   (void)this;
}
