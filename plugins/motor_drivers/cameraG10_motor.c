/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/**
 * \file  
 * \date 24 fevrier 2010
 * \brief to manage the 50g10Y from RC-Tech
 * \author David BAILLY
 * \version 0.1
 *
 * Description: Protocole to use the 50G10Y camera from RC-Tech
 *
 * Macro:
 * -none
 *
 * Local variables:
 * - none
 *
 * Global variables:
 * -none
 *
 * Internal Tools:
 * -none
 *
 * External Tools:
 * -none
 *
 * Links:
 * - description: none/ XXX
 * - input expected group: none/xxx
 * - where are the data?: none/xxx
 * 
 *
 * Known bugs: Be carefull to the fact that the focus range depends on the zoom
 * position. If the zoom position is changed the focus min and max will have to
 * be change. I haven't found an easy solution to that problem knowing that we
 * want to keep the two separate. One possibility is to update it in the
 * getFOCUSposition function but that imply a loss in performances. So if you
 * want to use this option uncomment the \#define UPDATE_FOCUS.
**************************************************************************** !*/
#include <net_message_debug_dist.h>
#include <Components/motor.h>

/**/

/*#define DEBUG*/


/* -------- Pour le calcul du CRC (checksum) -------- */
#define CRCHIGHBIT 	0x8000
#define CRCINIT		0x0000
#define POLYNOME	0x8005


/* -------- Pour les messages -------- */
#define ENQ		0x05
#define ACK		0x06
#define NAK		0x15
#define NB_DATA_CRC	16
#define TRAM_LENGTH	19
/* -------- Buf[0] -------- */
#define STX		0x02
/* -------- Buf[1] -------- */
#define WRITE_CMD	0x21
#define READ_CMD	0x31
#define OK_RSP		0xa0
#define KO_RSP		0xa2
/* -------- Buf[16] -------- */
#define ETX		0x03

/* -------- Update of the focus min and max in getFOCUSposition (cf Know bugs in headings) -------- */
/*#define UPDATE_FOCUS
*/


typedef struct cameraG10_motor
{
  /* Attention a l'ordre des includes ! */
  #include <Components/generic_fields.h>
  #include <Components/motor_fields.h>
  
  char name[128];
  float position_init;
  long min;
  long max;
  int position;
  int former;  
}CameraG10_motor;


/* -------- Local functions for commmunication -------- */

/* -------- Checksum functions -------- */

/*!-----------------------------------------------------------------------------
 * \fn unsigned short reflect (unsigned short crc, int bitnum) 
 * \param[in] crc 	checksum
 * \param[in] bitnum 	indique sur combien de bit la reflection doit etre faite
 * \return checksum apres reflection bit a bit
-----------------------------------------------------------------------------!*/
unsigned short reflect (unsigned short crc, int bitnum)
{
  unsigned short i, j=1, crcout=0;

  for (i=(unsigned short)1<<(bitnum-1); i; i>>=1) /*for (i=0; i<bitnum; i++)*/
  {
    if(crc & i) crcout|=j;
    j<<= 1;
  }
  return (crcout);
}


/*!-----------------------------------------------------------------------------
 * \fn unsigned short crc16(unsigned char* p, unsigned int len)
 * \param[in] p 	pointeur sur le premier element du message
 * \param[in] len 	nombre d'octets dans le message
 * \return checksum associe au message
-----------------------------------------------------------------------------!*/
unsigned short crc16(unsigned char* p, unsigned int len)
{
  unsigned int i, j, c, bit;
  unsigned short crc = CRCINIT;

  for (i=0; i<len; i++)
  {
    c = (unsigned short)*p++;
    /* reflection de l'octet*/
    c = reflect(c, 8);

    for (j=0x80; j; j>>=1)
    {
      bit = crc & CRCHIGHBIT;
      crc<<= 1;
      if(c & j) bit^= CRCHIGHBIT;
      if(bit) crc^= POLYNOME;
    }
  }	
  /* reflection du crc*/
  crc=reflect(crc, 16);

  return(crc);
}


/*!-----------------------------------------------------------------------------
 * \fn void fillInCRC(char* message)
 * \param[in] message 	pointeur sur le premier element du message
-----------------------------------------------------------------------------!*/
void fillInCRC(char* message)
{
  unsigned short crc;

  
  crc = crc16((unsigned char *)&message[1], NB_DATA_CRC);
  message[18] = (unsigned char)crc;
  crc >>= 8;
  message[17] = (unsigned char)crc;
}



/* -------- Initialisation functions -------- */

/*!-----------------------------------------------------------------------------
 * \fn int setFOCUSSPEED(CameraG10_motor *this, int level)
 * \param[in] this 	pointeur
 * \param[in] level	niveau de vitesse entre 0x00 et 0x04
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int setFOCUSSPEED(CameraG10_motor *this, int level)
{
  int ret;
  char buffer[32];
  
  
  if(level<0) level = 0x00;
  if(level>4) level = 0x04;
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x07;
  buffer[4] = (char)level;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_FOCUS_SPEED...\n", __FUNCTION__);
#endif
  
  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_FOCUS_SPEED message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_FOCUS_SPEED message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : SET_FOCUS_SPEED envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_FOCUS_SPEED message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


/*!-----------------------------------------------------------------------------
 * \fn int setZOOMSPEED(CameraG10_motor *this, int level)
 * \param[in] this 	pointeur
 * \param[in] level	niveau de vitesse entre 0x00 et 0x04
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int setZOOMSPEED(CameraG10_motor *this, int level)
{
  int ret;
  char buffer[32];
  
  
  if(level<0) level = 0x00;
  if(level>4) level = 0x04;
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x05;
  buffer[4] = (char)level;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_ZOOM_SPEED...\n", __FUNCTION__);
#endif
  
  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_ZOOM_SPEED message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_ZOOM_SPEED message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : SET_ZOOM_SPEED envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_ZOOM_SPEED message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


/*!-----------------------------------------------------------------------------
 * \fn int setINITIAL(CameraG10_motor *this, char st)
 * \param[in] this 	pointeur
 * \param[in] st	etat ON (1) ou OFF (0)
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int setINITIAL(CameraG10_motor *this, char st)
{
  int ret;
  char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x49;
  buffer[3] = st;
  buffer[4] = 0x00;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_INITIAL ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_INITIAL message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_INITIAL message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : SET_INITIAL ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_INITIAL message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


/*!-----------------------------------------------------------------------------
 * \fn int setFREEZE(CameraG10_motor *this, char st)
 * \param[in] this 	pointeur
 * \param[in] st	etat ON (1) ou OFF (0)
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int setFREEZE(CameraG10_motor *this, char st)
{
  int ret;
  char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x1a;
  buffer[3] = 0x03;
  buffer[4] = st;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_FREEZE ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_FREEZE message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_FREEZE message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : SET_FREEZE ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_FREEZE message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


/*!-----------------------------------------------------------------------------
 * \fn int setAUTOFOCUS(CameraG10_motor *this, char st)
 * \param[in] this 	pointeur
 * \param[in] st	etat ON (1) ou OFF (0)
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int setAUTOFOCUS(CameraG10_motor *this, char st)
{
  int ret;
  char buffer[32];
  
  
  /* Auto Focus on/off */
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x02;
  buffer[4] = st;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_AUTOFOCUS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_AUTOFOCUS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_AUTOFOCUS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_AUTOFOCUS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_AUTOFOCUS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


/*!-----------------------------------------------------------------------------
 * \fn int setAUTOFOCUS(CameraG10_motor *this, char st)
 * \param[in] this 	pointeur
 * \param[in] st	etat ON (1) ou OFF (0)
 * \return 	0 si aucune erreur n'est survenue
 *		1 si une erreur s'est produite
-----------------------------------------------------------------------------!*/
int pushAUTOFOCUS(CameraG10_motor *this)
{
  int ret;
  char buffer[32];
  
  
  /* Push Auto Focus */
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x03;
  buffer[4] = 0x00;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de PUSH_AUTOFOCUS ...\n", __FUNCTION__);
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_AUTOFOCUS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_AUTOFOCUS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : PUSH_AUTOFOCUS envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_AUTOFOCUS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}


int setPeak(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x48;
  buffer[3] = 0x00;
  buffer[4] = 0x00;
  buffer[5] = st;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_PEACK ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_PEACK message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_PEACK message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_PEACK ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_PEACK message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif


  return 0;
}

int setALCFIX(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x48;
  buffer[3] = 0x01;
  buffer[4] = 0x00;
  buffer[5] = st;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_ALCFIX ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_ALCFIX message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_ALCFIX message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_ALCFIX ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_ALCFIX message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}

int setAESFIX(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x48;
  buffer[3] = 0x02;
  buffer[4] = 0x00;
  buffer[5] = st;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_AESFIX ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_AESFIX message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_AESFIX message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_AESFIX ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_AESFIX message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}

int setBLC(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x18;
  buffer[3] = 0x00;
  buffer[4] = st;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_BLC ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_BLC message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_BLC message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_BLC ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_BLC message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}

int setAGCSENS(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x1a;
  buffer[3] = 0x03;
  buffer[4] = st;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_AGCSENS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_AGCSENS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_AGCSENS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_AGCSENS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_AGCSENS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}

int setPRESET(CameraG10_motor *this, char st)
{
  int ret;
  unsigned char buffer[32];
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x20;
  buffer[3] = 0x02;
  buffer[4] = st;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_AGCSENS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf("...\n");
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_AGCSENS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_AGCSENS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_AGCSENS ", __FUNCTION__);
  if(st) printf("ON");
  else printf("OFF");
  printf(" envoye\n");
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_AGCSENS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  return 0;
}

float getFOCUSposition(CameraG10_motor *this)
{
  int ret;
  float position, min, max;
  unsigned char buffer[32];
  
  
  /* Update of focus min and max (un comment if wanted) */
#ifdef UPDATE_FOCUS
    buffer[0] = STX;
    buffer[1] = READ_CMD;
    buffer[2] = 0x45;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x00;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = 0x00;
    buffer[12] = 0x00;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    buffer[16] = ETX;
    
    fillInCRC((char *)buffer);
    ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
    if(ret == 0) EXIT_ON_ERROR("error when sending GET_ZOOM message\n");
    
    ret = this->bus->receive(this->bus, (char *)buffer, 1);
    if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_ZOOM message\n");
      
    ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
    if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_ZOOM message (message length < TRAM_LENGTH)\n");
    
    buffer[0] = ACK;
    ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
    if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
    
    this->min = ((int)(buffer[10])<<8) | ((int)(buffer[11]));
    this->max = ((int)(buffer[8])<<8) | ((int)(buffer[9]));
#endif
    
  buffer[0] = STX;
  buffer[1] = READ_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x01;
  buffer[4] = 0x00;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de GET_FOCUS...\n", __FUNCTION__);
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending GET_FOCUS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_FOCUS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : GET_FOCUS envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_FOCUS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif

  this->position = ((int)(buffer[6])<<8) | ((int)(buffer[7]));
  min = (float)(this->min);
  max = (float)(this->max);
  position = ((float)(this->position) - min)/(max - min);

#ifdef DEBUG
  printf("dbg (%s) : read position = %f (%i [%li %li])\n", __FUNCTION__, position, this->position, this->min, this->max);
#endif

  return position;
}

float getZOOMposition(CameraG10_motor *this)
{
  int ret;
  float position = 0, min, max;
  unsigned char buffer[32];
  
  
  buffer[0] = STX;
  buffer[1] = READ_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x00;
  buffer[4] = 0x00;
  buffer[5] = 0x00;
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de GET_ZOOM...\n", __FUNCTION__);
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending GET_ZOOM message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_ZOOM message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : GET_ZOOM envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_ZOOM message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif
  
  this->position = ((int)(buffer[6])<<8) | ((int)(buffer[7]));
  min = (float)(this->min);
  max = (float)(this->max);
  position = ((float)(this->position) - min)/(max - min);
  
  return position;
}

void setFOCUSposition(CameraG10_motor *this, float normalized_position)
{
  int ret, position;
  float min, max;
  unsigned char buffer[32];
  
#ifdef UPDATE_FOCUS 
  /* Focus range depends on the zoom position */
    buffer[0] = STX;
    buffer[1] = READ_CMD;
    buffer[2] = 0x45;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x00;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = 0x00;
    buffer[12] = 0x00;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    buffer[16] = ETX;

    /* Send the command */
    fillInCRC((char *)buffer);
    ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
    if(ret == 0) EXIT_ON_ERROR("error when sending GET_ZOOM message\n");
    
    /* Wait for ACK */
    ret = this->bus->receive(this->bus, (char *)buffer, 1);
    if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_ZOOM message\n");
      
    /* Read the answer message */
    ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
    if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_ZOOM message (message length < TRAM_LENGTH)\n");
  
    /* Send an ACK message */
    buffer[0] = ACK;
    ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
    if(ret == 0) EXIT_ON_ERROR("error when sending ACK message\n");
    
    this->min = ((int)(buffer[10])<<8) | ((int)(buffer[11]));
    this->max = ((int)(buffer[8])<<8) | ((int)(buffer[9]));
    
    printf("%s %s : focus min = %d  focus max = %d\n", __FUNCTION__, this->name, this->min, this->max);  
#endif 
  
  min = (float)(this->min);
  max = (float)(this->max);
  position = (int)(normalized_position*(max - min) + min);
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x01;
  buffer[4] = (unsigned char)(position>>8);
  buffer[5] = (unsigned char)(position&0xff);
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_FOCUS %f %i [%li %li]...\n", __FUNCTION__, normalized_position, position, this->min, this->max);
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_FOCUS message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_FOCUS message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_FOCUS envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_FOCUS message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif
}

void setZOOMposition(CameraG10_motor *this, float normalized_position)
{
  int ret, position;
  float min, max;
  unsigned char buffer[32];
  
  
  min = (float)(this->min);
  max = (float)(this->max);
  position = (int)(normalized_position*(max - min) + min);
  
  buffer[0] = STX;
  buffer[1] = WRITE_CMD;
  buffer[2] = 0x45;
  buffer[3] = 0x00;
  buffer[4] = (unsigned char)(position>>8);
  buffer[5] = (unsigned char)(position&0xff);
  buffer[6] = 0x00;
  buffer[7] = 0x00;
  buffer[8] = 0x00;
  buffer[9] = 0x00;
  buffer[10] = 0x00;
  buffer[11] = 0x00;
  buffer[12] = 0x00;
  buffer[13] = 0x00;
  buffer[14] = 0x00;
  buffer[15] = 0x00;
  buffer[16] = ETX;
  
#ifdef DEBUG
  printf("   dbg (%s) : envoi de SET_ZOOM %i [%li %li]...\n", __FUNCTION__, position, this->min, this->max);
#endif

  /* Send the command */
  fillInCRC((char *)buffer);
  ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
  if(ret == 0) EXIT_ON_ERROR("error when sending SET_ZOOM message\n");
  
  /* Wait for ACK */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to SET_ZOOM message\n");
    
#ifdef DEBUG
  printf("   dbg (%s) : SET_ZOOM envoye\n", __FUNCTION__);
  printf("   dbg (%s) : en attente de reponse...\n", __FUNCTION__);
#endif
  
  /* Read the answer message */
  ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
  if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to SET_ZOOM message (message length < TRAM_LENGTH)\n");
  
  /* Send an ACK message */
  buffer[0] = ACK;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret == 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
#ifdef DEBUG
  printf("   dbg (%s) : reponse recu\n", __FUNCTION__);
#endif
}


/* -------- End of local functions -------- */




/* -------- Hardware functions -------- */

CameraG10_motor *constructor()
{
	return ALLOCATION(CameraG10_motor);
}

void init(CameraG10_motor *this)
{
  int ret;
  unsigned char buffer[32];
  const char *pt=NULL;
  float pos;

  
  strcpy(this->name, xml_get_string(this->node, "name"));
  
#ifdef DEBUG
  printf("entering in cameraG10_motor.%s %s\n", __FUNCTION__, this->name);
#endif
  
  /* Start the communication by sending the ENQ byte */
  buffer[0] = ENQ;
  ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
  if(ret <= 0) EXIT_ON_ERROR("error when sending ENQ message\n");
  
  /* Wait for the ACK message */
  ret = this->bus->receive(this->bus, (char *)buffer, 1);
  if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no answer to ENQ message\n");
  
  /* Initialisation for zoom or focus */
  if((this->name[0] == 'Z') | (this->name[0] == 'z'))
  {
    /* Could be change in the future but for now we use the min/max for no digital zoom (cf 54G5&G10protocol.pdf pp 51) */
    this->min = 0x1000;
    this->max = 0x145b;
    setINITIAL(this, 1);
    setFREEZE(this, 0);
    setAUTOFOCUS(this, 0);
    setALCFIX(this, 1);
    pt = xml_try_to_get_string(this->node, "position_init");
    if(pt != NULL) this->position_init = atof(pt);

    pos = getZOOMposition(this);
    while( (pos < this->position_init-0.01) |( pos > this->position_init+0.01))
    {
		setZOOMposition(this, this->position_init);
		pos = getZOOMposition(this);
	}
	
  }
  else if((this->name[0] == 'F') | (this->name[0] == 'f'))
  {
    /* Focus range depends on the zoom position */
    buffer[0] = STX;
    buffer[1] = READ_CMD;
    buffer[2] = 0x45;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x00;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = 0x00;
    buffer[12] = 0x00;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    buffer[16] = ETX;

    /* Send the command */
    fillInCRC((char *)buffer);
    ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
    if(ret == 0) EXIT_ON_ERROR("error when sending GET_ZOOM message\n");
    
    /* Wait for ACK */
    ret = this->bus->receive(this->bus, (char *)buffer, 1);
    if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_ZOOM message\n");
      
    /* Read the answer message */
    ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
    if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_ZOOM message (message length < TRAM_LENGTH)\n");
  
    /* Send an ACK message */
    buffer[0] = ACK;
    ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
    if(ret == 0) EXIT_ON_ERROR("error when sending ACK message\n");
    
    this->min = ((int)(buffer[10])<<8) | ((int)(buffer[11]));
    this->max = ((int)(buffer[8])<<8) | ((int)(buffer[9]));
    
    printf("%s %s : focus min = %ld  focus max = %ld\n", __FUNCTION__, this->name, this->min, this->max);
    
    pt = (char*)xml_try_to_get_string(this->node, "position_init");
    if(pt != NULL) this->position_init = atof(pt);
    while(getZOOMposition(this) < this->position_init-0.1) setZOOMposition(this, this->position_init+0.1);
    /*while((getFOCUSposition(this) > 0.55) | (getFOCUSposition(this) < 0.45)) setFOCUSposition(this, 0.5);
    */
  }
  else if((this->name[0] == 'A') | (this->name[0] == 'a'))
  {
    /* Focus range depends on the zoom position */
    buffer[0] = STX;
    buffer[1] = READ_CMD;
    buffer[2] = 0x45;
    buffer[3] = 0x00;
    buffer[4] = 0x00;
    buffer[5] = 0x00;
    buffer[6] = 0x00;
    buffer[7] = 0x00;
    buffer[8] = 0x00;
    buffer[9] = 0x00;
    buffer[10] = 0x00;
    buffer[11] = 0x00;
    buffer[12] = 0x00;
    buffer[13] = 0x00;
    buffer[14] = 0x00;
    buffer[15] = 0x00;
    buffer[16] = ETX;

    /* Send the command */
    fillInCRC((char *)buffer);
    ret = this->bus->send_command(this->bus, (const char *)buffer, TRAM_LENGTH);
    if(ret == 0) EXIT_ON_ERROR("error when sending GET_ZOOM message\n");
    
    /* Wait for ACK */
    ret = this->bus->receive(this->bus, (char *)buffer, 1);
    if((ret == 0) | (buffer[0] != ACK)) EXIT_ON_ERROR("no ACK to GET_ZOOM message\n");
      
    /* Read the answer message */
    ret = this->bus->receive(this->bus, (char *)buffer, TRAM_LENGTH);
    if(ret < TRAM_LENGTH) EXIT_ON_ERROR("error when receiving answer to GET_ZOOM message (message length < TRAM_LENGTH)\n");
  
    /* Send an ACK message */
    buffer[0] = ACK;
    ret = this->bus->send_command(this->bus, (const char *)buffer, 1);
    if(ret == 0) EXIT_ON_ERROR("error when sending ACK message\n");
    
    this->min = ((int)(buffer[10])<<8) | ((int)(buffer[11]));
    this->max = ((int)(buffer[8])<<8) | ((int)(buffer[9]));
    
    
    pt = (char*)xml_try_to_get_string(this->node, "position_init");

    if(pt != NULL) this->position_init = atof(pt);
    while(getZOOMposition(this) < this->position_init-0.1) setZOOMposition(this, this->position_init+0.1);
    /*while((getFOCUSposition(this) > 0.55) | (getFOCUSposition(this) < 0.45)) setFOCUSposition(this, 0.5);
    */
  }
  else EXIT_ON_ERROR("error in the identification: %s is unknown\n", this->name);
    
  
  
#ifdef DEBUG
  printf("buffer[8]=%x, buffer[9]=%x, buffer[10]=%x, buffer[11]=%x\n", buffer[8], buffer[9], buffer[10], buffer[11]);
  printf("camera50G %s : min=%lx=%li max=%lx=%li\n", this->name, this->min, this->min, this->max, this->max);
  printf("exiting camera50G_motor.%s\n", __FUNCTION__);
#endif
}



float get_position(CameraG10_motor *this)
{
  float position;
#ifdef DEBUG
  printf("entering in cameraG10_motor.%s\n", __FUNCTION__);
#endif

  if((this->name[0] == 'Z') | (this->name[0] == 'z'))
  {
    position = getZOOMposition(this);
#ifdef DEBUG  
  printf("exiting camera50G_motor.%s return %f\n", __FUNCTION__, position);
#endif
    return position;
    /*return this->position_0+this->positional_gain*position;*/
  }
  else if((this->name[0] == 'F') | (this->name[0] == 'f'))
  {
    position = getFOCUSposition(this);
#ifdef DEBUG  
  printf("exiting camera50G_motor.%s return %f\n", __FUNCTION__, position);
#endif
    return position;
    /*return this->position_0+this->positional_gain*position;*/
  }
  else if((this->name[0] == 'A') | (this->name[0] == 'a'))
  {
    position = getFOCUSposition(this);
#ifdef DEBUG  
  printf("exiting camera50G_motor.%s return %f\n", __FUNCTION__, position);
#endif
    return position;
    /*return this->position_0+this->positional_gain*position;*/
  }
  else EXIT_ON_ERROR("unknown CameraG10\n");
  
#ifdef DEBUG  
  printf("exiting camera50G_motor.%s return 0.5\n", __FUNCTION__);
#endif
  return 0.5;
}


void set_position(CameraG10_motor *this, float normalised_position)
{
	float pos=0.5;
#ifdef DEBUG
  printf("entering in cameraG10_motor.%s\n", __FUNCTION__);
#endif

  if(normalised_position < 0.0) normalised_position = 0.0;
  if(normalised_position > 1.0) normalised_position =1.0;
  if((this->name[0] == 'Z') | (this->name[0] == 'z'))
  {
	do
	{
    
	  /*setZOOMposition(this, positional_gain*normalised_position-position_0);*/
    setZOOMposition(this, normalised_position);
    pos = get_position(this);
#ifdef DEBUG
    printf("position = %f\n", pos);
#endif
    }while((pos<(normalised_position-0.1)) | (pos>(normalised_position+0.1)));
  }
  else if((this->name[0] == 'F') | (this->name[0] == 'f'))
  {
    do
    {
	  setFOCUSposition(this, normalised_position);
    pos = get_position(this);
#ifdef DEBUG
    printf("position = %f\n", pos);
#endif
    }while((pos<(normalised_position-0.1)) | (pos>(normalised_position+0.1)));
  }
  else if((this->name[0] == 'A') | (this->name[0] == 'a'))
  {
    if(normalised_position > 0.5)
    {
      if(this->former != 1)
      {
        pushAUTOFOCUS(this);
        this->former = 1;
      }
    }
    else
      this->former = 0;
  }
  else EXIT_ON_ERROR("unknown CameraG10\n");

#ifdef DEBUG  
  printf("exiting camera50G_motor.%s\n", __FUNCTION__);
#endif
}


void set_speed(CameraG10_motor *this, float normalised_speed)
{
  char spd;
  
  
#ifdef DEBUG  
  printf("entering in cameraG10_motor.%s\n", __FUNCTION__);
#endif

  if(normalised_speed < 0) spd = 0;
  else if(normalised_speed > 4) spd = 4;
  else spd = (unsigned char)(normalised_speed*4.0);
  
  if((this->name[0] == 'Z') | (this->name[0] == 'z')) setZOOMSPEED(this, spd);
  else if((this->name[0] == 'F') | (this->name[0] == 'f')) setFOCUSSPEED(this, spd);
  
#ifdef DEBUG
  printf("exiting camera50G_motor.%s\n", __FUNCTION__);
#endif
}
