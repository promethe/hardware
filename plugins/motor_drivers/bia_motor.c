/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/if_arp.h>
#include <sys/select.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include "limits.h"

#include "basic_tools.h"
#include <Components/bus.h>
#include <Components/motor.h>

typedef struct bia_motor {
#include <Components/generic_fields.h>
#include <Components/motor_fields.h>
  unsigned int command_address, sensor_address;
  unsigned int torque_address, speed_address;
} type_bia_motor;

typedef struct bia {
  int socket;
  struct sockaddr* socket_address;
} type_bia;

type_bia_motor *constructor()
{
  return ALLOCATION(type_bia_motor);
}

void init(type_bia_motor *motor)
{
  const char *address_name;
  unsigned int forme_addr;
  int forme=0;
  motor->command_address = 0;
  motor->sensor_address = 0;
  address_name = xml_get_string(motor->node, "command_addr");
  if (sscanf(address_name, "%x", &motor->command_address) != 1) EXIT_ON_ERROR("'%s' is not a valid address.");
  address_name = xml_get_string(motor->node, "sensor_addr");
  if (sscanf(address_name, "%x", &motor->sensor_address) != 1) EXIT_ON_ERROR("'%s' is not a valid address.");

  address_name = xml_try_to_get_string(motor->node, "torque_addr");
  if (address_name != NULL )
  {
    if (sscanf(address_name, "%x", &motor->torque_address) != 1) EXIT_ON_ERROR("'%s' is not a valid address.");
  }
  else motor->torque_address = 0;
  address_name = xml_try_to_get_string(motor->node, "speed_addr");
  if (address_name != NULL )
  {
    if (sscanf(address_name, "%x", &motor->speed_address) != 1) EXIT_ON_ERROR("'%s' is not a valid address.");
  }
  else motor->speed_address = 0;
  address_name = xml_try_to_get_string(motor->node, "forme_addr");
  if (address_name != NULL)  if (sscanf(address_name, "%x", &forme_addr) != 1) EXIT_ON_ERROR("'%s' is not a valid address.");
  xml_try_to_get_int(motor->node, "forme",&forme);
  motor->bus->send_command_on_channel(motor->bus, (char*)&forme, 4, (unsigned int)forme_addr);
}

/* Only one motor at a time */
void get_position(type_bia_motor *motor, float *positions)
{
  type_bus *bus = (type_bus*) motor->bus;
  float value;

  bus->receive_on_channel(bus, (char*) &value, 4, (unsigned int) motor->sensor_address);
  *positions = motor_get_normalized_position((type_motor*) motor, value);
}

void set_position(type_bia_motor *motor, float *normalized_positions)
{
  type_bus *bus = (type_bus*) motor->bus;
  float absolute_position;

  absolute_position = motor_get_absolute_position((type_motor*) motor, *normalized_positions);
  bus->send_command_on_channel(bus, (char*) &absolute_position, 4, (unsigned int) motor->command_address);
}

void set_speed(type_bia_motor *motor, const float *normalized_values)
{
  type_bus *bus = (type_bus*) motor->bus;
  float speed;
  //float absolute_speed, absolute_position;
  
  /*
  if (normalized_values[0] > 0) 
  {
	  absolute_position = ((type_motor*) motor)->position_max;
	  absolute_speed = motor_get_absolute_speed((type_motor*)motor, normalized_values[0]);
  }
  else
  {
    absolute_position = ((type_motor*)motor)->position_min;
    absolute_speed = motor_get_absolute_speed((type_motor*)motor, -normalized_values[0]);;
  }
*/
  speed = ((type_motor*)motor)->speed_1 * normalized_values[0]; //(faire pour chaque axe avant d'envoyer ???')

  //bus->send_command_on_channel(bus, (char*) &absolute_speed, 4, (unsigned int) motor->speed_address);
  bus->send_command_on_channel(bus, (char*)&speed, 4, (unsigned int) motor->command_address);
}

void set_torque(type_bia_motor *motor, float *normalized_values)
{
  type_bus *bus = (type_bus*) motor->bus;
  float absolute_torque;

  if (motor->torque_address == 0) EXIT_ON_ERROR("You need to define a torque_addr");

  absolute_torque = motor_get_absolute_torque((type_motor*) motor, *normalized_values);
  bus->send_command_on_channel(bus, (char*) &absolute_torque, 4, (unsigned int) motor->torque_address);
}

