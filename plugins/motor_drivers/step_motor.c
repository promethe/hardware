/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**

\file 
\brief

Author: Pierre Delarboulas
Created: 22/05/2015


 * Description :  driver for step motor

	// Controle du step motor en position seulement
	// aucun retour sur sa position
 */

#include <Components/motor.h>
#include <unistd.h>
#include <math.h>

typedef struct step_motor
{
      /* Attention a l'ordre des includes ! */
#include <Components/generic_fields.h>
#include <Components/motor_fields.h>

  const char *name;
  int cmd;	

  int actu;

}Step_motor;

void set_speed_th(Step_motor *this);

Step_motor *constructor()
{
	return ALLOCATION(Step_motor);
}


void init(Step_motor *this)
{ 
   pthread_t th_step;	
   this->name = xml_try_to_get_string(this->node, "name");
   printf("name = %s \n",this->name);

   if(!xml_try_to_get_float(this->node, "speed", &(this->speed_1))) {
      EXIT_ON_ERROR("Missing 'speed' for step motor %s !\n", this->name);
   }

   // valeur en step par seconde => converti en step par ms
   this->speed_1 = this->speed_1/1000;
   
   if (pthread_create(&th_step, NULL, (void *(*)(void*))set_speed_th , this) != 0)
      {
         EXIT_ON_ERROR("step motor init : Cannot create thread for step motor");
      }

   this->cmd = 0;
   this->actu = 0;

}


float get_normalized_position(Step_motor *this, float position)
{
  return ((float)(position)-this->position_0) /this->positional_gain;
}
float get_absolute_position(Step_motor *motor, float normalised_position)
{
   return motor->position_0 + motor->positional_gain * normalised_position;
}

void set_position(Step_motor *this, float *normalised_position)
{
   (void)this;
   (void)normalised_position;
}


void stop (Step_motor *this)
{
   (void)this;
}


void get_position(Step_motor *this, float *positions)
{
	
   (void)this;
   (void)positions;
}


// Controle du step motor en position 
// pas de temps : 1MS (echantillonage le plus rapide
// nombre de pas de 1 à speed_1 (donné dans la spec du moteur)
void set_speed(Step_motor *this, float *normalised_speed)
{
   float fcmd;
   float norm_speed = *normalised_speed	;

   if(  norm_speed > 1.0) norm_speed = 1.;
   if(  norm_speed < -1.0) norm_speed = -1.;	

   fcmd = norm_speed *this->speed_1 ;		
   this->cmd = round(fcmd);
        
   this->actu = 0;

}

void get_torque(Step_motor *this)
{
   /* not implemented */
   (void)this;
}

void set_torque(Step_motor *this, float normalized_torque)
{
   /* not implemented */
   (void)this;
   (void)normalized_torque;
}



void set_speed_th(Step_motor *this)
{
   char buff[100];
   int buff_size =0;
   char retour[100];

   while(1)
   {
	if( this->actu > 20) this->cmd=0;	
	else this->actu++;

   	buff_size = sprintf(buff,"SM,10,%d,%d\n", this->cmd,this->cmd); 
   
   	 /* envoi vers le port serie */
   	this->bus->send_and_receive(this->bus,buff,buff_size, retour ,  100);
  	usleep(100);

   }	
}

