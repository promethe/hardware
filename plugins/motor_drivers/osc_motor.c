/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <net_message_debug_dist.h>
#include <Components/motor.h>

#define MESSAGE_MAX 256

typedef struct osc_motor
{
	/* Attention a l'ordre des includes ! */
	#include <Components/generic_fields.h>
	#include <Components/motor_fields.h>
    const char* channel;
    type_buffer positions_buffer;
}type_osc_motor;

type_osc_motor *constructor()
{
	return ALLOCATION(type_osc_motor);
}

void osc_receive_callback(char *buffer, int size, void *user_data)
{
  type_buffer *receive_buffer = user_data;

  buffer_replace(receive_buffer, buffer, size);
  BUFFER_UNLOCK(receive_buffer);
}

void init(type_osc_motor *motor)
{
  type_bus *bus = motor->bus;

  motor->channel = xml_get_string(motor->node, "channel");
  BUFFER_INIT(&motor->positions_buffer);
  BUFFER_LOCK(&motor->positions_buffer);
  bus_add_channel(bus, motor->channel, osc_receive_callback, (void*)&motor->positions_buffer);
}

void get_position(type_osc_motor *motor, float *values)
{
  char *type_data, *data;
  int i, read_position;
  type_buffer *buf = &motor->positions_buffer;
  uint32_t host_value, integer;

  BUFFER_LOCK(buf);

  if (buf->data[0] != ',') EXIT_ON_ERROR("Wrong data format for OSC, it is suppose to start with a ',' but it is : '%s'.", buf->data);

  type_data = &buf->data[1];

  read_position = 1 +  motor->elements_nb;
  read_position += 4 - read_position % 4;
  data = &buf->data[read_position];

  for (i = 0; i < motor->elements_nb; i++)
   {
     if (type_data[i] == 'f')
     {
       host_value = ntohl(*(uint32_t*) &data[sizeof(uint32_t) * i]);
       memcpy(&values[i], &host_value, sizeof(uint32_t));
     }
     else if (type_data[i] == 'i')
     {
       host_value = ntohl(*(uint32_t*) &data[i*sizeof(uint32_t)]);
       memcpy(&integer, &host_value, sizeof(uint32_t));
       values[i] = integer; /* Conversion de int en float */
     }
   }
}


/** Pourrait directement appeler data_set_values ... */
void set_position(type_osc_motor *motor, float *normalized_positions)
{
  uint32_t value;
  type_bus *bus = motor->bus;
  int i;
  char empty_array[4] = {0};
  int missing_bytes;
  float absolute_position;

  bus_bufferizef(bus, "/%s/M", motor->channel);

  missing_bytes = 4 - bus->send_buffer.position % 4;
  buffer_append(&bus->send_buffer, empty_array, missing_bytes);
  buffer_append(&bus->send_buffer, ",f" , 2);

  for (i = 1; i< motor->elements_nb; i++) buffer_append(&bus->send_buffer, "f" , 1);

  missing_bytes = 4 - bus->send_buffer.position % 4;
  buffer_append(&bus->send_buffer, empty_array, missing_bytes);

  for (i = 0; i< motor->elements_nb; i++)
  {
    absolute_position = motor_get_absolute_position((type_motor*)motor, normalized_positions[i]);
    memcpy(&value, &absolute_position, sizeof(uint32_t));
    value = htonl(value);
    buffer_append(&bus->send_buffer,  (char*)&value, sizeof(uint32_t));
  }
  missing_bytes = 4 - bus->send_buffer.position % 4;
  buffer_append(&bus->send_buffer, empty_array, missing_bytes);
}

void set_speed(type_osc_motor *this, float normalised_speed)
{
   (void)this;
   (void) normalised_speed;
}

void set_activation(type_osc_motor *this, float normalized_activation1, float normalized_activation2)
{
   (void)this;
   (void) normalized_activation1;
   (void) normalized_activation2;
}

void get_torque(type_osc_motor *this)
{
   (void)this;
}

void get_3D_pos_of_joint(type_osc_motor *this)
{
   (void)this;
}

void set_torque(type_osc_motor *this, float normalized_torque)
{
   (void)this;
   (void)normalized_torque;
}

