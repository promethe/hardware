#include <net_message_debug_dist.h>
#include <Components/distance_sensor.h>

typedef struct eskin_distance_sensor
{
  /* Attention a l'ordre des includes ! */
  #include <Components/generic_fields.h>
  #include <Components/distance_sensor_fields.h>
  float coeff;
 float* distance_matrix;
 float* distance_matrix_ref;
 unsigned char *buffer;
 int width, height;
 size_t buffer_size;
 
}type_eskin_distance_sensor;


type_eskin_distance_sensor *constructor() {
  return ALLOCATION(type_eskin_distance_sensor);
}


void init(type_eskin_distance_sensor *sensor)
{
 sensor->width = xml_get_int(sensor->node, "width"); 
 sensor->height = xml_get_int(sensor->node, "height"); 
 sensor->buffer_size = sensor->width*sensor->height*2+2;
 sensor->buffer = MANY_ALLOCATIONS(sensor->buffer_size, unsigned char );
 sensor->distance_matrix = MANY_ALLOCATIONS(sensor->width*sensor->height ,  float );
 sensor->distance_matrix_ref = MANY_ALLOCATIONS(sensor->width*sensor->height ,  float );
}


float *get_matrix(type_eskin_distance_sensor *sensor)
{
	int i, ret;

  ret = sensor->bus->send_and_receive(sensor->bus,"S", 1, (char*)sensor->buffer, sensor->buffer_size );
  if (ret != sensor->buffer_size ) EXIT_ON_ERROR("No enough (%i) values to read ", ret);
  if ((sensor->buffer[0] != 255) || (sensor->buffer[1] != 255)) EXIT_ON_ERROR("Bad values sent by the skin. \n\tCheck that your .dev has the right port.");

  for(i=0; i<sensor->buffer_size;i++)
  {
   sensor->distance_matrix[i] = (float)(sensor->buffer[2+i*2] + (sensor->buffer[2+i*2+1]<<8))/4096.0; /*Pour Arduino Duo, resolution ADC 12bits*/
  }

  return sensor->distance_matrix;
}

void stop(type_eskin_distance_sensor *sensor)
{
free(sensor->buffer);
free(sensor->distance_matrix);
free(sensor->distance_matrix_ref);
}

