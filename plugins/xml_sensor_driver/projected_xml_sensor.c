/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file 
\brief

Author: Antoine de Rengervé
Created: 14/01/2015


Description:
* sensor based on XML description in .dev file.
* The code is dedicated to sensors projected on neural population. The description (.dev) defines how the sensor works.
* proj_pos : a list of projection pos. There must be one for each sensor.
* proj_zero : zero of projection pos.
* proj_range : range of projection pos.
* inv : allow to inverse values.
* Computed index i of neuron for each sensor is (NB: first index of neuron is 0): 
* $$i_n = (proj_pos_n - proj_zero)/proj_range * (nb_of_neuron-1)$$


http://www.doxygen.org
************************************************************/
#include <net_message_debug_dist.h>
#include <Components/xml_sensor.h>

#define SIZE_BUFF 524

#define AVERAGING_PERIOD 10
#define REFRESH_PERIOD 15

#define DEBUG
/*#define LOGDEBUG*/

typedef struct projected_xml_sensor{

  /* Attention a l'ordre des includes ! */
  
  #include <Components/generic_fields.h>
  #include <Components/xml_sensor_fields.h>
  
  const char *name;
  float *vals;
  float *proj_pos;
  int nb_vals;
  int header_size;
  const char *command;
  int command_size;
  const char *reply_header;
  int reply_header_size;
  char buff[SIZE_BUFF];
  int buff_size;
  float zero;
  float range;
  float proj_zero;
  float proj_range;
	int inv;
  int proj_size;
  float *proj_tab;
#ifdef LOGDEBUG
  FILE *f_debug;  
  FILE *fichier;
#endif
  time_t prev_time;
} Projected_xml_sensor;

Projected_xml_sensor *constructor()	{
	return ALLOCATION(Projected_xml_sensor);
}

void init(Projected_xml_sensor* this)
{
	const char *proj_pos_str;
  char *save_ptr;
  char *extract_buf;
	char *buf_ptr;
	int i,ret;
	
  this->name = xml_get_string(this->node, "name");

	/** init */
  this->prev_time = 0;
  this->command = xml_get_string(this->node, "command");
  this->command_size = strlen(this->command);
  
  this->reply_header = xml_get_string(this->node, "reply");
  this->reply_header_size = strlen(this->reply_header);

  this->nb_vals = xml_get_int(this->node,"num"); 
  this->vals = MANY_ALLOCATIONS(this->nb_vals, float);
  
	this->zero=0.;
	xml_try_to_get_float(this->node, "zero",&(this->zero));
	this->range=1.;
	xml_try_to_get_float(this->node, "range",&(this->range));
  
  /** init proj_pos tab */
  buf_ptr = proj_pos_str = xml_get_string(this->node, "proj_pos");
  this->proj_pos = MANY_ALLOCATIONS(this->nb_vals, float);

 	extract_buf=strtok_r(buf_ptr," ",&save_ptr);
	for(i=0; i<this->nb_vals; i++) {
		if(extract_buf==NULL)
			EXIT_ON_ERROR("Failed to extraction projection position from %s with delim ' ' in sensor %s \n", proj_pos_str, this->name);
		ret=sscanf(extract_buf,"%f",this->proj_pos+i);
  	extract_buf=strtok_r(NULL," ",&save_ptr);
		if(ret!=1)
			EXIT_ON_ERROR("Failed to read projection position of %d/%d sensor in %s \n", i, this->nb_vals, this->name);
	}

	this->proj_zero=0.;
	xml_try_to_get_float(this->node, "proj_zero",&(this->proj_zero));
	this->proj_range=1.;
	xml_try_to_get_float(this->node, "proj_range",&(this->proj_range));
	this->proj_size=-1;
	this->proj_tab=NULL;
	
	
	this->inv=0;
	xml_try_to_get_int(this->node, "inv",&(this->inv));
	
#ifdef DEBUG
	printf("Sensor %s (%d) \n Val cfg : (%f,%f)\n Proj pos cfg : (%f,%f) [inv %d]\n",this->name,this->nb_vals, this->zero, this->range, this->proj_zero, this->proj_range, this->inv);
	for(i=0; i<this->nb_vals; i++) {
		printf("[%d]: %f \n",i,this->proj_pos[i]);
	}
#endif
	

	
#ifdef DEBUG
	printf("\n XML : name %s : nb val : %d\n command %s (%d | %d)\n",this->name,this->nb_vals,
					this->command,this->command_size, this->reply_header_size);
#endif
	
#ifdef LOGDEBUG
   /** init file for debug */
   this->f_debug=fopen("log_debug.txt","w");
	if(this->f_debug==NULL) {
		printf("Error opening log_debug.txt !\n");
		exit(1);
	}
   /** init file for debug */
   this->fichier=fopen("log.txt","w");
	if(this->fichier==NULL) {
		printf("Error opening log.txt !\n");
		exit(1);
	}
#endif
}


void stop (Projected_xml_sensor *this)
{
	(void)this;
	free(this->vals);
	free(this->proj_tab);
#ifdef LOGDEBUG
	fclose(this->f_debug);
	fclose(this->fichier);
#endif
}

/**
 *  Il faut passer une adresse sur un tableau de float et une adresse sur un entier
 * */
void get_sensor(Projected_xml_sensor *this, float **vals, int *nb_vals)
{
	int i, index, ret;
	int header_chck;
	float zero=this->zero;
	float range=this->range;
  char *buf_ptr;
  char *extract_buf;
  char *save_ptr;
  int inv=this->inv;
	#ifdef LOGDEBUG
	FILE *file=this->f_debug;
	FILE *fichier=this->fichier;
	#endif

	/** init proj tab if not already done */
	if(*nb_vals != this->proj_size) {
		if(*nb_vals>0) {
			this->proj_tab=MANY_ALLOCATIONS(*nb_vals, float);
			this->proj_size=*nb_vals;
			memset((void*)this->proj_tab, 0, *nb_vals*sizeof(float));
		}
	}


	/** update this->vals **/
	do {
		ret=this->bus->send_and_receive(this->bus,this->command,this->command_size,this->buff,SIZE_BUFF);	
		header_chck = strncmp(this->reply_header,this->buff,this->reply_header_size);
#ifdef DEBUG			
		if(!ret)
			printf("Char read = %d\n",ret);
		if(header_chck)
			printf("PB reply buffer - header mismatch (%s//%s)\n Retry to read !\n\n",this->reply_header,this->buff);
#endif			
	} while (ret==0 || header_chck);	/* test sur le header de retour */

	/* if reply header ok : */
	buf_ptr=this->buff+this->reply_header_size;
 	extract_buf=strtok_r(buf_ptr,":",&save_ptr);
	for(i=0; i<this->nb_vals; i++) {
		if(extract_buf==NULL)
			EXIT_ON_ERROR("Failed to read projection position of %d/%d sensor in %s (pb of delim ? Shoulod be ':')\n", i, this->nb_vals, this->name);
		ret=sscanf(extract_buf,"%f",this->vals+i);
  	extract_buf=strtok_r(NULL,":",&save_ptr);
		if(ret!=1)
			EXIT_ON_ERROR("Failed to read projection position of %d/%d sensor in %s (not float ?)\n", i, this->nb_vals, this->name);
		this->vals[i]=(this->vals[i]-zero)/range;
		if(inv) {
			this->vals[i] = 1 - this->vals[i];
		}
		/** update proj tab */
		index = (this->proj_pos[i]-this->proj_zero)/this->proj_range * (*nb_vals-1);
		this->proj_tab[index] = this->vals[i];
	}


  *vals= this->proj_tab;
}
