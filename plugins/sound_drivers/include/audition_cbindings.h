/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
 * audition_cbindings.h
 * interface C de la classe audition
 *
 * Author: Mathieu Bernard. BVS. 2009-2010.
 * Contact: mathieu.bernard@bvs-tech.com
 */

#ifndef AUDITION_CBINDINGS_H
#define AUDITION_CBINDINGS_H

#ifdef __cplusplus
extern "C"
{
#else
  typedef struct Audition
  Audition;
#endif


  extern Audition* new_audition();
  extern void delete_audition(Audition *audition);


  /**
   * device : nom ALSA du périphérique de capture
   * fs : frequence d'echantillonage de capture (Hz)
   *
   * return : 0 si init reussie. -1 si erreur
   */
  extern int init_capture(Audition *audition, const char *device, float fs);

  /**
   * nb_channels : nombre de canaux par cochlee
   * lf : fréquence du canal à la plus basse fréquence (Hz)
   * hf : fréquence du canal à la plus haute fréquence (Hz)
   */
  extern void init_cochlea(Audition *audition, int nb_channels, float lf, float hf);

  /**
   * int_time : duree d'integration de l'energie (s)
   */
  extern void init_energy(Audition *audition, float int_time);

  /**
   * inter_dist : distance interaurale (m)
   *
   * return : nombre de delais nb_delays entre -90° et 90°. 
   */
  extern int init_delay_lines(Audition *audition, float inter_dist);

  /**
   * thld : valeur du seuil en sortie de cochlee
   */
  extern void set_spike_thld(Audition *audition, float thld);



  /**
   * calcul sur une nouvelle sample.
   * return : -1 si erreur. 0 sinon.
   */
  extern int compute_new_sample(Audition *audition);



  /**
   * return : échantillon capturé a gauche et a droite.
   */
  extern float get_left_input_sample(Audition *audition);
  extern float get_right_input_sample(Audition *audition);

  /**
   * return : sortie de cochlee pour la sample courante. size nb_channels.
   */
  extern float* get_left_cochleo_sample(Audition *audition);
  extern float* get_right_cochleo_sample(Audition *audition);

  /**
   * return : valeur de l'energie pour la sample courante. size nb_channels.
   */
  extern float* get_left_energy_sample(Audition *audition);
  extern float* get_right_energy_sample(Audition *audition);

  /**
   * return : accumulation des delais depuis le dernier appel à get_delay_table(). size nb_channels*nb_delays.
   */
  extern float* get_delay_table(Audition *audition);

#ifdef __cplusplus
}
#endif

#endif
