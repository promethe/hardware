/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/


#ifndef CAPTURE_ALSA_H
#define CAPTURE_ALSA_H

/**
 * captureAlsa.hh : Capture audio temps réel via ALSA.
 * 
 * Capture stéréo depuis une carte son reconnue par ALSA.
 *
 * Author: Mathieu Bernard
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <alsa/asoundlib.h>

class captureAlsa
{
public:
  /**
   * device : nom ALSA du périphérique de capture 
   * desiredNbFrame : nbre de frames par bloc en sortie
   *     depend d'ALSA, donc pas forcement accepté.
   *     obtenir la valeur réelle : getNbFrames();
   */
  captureAlsa(const char *device="hw:0,0", int desiredNbFrame=1024);
  ~captureAlsa();

  /**
   * retourne 1 si l'initialisation s'est bien passée
   */
  int initOk();

  int getFs();
  int getBlockSize();

  /**
   * capture_sound : enregistre le signal provenant de device pendant nbFrames a 44100 Hz
   * return 1 : erreur, 0 : ok
   */
  int captureSound(float *left, float *right) const;

private:
  const char* device;  
  uint fs;
  uint nbFrames;
  int errorInit;
  short* buffer;
  uint bufferFrames;
  snd_pcm_t* captureHandle;
  snd_pcm_hw_params_t *hwParams;

  int initCapture();
};

#endif
