/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/


#ifndef _ENERGY_H
#define _ENERGY_H

/**
 * energy.hh : A partir d'un signal multicannal, calcul de l'énergie instantanée par canal. Moyenne et variance.
 * Signal continu ou train de spikes.
 *
 * Author: Mathieu Bernard
 */

#include "libauditory/perceptualWindow.hh"
#include "libauditory/integrationWindow.hh"

/*
 * Flag pour activer/desactiver le calcul de variance
 */
#define VAR_OFF 0
#define VAR_ON  1

/*
 * Flag pour définir le type de fenetre d'intégration
 */
#define PERCEP 0
#define INTEGR 1


class energy
{
 public:
  /**
   * Constructeur version perceptualWindow
   * nbChannels : nombre de canaux du signal en entrée.
   * fs : frequence d'echantillonage
   * cf : la frequence de resonance de chaque filtre.
   * var : flag pour activer/desactiver le calcul de variance.
   */
  energy(int nbChannels, float fs, vector<float> &cf, int var=VAR_OFF);

  /**
   * Constructeur version integrationWindow
   * nbChannels : nombre de canaux du signal en entrée.
   * fs: frequence d'echantillonage
   * d : duree d'integration (en s)
   * var : flag pour activer/desactiver le calcul de variance.
   */
  energy(int nbChannels, float fs, float d, int var=VAR_OFF);

  ~energy();

  /** 
   * Input
   *  frame : la frame courante du signal multicanal.
   *
   * Output
   *  mean : energie moyenne par canal
   *  var : variance de l'energie par canal (si calcul de variance activé)
   */
  void set(float *frame, float *mean, float *var = NULL);

 private:
  int nbChannels, varFlag, winFlag;
  float d;
  perceptualWindow *meanpw, *varpw;
  integrationWindow *meaniw, *variw;
};

#endif // _ENERGY_H
