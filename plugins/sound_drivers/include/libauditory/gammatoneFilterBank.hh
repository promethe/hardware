/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/


#ifndef _GAMMATONEFILTERBANK_H
#define _GAMMATONEFILTERBANK_H

/**
 * gammatoneFilterBank.hh : banc de filtre modélisant la cochlée humaine (membrane basilaire).
 * Modèle gammatone.
 *
 * ATTENTION : version float. risque d'explosion des erreurs d'arrondi en BF (<300 Hz pour une fs à 20kHz). En cas de doute, voir nanQuest.hh
 *
 * Authors: Ning Ma (original C and MatLab code), Mathieu Bernard (C++ and further optimisations).
 */

#include <math.h>

#define EARQ   9.26449
#define MINBW  24.7
#define ORDER  1
#define HRECT  0
#define BW_CORRECTION   1.0190
#ifndef M_PI
#define M_PI            3.14159
#endif


class gammatoneFilterBank
{
public:
  /**
   * lf : freq théorique du canal le plus BF (en pratique, on tombe un peu au dessus). Hz.
   * hf : freq du canal le plus HF (Attenion : hf < fs/2.0). Hz.
   * nbChannels : nombre de canaux (et donc de filtres) entre lf et hf.
   * fs : frequence d'échantillonage du signal en entrée. Hz.
   * earQ, minBw, order : NE PAS TOUCHER. Paramêtres de Glasberg et Moore. 
   * hrect : activer (hrect=1) ou désactiver (hrect=0) la half-wave rectification (i.e. mise à zéro des valeurs négatives en sortie de cochlée).
   */
  gammatoneFilterBank(float lf, float hf, int nbChannels, float fs, float earQ = EARQ, float minBw=MINBW, int order=ORDER, int hrect=HRECT);
  ~gammatoneFilterBank();

  /**
   * in : frame courante du signal d'entrée (le signal ne doit pas forcément être normalisé)
   * out[nbChannels] : réponse de la cochlée pour la frame courante. 
   */
  void compute(float in, float *out);

  /**
   * renvoie un pointeur vers les valeurs des frequences centrale de chaque filtre
   */
  float* getCf();

private:
  float getErb(float freq);
  float getCf(float lf, float hf, int c);

  float fs;
  int nbChannels;
  int hrect;
  float earQ, minBw, envf;
  int order;  
  float *cf;
  float *erb;

  int i, j, t;
  float *a, *tpt, *tptbw, *gain;
  float *p0r, *p1r, *p2r, *p3r, *p4r, *p0i, *p1i, *p2i, *p3i, *p4i;
  float *a1, *a2, *a3, *a4, *a5, *u0r, *u0i;
  float *qcos, *qsin, *oldcs, *oldsn, *coscf, *sincf;
};

#endif
