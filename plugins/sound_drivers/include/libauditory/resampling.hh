/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#ifndef RESAMPLING_H
#define RESAMPLING_H

/*
 * resampling.hh : sous/suréchantilloonage bloc par bloc.
 * Interface la bibliothèque samplerate.
 *
 * Author: Mathieu Bernard
 */

#include <samplerate.h>
#include <stdio.h>
#include <vector>
using namespace std;

class resampling
{
public:
  /*
   * inFS : fréquence d'échantillonage en entrée
   * outFs :fréquence d'échantillonage en sortie
   * quality : facteur de qualité -> choix du type d'algo.
   *  voir la doc samplerate pour plus d'infos.
   *
   * initilisation :
   *  appel au constructeur puis a une des 2 fonction setSize
   */
  resampling(int inFs, int outFs, int quality = 1);

  /*
   * Constructeur de copie
   *
   * initilisation :
   *  appel au constructeur puis a une des 2 fonction setSize
   */
  resampling(const resampling &sample);
  ~resampling();

  /*
   * inSize : la taille des blocs en entrée.
   */
  void setInSize(int inSize);

  /*
   * outSize : la taille des blocs en sortie.
   */
  void setOutSize(int outSize);

  /*
   * Quelques accesseurs
   */
  int getInSize();
  int getOutSize();
  int getInFs();
  int getOutFs();
  float getRatio();

  /*
   * in : le bloc en entrée
   * out : le bloc rééchantillonné en sortie
   *
   * retourne 0 si OK, 1 sinon
   */ 
  int sample(float *in, float *out);
  
private:
  int inFs, outFs, inSize, outSize, quality;
  SRC_STATE *src_state;
  SRC_DATA src_data;
  int error;
  float ratio;
};

#endif
