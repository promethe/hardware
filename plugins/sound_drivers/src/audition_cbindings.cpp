/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "audition.h"

extern "C" void delete_audition(Audition *audition)
{
  delete audition;
}

extern "C" Audition* new_audition()
{
  return new Audition;
}

extern "C" int init_capture(Audition *audition, const char *device, float fs)
{
  int err1=0;

  audition->capt = new captureAlsa(device, (int)fs/10);
  err1 = audition->capt->initOk();
  audition->fs = fs;

  audition->left_input_44 = new float[audition->capt->getBlockSize()];
  audition->right_input_44 = new float[audition->capt->getBlockSize()];

  audition->lrs = new resampling(audition->capt->getFs(), fs);
  audition->rrs = new resampling(audition->capt->getFs(), fs);
  audition->lrs->setInSize(audition->capt->getBlockSize());
  audition->rrs->setInSize(audition->capt->getBlockSize());

  audition->block_size = audition->lrs->getOutSize();
  audition->left_input = new float[audition->block_size];
  audition->right_input = new float[audition->block_size];

  err1 = audition->capt->captureSound(audition->left_input_44, audition->right_input_44);
  audition->lrs->sample(audition->left_input_44, audition->left_input);
  audition->rrs->sample(audition->right_input_44, audition->right_input);
  err1 = audition->capt->captureSound(audition->left_input_44, audition->right_input_44);
  audition->lrs->sample(audition->left_input_44, audition->left_input);
  audition->rrs->sample(audition->right_input_44, audition->right_input); 
  err1 = audition->capt->captureSound(audition->left_input_44, audition->right_input_44);
  audition->lrs->sample(audition->left_input_44, audition->left_input);
  audition->rrs->sample(audition->right_input_44, audition->right_input); 
  err1 = audition->capt->captureSound(audition->left_input_44, audition->right_input_44);
  audition->lrs->sample(audition->left_input_44, audition->left_input);
  audition->rrs->sample(audition->right_input_44, audition->right_input); 

  return err1;
}

extern "C" void init_cochlea(Audition *audition, int nb_channels, float lf, float hf)
{
  audition->nb_channels = nb_channels;
  audition->lfb = new gammatoneFilterBank(lf, hf, nb_channels, audition->fs);
  audition->rfb = new gammatoneFilterBank(lf, hf, nb_channels, audition->fs);
  audition->lp = new peak(nb_channels);
  audition->rp = new peak(nb_channels);

  audition->left_cochleo_sample = new float[nb_channels];
  audition->right_cochleo_sample = new float[nb_channels];
  audition->left_peak_sample = new float[nb_channels];
  audition->right_peak_sample = new float[nb_channels];
}

extern "C" void init_energy(Audition *audition, float int_time)
{
  audition->len = new energy(audition->nb_channels, audition->fs, int_time);
  audition->ren = new energy(audition->nb_channels, audition->fs, int_time);

  audition->left_energy_sample = new float[audition->nb_channels];
  audition->right_energy_sample = new float[audition->nb_channels];
}

extern "C" int init_delay_lines(Audition *audition, float inter_dist)
{
  int N= (int)floorf((audition->fs*inter_dist)/(float)V_SON);
  audition->itd = new delayLines(audition->nb_channels, N);

  audition->itd_table = new float[(2*N+1)*audition->nb_channels];

  return 2*N+1;
}

extern "C" void set_spike_thld(Audition *audition, float thld)
{
  audition->spike_thld = thld;
}

extern "C" int compute_new_sample(Audition *audition)
{
	// capture
	  int err=0;
	  if(audition->sample_count >= audition->block_size)
	    {
	      err = audition->capt->captureSound(audition->left_input_44, audition->right_input_44);
	      audition->lrs->sample(audition->left_input_44, audition->left_input);
	      audition->rrs->sample(audition->right_input_44, audition->right_input);
	      audition->sample_count = 0;
	      if(err) return -1;
	    }

	  // cochlees
	  audition->lfb->compute(audition->left_input[audition->sample_count], audition->left_cochleo_sample);
	  audition->rfb->compute(audition->right_input[audition->sample_count], audition->right_cochleo_sample);

	  // peaks
	  for(int i=0; i<audition->nb_channels; i++)
	    {
	      audition->left_peak_sample[i] = audition->left_cochleo_sample[i];
	      audition->right_peak_sample[i] = audition->right_cochleo_sample[i];
	    }
	  audition->lp->set(audition->left_peak_sample, audition->spike_thld);
	  audition->rp->set(audition->right_peak_sample, audition->spike_thld);

	  // energie
	  audition->len->set(audition->left_peak_sample, audition->left_energy_sample);
	  audition->ren->set(audition->right_peak_sample, audition->right_energy_sample);

	  // itd
	  audition->itd->set(audition->left_peak_sample, audition->right_peak_sample);

	  audition->sample_count++;
	  return 0;
}


extern "C" float get_left_input_sample(Audition *audition)
{
  return audition->left_input[audition->sample_count];
}
extern "C" float get_right_input_sample(Audition *audition)
{
  return audition->right_input[audition->sample_count];
}

extern "C" float* get_left_cochleo_sample(Audition *audition)
{
  return audition->left_cochleo_sample;
}

extern "C" float* get_right_cochleo_sample(Audition *audition)
{
  return audition->right_cochleo_sample;
}

extern "C" float* get_left_energy_sample(Audition *audition)
{
  return audition->left_energy_sample;
}

extern "C" float* get_right_energy_sample(Audition *audition)
{
  return audition->right_energy_sample;
}

extern "C" float* get_delay_table(Audition *audition)
{
  audition->itd->get(audition->itd_table);
  audition->itd->clear();

  return audition->itd_table;
}
