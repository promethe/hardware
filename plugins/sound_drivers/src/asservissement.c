/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * asservissement.c
 * Système auditif binaural indiquant dans quelle direction tourner la tête pour s'orienter vers la source sonore la plus intense.
 * Fonctionnement temps réel.
 * Capture sonore stereo (ALSA), cochlées, estimation de l'energie instantannée.
 *
 * Le programme est purement basé sur l'energie. Les lignes de delai mesurant le retard interaural ne sont pas utilisées.
 *
 * Author: Mathieu Bernard. BVS. fevrier 2011.
 * Contact: mathieu.bernard@bvs-tech.com
 */


#include "audition.h"
#include "audition_cbindings.h"
#include <stdio.h>

#define OFF 0
#define ON  1

#define AUD_DEBUG  ON        // mode verbeux ON/OFF


// parametres hardware
#define AUD_BLK_T  0.1       // duree du signal traité par chaque appel à compute() (s).
#define AUD_INTER  0.20      // distance entre les 2 micros (m)
#define AUD_DEVICE "hw:0,0"  // ID alsa de la carte son
#define AUD_FS     20000.0   // frequence d'echantillonage (Hz)
/* FIXME
 * La capture ne fonctionne pas pour la fs native de la carte son.
 * Sous echantillonage obligatoire.
 */
/* Pour régler AUD_DEVICE :
 *  - hw:0,0 = carte son par défaut. Régler le volume de capture avec "alsamixer"
 *  - hw:1,0 = carte son externe/secondaire. "alsamixer -c 1"
 * Audacity permet de vérifier les id et la capture simplement (Edition->Preferences).
 */

// parametres cochlees
#define AUD_NBC    30        // nombre de canaux de cochlee
#define AUD_LF     300.0     // frequence min de la cochlee (Hz)      
#define AUD_HF     8000.0    // frequence max de la cochlee (Hz)      
#define AUD_THLD   0.001     // seuil en sortie de cochlee
/*
 * FIXME
 * Ne pas utiliser la cochlee en dessous de 300 Hz (pour fs = 20kHz).
 * En cas d'explosion NAN des canaux BF, relever AUD_LF.
 */

// parametres energie
#define AUD_ENG_T  0.06       // duree de la fenetre glissante pour integration de l'energie (s)
#define AUD_ENG_P  0.3        // rapport (difference interaurale d'energie) / (energie binaurale moyenne) minimal pour localiser la source sur le coté.


// les sorties de la fonction compute()
#define AUD_ERROR   -1       // erreur au niveau de la capture
#define AUD_SILENCE 0        // silence : pas d'activité au dessus du seuil AUD_THLD
#define AUD_LEFT    1        // présence d'une source sonore à gauche
#define AUD_CENTER  2        // présence d'une source sonore au centre
#define AUD_RIGHT   3        // présence d'une source sonore à droite


// les variables globales
Audition *audition;
int block_size, nb_delay;


// initialisation du modele.
int init();


// calcul sur AUD_BLK_T secondes de signal.
// renvoie une des sorties en #define
int compute();


void clean()
{
  delete_audition(audition);
}


int main()
{
  // initialisation
  if(init())
    {
      printf("init error !\n");
      clean();
      return -1;
    }

  // compute et affichage de la sortie à chaque changement de valeur
  int val = 0, val_prec = 0;
  while(((val = compute()) != AUD_ERROR))
    {
      if(val != val_prec)
      	printf("%d\n", val);
      val_prec = val;

      // ICI
      // Relier la sortie val aux commandes motrices pour asservir le cou
    }

  // desallocation
  clean();
  return 0;
}


/***************************
 * init
 ***************************/
int init()
{
  audition = new_audition();
  if(init_capture(audition, AUD_DEVICE, AUD_FS)) // erreurs possibles si mauvaise conf
    {
      printf("init_capture error !\n");
      return -1;
    }
  init_cochlea(audition, AUD_NBC, AUD_LF, AUD_HF);
  init_energy(audition, AUD_ENG_T);
  nb_delay = init_delay_lines(audition, AUD_INTER);
  set_spike_thld(audition, AUD_THLD);

  // calcul du nombre d'echantillons a traiter par compute()
  block_size = (int)(AUD_BLK_T*AUD_FS);

  return 0;
}


/************************
 * compute
 ************************/
int compute()
{
  float *left_energy, *right_energy, *delay_table;
  float left_energy_sum = 0, right_energy_sum = 0;

  // pour chaque echantillon
  for(int sample_count=0; sample_count < block_size; sample_count++)
    {
      if(compute_new_sample(audition))
	{
	  printf("compute_new_sample error !\n");
	  return AUD_ERROR;
	}

      // infos energie
      left_energy = get_left_energy_sample(audition);
      right_energy = get_right_energy_sample(audition);

      // calcul de la somme de l'energie
      for(int i=0; i<AUD_NBC; i++)
	{
	  left_energy_sum += left_energy[i];
	  right_energy_sum += right_energy[i];
	}            
    }
  float diff = left_energy_sum - right_energy_sum;

  // recuperation des delais accumules
  //  delay_table = get_delay_table(audition);
  
#if AUD_DEBUG == ON
  printf("%f %f\n", left_energy_sum, right_energy_sum);
#endif
  
  // silence ?
  if(left_energy_sum + right_energy_sum < 0.01)
    return AUD_SILENCE;
  
  // lateralisation
  if(2*fabsf(diff) <= AUD_ENG_P*(left_energy_sum+right_energy_sum))
    return AUD_CENTER;
  if(diff < 0)
    return AUD_LEFT;
  if(diff > 0)
    return AUD_RIGHT;

  return AUD_ERROR;
}

