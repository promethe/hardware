/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/ 
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <alsa/asoundlib.h>
#include <Components/sound.h>
#include "blc.h"
     
#define EXIT_ON_SOUND_ERROR(err, msg, ...) EXIT_ON_ERROR("ALSA:%s\n\t"msg, snd_strerror(err), ##__VA_ARGS__)

// Size of the buffer in order to compute average intensity. It does not influence the calculus of direction but the display.
#define SOUND_FRAMES_NB 1024
//forgetting factor. 1 means instantaneous value, 0 means average on all the samples. 0.1 allows some time filtering.
#define MU 0.01
// This define the threshold of sound intensity under which the position is considered as impossible to predict
#define CONFIDENCE_THREHOLD 0.1


typedef struct alsa_sound
{
  /* Attention a l'ordre des includes ! */
#include <Components/generic_fields.h>
#include <Components/sound_fields.h>
const char *device_name;
snd_pcm_t *capture_handle;
snd_pcm_t *playback_handle;
//int nbIt;
}Alsa_sound;
/* Taille de l'en-tête 44 octets */

//sert à quelque chose ?
Alsa_sound *constructor()
{
	return ALLOCATION(Alsa_sound);	
}


snd_pcm_t *init_sound(char const *device_name)
{
   int  result, dir = 0;
   unsigned int sample_rate = 44100;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *capture_handle = NULL;

   result = snd_pcm_open(&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, " Cannot open audio device %s.\n\t You can give it as first argument. Default is 'hw:0,0'.", device_name);
   result = snd_pcm_hw_params_malloc(&hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot allocate hardware parameter structure");
   result = snd_pcm_hw_params_any(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot initialize hardware parameter structure");
   result = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set access type");
   result = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample format");
   result = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &sample_rate, &dir);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample rate");
   result = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set channel count");
   
   
   result = snd_pcm_hw_params(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set parameters");
   snd_pcm_hw_params_free(hw_params);
   result = snd_pcm_prepare(capture_handle);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot prepare audio interface for use");
   return capture_handle;
}

snd_pcm_t *init_sound_output(char const *device_name)
{
	int err, dir=0;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *playback_handle = NULL;
   unsigned int sample_rate = 44100, exact_rate;

   //Infos sortie
   	if ((err = snd_pcm_open (&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0) 
   	{
		EXIT_ON_SOUND_ERROR(err, "cannot open audio device %s (%s)\n", device_name);
	}
		if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
		{
			EXIT_ON_SOUND_ERROR(err, "cannot allocate hardware parameter structure (%s)\n");
		}
				 
		if ((err = snd_pcm_hw_params_any (playback_handle, hw_params)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot initialize hardware parameter structure (%s)\n");
		}
	
		if ((err = snd_pcm_hw_params_set_access (playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set access type (%s)\n");
		}
	
		if ((err = snd_pcm_hw_params_set_format (playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set sample format (%s)\n");
		}
	
	
		if ((err = snd_pcm_hw_params_set_channels (playback_handle, hw_params, 2)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set channel count (%s)\n");
		}
		exact_rate=sample_rate;
		if ((err = snd_pcm_hw_params_set_rate_near (playback_handle, hw_params, &exact_rate, &dir)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set sample rate (%s)\n");
		}
		if(exact_rate != sample_rate)
		{
			printf("\nThe rate %d Hz is not supported by your hardware.\n ==> Using %d Hz instead.\n\n", sample_rate, exact_rate);
		}else printf("\nThe rate %d Hz is supported by your hardware.\n\n\n", exact_rate);


		if ((err = snd_pcm_hw_params (playback_handle, hw_params)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot set parameters (%s)\n");
		}

		
		snd_pcm_hw_params_free (hw_params);
		if ((err = snd_pcm_prepare (playback_handle)) < 0) 
		{
			EXIT_ON_SOUND_ERROR(err, "cannot prepare audio interface for use (%s)\n");
		}

		return  playback_handle;
}

void init(Alsa_sound *this)
{
    char const* carteSon = xml_try_to_get_string(this->node, "carteSon");

        if(carteSon == NULL)
        {
            this->device_name = "hw:0,0"; /*par défaut*/
        }else
        {
            this->device_name = carteSon;
        }
     this->capture_handle = init_sound(this->device_name);
     this->playback_handle = init_sound_output(this->device_name);
    // this->nbIt=0;

}

void get_signal(Alsa_sound *sound,float* spectrum_l,float* spectrum_r, FILE *fp, int16_t num_gpe)
{
  int j,result;
  int16_t buf[SOUND_FRAMES_NB * 2 + 1]={0}; // Quand il y a n cannaux (ici deux), le buffer doit être n fois plus grand

	//sound->nbIt++;
  if ((result = snd_pcm_readi(sound->capture_handle, &buf[1], SOUND_FRAMES_NB)) != SOUND_FRAMES_NB)
  {
    if (result == -EPIPE)
    {
      // On a pas lu assez vite une partie de l'echantillon a ete perdu, mais on continue.
      //Si c'est un problème, il  faudrait faire un thread.

      snd_pcm_recover(sound->capture_handle, result, 0);
      PRINT_WARNING("Miss sound sample");
    }
    else EXIT_ON_SOUND_ERROR(result, "read from audio interface '%s' failed %d\n", sound->device_name, result);
  }
	//~ //TEST SORTIE
	//~ if ((result = snd_pcm_writei (sound->playback_handle, &buf[1], SOUND_FRAMES_NB)) != SOUND_FRAMES_NB) 
	//~ {
		//~ if( result == -EPIPE)
		//~ {
		  //~ snd_pcm_recover(sound->playback_handle, result, 0);
          //~ PRINT_WARNING("Don't right sound sample");
          //~ snd_pcm_prepare( sound->playback_handle) ;
		//~ }
		//~ else EXIT_ON_SOUND_ERROR (result, "write to audio interface failed (%s)\n", sound->device_name);			
	//~ }//else printf("Recopie correcte\n");
      if(fp !=NULL)
      {  
		  if(num_gpe > (int16_t) -1)
		  {
			  buf[0] = num_gpe;
			  //printf("num expression : %d premiere val %d derniere : %d\n", buf[0], buf[1], buf[SOUND_FRAMES_NB*2]);
			fwrite(buf, sizeof(int16_t), SOUND_FRAMES_NB*2 + 1, fp);  
		  }else
		  {
			  //printf("inférieur ou égal à -1 !!\n");
			  fwrite(&buf[1], sizeof(int16_t), SOUND_FRAMES_NB*2, fp);  
		  }
		  
		  
	  }
	FOR (j, SOUND_FRAMES_NB)
	{
		//We normalize the data. Max  is 1 << 15
		// Pour optimiser il ne faudrait peut être pas normalise et ne travailler que sur des entiers. Arnaud B.
		spectrum_l[j] = (float)  buf[j * 2] / (1 << 15);
		spectrum_r[j] =  (float) buf[j * 2 + 1] / (1 << 15);	
	}


}


void stop(Alsa_sound *this)
{
   (void)this;
   
   	snd_pcm_close(this->capture_handle);
   	//snd_pcm_close(this->playback_handle);
}
