/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <net_message_debug_dist.h>
#include <Tools/component_tools.h>
  #include <locale.h>


#include <Components/motor.h>

CREATE_MISSING(get_position)
CREATE_MISSING(set_position)
CREATE_MISSING(set_speed)
CREATE_MISSING(set_activation)
CREATE_MISSING(set_torque)
CREATE_MISSING(get_torque)
CREATE_MISSING(get_3D_pos_of_joint)


float motor_get_normalized_position(type_motor *motor, float position)
{
  return ((float) (position) - motor->position_0) / motor->positional_gain;
}

float motor_get_absolute_position(type_motor *motor, float normalised_position)
{
  float absolute_position;

  absolute_position = motor->position_0 + motor->positional_gain * normalised_position;

  if (absolute_position > motor->position_max) absolute_position = motor->position_max;
  else if (absolute_position < motor->position_min) absolute_position = motor->position_min;

  return absolute_position;
}

float motor_get_absolute_speed(type_motor *motor, float normalised_value)
{
  float absolute_value;

  absolute_value = motor->speed_1 * normalised_value;

  if (absolute_value > motor->speed_1) absolute_value = motor->speed_1;
  else if (absolute_value < 0) absolute_value = 0;

  return absolute_value;
}


float motor_get_absolute_torque(type_motor *motor, float normalized_value)
{
  float absolute_value;

  if (!(motor->torque_max > 0)) PRINT_WARNING("Undefined torque max");
  absolute_value = normalized_value*motor->torque_max;

  if (absolute_value > motor->torque_max) absolute_value = motor->torque_max;

  return absolute_value;
}

void init(type_motor *motor)
{
setlocale(LC_ALL, "C");

  motor->position_0 = xml_get_float(motor->node, "position_0");
  motor->position_1 = xml_get_float(motor->node, "position_1");

  if (!xml_try_to_get_float(motor->node, "position_min", &motor->position_min)) motor->position_min = motor->position_0;

  if (!xml_try_to_get_float(motor->node, "position_max", &motor->position_max))
  {
    if (motor->position_min < motor->position_1) motor->position_max = motor->position_1; /* The order can be reversed */
    else
    {
      motor->position_min = motor->position_1;
      motor->position_max = motor->position_0;
    }
  }

  if (!xml_try_to_get_float(motor->node, "torque_max", &motor->torque_max)) motor->torque_max = 0;

  motor->positional_gain = motor->position_1 - motor->position_0;

  if (!xml_try_to_get_float(motor->node, "speed_1", &motor->speed_1)) motor->speed_1 = 0;

  LINK_DRIVER(motor, set_position)
  LINK_DRIVER(motor, set_speed)
  LINK_DRIVER(motor, set_torque)
  LINK_DRIVER(motor, set_activation)
  LINK_DRIVER(motor, get_position)
  LINK_DRIVER(motor, get_torque)
  LINK_DRIVER(motor, get_3D_pos_of_joint)
}


