/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <net_message_debug_dist.h>
#include <Components/temperature_sensor.h>

#define BUFFER_SIZE 2+8

typedef struct TPA81_distance_sensor
{
  /* Attention a l'ordre des includes ! */
  #include <Components/generic_fields.h>
  #include <Components/temperature_sensor_fields.h>
  float temperature_matrix[8];
}type_TPA81_distance_sensor;


type_TPA81_distance_sensor *constructor() {
  return ALLOCATION(type_TPA81_distance_sensor);
}

/*
void init(type_TPA81_distance_sensor *sensor)
{
 
}

void stop (type_TPA81_distance_sensor *sensor)
{
  
}
*/
float *get_matrix(type_TPA81_distance_sensor *sensor)
{
  int i, ret;
  unsigned char buffer[BUFFER_SIZE];
   
  ret = sensor->bus->send_and_receive(sensor->bus,"T", 1, (char*)buffer, BUFFER_SIZE);
  if (ret != BUFFER_SIZE) EXIT_ON_ERROR("No enough (%i) values to read ", ret);
  if ((buffer[0] != 255) || (buffer[1] != 255)) EXIT_ON_ERROR("Bad values sent by the TPA81. \n\tCheck that your .dev has the right port.");

  for(i=0; i<8;i++)
  {
    sensor->temperature_matrix[i] = (float)(buffer[2+i]);
  }

  return sensor->temperature_matrix;
}

