/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
Author: Arnaud Blanchard
Created: 9/11/2009

************************************************************/

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>


#include <net_message_debug_dist.h>

#include <Components/bus.h>

typedef struct urbi_bus
{
	#include <Components/generic_fields.h>
	#include <Components/bus_fields.h>

	const char* ip;
	int port;
	int socket;
	fd_set fd_settings;
    pthread_mutex_t mutex_socket;
}Urbi_bus;


int receive(Urbi_bus *this, char *buffer, int length);
int there_is_data_to_receive(Urbi_bus *this);


Urbi_bus *constructor()	{
  return ALLOCATION(Urbi_bus);
}



void init(Urbi_bus *this)
{
    int rc;

   	struct hostent *address_of_host;
   	struct sockaddr_in address_of_socket;

    if (pthread_mutex_init(&(this->mutex_socket), NULL)) EXIT_ON_ERROR("The mutex_socket cannot be initialized");

    this->send_buffer.data=MANY_ALLOCATIONS(SIZE_OF_SEND_BUFFER,char);
    this->send_buffer.size=SIZE_OF_SEND_BUFFER;

    this->receive_buffer.data=MANY_ALLOCATIONS(SIZE_OF_RECEIVE_BUFFER,char);
    this->receive_buffer.size=SIZE_OF_RECEIVE_BUFFER;

    (this->send_buffer).position = 0;

    this->ip = xml_get_string(this->node, "ip");
    this->port = xml_get_int(this->node, "port");

    address_of_socket.sin_family = AF_INET;
    address_of_socket.sin_port = htons(this->port);
    address_of_host = gethostbyname(this->ip);

    if (address_of_host == NULL) EXIT_ON_ERROR("Ip %s is not valid", this->ip);
    memcpy(&address_of_socket.sin_addr.s_addr, address_of_host->h_addr_list[0], address_of_host->h_length);
    this->socket = socket(AF_INET, SOCK_STREAM, 0);

    /**  Try to connect to the serveur the hard casting is the "proper way" to do it ! */
    rc=connect( this->socket, (struct sockaddr *) &address_of_socket, sizeof(address_of_socket));
    while(rc != 0)
    {
     /***si echec attente 20ms***/
        usleep(200000);
        rc=connect( this->socket, (struct sockaddr *) &address_of_socket, sizeof(address_of_socket));
        perror("URBI connection error to ");
        printf(" IP : %s port: %d  \n waiting 2seconds before trying to reconnect.",this->ip, this->port);
    }
    printf("Urbi : connection granted to  %s : %d  \n", this->ip, this->port);

	/** On defini la socket (ou fd) a surveiller */
	FD_ZERO(&this->fd_settings);
	FD_SET(this->socket, &this->fd_settings);

	printf("We are waiting for the server ...\n");

	while (there_is_data_to_receive(this))
	{
		receive(this, (this->receive_buffer).data, SIZE_OF_RECEIVE_BUFFER);
		printf("%s\n", (this->receive_buffer).data);
	}
}

void lock(Urbi_bus *my_data)
{
   pthread_mutex_lock(&(my_data->mutex_socket));
}


void unlock(Urbi_bus *my_data)
{
      pthread_mutex_unlock(&(my_data->mutex_socket));
}

int send_command(Urbi_bus *this, const char *command, int size)
{
   int size_of_sent_data;
   size_of_sent_data = send(this->socket, command, size, 0);
   return size_of_sent_data;
}


int receive(Urbi_bus *this, char *buffer, int length)
{
	return recv(this->socket, buffer, length, 0);
}

int there_is_data_to_receive(Urbi_bus *this)
{
    struct timeval time_to_wait;

	/** On attendra 1 s */
	time_to_wait.tv_sec = 1;
	time_to_wait.tv_usec = 0;

	return select(this->socket+1, &this->fd_settings, NULL, NULL, &time_to_wait);
}


void stop(Urbi_bus *this)
{
  if(this->send_buffer.data != NULL) free(this->send_buffer.data);
  if(this->receive_buffer.data != NULL) free(this->receive_buffer.data);
  shutdown(this->socket, SHUT_RDWR);

  free(this);
}

