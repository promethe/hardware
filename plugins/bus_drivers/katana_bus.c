/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  katana_bus.c
\brief

Author: Sebastien Razakarivony
Created:
Modified:
- author:
- description:
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
- Containts functions needed to implment a bus from the katana to the broker

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: bus
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet.

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <net_message_debug_dist.h>
#include <Components/bus.h>

#include <tools.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <pthread.h>

#include <sys/time.h>
#define MAXIMUM_SIZE_OF_BUFFER_KATANA 256
#define NB_SEND 10
#define NB_PING 10
#define TIME_MAX 50000000
/* #define VERBOSE */

typedef struct katana_bus
{
    #include <Components/generic_fields.h>
	#include <Components/bus_fields.h>

    char *name;                     /*Nom donne dans le .dev */
    int port;                       /*port donne dans le .dev */
    const char *host;                     /*IP_host donne dans le .dev*/
    pthread_mutex_t mutex_socket;   /*structure de mutex utile pour avoir plusieurs promethes*/
    int _socket;                     /*socket de communication */
    struct sockaddr_in serv_addr;   /*structure du serveur */
    int ping;
    long compteur; /*compteur des trames*/
    int watchDog; /*watchDog : 1 running, 0 to be stopped, -1 stopped (a mutex would be better)*/
} Katana_bus;

int CKecriture_vers_socket(Katana_bus * chw);
int CKinit_connection(Katana_bus * chw);
int CKlecture_du_socket(Katana_bus * chw);
void CKinit_watchdog(Katana_bus * chw);
long CKping(Katana_bus * chw);
int CKtransmit_receive(Katana_bus * , char *, char *);

/*int create(Device *parent, mxml_node_t *node);*/
void flush(Katana_bus *bus);
void receive(Katana_bus *);
Katana_bus *constructor()	{
   Katana_bus *bus=ALLOCATION(Katana_bus);
   memset(bus,0,sizeof(Katana_bus));
   return bus;
}
void init(Katana_bus *this)
{

    int _socket;
    struct sockaddr_in serv_addr;
    pthread_t my_thread;
    static int already_init = 0;
    long ping=0;
    int i;
    char commande[MAXIMUM_SIZE_OF_BUFFER_KATANA];


    this->send_buffer.data=MANY_ALLOCATIONS(SIZE_OF_SEND_BUFFER,char);
    this->send_buffer.size=SIZE_OF_SEND_BUFFER;

    this->receive_buffer.data=MANY_ALLOCATIONS(SIZE_OF_RECEIVE_BUFFER,char);
    this->receive_buffer.size=SIZE_OF_RECEIVE_BUFFER;

    (this->send_buffer).position = 0;

#ifdef VERBOSE
    printf("Enter in init Katana Bus\n");
#endif


    this->host = xml_get_string(this->node, "host");
#ifdef VERBOSE
    printf("Host is %s\n",this->host);
#endif
    this->port = xml_get_int(this->node,"port");
#ifdef VERBOSE
    printf("Port is %d\n",this->port);
#endif

    /* Creation socket modèle UDP AF_INET entre machines */
    _socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (_socket < 0)
    {
        perror ("socket");
        exit(1);
    }
    /* mise a zero de la socket */
    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(this->port);
    serv_addr.sin_addr.s_addr = inet_addr(this->host);

    this->_socket = _socket;
    this->serv_addr = serv_addr;
    
    this->send_buffer.data  = MANY_ALLOCATIONS(MAXIMUM_SIZE_OF_BUFFER_KATANA, char);
    this->receive_buffer.data  = MANY_ALLOCATIONS(MAXIMUM_SIZE_OF_BUFFER_KATANA, char);
    this->send_buffer.size = MAXIMUM_SIZE_OF_BUFFER_KATANA;
    this->receive_buffer.size = MAXIMUM_SIZE_OF_BUFFER_KATANA;


    if (pthread_mutex_init(&((this)->mutex_socket), NULL))
    {
        kprints("*** %s : %d : Can't initialise mutex : mutex_client_read ***\n", __FUNCTION__, __LINE__);
    }

    /*CKinit_connection(this);*/

    /* Calcul du ping moyen */
    for(i=0;i<NB_PING;i++) {
        ping+=CKping(this);
    }
    this->ping=ping/NB_PING;
#ifdef VERBOSE
    printf("ping moyen : %d\n",this->ping);
#endif

    if (already_init == 0)
    {
        if (pthread_create(&my_thread, NULL, (void *(*)(void *))CKinit_watchdog, this) != 0)
        {
            printf("%s : Erreur de creation du thread pour le watchdog du clientKatana\n", __FUNCTION__);
            exit(1);
        }
        already_init = 1;
    }

    /* ATTENTION : c'est ici qu'on lance la commande init_params qui initialise les parametres du moteur */
    sprintf(commande, "init_params");
    CKtransmit_receive(this,commande,NULL); /* quid de cette commande qui traine ?*/


#ifdef VERBOSE
    printf("\t\tconnection_status=OK\n");
#endif
}

void send_command_old(Katana_bus *this,char *command,int length)
{
    char sendline[MAXIMUM_SIZE_OF_BUFFER_KATANA];
   (void)length;
    if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
	{
	perror("pthread_mutex_lock : flush");
	}

#ifdef VERBOSE
    printf("Buff send : #%s#\n", command);
#endif

    this->compteur++;
    /*on incremente le compteur*/
    sprintf(sendline, "[%ld] %s", this->compteur, command);

    if ( sendto(this->_socket, sendline, strlen(sendline), 0, (struct sockaddr *) &(this->serv_addr), sizeof(this->serv_addr)) < 0) {
        perror ("sendto");
        exit(1);
    }

    //this->position_of_send_buffer = 0;

    pthread_mutex_unlock(&(this->mutex_socket));

    receive(this);
}

/* modification pour utilisation de CK_transmit_receive teste et approuve jusqua passage aux plugins...*/
void send_command(Katana_bus *this,char *command,int length)
{
   (void)length;
#ifdef VERBOSE
    printf("Buff send : #%s#\n", command);
#endif
    CKtransmit_receive(this, command, NULL);

}


void send_and_receive(Katana_bus *this,char *command, int lengthC, char *reply, int lengthR)
{
   (void)lengthC;
   (void)lengthR;
#ifdef VERBOSE
    printf("Buff send : #%s#\n", command);
#endif
/**
 * WARNING : reply must be big enough to store the received msg 
 */
    CKtransmit_receive(this, command, reply);
#ifdef VERBOSE
    printf("Buff received : #%s#\n", reply);
#endif
}

void receive(Katana_bus *this)
{
    int n;
    int result_select;
    struct timeval wait;
    fd_set set;
    int biggest_fd;
    char reponse[MAXIMUM_SIZE_OF_BUFFER_KATANA];
    long cpt;
	struct sockaddr_in from;
	socklen_t salong=sizeof(from);

    wait.tv_sec = 0;
    wait.tv_usec = 1000000; /* 25ms 50000*/

    FD_ZERO(&set);
    FD_SET(this->_socket, &set);

    biggest_fd=this->_socket+1;

    if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
	{
        perror("pthread_mutex_lock : received");
	}

    result_select=select(biggest_fd, &set, NULL, NULL, &wait);

    if ( result_select < 0 ){
        if (errno != EINTR) {
            perror("select");
            exit(1);
        }
    }
    else if (result_select==0){ /*timeout*/
       printf("TIMEOUT dans %s\n",__FUNCTION__);
       sprintf((this->receive_buffer).data,"%s","");
    }
    else
    {
        if ( (n=recvfrom(this->_socket, reponse, MAXIMUM_SIZE_OF_BUFFER_KATANA, 0,(struct sockaddr *)&from,&salong)) < 0 ) {
            perror("recvfrom");
            exit(1);
        }

        reponse[n] = (char) 0;

        sscanf(reponse,"[%ld]",&cpt);/* lecture du numero de la trame */
        sprintf((this->receive_buffer).data,"%s",strstr(reponse," ")+1); /* lecture de la commande */
        if( cpt < (this->compteur) ) {
			printf("WARNING le numero de la trame est incorrect : %s -%li-\n",reponse, this->compteur);
        } else if ( cpt > (this->compteur) ) {
            printf("ERREUR le numero de la trame est incorrect : %s -%li-\n",reponse, this->compteur);
            exit(1);
        }

#ifdef VERBOSE
    printf("Buff received : #%s#\n", this->receive_buffer);
#endif


        /* On test si la reponse correspond a l'un des messages d'erreur */
        if (( strcmp((this->receive_buffer).data,"UnknownCommand")==0 )|| ( strcmp((this->receive_buffer).data,"BadSyntax")==0 )){
            printf("ERREUR clientKatana : %s\n",reponse);
            exit(1);
        }
    }

    pthread_mutex_unlock(&(this->mutex_socket));

}

void stop(Katana_bus* this)
{
  char commande[30];
    this->watchDog=0;
    while(this->watchDog!=-1) {
      usleep(20);
    }

    /* Lorsque l'on quitte : on remet dans l'etat initial les parametres : cela inclue de stopper le bras par securite*/
    sprintf(commande, "init_params");
    CKtransmit_receive(this,commande,NULL);

	free(this->send_buffer.data);
	this->send_buffer.size = 0;
	
	free(this->receive_buffer.data);
	this->receive_buffer.size = 0;
	
	
    shutdown(this->_socket, SHUT_RDWR);
    free(this);
}


/*
void send_and_receive(Device *device)
{
    Katana_bus *this;

    this = (Katana_bus*)device->data_of_driver;

    if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
	{
        perror("pthread_mutex_lock : flush");
	}
#ifdef VERBOSE
    printf("Buff send : #%s#\n", this->buffer);
#endif

    CKecriture_vers_socket(this);
    this->current_position_of_buffer = 0;
    pthread_mutex_unlock(&(this->mutex_socket));

    CKlecture_du_socket(this);

#ifdef VERBOSE
    printf("Buff received : #%s#\n", this->received_buffer);
#endif
}
*/


int CKtransmit_receive(Katana_bus * this, char *commande, char *resultat)
{
    int valeur;
    int i;

    if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
	{
	perror("pthread_mutex_lock : transmit_receive");
	}

    for(i=0;i< NB_SEND;i++) {
       /** avoid pb if command is equal to this->send_buffer*/
       if((this->send_buffer).data != commande)
	  sprintf((this->send_buffer).data,"%s", commande);
       CKecriture_vers_socket(this);
       valeur = CKlecture_du_socket(this);
       if ( valeur == 0 ) {
	  
	  if ( resultat!=NULL )
	     sprintf(resultat, "%s", (this->receive_buffer).data);
	  pthread_mutex_unlock(&(this->mutex_socket));
	  
	  return 0;
       }
       
    }
    pthread_mutex_unlock(&(this->mutex_socket));

    perror("Ah ba mince alors, je crois qu'on a perdu la connexion !!\n");
    exit(1);
}

int transmit_receive(Katana_bus * this, char *commande, char *resultat)
{
    int valeur;
    int i;

    if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
	{
	perror("pthread_mutex_lock : transmit_receive\n");
	}

    for(i=0;i< NB_SEND;i++) {

        sprintf((this->send_buffer).data,"%s", commande);

        CKecriture_vers_socket(this);
        valeur = CKlecture_du_socket(this);


        if ( valeur == 0 ) {
	   
	   if ( resultat!=NULL )
	      sprintf(resultat, "%s", (this->receive_buffer).data);
	   pthread_mutex_unlock(&(this->mutex_socket));
	   
	   return 0;
        }
    }
    pthread_mutex_unlock(&(this->mutex_socket));

    printf("Ah ba mince alors, je crois qu'on a perdu la connexion !!\n");
    exit(0);
}

int CKecriture_vers_socket(Katana_bus *this)
{
    int n;
    char sendline[MAXIMUM_SIZE_OF_BUFFER_KATANA];

    this->compteur++;
    /*on incremente le compteur*/
    sprintf(sendline, "[%ld] %s", this->compteur, (this->send_buffer).data);
    n = strlen(sendline);
	
    if ( sendto(this->_socket, sendline, n, 0, (struct sockaddr *) &(this->serv_addr), sizeof(this->serv_addr)) < 0) {
        perror ("sendto");
        exit(1);
    }

    return 0;
}


int CKlecture_du_socket(Katana_bus * chw)
{

    int n,ret;
    int result_select;
    struct timeval wait;
    fd_set set;
    int biggest_fd;
    char reponse[MAXIMUM_SIZE_OF_BUFFER_KATANA];
    long cpt;
	struct sockaddr_in from;
	socklen_t salong=sizeof(from);

    wait.tv_sec = 0;
    wait.tv_usec = 1000000; /* 25ms 50000*/

    FD_ZERO(&set);
    FD_SET(chw->_socket, &set);

    biggest_fd=chw->_socket+1;

    result_select=select(biggest_fd, &set, NULL, NULL, &wait);

    if ( result_select < 0 )
    {
        if (errno != EINTR) {
            perror("select");
            exit(1);
        }
    }
    else if (result_select==0)
    {
        /*timeout*/
       printf("TIMEOUT dans %s\n",__FUNCTION__);
        sprintf((chw->receive_buffer).data,"%s","");

        return 1;
    }
    else
    {
		/*printf("je rentre ds la fonction %s\n",__FUNCTION__);*/
        if ( (n=recvfrom(chw->_socket, reponse, MAXIMUM_SIZE_OF_BUFFER_KATANA, 0,(struct sockaddr *)&from,&salong)) < 0 ) {
            perror("recvfrom");
            exit(1);
        }
		/*printf("apres le recvfrom ds la fonction %s\n",__FUNCTION__);*/

        reponse[n] = (char) 0;

        ret = sscanf(reponse,"[%ld]",&cpt);/* lecture du numero de la trame */
        (void)ret;
/* 	printf("reponse : %s, ret : %d, cpt : %d, compteur : %d\n",reponse,ret, cpt, chw->compteur); */
        sprintf((chw->receive_buffer).data,"%s",strstr(reponse," ")+1); /* lecture de la commande */
        if( cpt < (chw->compteur) ) {
			printf("WARNING le numero de la trame est incorrect : %s -%li-\n",reponse, chw->compteur);
            return CKlecture_du_socket(chw);
        } else if ( cpt > (chw->compteur) ) {
            printf("ERREUR le numero de la trame est incorrect : %s -%li-\n",reponse, chw->compteur);
            exit(1);
        }

        /* On test si la reponse correspond a l'un des messages d'erreur */
        if (( strcmp((chw->receive_buffer).data,"UnknownCommand")==0 )|| ( strcmp((chw->receive_buffer).data,"BadSyntax")==0 )){
            printf("ERREUR clientKatana : %s\n",reponse);
            exit(0);
        }
    }

    return 0;
}

long CKping(Katana_bus * chw)
{
    long Secondes;
    long MicroSecondes;
    struct timeval Input;
    struct timeval Output;
    char msg[1024];

    gettimeofday(&Input, (void *) NULL);

    sprintf(msg, "Alive");

    CKtransmit_receive(chw,msg,NULL);

    gettimeofday(&Output, (void *) NULL);
    Secondes = Output.tv_sec - Input.tv_sec;
    MicroSecondes = 1000000*Secondes + Output.tv_usec - Input.tv_usec;

#ifdef VERBOSE
    printf("ping respond in\t %ld microsec\n", MicroSecondes);
#endif
    return MicroSecondes;
}

int CKinit_connection(Katana_bus * this)
{

    pthread_t my_thread;
    static int already_init = 0;
    long ping=0;
    int i;
    char commande[MAXIMUM_SIZE_OF_BUFFER_KATANA];

    /* Calcul du ping moyen */
    for(i=0;i<NB_PING;i++) {
        ping+=CKping(this);
    }

    this->ping=ping/NB_PING;
    printf("ping moyen : %d\n",this->ping);

    if (already_init == 0)
    {
        if (pthread_create(&my_thread, NULL, (void *(*)(void *))CKinit_watchdog, this) != 0)
        {
            printf("%s : Erreur de creation du thread pour le watchdog du clientKatana\n", __FUNCTION__);
            exit(0);
        }
        already_init = 1;

    }

    /* ATTENTION : c'est ici qu'on lance la commande StartRobot qui autorise l'activation des moteurs */
    sprintf(commande, "StartRobot");
    CKtransmit_receive(this,commande,NULL); /* quid de cette commande qui traine ?*/

    return 0;
}

void CKinit_watchdog(Katana_bus * this)
{
    int init_watchdog_Katana=1;
    char commande[MAXIMUM_SIZE_OF_BUFFER_KATANA];
    struct timespec *wait=(struct timespec *)malloc(sizeof(struct timespec));
    char msg_back[64];

    gestion_mask_signaux_hardware();

    if ( this->ping > TIME_MAX ) {
        printf("Ouh la la, va falloir changer de connection! Le ping est vraiment trop haut !\n");
        exit(0);
    }

    wait->tv_sec=0;
    wait->tv_nsec=TIME_MAX;

    this->watchDog = 1; /*run watchDog*/

    sprintf(commande, "Alive");
    while ( this->watchDog == 1 ) {
        CKtransmit_receive(this,commande,msg_back);
	if(strcmp(msg_back,"Alive")) {
	  kprints("WARNING : Alive got back wrong msg : %s\n",msg_back);
	}
        if ( init_watchdog_Katana == 0 && strcmp((this->receive_buffer).data,"alive False")==0 ) { //absurde ? si le watchdog est mort, on aura pas de reponse, non ?
            printf("\nArf, le watchdog est mort... : %s\n",(this->receive_buffer).data);
            exit(0);
        }
        init_watchdog_Katana=0;
        nanosleep(wait, NULL);
    }
    this->watchDog = -1;
}
