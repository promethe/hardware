/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
 Author: Antoine de Rengerve
 Created: 25-08-2010

 ************************************************************/


#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#include <net_message_debug_dist.h>

#include <Components/bus.h>

/* #define VERBOSE */
#define MAX_SIZE_BUFF 256
#define UDP_DEFAULT "defaultID"

typedef struct void_bus {
#include <Components/generic_fields.h>
#include <Components/bus_fields.h>
  const char* host;
  const char* id;
  int port;
  int socket;
  struct sockaddr_in serv_addr; /*structure du serveur */
  fd_set fd_settings;
  pthread_mutex_t mutex_socket;
  long compteur; /*compteur des trames*/
  int retry; /* nombre d'essai avant d'abandonner la lecture */
  int raw;
} Udp_bus;

Udp_bus *constructor()
{
  return ALLOCATION(Udp_bus);
}

void init(Udp_bus *bus)
{
  int _socket;
  struct sockaddr_in serv_addr;

#ifdef VERBOSE
  printf("Enter in init Udp Bus %s\n", this->id);
#endif

  bus->host = xml_get_string(bus->node, "host");
#ifdef VERBOSE
  printf("Host is %s\n",bus->host);
#endif
  bus->port = xml_get_int(bus->node, "port");
#ifdef VERBOSE
  printf("Port is %d\n",bus->port);
#endif
  bus->retry = xml_get_int(bus->node, "retry");
#ifdef VERBOSE
  printf("Retry is %d\n",bus->retry);
#endif

  /* shoudl be done automatically for a bus */
  bus->id = xml_try_to_get_string(bus->node, "id");
  if (bus->id == NULL)
  {
    bus->id = UDP_DEFAULT;
    printf("Add name for bus in .dev for more accurate debug\n");
  }

  if (!xml_try_to_get_int(bus->node, "raw", &bus->raw)) bus->raw = 0;

#ifdef VERBOSE  
  printf("ID is %s\n",this->id);
#endif

  /* Creation socket modele UDP AF_INET entre machines */
  _socket = socket(AF_INET, SOCK_DGRAM, 0);
  if (_socket < 0)
  {
    perror("socket");
    exit(1);
  }
  /* mise a 0 de la socket */
  memset(&serv_addr, 0, sizeof(struct sockaddr_in));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(bus->port);
  serv_addr.sin_addr.s_addr = inet_addr(bus->host);

  bus->socket = _socket;
  bus->serv_addr = serv_addr;
  bus->compteur = 0;
  buffer_allocate_min(&bus->send_buffer, SIZE_OF_SEND_BUFFER); /* 16Mo it is probably toot much  */
  bus->send_buffer.position = 0;
  bus->send_buffer.data[0] = '\0';
  
  buffer_allocate_min(&bus->receive_buffer, SIZE_OF_RECEIVE_BUFFER);

  if (pthread_mutex_init(&(bus->mutex_socket), NULL)) EXIT_ON_ERROR("The mutex_socket cannot be initialized");

}

/*
 * Attention, usage de size : C'est position du dernier octet a envoyer dans le
 * buffer d'envoi (cf. flush.)
 */
int send_command(Udp_bus *bus, const char *command, int size)
{
  char sendline[MAX_SIZE_BUFF];
  int ret = 0;

  if (size != 0)
  {

    if (pthread_mutex_lock(&(bus->mutex_socket)) != 0) EXIT_ON_SYSTEM_ERROR("pthread_mutex_lock : send_command");

#ifdef VERBOSE
    printf("(%s)Buff send : #%s#\n",this->id, command);
#endif

    if (bus->raw)
    {
      if (sendto(bus->socket, command, size, 0, (struct sockaddr *) &(bus->serv_addr), sizeof(bus->serv_addr)) < 0) EXIT_ON_SYSTEM_ERROR("Fail sending raw udp data");
    }
    else
    {
      bus->compteur++;
      /*on incremente le compteur*/
      sprintf(sendline, "[%ld] %s", bus->compteur, command);

      if (sendto(bus->socket, sendline, strlen(sendline), 0, (struct sockaddr *) &(bus->serv_addr), sizeof(bus->serv_addr)) < 0) EXIT_ON_SYSTEM_ERROR("Fail sending raw udp data");
      bus->send_buffer.position = 0;
    }
    pthread_mutex_unlock(&(bus->mutex_socket));

  }
  else
  {
#ifdef VERBOSE
    printf("Size of buff to be sent is null, nothing is sent\n");
#endif
    return 0;
  }

  return ret;
}

char *check_recv_header(Udp_bus *this, char *buffer)
{
  char *tmp_buffer;
  long cpt;
  sscanf(buffer, "[%ld]", &cpt);/* lecture du numero de la trame */

  tmp_buffer = (char *) (strchr(buffer, ' ') + 1);

  if (cpt < (this->compteur))
  {
    printf("WARNING le numero de la trame est incorrect : %s -%li-\n", buffer, this->compteur);
  }
  else if (cpt > (this->compteur))
  {
    EXIT_ON_ERROR("Le numero de la trame est incorrect : %s -%li-\n", buffer,  this->compteur);
  }

  /*  printf("%ld>>%s<<\n",(long)tmp_buffer,tmp_buffer);*/
  return tmp_buffer;
}

int receive_no_check(Udp_bus *this, char *buffer, int length)
{
  int n=0;
  int result_select;
  struct timeval wait;
  fd_set set;
  int biggest_fd;
  int count;
  struct sockaddr_in from;
  socklen_t salong = sizeof(from);
  int msg_recv = 0;

  wait.tv_sec = 0;
  wait.tv_usec = 1000000; /* 25ms 50000*/

  FD_ZERO(&set);
  FD_SET(this->socket, &set);

  biggest_fd = this->socket + 1;

  if (pthread_mutex_lock(&(this->mutex_socket)) != 0)
  {
    perror("pthread_mutex_lock : received");
    exit(1);
  }

  count = 0;
  msg_recv = 0;

  while (count < this->retry && msg_recv == 0)
  {

    result_select = select(biggest_fd, &set, NULL, NULL, &wait);

    if (result_select < 0)
    {
      if (errno != EINTR)
      {
        perror("select");
        exit(1);
      }
    }
    else if (result_select == 0)
    { /*timeout*/
      printf("TIMEOUT dans %s for udp_bus (%s)\n", __FUNCTION__, this->id);
      count++;
      sprintf(buffer, "%s", "");
      usleep(10);
    }
    else
    {
      if ((n = recvfrom(this->socket, buffer, length, 0, (struct sockaddr *) &from, &salong)) < 0)
      {
        perror("recvfrom");
        exit(1);
      }
      msg_recv = 1;
    }
  }

  if (msg_recv == 0)
  {
    EXIT_ON_ERROR("No answer after %d trials.\nCannot reach udp server @%s:%d for %s\n", this->retry, this->host, this->port, this->id);
  }

  pthread_mutex_unlock(&(this->mutex_socket));
  return n;
}

int receive(Udp_bus *this, char *buffer, int length)
{
  int size;

  if (buffer == NULL)  
  {
    size = receive_no_check(this, this->receive_buffer.data, this->receive_buffer.size);
  }
  else
  {
    if (length > SIZE_OF_RECEIVE_BUFFER - 1)
    {
      length = SIZE_OF_RECEIVE_BUFFER - 1;
    }
    size = receive_no_check(this, buffer, length);
  }
  return size;
}

