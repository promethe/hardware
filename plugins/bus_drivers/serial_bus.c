/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*! ****************************************************************************
 * \file  rctech50g10y_bus.c
 * \date 24 fevrier 2010
 * \brief to manage the 50g10Y from RC-Tech
 * \author David BAILLY
 * \version 0.1
 *
 * Description: Protocole to use the 50G10Y camera from RC-Tech
 *
 * Macro:
 * -none
 *
 * Local variables:
 * - none
 *
 * Global variables:
 * -none
 *
 * Internal Tools:
 * -none
 *
 * External Tools:
 * -none
 *
 * Links:
 * - description: none/ XXX
 * - input expected group: none/xxx
 * - where are the data?: none/xxx
 * 
 *
 * Known bugs: none found yet.
**************************************************************************** !*/
/*#include <netdb.h>*/
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

/* For serial port management */
#include <termios.h>

#include <net_message_debug_dist.h>

#include <Components/bus.h>

#define NB_TRY_DEFAULT		10000
#define TIME_WAIT_DEFAULT	0
/*#define DEBUG */

#ifdef Darwin
#define B1000000 1000000
#endif

typedef struct serial_bus
{
  #include <Components/generic_fields.h>
  #include <Components/bus_fields.h>
  
  int fd; /* file descriptor for the serial port */
  struct termios oldtio, newtio; /* structure to store attributes */
  char name[128];
  long baudrate;
  long databits;
  long parity;
  long stopbits;
  int nbtry;
  int timewait;
  int icanon;
  int block; /* 1 if blocking port (default), 0 otherwise */
  pthread_mutex_t mutex;
}Serial_bus;



Serial_bus *constructor()	{
	return ALLOCATION(Serial_bus);
}


void init(Serial_bus *this)
{
  const char *str; 
  int flags, val;
  
#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif

  strcpy(this->name, xml_get_string(this->node, "name")); /* name in /dev */
  
  if (pthread_mutex_init(&(this->mutex), NULL)) EXIT_ON_ERROR("The mutex_socket cannot be initialized");
  
  /* Set the baudrate rate */
  
  switch(xml_get_int(this->node, "baudrate"))
  {
    case 0:
      this->baudrate = B0;
      break;
    case 300:
      this->baudrate = B300;
      break;
    case 600:
      this->baudrate = B600;
      break;
    case 1200:
      this->baudrate = B1200;
      break;
    case 2400:
      this->baudrate = B2400;
      break;
    case 4800:
      this->baudrate = B4800;
      break;
    default:
    case 9600:
      this->baudrate = B9600;
      break;
    case 19200:
      this->baudrate = B19200;
      break;
    case 38400:
      this->baudrate = B38400;
      break;
    case 57600:
      this->baudrate = B57600;
      break;
    case 115200:
      this->baudrate = B115200;
      break;
    case 1000000:
      this->baudrate = B1000000;
      break;
  }
  
  /* Set the number of data bits */
  switch(xml_get_int(this->node, "databits"))
  {
    case 5:
      this->databits = CS5;
      break;
    case 6:
      this->databits = CS6;
      break;
    case 7:
      this->databits = CS7;
      break;
    case 8:
      this->databits = CS8;
    break;
    default:
	EXIT_ON_ERROR("This databits is not possible. You have to set (5, 6, 7 or 8)"); 
      break;
  }
  

  /* Set parity yes or no */
  str = xml_get_string(this->node, "parity");
  this->parity = PARENB | PARODD; /* equivalent if odd or impair is written in the .dev */
  if(strncasecmp(str, "no", 2) == 0) this->parity = 0;
  else if((strncasecmp(str, "pair", 4) == 0) | (strncasecmp(str, "even", 4) == 0)) this->parity = PARENB;

  /* Set number of tries when sending a message */
  str = xml_try_to_get_string(this->node, "nbtry");
  if(str != NULL) this->nbtry = atoi(str);
  else this->nbtry = NB_TRY_DEFAULT;
  
  /* Set wait time between tries when sending a message */
  str = xml_try_to_get_string(this->node, "timewait");
  if(str != NULL) this->timewait = atoi(str);
  else this->timewait = TIME_WAIT_DEFAULT;

  /* Set canonical reading mode - wait for endline (etc.) character before ending reading */
  this->icanon = 0;
  val = xml_try_to_get_int(this->node, "icanon",&(this->icanon));
  if(val && this->icanon) {
		this->icanon = ICANON;
	}

  
  /* option blocking or not */
  /* default is blocking - to match static lib management of serial*/
  flags = O_RDWR;
  this->block=1;
  if(xml_try_to_get_int(this->node, "nonblock",&val))
	if(val != 0) {
		flags=flags | O_NONBLOCK;
    this->block=0;
#ifdef DEBUG
		printf("not blocking\n");
#endif
	}
  
  /* Try to open the serial port */
  this->fd = open(this->name, flags);
  if(this->fd < 0) EXIT_ON_ERROR("error when opening serial port %s\n", this->name);
  
  /* Attributes setup */
  tcgetattr(this->fd,&(this->oldtio)); /* saves current attributes of serial port*/

  tcgetattr(this->fd,&(this->newtio)); /* a utiliser ? cf. serial_setparam dans lib hardware static. Necessaire pour init correcte de newtio.*/
/*  printf("initially : %x %x %x %x %x\n", this->newtio.c_cflag, this->newtio.c_iflag, this->newtio.c_oflag, this->newtio.c_lflag, this->newtio.c_cc[VMIN], this->newtio.c_cc[VTIME]); */

  this->newtio.c_cflag =  this->baudrate | this->databits | this->parity | CLOCAL | CREAD;
  this->newtio.c_iflag = IGNBRK; /* IGNPAR - coherence avec fonction statique de gestion port serie */
  this->newtio.c_oflag = 0;
  this->newtio.c_lflag = 0;
	this->newtio.c_lflag |= this->icanon;
  this->newtio.c_cc[VMIN]=1;
  this->newtio.c_cc[VTIME]=0;

  /* No Flow control */
  this->newtio.c_iflag &= ~(IXON | IXOFF | IXANY); /* coherence avec gestion serial en statique */

  /* Stop bits */
  switch(xml_get_int(this->node, "stopbits"))
  {
    default:
    case 1:
	   this->stopbits=1;
       this->newtio.c_cflag &= ~CSTOPB; /* + voir ligne plus haut : attention pas coherent avec
				  * serial_setparam */
      break;
    case 2:
	  this->stopbits=2;
      this->newtio.c_cflag |= CSTOPB;
      break;
  }


  tcflush(this->fd, TCIFLUSH);
  tcsetattr(this->fd,TCSANOW,&(this->newtio));

#ifdef DEBUG
  printf("name=%s, baudrate=%x, databits=%x, parity=%x, stopbits=%x, nbtry=%d, timewait=%d\n", this->name, (unsigned int) this->baudrate, (unsigned int)this->databits, (unsigned int)this->parity, (unsigned int)this->stopbits, this->nbtry, this->timewait);
#endif


#ifdef DEBUG
  printf("exiting %s %s\n", __FUNCTION__, this->name);
#endif
}

int send_command(Serial_bus *this, const char *command, int size)
{
  int ret;
  
  
#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif

  pthread_mutex_lock(&(this->mutex));

  tcflush(this->fd, TCIOFLUSH); /* destroy all send but not received datas and all received but not read datas */
  ret=write(this->fd, command, size*sizeof(char));
  tcdrain(this->fd); /* wait for all datas to be transmitted */
  
  pthread_mutex_unlock(&(this->mutex));
  
  if(ret < 0) EXIT_ON_ERROR("error when sending a message (write returned -1)\n");

#ifdef DEBUG
  printf("exiting %s %s\n", __FUNCTION__, this->name);
#endif
  return ret;
}

int there_is_data_to_receive(Serial_bus *this)
{
   (void)this;
   return 0;
}

int receive(Serial_bus *this, char *buffer, int length)
{
  int lu, ret, cpt;
  char *p;
  
  p = buffer;
  lu = 0;
  cpt = 0;
  
  
#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif

  /* Start of read loop */
  pthread_mutex_lock(&(this->mutex));
//  tcflush(this->fd, TCIOFLUSH);

  do
  {
    ret=read(this->fd, p, length-lu);
    if(ret>0)
    {
      lu += ret;
      p += ret;
    }
    cpt++;
    if((lu != length) && (this->timewait != 0)) usleep(this->timewait); /* apply a wait time if requested */
  }
  while((lu < length) && (cpt < this->nbtry));
  /* put endstring character */
  p[0] = '\0';
  
  pthread_mutex_unlock(&(this->mutex));
  /* End of read loop */
  
  if((lu<length) && (ret<0) && (this->block)) EXIT_ON_ERROR("error when receiveing the message (%d total read and read returned %d)\n", lu, ret);

#ifdef DEBUG
  printf("exiting %s %s\n", __FUNCTION__, this->name);
#endif
  return lu;
}

void flush(Serial_bus *this)
{
#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif

	tcflush(this->fd, TCIOFLUSH);
  
#ifdef DEBUG
  printf("exiting %s %s\n", __FUNCTION__, this->name);
#endif
}

int send_and_receive(Serial_bus *this, char *send_buffer, int send_length, char* receive_buffer, int receive_length)
{
	int lu, ret, cpt;
	int length = receive_length;
  char *p;
	fd_set rfds;
  struct timeval tv;
  struct timeval *tv_ptr = NULL;

#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif


  //~ if(this->block) {
		//~ /* obsolete */
		//~ send_command(this, send_buffer, send_length);
		//~ lu = receive(this, receive_buffer, receive_length);
		//~ return lu;
	//~ }
	
  
  p = receive_buffer;
  lu = 0;
  cpt = 0;
  
  FD_ZERO(&rfds); 
  FD_SET(this->fd, &rfds);
  
  if(!this->block) {
		tv.tv_sec = 0;
		tv.tv_usec = this->timewait;
		tv_ptr = &tv;
	}
	
  /* Start of write/read loop */
  pthread_mutex_lock(&(this->mutex));
	
	tcflush(this->fd, TCIOFLUSH); /* destroy all send but not received datas and all received but not read datas */
  ret=write(this->fd, send_buffer, send_length*sizeof(char));
  if(ret < 0)
    {
        perror("write serial");
        exit(42);
    }
  tcdrain(this->fd); /* wait for all datas to be transmitted */
  
  /* test on select to avoid freeze if no data received */
  ret = select(this->fd+1, &rfds, NULL, NULL, tv_ptr);
  
  if (ret == -1)
	{
      perror("select()");
  }        
	else if (ret)
	{
  
		do
		{
			ret=read(this->fd, p, length-lu);
			if(ret>0)
			{
				lu += ret;
				p += ret;
			}
			cpt++;
			if((cpt < this->nbtry) && (lu != length) && (this->timewait != 0)) usleep(this->timewait); /* apply a wait time if requested */
		}
		while((lu < length) && (cpt < this->nbtry));
		/* put endline character */
		if(lu > 0 && lu < length)
		{
			p[0] = '\0';
		}
		else
		{
			perror("read serial");
			exit(42);
		}
		
	}
	else {
		p[0] = '\0';
	}
	
	pthread_mutex_unlock(&(this->mutex));
  /* End of write/read loop */
	
	return lu;
}

void stop(Serial_bus *this)
{
#ifdef DEBUG
  printf("entering %s %s\n", __FUNCTION__, this->name);
#endif

  /* Closing of the serial port */
  tcsetattr(this->fd,TCSANOW,&(this->oldtio)); /* gives back the old attributes */
  close(this->fd);
  
#ifdef DEBUG
  printf("exiting %s %s\n", __FUNCTION__, this->name);
#endif
}

