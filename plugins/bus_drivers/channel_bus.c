/*************************************************************
 Author: Arnaud Blanchard
 Created: 25-08-2010

 ************************************************************/

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <net_message_debug_dist.h>
#include "basic_tools.h"

#include <Components/data.h>
#define HTONL(x) x = htonl(x)

typedef struct channel {
#include <Components/generic_fields.h>
#include <Components/bus_fields.h>
  const char *output_channel;
} type_channel_bus;

type_channel_bus *constructor()
{
  return ALLOCATION(type_channel_bus);
}

void osc_receive_callback(char *buffer, int size, void *user_data)
{
  type_buffer *buf = user_data;

  buffer_replace(buf, buffer, size);
  BUFFER_UNLOCK(buf);
}

void init(type_channel_bus *bus)
{
  const char *input_channel;
  const char *protocol;

  input_channel = xml_try_to_get_string(bus->node, "input_channel");
  protocol = xml_get_string(bus->node, "protocol");

  if (strcmp(protocol, "osc") == 0) if (input_channel != NULL ) bus_add_channel(bus->bus, input_channel, osc_receive_callback, (void*)&bus->receive_buffer);
  else EXIT_ON_ERROR("Unknonwn protocol : '%s'.", protocol);
  bus->output_channel = xml_try_to_get_string(bus->node, "output_channel");
}

void get_values(type_channel_bus *bus, float *values)
{
  char *types_buffer;
  int read_position;
  int32_t integer;
  int i;
  type_buffer *buf = &bus->receive_buffer;
  uint32_t host_value;

  BUFFER_LOCK(buf);

  if (buf->data[0] != ',') EXIT_ON_ERROR("Wrong data format for OSC, it is suppose to start with a ',' but it is : '%s'.", buf->data);

  types_buffer = &buf->data[1];
  for (i = 0; i < bus->elements_nb; i++)
  {
    if ((types_buffer[i] != 'f') && (types_buffer[i] != 'i')) EXIT_ON_ERROR("There is not enough floats 'f' or int 'i'. It should have %d but it has '%s' instead.", bus->elements_nb, &buf->data[1]);
  }
  read_position = 1 + i;
  if (buf->data[read_position] != '\0') EXIT_ON_ERROR("There is too much data : '%s'", &buf->data[1 + i]);
  read_position += 4 - read_position % 4;

  for (i = 0; i < bus->elements_nb; i++)
  {
    if (types_buffer[i] == 'f')
    {
      host_value = ntohl(*(uint32_t*) &buf->data[read_position + 4 * i]);
      memcpy(&values[i], &host_value, sizeof(uint32_t));
    }
    else if (types_buffer[i] == 'i')
    {
      host_value = ntohl(*(uint32_t*) &buf->data[read_position + 4 * i]);
      memcpy(&integer, &host_value, sizeof(uint32_t));
      values[i] = integer; /* Conversion de int en float */
    }
  }
  buf->position = 0;
}

void set_values(type_channel_bus *bus, const float *values)
{
  int i;
  type_buffer *buf = &bus->send_buffer;
  char null_buffer[4] =
    { 0, 0, 0, 0 };
  uint32_t net_value;

  buffer_replace(buf, "/", 1);
  buffer_append(buf, bus->output_channel, strlen(bus->output_channel));
  buffer_append(buf, null_buffer, 4 - buf->position % 4);
  buffer_append(buf, ",", 1);

  for (i = 0; i < bus->elements_nb; i++)
  buffer_append(buf, "f", 1);
  buffer_append(buf, null_buffer, 4 - buf->position % 4);

  for (i = 0; i < bus->elements_nb; i++)
  {
    memcpy(&net_value, &values[i], sizeof(uint32_t));
    HTONL(net_value);
    buffer_append(buf, (const char*) &net_value, sizeof(uint32_t));
  }
  buffer_append(buf, null_buffer, 4 - buf->position % 4);
  bus->bus->send_command(bus->bus, buf->data, buf->position);
}

