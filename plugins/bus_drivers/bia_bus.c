/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/if_arp.h>
#include <sys/select.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include <Components/bus.h>

#include "basic_tools.h"
#include "xml_tools.h"

#define SYSTEM_COMMAND_MAX 4096
#define READ_CMD 0x16ff
#define WRITE_CMD 0x1aff
#define MAC_ADDR_LEN 6

#pragma pack(2)
typedef struct {
  char Dest[6];
  char Src[6];
  short EtherType; /* 0x1313 pour les centrales*/
  unsigned short Length; /* nboctet apres entete*/
  short Type;
  short Bloc; /* bit 15=1 si dernier bloc*/
  int res1;
  int res2;
  short Cpt10k;
} TEnteteTrame;

typedef struct SndCmd {
  unsigned short cmd;
  unsigned int Addr;
  unsigned short Nboct;
  char _data[1502];
} TSndCmd;

#pragma pack()

static const  char *MacCentrales[8] =
  { "\0\0BIA ", "\0\0BIA!", "\0\0BIA\"", "\0\0BIA#", "\0\0BIA$", "\0\0BIA%", "\0\0BIA&", "\0\0BIA'" };
static unsigned char WinMacAddr[6] =
  { 0x00, 0x13, 0xa9, 0x8d, 0x0a, 0xa8 };


struct timeval timeOut;
/*----- Data management -----*/

/*----- Network management -----*/
char hostname[16];
unsigned int hostport;
struct ifreq s_ifr;
struct sockaddr_in host_addr, prom_addr;
/*----- Other -----*/
char buffer[2048];
/*----- Data transmission -----*/
int ret;
socklen_t addrlen = sizeof(struct sockaddr_in);
fd_set set;

typedef struct bia_bus {
#include <Components/generic_fields.h>
#include <Components/bus_fields.h>
  char *interface;
  int socket;
  struct sockaddr_ll sockaddr;

} type_bia_bus;

type_bia_bus *constructor()
{
  type_bia_bus *bus = ALLOCATION(type_bia_bus);
  return bus;
}

void init(type_bia_bus *bus)
{
  char system_command[SYSTEM_COMMAND_MAX];

  bus->interface =  (char*)xml_get_string(bus->node, "interface");

  sprintf(system_command, "ifconfig %s promisc", bus->interface);
  printf("%s\n", system_command);

  if (system(system_command) != 0) EXIT_ON_ERROR("Fail to execute: `%s`. Check that you are `sudo`", system_command);

  /*----- Init data -----*/
  memset(&bus->sockaddr, 0x00, sizeof(struct sockaddr_ll));
  memset(buffer, 0x00, sizeof(buffer));
  /*----- Init network -----*/
  bus->socket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (bus->socket < 0)
  {
    EXIT_ON_SYSTEM_ERROR("Creating BIA socket.  Check that you are `sudo`.");
  }

  /*----- Get the eth to be used -----*/
  /*(eth is specified on the command line cf brocker.c)*/
  /*----- Get interface index of the eth specified -----*/
  strncpy(s_ifr.ifr_name, bus->interface, sizeof(s_ifr.ifr_name));
  ioctl(bus->socket, SIOCGIFINDEX, &s_ifr);
  memset(&bus->sockaddr, 0, sizeof(struct sockaddr_ll));

  bus->sockaddr.sll_protocol = htons(ETH_P_ALL);
  bus->sockaddr.sll_family = AF_PACKET;
  bus->sockaddr.sll_ifindex = s_ifr.ifr_ifindex;/*physical eth output to use*/
  bus->sockaddr.sll_hatype = ARPHRD_ETHER;
  bus->sockaddr.sll_pkttype = 0;/*PACKET_OUTGOING;*/
  bus->sockaddr.sll_halen = ETH_ALEN; /*defined as 6 */
  memcpy(bus->sockaddr.sll_addr, MacCentrales[0], 6 * sizeof(char));

  /*----- Get the Mac addr of host -----*/
  /* We don't use it now because the BIA bay answer only to the window
   * computer that load the program. For now we just cheat and speak to
   * the robot as if we were the window pc
   ioctl(sockBia, SIOCGIFHWADDR, &s_ifr);
   memcpy(HostMacAddr, s_ifr.ifr_hwaddr.sa_data, MAC_ADDR_LEN);
   */

  /*----- Bind the socket to the eth specified -----*/
  if (bind(bus->socket, (struct sockaddr *) &bus->sockaddr, sizeof(struct sockaddr_ll)) != 0) PRINT_WARNING("couldn't bind sockfd");
  kprints("BIA bus: bind on %s succesfull\n", bus->interface);

}


int send_command(type_bia_bus *bus, char *data, size_t length)
{
  (void)bus;
  (void)data;
  (void)length;

  return 0;
}


int send_command_on_channel(type_bia_bus *bus, char *data, size_t length, long unsigned int channel_id)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int i;
  struct timeval timeOut;
  fd_set set;


  if (length != 4) EXIT_ON_ERROR("You can only write 4 chars (like a float) !\n\tYou try to read %i", length);

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = WRITE_CMD;
  MsgCmd->Addr = channel_id;
  MsgCmd->Nboct = 4;
  memcpy(MsgCmd->_data, data, length);

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 13;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;
  size = 60;
  BUFFER_LOCK(&bus->send_buffer);

  /*----- Send the message -----*/
  if ((i = sendto(bus->socket, buffer, size, 0, (struct sockaddr*)&bus->sockaddr, sizeof(struct sockaddr_ll))) < size) PRINT_WARNING("not all bytes have been sent (%d)", i);

  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(bus->socket, &set);

    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      EXIT_ON_ERROR("select");
    }
	if(read(bus->socket, buffer, sizeof(buffer)) == -1) EXIT_ON_ERROR("Erreur de lecture de la socket dans bias_bus");
  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  BUFFER_UNLOCK(&bus->send_buffer);
  return 0;

}

int receive_on_channel(type_bia_bus *bus, char *data, int length, long unsigned int channel_id)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int i;
  struct timeval timeOut;
  fd_set set;

  if (length != 4) EXIT_ON_ERROR("You can only read 4 char (like a float) !\n\tYou try to red %i", length);

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = READ_CMD;
  MsgCmd->Addr =  channel_id;
  MsgCmd->Nboct = 4;
  MsgCmd->_data[0] = 0x08;

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 9;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;

  size = 60;

  BUFFER_LOCK(&bus->send_buffer);

  /*----- Send the message -----*/
  if ((i = sendto(bus->socket, buffer, size, 0, (struct sockaddr*)&bus->sockaddr, sizeof(struct sockaddr_ll))) < size) PRINT_WARNING("not all bytes have been sent (%d)", i);
  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(bus->socket, &set);
    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      return -1;
    }
    /*The socket receive all the message from eth0 so we need to filter them manualy*/
    if(read(bus->socket, buffer, sizeof(buffer))== -1) EXIT_ON_ERROR("Erreur de lecture de la socket dans bias_bus");

  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  memcpy(data, MsgCmd, length);
  BUFFER_UNLOCK(&bus->send_buffer);

  return length;
}

