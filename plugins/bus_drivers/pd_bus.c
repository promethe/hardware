/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
 Author: Antoine de Rengerve
 Created: 25-08-2010

 ************************************************************/

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <net_message_debug_dist.h>

#include <Components/bus.h>

#define MESSAGE_MIN 256

typedef struct pd_bus {
#include <Components/generic_fields.h>
#include <Components/bus_fields.h>
  int input_socket_fd, output_socket_fd;
  struct sockaddr_in serv_addr; /*structure du serveur */
} type_pd_bus;

static int maxfd;

type_pd_bus *constructor()
{
  return ALLOCATION(type_pd_bus);
}

int create_socket()
{
  int fd;
  fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (fd < 0) EXIT_ON_SYSTEM_ERROR("Creation socket");
  if (fd > maxfd) maxfd = fd;
  return fd;
}

void *osc_receive_manager(void* user_data)
{
  int path_size, data_position, ret;
  fd_set readset;
  struct timeval timeout;
  int i;
  type_pd_bus *bus = user_data;

  type_buffer *buffer = &bus->receive_buffer;

  FD_ZERO(&readset);
  FD_SET(bus->input_socket_fd, &readset);

  buffer_allocate_min(buffer, MESSAGE_MIN);
  timeout.tv_sec = 0;
  timeout.tv_usec = 0;

  while (1)
  {
    buffer->position = 0;
    if (select(maxfd + 1, &readset, NULL, NULL, 0) < 0) EXIT_ON_SYSTEM_ERROR("select problem");

    if (FD_ISSET(bus->input_socket_fd, &readset))
    {
      ret = recv(bus->input_socket_fd, buffer->data, buffer->size, 0);
      if (ret < 0) EXIT_ON_SYSTEM_ERROR("recv error");
      buffer->position = ret;

      /* Il se peut que le buffer de reception soit plein. On le double et on relit la suite. */
      if (ret == buffer->size)
      {
        while (select(maxfd + 1, &readset, NULL, NULL, &timeout) > 0)
        {
          buffer_allocate_min(buffer, buffer->size * 2);
          if (FD_ISSET(bus->input_socket_fd, &readset))
          {
            ret = recv(bus->input_socket_fd, &buffer->data[buffer->position], buffer->size - buffer->position, 0);
            buffer->position += ret;
          }
        }
      }
      if ((buffer->position % 4) != 0) EXIT_ON_SYSTEM_ERROR("OSC data size must be a multiple of 4");

      path_size = strnlen(buffer->data, buffer->size);
      data_position = path_size + 4 - path_size % 4;

      for (i = 0; i < bus->channels_nb; i++)
      {
        if (strcmp(&buffer->data[1], bus->channels[i].id) == 0) /*+1 correspond au premier '/' */
        {
          bus->channels[i].receive_callback(&buffer->data[data_position], buffer->position - data_position, bus->channels[i].user_data);
          break;
        }
      }
      BUFFER_UNLOCK(&bus->receive_buffer);
    }
  }
  return NULL;
}

void init(type_pd_bus *bus)
{
  const char* hostname;
  int output_port, input_port;
  struct sockaddr_in my_server, distant_server;
  struct hostent *distant_address;
  pthread_t receive_thread;

  hostname = xml_get_string(bus->node, "host");
  input_port = xml_get_int(bus->node, "input_port");
  output_port = xml_get_int(bus->node, "output_port");

  /* In order to receive */
  bus->input_socket_fd = create_socket();

  my_server.sin_family = AF_INET;
  my_server.sin_addr.s_addr = INADDR_ANY;
  my_server.sin_port = htons((unsigned short) input_port);

  if (bind(bus->input_socket_fd, (struct sockaddr *) &my_server, sizeof(my_server)) < 0) EXIT_ON_SYSTEM_ERROR("input binding problem");

  /* In order to send */
  bus->output_socket_fd = create_socket();

  distant_address = gethostbyname(hostname);
  if (distant_address == NULL ) EXIT_ON_ERROR("Wrong address %s", hostname);
  distant_server.sin_family = AF_INET;
  distant_server.sin_addr = *(struct in_addr *) distant_address->h_addr;
  distant_server.sin_port = htons((unsigned short) output_port);

  if (connect(bus->output_socket_fd, (struct sockaddr *) &distant_server, sizeof(distant_server)) < 0) EXIT_ON_SYSTEM_ERROR("connect problem");

  pthread_create(&receive_thread, NULL, osc_receive_manager, bus);
}

int send_command(type_pd_bus *bus, const char *buffer, int size)
{
  return send(bus->output_socket_fd, buffer, size, 0);
}


