/*
 * webrtc_publish.c
 *
 *  Created on: 30 juil. 2014
 *      Author: sylvain
 */

#include <iostream>
#include <unistd.h>

#include <basic_tools.h>

#include <Components/publish.h>

#include <mayartc/RTCPeerInterface.hpp>
#include <mayartc/RTCChannelInterface.hpp>
#include <mayartc/MayaSignaling.hpp>


using namespace maya;

typedef struct{
	/** Attention a l'ordre. Ne pas rajouter de champs ici mais dans net_sensor_fields.h */
	#include <Components/generic_fields.h>
	#include <Components/publish_fields.h>

	MayaSignalingInterface *signaling;
	RTCPeerInterface *server;

} webrtc_publish;

extern "C" {

webrtc_publish * constructor()	{
	printf("RTC: constructor\n");
	return ALLOCATION(webrtc_publish);
}

void init(webrtc_publish *self){
	printf("RTC: init\n");
	initRTC();
	self->signaling = MayaSignalingInterface::create();
	self->server = RTCPeerInterface::create(self->signaling);
}

void start(webrtc_publish *self){
	printf("RTC: start\n");
	self->signaling->start();
}

void stop(webrtc_publish *self){
	printf("RTC: stop\n");
	self->signaling->stop();
}

void * channel_create(webrtc_publish *self, const char* name, int reliable){
	printf("RTC: channel create (%s)\n", name);
	if(self->server == NULL) self->server = (RTCPeerInterface*) self->signaling->getPeer();

	RTCChannelInterface *channel = self->server->registerChannel(name, reliable);
	if(channel == NULL) return NULL;
	return (void*)channel;
}

int channel_is_connected(webrtc_publish *self, void * channelID){
	RTCChannelInterface *channel =  (RTCChannelInterface*) channelID;
	if(channel == NULL) return 0;
	return channel->isConnected();
}
void channel_write(webrtc_publish *self,void * channelID, char *buffer, int bufferSize){
	RTCChannelInterface *channel =  (RTCChannelInterface*) channelID;
	if(channel == NULL) return ;

	channel->sendData((char*)buffer, bufferSize);
}

void channel_set_negociation_message(publish *self, void * channelID, char * buffer, int bufferSize){
	RTCChannelInterface *channel =  (RTCChannelInterface*) channelID;
	if(channel == NULL) return ;

	channel->setNegociationMessage(buffer, bufferSize);
}

void channel_register_receive_callback(publish *self, void * channelID, type_recv_cb cb, void *userData){
	RTCChannelInterface *channel =  (RTCChannelInterface*) channelID;
	if(channel == NULL) return ;

	 channel->registerReceiveCallback(cb, userData);
}

void destroy(webrtc_publish *self){
	printf("RTC: destroy\n");
}

}
