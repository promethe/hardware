/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file 
\brief

Author: Antoine de Rengervé
Created: 18/02/2014


Description:
- SHE sensor particular driver of device net_sensor

http://www.doxygen.org
************************************************************/
#include <net_message_debug_dist.h>
#include <Components/net_sensor.h>

/** nombre max de SHE = 100 **/
#define MAX_NB_SHE 5 // 100? 
#define SIZE_BUFF 256

#define AVERAGING_PERIOD 10
#define REFRESH_PERIOD 15

/*#define DEBUG*/
/*#define LOGDEBUG*/

typedef struct she_net_sensor{

  /* Attention a l'ordre des includes ! */
  
  #include <Components/generic_fields.h>
  #include <Components/net_sensor_fields.h>
  
  const char *name;
  int nb_she;
  char updated[MAX_NB_SHE];
  int curr[MAX_NB_SHE];
  int vol[MAX_NB_SHE];
  int num[MAX_NB_SHE];
  float energy[MAX_NB_SHE];
  float val_energy[MAX_NB_SHE];
  int wrong;
  int ok;
  int discard;
#ifdef LOGDEBUG
  FILE *f_debug;  
  FILE *fichier;
#endif
  time_t prev_time;
} She_net_sensor;

She_net_sensor *constructor()	{
	return ALLOCATION(She_net_sensor);
}

void init(She_net_sensor* this)
{
	int i;
  this->name = xml_get_string(this->node, "name");
  
  this->nb_she = MAX_NB_SHE;
  this->wrong = 0;
  this->ok = 0;
  this->discard = 0;
  
  if(sizeof(int)<4) {
		EXIT_ON_ERROR("int should be 32 bytes otherwise use long for curr and vol !\n");
	}
  
  /* init tab -1 (never read) */
  for(i=0; i<this->nb_she; i++) {
		this->val_energy[i]=-1.;
		this->updated[i]=0;
		this->curr[i]=0;
		this->vol[i]=0;
		this->num[i]=0;
		this->energy[i]=0.;
	}
	
	/** init time */
	this->prev_time = 0;

#ifdef LOGDEBUG
   /** init file for debug */
   this->f_debug=fopen("log_debug.txt","w");
	if(this->f_debug==NULL) {
		printf("Error opening log_debug.txt !\n");
		exit(1);
	}
   /** init file for debug */
   this->fichier=fopen("log.txt","w");
	if(this->fichier==NULL) {
		printf("Error opening log.txt !\n");
		exit(1);
	}
#endif

   printf("\n SHE : name %s : nb max SHE : %d\n\n",this->name,this->nb_she);
}


void stop (She_net_sensor *this)
{
   (void)this;
#ifdef LOGDEBUG
	fclose(this->f_debug);
	fclose(this->fichier);
#endif
}

/**
 *  Il faut passer une adresse sur un tableau de float et une adresse sur un entier
 * */
void get_sensor(She_net_sensor *this, float **vals, int *nb_sensors)
{
  char buf[SIZE_BUFF];
  int idx = -1,i;
  int *curr = this->curr, *vol=this->vol;
  int lu=0;
  int discard = 0;
#ifdef LOGDEBUG
  FILE *file=this->f_debug;
  FILE *fichier=this->fichier;
#endif
  time_t now;

  /** init timer */
  if(this->prev_time==0)
			this->prev_time = time(NULL);
  
  /** assume request for update of data **/  
  
  /** find first byte of trame **/
  do {
		lu=this->bus->receive(this->bus,buf,1);
		if(lu==1) {
#ifdef DEBUG
			printf("received char : %c %x\n",buf[0],buf[0]);
#endif
			if((buf[0] & 0x1F) == 30) { /* check command code - rq: here mixed with size of trame (in bytes)*/
				idx = (buf[0] & 0x3F) >> 5; /* get id of SHE - only 1 byte ??? */
				this->ok++;
	/** Not in the case of instantaneous consumption
				if(this->val_energy[idx]<-0.5) {
					this->val_energy[idx]=0; * activate found sensor *
				}**/

			  /** get and extract data **/
				lu=this->bus->receive(this->bus,buf,30); /* size of data : 30 bytes - 5 ech for A then V */
	
				if(lu==30) {
          /* hard coded values 5 etc. */
          for(i=0; i<5;i++) {
				discard = 0;
#ifdef DEBUG
            printf("%x %x %x %x %x %x\n",buf[i*6],buf[i*6+1],buf[i*6+2],buf[i*6+3],buf[i*6+4],buf[i*6+5]);
#endif
            /** build 24 bits value from the 3 received bytes (same for cur and vol) */
            curr[idx] = (buf[i*6]<<16) + ((buf[i*6+1]&0xff)<<8) + (buf[i*6+2]&0xff); /*attention conversion, consider 2s complement because buf is char[]*/
            vol[idx] = (buf[i*6+3]<<16) + ((buf[i*6+4]&0xff)<<8) + (buf[i*6+5]&0xff);
#ifdef DEBUG
            printf("curr : %x, vol %x\n",curr[idx],vol[idx]);
#endif
            /** manage 2's complement from 24 bits to int size (cur) - useless if buf is char[] **/
        //	  if(curr[idx]>>23) curr[idx] = (~((~curr[idx])&0xFFFFFF));
            /** manage 2's complement from 24 bits to int size (vol) **/
        //	  if(vol[idx]>>23) vol[idx] = (~((~vol[idx])&0xFFFFFF));

            // dirty fix, avoid peak of current (unknow bug, adc not ready?)
            if(abs(curr[idx]) > 120000) {
              printf("\n\n\n bug occured [%d] : curr is %d (now set to 0) \n\n\n",idx,curr[idx]);
#ifdef LOGDEBUG
              fprintf(file,"\n\n\n bug occured [%d] : curr is %d (now set to 0) \n\n\n",idx,curr[idx]);
#endif
              //curr[idx] = 0; /* TODO remove comment */
					discard=1; /* TODO put 1 here */
            }

            if(abs(vol[idx]) > 4000000) {
              printf("\n\n\n bug occured [%d] : vol is %d (now set to 0) \n\n\n",idx,vol[idx]);
#ifdef LOGDEBUG
              fprintf(file,"\n\n\n bug occured [%d] : curr is %d (now set to 0) \n\n\n",idx,vol[idx]);
#endif
              //vol[idx] = 0; /* TODO remove comment */
    			  discard = 1; /* TODO put 1 here */
            }

/*            if(abs(vol[idx]) > 4000000) {
              printf("\n\n\n debug [%d] : vol is %d (%x) \n\n\n",idx,vol[idx],vol[idx]);
#ifdef LOGDEBUG
              fprintf(file,"\n\n\n debug [%d] : vol is %d (%x) \n\n\n",idx,vol[idx],vol[idx]);
#endif
            }*/

				if(!discard) {
	            this->energy[idx] += ((float)curr[idx]*vol[idx]) / (6690*10747); /* conversion 6690 = 1A ? , 10747 = 1V ? with a factor 10 somewhere... */
	            this->num[idx] ++;
#ifdef DEBUG
		         printf("[%d ] ech %d : num %d, energy %f, curr %d, vol %d\n",idx, i, this->num[idx], this->energy[idx], curr[idx], vol[idx]);
#endif
#ifdef LOGDEBUG
		         fprintf(file,"[%d ] ech %d : num %d, energy %f, curr %d, vol %d\n",idx, i, this->num[idx], this->energy[idx], curr[idx], vol[idx]);            
					fprintf(fichier,"%d, %d\n",curr[idx], vol[idx]);
#endif
				}
				else {
					this->discard++;
				}
          }
			  }
			}
			else
			{
				this->wrong++;
			}
		}
#ifdef DEBUG
		dprints("status : wrong %d, ok %d, discarded %d\n",this->wrong, this->ok, this->discard); /* debug */
#endif

		now=time(NULL);

	} while(now-this->prev_time<=AVERAGING_PERIOD);
	/* TODO ok if and only if AVERAGING_PERIOD elapsed - should measure exactly the elapsed time for each mesure !! */
   /* need accurate sampling over time >> 50 Hz */
	/* pb at the start up */

	printf("\n");
	printf("status : wrong %d, ok %d, discard %d\n",this->wrong, this->ok, this->discard); 
	this->prev_time = now;
	for(i=0; i<this->nb_she;i++) {
		if(this->num[i]!=0) {
			/* TODO : average for AVERAGING_PERIOD (s) but not time depending - limited accuracy, pourquoi fabs necessaire ? */
			this->updated[i]=1;
			this->val_energy[i] =  fabs(this->energy[i])/this->num[i];  
			printf("[%d ] num %d energy_acc %f, energy (Ws) :  %f \n",i, this->num[i], this->energy[i], this->val_energy[i]);
#ifdef LOGDEBUG
			fprintf(file,"[%d ] num %d energy_acc %f, energy (Ws) :  %f \n\n",i, this->num[i], this->energy[i], this->val_energy[i]);
#endif	 
		}
		else {
			this->updated[i]=0; /* no updates during this time window*/
			this->val_energy[i]=-1;
		}
		this->num[i]=0;
		this->energy[i]=0;
	}
	printf("\n");


		/** show values for discovered she */
		/** NB : result is in 0.1Ws */
//		for(i=0; i<this->nb_she;i++) {
//			printf("[%d ] num %d energy %f, curr %d, vol %d\n",i, this->val_energy[i], curr[i], vol[i]);
//		}

#ifdef DEBUG
  if(*nb_sensors > this->nb_she)
    printf("More neurons than she_net_sensors (%d>%d) !\n",*nb_sensors,this->nb_she);
#endif

	/** recuperation des donnees et du nombre de SHE */
	/** anticipation si le nombre de senseurs peut changer un jour */
  *vals = this->val_energy;
  *nb_sensors = this->nb_she;
}
