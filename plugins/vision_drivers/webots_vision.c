/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
Author: Arnaud Blanchard
Created: 12/11/2009

************************************************************/
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <net_message_debug_dist.h>
#include <Components/bus.h>
#include <Components/vision.h>

#define SIZE_OF_FORMAT 32
#define MAXIMUM_SIZE_OF_LINE 64

/* #define VERBOSE */

typedef struct webots_vision
{

	#include <Components/generic_fields.h>
	#include <Components/vision_fields.h>

  float width_ratio; /* ratio entre la resolution d'entree (cam) et la resolution de sortie (soft)*/
  float height_ratio;
  int color_type;

}Webots_vision;


Webots_vision *constructor()	{
	return ALLOCATION(Webots_vision);
}

void init(Webots_vision *this)
{
  const char  *requete= "ImgInfo";
  int tmp_width=-1, tmp_height=-1, tmp_color_type=-1;
  int ret;
  char *reponse=NULL;
  
  
  int length=0;
  this->width = xml_get_int(this->node,"width");
#ifdef VERBOSE
  printf("Width is %d\n",this->width);
#endif
  this->height = xml_get_int(this->node,"height");
#ifdef VERBOSE
  printf("Height is %d\n",this->height);
#endif
  
  this->bus->send_command(this->bus, requete, strlen(requete));
  
  usleep(10000);
  length = this->bus->receive(this->bus, this->bus->receive_buffer.data, SIZE_OF_RECEIVE_BUFFER-1);
  this->bus->receive_buffer.data[length]=0;
  reponse=this->bus->check_recv_header(this->bus, this->bus->receive_buffer.data);

  ret = sscanf(reponse, "imgInfo %d %d %d", &tmp_width, &tmp_height, &tmp_color_type);
  if(ret!=3) {
    EXIT_ON_ERROR("Number of param returned by imgInfo is not 3 (=%d) \n",ret);
  }

#ifdef VERBOSE  
  printf("Vision: width = %d, height = %d\n", tmp_width, tmp_height);
  printf("Color is %d\n",this->color_type);
  
  if(tmp_width!=this->width || tmp_height!= this->height) 
    printf("WARNING : Hardware resolution is different from software resolution. Soft computation to resolve this difference (%d/%d-%d/%d)\n", this->width, tmp_width, this->height,tmp_height );
#endif

  this->width_ratio = (float)(this->width)/tmp_width;
  this->height_ratio = (float)(this->height)/tmp_height;
}


void set_format(Webots_vision *this, char *format)
{  
  if (strcmp(format, "RGB") == 0)
    {
      this->number_of_channels = 3;
    }
  else EXIT_ON_ERROR("Format %s is unknown for this camera. The possible format is: 'RGB'.", format);
}

void update_image(Webots_vision *this, unsigned char *image)
{
	const char  *requete = "ImageRGB";
	char *reponse=NULL;

	int length=-1, ret=-1;
	int width=-1, height=-1, nb_channel=-1;
	int  size_of_data, read_data, size_of_first_line;

	long id;

	this->bus->send_command(this->bus, requete, strlen(requete));
	
	length = this->bus->receive(this->bus,this->bus->receive_buffer.data, SIZE_OF_RECEIVE_BUFFER-1);
	reponse=this->bus->check_recv_header(this->bus, this->bus->receive_buffer.data);

	ret = sscanf(this->bus->receive_buffer.data, "[%ld] imageRGB %4d %4d %1d |",&id, &width, &height, &nb_channel);
	if(ret!=4) {
	  EXIT_ON_ERROR("In vision : Incorrect answer received for update image : %s\n",reponse); /* TODO: donner un nom a vision */
	}
	
	size_of_data=width*height*nb_channel;
	for (size_of_first_line=0; this->bus->receive_buffer.data[size_of_first_line]!='|'; size_of_first_line++);
	size_of_first_line++; /** Pour compter aussi le "|" */

	read_data = length - size_of_first_line;

	memcpy(image, &(this->bus->receive_buffer.data[size_of_first_line]), read_data);
	while(read_data < size_of_data)
	{
		read_data+= this->bus->receive(this->bus, (char*)&image[read_data], size_of_data-read_data);
#ifdef VERBOSE
		printf("read_data %d (/buff: %d <image :%d>)\n",read_data,SIZE_OF_RECEIVE_BUFFER-1,size_of_data);
#endif
	}

}

void stop(Webots_vision *this)
{
   (void)this;
}

