/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
Author: Arnaud Blanchard
Created: 12/11/2009

************************************************************/
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <net_message_debug_dist.h>
#include <Components/bus.h>
#include <Components/vision.h>

#define SIZE_OF_FORMAT 32
#define MAXIMUM_SIZE_OF_LINE 64


typedef struct urbi_vision
{

	#include <Components/generic_fields.h>
	#include <Components/vision_fields.h>

}Urbi_vision;


Urbi_vision *constructor()	{
	return ALLOCATION(Urbi_vision);
}

void init(Urbi_vision *this)
{
	const char *init = "camera.load=1;camera.getSlot(\"val\").notifyChange(uobjects_handle, closure(){camera.val});\n";
	const char  *requete;
	int id;

	this->bus->send_command(this->bus, init, strlen(init));

	usleep(1000000);
	this->bus->receive(this->bus, this->bus->receive_buffer.data, MAXIMUM_SIZE_OF_LINE);
	while (this->bus->there_is_data_to_receive(this->bus))
	{
		this->bus->receive(this->bus, this->bus->receive_buffer.data, SIZE_OF_RECEIVE_BUFFER);
		usleep(100000);
	}

	requete = "camera.width;camera.height;";
	this->bus->send_command(this->bus, requete, strlen(requete));
	usleep(100000);

	this->bus->receive(this->bus, this->bus->receive_buffer.data, MAXIMUM_SIZE_OF_LINE);
	sscanf(this->bus->receive_buffer.data, "[%d] %d \n [%d] %d\n", &id, &this->width, &id, &this->height);

	printf("\nVision: width = %d, height = %d\n", this->width, this->height);
}


void set_format(Urbi_vision *this, char *format)
{
	const char  *requete;

	if (strcmp(format, "YCbCr") == 0)
	{
		requete = "camera.format=0;\n";

		this->bus->send_command(this->bus, requete, strlen(requete));
		usleep(100000);
		this->bus->receive(this->bus, this->bus->receive_buffer.data, SIZE_OF_RECEIVE_BUFFER);
      usleep(100000);

      while (this->bus->there_is_data_to_receive(this->bus))
      {
         this->bus->receive(this->bus, this->bus->receive_buffer.data, SIZE_OF_RECEIVE_BUFFER);
         usleep(100000);
      }
		this->number_of_channels = 3;
	}
	else EXIT_ON_ERROR("Format %s is unknown for this camera. The possible format is: 'YCbCr'.", format);
}

void update_image(Urbi_vision *this, unsigned char *image)
{
	const char  *requete = "camera.val;\n";
	int  size_of_data, id, read_data, size_of_first_line;
	int length=0;

	read_data = 0;

   this->bus->lock(this->bus);
	this->bus->send_command(this->bus, requete, strlen(requete));
	length = this->bus->receive(this->bus, this->bus->receive_buffer.data, MAXIMUM_SIZE_OF_LINE);

	sscanf(this->bus->receive_buffer.data, "[%d] BIN %d", &id, &size_of_data);

	for (size_of_first_line=0; this->bus->receive_buffer.data[size_of_first_line]!='\n'; size_of_first_line++);
	size_of_first_line++; /** Pour compter aussi le "\n" */

	read_data = length - size_of_first_line;

	memcpy(image, &this->bus->receive_buffer.data[size_of_first_line], read_data);
	while(read_data < size_of_data)
	{
		read_data+= this->bus->receive(this->bus, (char*)&image[read_data], size_of_data-read_data);
	}

	/** Lecture retour a  la ligne */
	read_data+= this->bus->receive(this->bus, this->bus->receive_buffer.data, 1); /** Return char */

	this->bus->unlock(this->bus);
}

