/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*************************************************************
 Author: Julien Vasseur
 Created: 23/05/2011

 ************************************************************/
#include <gst/gst.h>
#include <glib.h>
#include <gst/app/gstappsink.h>
#include <gst/app/gstappsrc.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <float.h>

#include "basic_tools.h"
#include <net_message_debug_dist.h>
#include <Components/vision.h>

#define VERBOSE

static GStaticMutex gst_mutex = G_STATIC_MUTEX_INIT;
static GStaticMutex gst_mutex_buf_full = G_STATIC_MUTEX_INIT;

typedef struct gstreamer_vision {
#include <Components/generic_fields.h>
#include <Components/vision_fields.h>

   const char *source;
   const char *locationDev;
   int devicePort;
   float frameRate;
   short isInterlaced;

   short isFirstFrame;
   int isWithFilter;

   GstBus *gst_bus;
   GstElement *gst_pipe;

   GstBuffer *gst_buf;

   unsigned char *gst_image;
} Gstreamer_vision;

Gstreamer_vision *constructor()
{
   return ALLOCATION(Gstreamer_vision);
}

/**
 * This function is called by the AppSink when a new buffer is
 * available on the bus.
 * It updates the gstreamer_vision buffer with the new datas.
 */
static GstFlowReturn new_buffer(GstAppSink *appsink, gpointer user_data)
{
   Gstreamer_vision *this;
   this = (Gstreamer_vision *) user_data;

   g_static_mutex_lock(&gst_mutex_buf_full);

   if ((this->gst_buf = gst_app_sink_pull_buffer(appsink)) == NULL)
   {
#ifdef VERBOSE
      g_printerr("gstreamer_vision: Getting buffer image failed.\n");
#endif /* VERBOSE */
      return GST_FLOW_ERROR;
   }

   /* First frame needs to be allocated */
   if (this->isFirstFrame == 1)
   {
      this->gst_image = malloc(this->gst_buf->size * sizeof(unsigned char));

      if (this->gst_image == NULL) EXIT_ON_ERROR("Source type %s is unknown for this camera. The possible format is: 'usb', 'hdmi', 'firewire'.", this->source);

      this->isFirstFrame = 0;
   }

   /* Copy of current buffer into gst_image */
   memcpy(this->gst_image, this->gst_buf->data, this->gst_buf->size);

   g_static_mutex_unlock(&gst_mutex_buf_full);
   g_static_mutex_lock(&gst_mutex);

   gst_buffer_unref(this->gst_buf);

   g_static_mutex_unlock(&gst_mutex);
   return GST_FLOW_OK;
}

/**
 * init_camera:
 * This function is called to initialize the camera.
 * This sets up all elements for the acquisition. It w)ill call the correct gstreamer source plugin for the correct camera.
 * @param this is the plugin structure
 */
void init_camera(Gstreamer_vision *this)
{
   int inputModeHDMI = 0;
   GstElement *source, *appsink, *filter;
   GstCaps *appSinkCaps;

   /* Initialisation */
   gtk_init(NULL, NULL);
   gst_init(NULL, NULL);

   /* Create gstreamer elements */
   this->gst_pipe = gst_pipeline_new("pipe");
   filter = gst_element_factory_make("ffmpegcolorspace", "filter");
   appsink = gst_element_factory_make("appsink", "appsink");

   if (!strcmp(this->source, "usb"))
   {
      source = gst_element_factory_make("v4l2src", "video-source");
      if (this->locationDev != NULL) g_object_set(G_OBJECT(source), "device", this->locationDev, NULL);
   }
   else if (!strcmp(this->source, "firewire"))
   {
      source = gst_element_factory_make("dc1394src", "video-source");

      if (this->devicePort >= 0) g_object_set(G_OBJECT(source), "camera-number", this->devicePort, "buffer-size", 1, NULL);
      else EXIT_ON_ERROR("You have to specify a correct device Port for firewire device. Exiting.\n");

   }
   else if (!strcmp(this->source, "decklink"))
   {
      /* We check that we got all infos */
      if (&(this->isInterlaced) == NULL || &(this->frameRate) == NULL || this->isInterlaced > 1 || this->isInterlaced < 0) EXIT_ON_ERROR("You have to specify the frame rate and if the video is interlaced. Exiting.\n");

      source = gst_element_factory_make("decklinksrc", "video-source");

      /* We are defining in which mode we have to set up the hdmi camera */
      if (this->height == 720) //720px height
      {
         if (this->isInterlaced) //720i
         EXIT_ON_ERROR("We can't handle 720i. Exiting.\n");
         else
         { //720p
            if (this->frameRate == 50) inputModeHDMI = 4;
            else if (this->frameRate < 59.94 && this->frameRate > 59.94) inputModeHDMI = 5;
            else if (this->frameRate == 60) inputModeHDMI = 6;
         }
      }
      else if (this->height == 1080) //1080px height
      {
         if (this->isInterlaced)
         { //1080i
            if (this->frameRate == 50) inputModeHDMI = 12;
            else if (this->frameRate < 59.94 && this->frameRate > 59.94) inputModeHDMI = 13;
            else if (this->frameRate == 60) inputModeHDMI = 14;
         }
         else
         { //1080p
            if (this->frameRate < 23.98 && this->frameRate > 23.98) inputModeHDMI = 7;
            else if (this->frameRate == 24) inputModeHDMI = 8;
            else if (this->frameRate == 25) inputModeHDMI = 9;
            else if (this->frameRate < 19.97 && this->frameRate > 19.97) inputModeHDMI = 10;
            else if (this->frameRate == 30) inputModeHDMI = 11;
            else if (this->frameRate == 50) inputModeHDMI = 15;
            else if (this->frameRate < 59.94 && this->frameRate > 59.94) inputModeHDMI = 16;
            else if (this->frameRate == 60) inputModeHDMI = 17;
         }
      }

      if (inputModeHDMI != 0) g_object_set(G_OBJECT(source), "input-mode", inputModeHDMI, "input", 2, NULL);
      else EXIT_ON_ERROR("You have to specify a correct frame rate and if the video is interlaced. Exiting.\n");

   }
   else EXIT_ON_ERROR("Source type %s is unknown for this camera. The possible format is: 'usb', 'decklink', 'firewire'.", this->source);

   /* Set up the pipeline */
   /* video-source | filter | appsink */

   appSinkCaps = gst_caps_new_simple("video/x-raw-rgb", "bpp", G_TYPE_INT, 24, "depth", G_TYPE_INT, 24, "width", G_TYPE_INT, this->width, "height", G_TYPE_INT, this->height, "endianess", G_TYPE_INT, 1234,
   NULL);
   gst_app_sink_set_caps((GstAppSink *) appsink, appSinkCaps);

   /* Set up the bus - the bus is a message handler*/
   this->gst_bus = gst_pipeline_get_bus(GST_PIPELINE(this->gst_pipe));
   gst_bus_add_signal_watch(this->gst_bus);

   /* we add the elements into the pipeline and we link them together*/
   if (this->isWithFilter == 1)
   {
      gst_bin_add_many(GST_BIN(this->gst_pipe), source, filter, appsink, NULL);
      gst_element_link_many(source, filter, appsink, NULL);
   }
   else if (this->isWithFilter == 0)
   {
      gst_bin_add_many(GST_BIN(this->gst_pipe), source, appsink, NULL);
      gst_element_link_many(source, appsink, NULL);
   }
   else EXIT_ON_ERROR("Value for isWithFilter is incorrect. It has to be 0 or 1");

   /* Set up the callbacks to appsink element */
   GstAppSinkCallbacks callbacksSink =
      { NULL, NULL, new_buffer, NULL,
         { NULL } };
   gst_app_sink_set_callbacks(GST_APP_SINK(appsink), &callbacksSink, this, NULL);

   /* Set the pipeline to "playing" state*/
   gst_element_set_state(this->gst_pipe, GST_STATE_PLAYING);
#ifdef VERBOSE
   g_print("gstreamer_vision: Playing pipeline...\n");
#endif /* VERBOSE */
}

/**
 * This function is called to initialize the plugin
 */
void init(Gstreamer_vision *this)
{
   this->isFirstFrame = 1;
   /* Updating device's informations from the XML file */
   g_static_mutex_lock(&gst_mutex);

   this->source = xml_get_string(this->node, "source");
   this->locationDev = xml_try_to_get_string(this->node, "location");
   this->width = xml_get_int(this->node, "width");
   this->height = xml_get_int(this->node, "height");
   xml_try_to_get_int(this->node, "devicePort", &(this->devicePort));

   if (!xml_try_to_get_int(this->node, "isWithFilter", &(this->isWithFilter))) this->isWithFilter = 1;

   if (!strcmp(this->source, "decklink")) /*Verification of hdmi parameters*/
   {
      this->isInterlaced = xml_get_int(this->node, "isInterlaced");
      this->frameRate = xml_get_float(this->node, "frameRate");
   }

#ifdef VERBOSE
   g_print("Initialization of the camera:  source=%s, width=%d, height=%d\n", this->source, this->width, this->height);
#endif /* VERBOSE */

   /* General init for the camera specified in the XML file */
   if (this->source != NULL) init_camera(this);
   else EXIT_ON_ERROR("gstreamer_vision: You have to specify at least the source type:{usb, hdmi, firewire}\n");

   g_static_mutex_unlock(&gst_mutex);
}

/**
 * This function is called when the acquisition is over.
 */
void stop(Gstreamer_vision *this)
{
   g_static_mutex_unlock(&gst_mutex_buf_full);
   g_static_mutex_unlock(&gst_mutex);

   /* Out of the main loop, clean up nicely */
   gst_element_set_state(this->gst_pipe, GST_STATE_NULL);
#ifdef VERBOSE
   g_print("gstreamer_vision: Stopping pipeline...\n");
#endif /* VERBOSE */

   gst_object_unref(GST_OBJECT(this->gst_pipe));

#ifdef VERBOSE
   g_print("gstreamer_vision: Deleting pipeline...\n");
#endif /* VERBOSE */
}

/**
 * This function is called when promethe wants an image.
 */
void update_image(Gstreamer_vision *this, unsigned char *image)
{
   if (this->isFirstFrame == 1) g_print("First image has not being grabbed yet\n");
   else
   {
      g_static_mutex_lock(&gst_mutex_buf_full);
      g_static_mutex_lock(&gst_mutex);

      memcpy(image, this->gst_image, this->gst_buf->size); /* ATTENTION si les 2 images ne sont pas de la meme taille (AB) */

      g_static_mutex_unlock(&gst_mutex_buf_full);
      g_static_mutex_unlock(&gst_mutex);
   }
}

/**
 * This sets the plugin format
 */
void set_format(Gstreamer_vision *this, char *format)
{
   if (strcmp(format, "RGB") == 0 || strcmp(format, "YCbCr") == 0)
   {
      strcpy(this->format, format);
      this->number_of_channels = 3;
   }
   else if (strcmp(format, "gray") == 0)
   {
      strcpy(this->format, format);
      this->number_of_channels = 1;
   }
   else EXIT_ON_ERROR("Format %s is unknown for this camera. The possible format is: 'RGB', 'YCbCR'.", format);
}
