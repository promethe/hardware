/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file 
\brief

Author: Sebastien Razakarivony
Created:
Modified:
- author:
- description:
- date:

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:
- Containts functions needed for a motor of the katana arm

Macro:
-none

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools:
-none

Links:
- type: bus
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx


Known bugs: none found yet.

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <net_message_debug_dist.h>
#include <Components/gripperSensor.h>

#define SIZE_BUFF 256

typedef struct katana_gripperSensor{

  /* Attention a l'ordre des includes ! */
  
  #include <Components/generic_fields.h>
  #include <Components/gripperSensor_fields.h>
  
  const char *name;
  int refresh;
  int sensor;
} Katana_gripperSensor;

Katana_gripperSensor *constructor()	{
	return ALLOCATION(Katana_gripperSensor);
}

void init(Katana_gripperSensor* this)
{
  this->name = xml_get_string(this->node, "name");
  
  if (this->device->number_of_components != 16)
    perror("Not the right number of sensors in .dev : it must be 16 !\n");

  printf("name %s : sensor_0 = %d, sensor_1 = %d\n",this->name,this->sensor_0, this->sensor_1);
  this->refresh=0;
}


void stop (Katana_gripperSensor *this)
{
   (void)this;
}

float get_normalized_sensor(Katana_gripperSensor *this)
{
  return ((float)(this->sensor)-this->sensor_0) /this->sensor_gain;
}

float get_sensor(Katana_gripperSensor *this)
{
  int i, err;
  int val=0;
  Katana_gripperSensor *gripperSensor;
  char *buff_ptr;
  char buff[SIZE_BUFF];
  
  if (this->refresh==0)
    {

      this->bus->send_and_receive(this->bus,"getSensor",14,buff,SIZE_BUFF);
      buff_ptr = buff;
      /*printf("#%s#\n",buff);*/
      
      for (i=0; i<this->device->number_of_components; i++) 
        { 
	  err = sscanf(buff_ptr,"%d:%s",&val,buff);
	  buff_ptr = buff;

	  if(err<1)
	    EXIT_ON_ERROR("Could not read sensor val from katana_broker for katana_gripperSensor %s in <%s>!\n", this->name,this->bus->receive_buffer);
	 
	  /* recuperation des valeurs des capteurs a finaliser */
	  gripperSensor = (Katana_gripperSensor*)this->device->components[i];
	  gripperSensor->sensor = val;
	  gripperSensor->refresh = 1;
        }
      
    }
  else
    {
      this->refresh = 0;
    }

  return get_normalized_sensor(this);
}
