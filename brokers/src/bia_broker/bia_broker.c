/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <linux/if_arp.h>
#include <sys/select.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <enet/enet.h>
#include "../prom_connection.h"
#include "basic_tools.h"

#define SYSTEM_COMMAND_SIZE_MAX 4096
#define READ_CMD 0x16ff
#define WRITE_CMD 0x1aff
#define MAC_ADDR_LEN 6

#pragma pack(2)
typedef struct
{
    char   Dest[6];
    char   Src[6];
    short  EtherType;       /* 0x1313 pour les centrales*/
    unsigned short Length;  /* nboctet apres entete*/
    short  Type;
    short  Bloc;            /* bit 15=1 si dernier bloc*/
    int    res1;
    int    res2;
    short  Cpt10k;
}TEnteteTrame;

typedef struct SndCmd
{
    unsigned short cmd;
    unsigned int Addr;
    unsigned short Nboct;
    char _data[1502];
}TSndCmd;

#pragma pack()

static char *MacCentrales[8]={
         "\0\0BIA ",
         "\0\0BIA!",
         "\0\0BIA\"",
         "\0\0BIA#",
         "\0\0BIA$",
         "\0\0BIA%",
         "\0\0BIA&",
         "\0\0BIA'"};
static unsigned char WinMacAddr[6]={0x00, 0x13, 0xa9, 0x8d, 0x0a, 0xa8};


typedef struct {
  char host[64];
  char robot[64];
  char prom[64];
  char data[512];
} type_bia_server;

typedef struct bia {
  int socket;
  struct sockaddr* socket_address;
} type_bia;

struct timeval timeOut;
/*----- Data management -----*/

/*----- Network management -----*/
char hostname[16];
unsigned int hostport;
int sockBia = -1, sockProm = -1, sock = -1;
struct sockaddr_ll bia_addr;
struct ifreq s_ifr;
struct sockaddr_in host_addr, prom_addr;
/*----- Other -----*/
char buffer[2048];
/*----- Data transmission -----*/
int ret;
socklen_t addrlen = sizeof(struct sockaddr_in);
fd_set set;

/** ********************************************************************
 ********** BIARead **********
 * ********************************************************************
 * @brief   Use the network to read a value at an address in the BIA bay
 * ********************************************************************
 * @param   sockfd      descriptor of the socket
 * @param   addr        address in memory of the BIA bay to read
 * @param   value       address of a float to store the value read
 * @param   dest_addr   descriptor of the protocol to use
 * @param   addrlen     size of dest_addr
 *
 * @return  -1          in case of error
 *          0           else
 ******************************************************************** **/
int bia_read(int sockfd, long unsigned int addr, float *value, struct sockaddr* dest_addr, socklen_t addrlen)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int ret, i;
  struct timeval timeOut;
  fd_set set;

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = READ_CMD;
  MsgCmd->Addr = addr;
  MsgCmd->Nboct = 4;
  MsgCmd->_data[0] = 0x08;

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 9;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;

  size = 60;

  /*----- Send the message -----*/
  if ((i = sendto(sockfd, buffer, size, 0, dest_addr, addrlen)) < size)
  PRINT_WARNING("not all bytes have been sent (%d)", i);
  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(sockfd, &set);
    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      return -1;
    }
    /*The socket receive all the message from eth0 so we need to filter them manualy*/
    ret = read(sockfd, buffer, sizeof(buffer));

  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  *value = *((float*) MsgCmd);
  return 0;
}

int bia_write(int sockfd, long unsigned int addr, float value, struct sockaddr* dest_addr, socklen_t addrlen)
{
  char buffer[1502];
  int size = 0;
  TEnteteTrame *header = (TEnteteTrame*) buffer;
  TSndCmd *MsgCmd = (TSndCmd*) (buffer + sizeof(TEnteteTrame));
  int i;
  struct timeval timeOut;
  fd_set set;

  memset(buffer, 0x00, sizeof(buffer));

  /*----- Filling in of the message -----*/
  MsgCmd->cmd = WRITE_CMD;
  MsgCmd->Addr = addr;
  MsgCmd->Nboct = 4;
  memcpy(MsgCmd->_data, &value, sizeof(value));

  /*----- Filling in of the header -----*/
  memcpy(header->Dest, MacCentrales[0], MAC_ADDR_LEN);
  memcpy(header->Src, WinMacAddr, MAC_ADDR_LEN);
  header->EtherType = 0x1313;
  header->Length = 13;
  header->Type = 0x0002;
  header->Bloc = 0x0000;
  header->res1 = 0x00000000;
  header->res2 = 0x00000000;
  header->Cpt10k = 0x0000;

  size = 60;

  /*----- Send the message -----*/
  if ((i = sendto(sockfd, buffer, size, 0, dest_addr, addrlen)) < size)   PRINT_WARNING("not all bytes have been sent (%d)", i);

  /*----- Wait for the message to arrive -----*/
  do
  {
    timeOut.tv_sec = 0;
    timeOut.tv_usec = 1000000;
    FD_ZERO(&set);
    FD_SET(sockfd, &set);
    if (select(sizeof(set), &set, NULL, NULL, &timeOut) <= 0)
    {
      EXIT_ON_ERROR("select");
    }
    read(sockfd, buffer, sizeof(buffer));
  } while (memcmp((void*) &(header->Src), (void*) MacCentrales[0], 6) != 0);

  return 0;
}

void bia_connect(type_bia_server *bia_server)
{
  char *host = bia_server->host;
  char *robot = bia_server->robot;
  char *prom = bia_server->prom;
  char *data = bia_server->data;

  /*----- Init data -----*/
  memset(&bia_addr, 0x00, sizeof(struct sockaddr_ll));
  memset(&prom_addr, 0x00, sizeof(struct sockaddr_in));
  memset(buffer, 0x00, sizeof(buffer));

  /*----- Init network -----*/
  sockBia = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (sockBia < 0)
  {
    EXIT_ON_SYSTEM_ERROR("when creating BIA socket. You probably need to use 'sudo'. ");
  }

  /*----- Get the eth to be used -----*/
  /*(eth is specified on the command line cf brocker.c)*/

  printf("%s : trying to bind on %s...\n", __FUNCTION__, robot);

  /*----- Get interface index of the eth specified -----*/
  strncpy(s_ifr.ifr_name, robot, sizeof(s_ifr.ifr_name));
  ioctl(sockBia, SIOCGIFINDEX, &s_ifr);

  memset(&bia_addr, 0, sizeof(struct sockaddr_ll));

  bia_addr.sll_protocol = htons(ETH_P_ALL);
  bia_addr.sll_family = AF_PACKET;
  bia_addr.sll_ifindex = s_ifr.ifr_ifindex;/*physical eth output to use*/
  bia_addr.sll_hatype = ARPHRD_ETHER;
  bia_addr.sll_pkttype = 0;/*PACKET_OUTGOING;*/
  bia_addr.sll_halen = ETH_ALEN; /*defined as 6 */

  memcpy(bia_addr.sll_addr, MacCentrales[0], 6 * sizeof(char));

  /*----- Get the Mac addr of host -----*/
  /* We don't use it now because the BIA bay answer only to the window
   * computer that load the program. For now we just cheat and speak to
   * the robot as if we were the window pc
   ioctl(sockBia, SIOCGIFHWADDR, &s_ifr);
   memcpy(HostMacAddr, s_ifr.ifr_hwaddr.sa_data, MAC_ADDR_LEN);
   */

  /*----- Bind the socket to the eth specified -----*/
  if (bind(sockBia, (struct sockaddr *) &bia_addr, sizeof(struct sockaddr_ll)) != 0)
  PRINT_WARNING("couldn't bind sockfd");

  printf("%s : bind on %s succesfull\n", __FUNCTION__, robot);

}

void bia_set_positions(float *data, size_t size)
{
  int i;
  long unsigned int addr;

  addr = 0x02001764; /* _TABCONSf7 */

  for (i=0; i*sizeof(float) < size; i++)
  {
    if (bia_write(sockBia, addr+i*sizeof(float), data[i], (struct sockaddr*) &bia_addr, sizeof(struct sockaddr_ll)) < 0) printf("nothing to receive. Waiting...");
  }
}

void bia_get_positions(float *data, size_t size)
{
  long unsigned int addr;

  addr = 0x02001780; /* _TABMESf7 */
  if (bia_read(sockBia, addr, (void*)data, (struct sockaddr*) &bia_addr, sizeof(struct sockaddr_ll)) < 0) printf("nothing to receive. Waiting...");
}

void bia_disconnect()
{

  printf("%s : preparation to quit...\n", __FUNCTION__);
  shutdown(sockBia, SHUT_RDWR);
  close(sockBia);
}

/** ********************************************************************
 ********** Main **********
 * ********************************************************************
 * @brief	Initialize the datas and launch the disired threads
 * 			(the choice being the server, the client or both) using the
 * 			arguments
 * ********************************************************************
 * @param	argv		number of arguments
 * @param	argc		array of strings containing the arguments
 * 
 * @return	-1			in case of error
 * 			0			else
 ******************************************************************** **/
int main(int argc, char *argv[])
{
  char system_command[SYSTEM_COMMAND_SIZE_MAX];
  type_bia_server bia_server;
  pthread_t srv_BIA;
  int i, option_index = 1, option_result;
  float value;
  type_prom_connection prom_connection;
  pthread_t prom_connection_thread;

  /*----- Initialize data -----*/

  bia_server.robot[0] = 0;

  while (option_index < argc)
  {
    if (strstr(argv[option_index], "eth") != NULL )
    {
      strcpy(bia_server.robot, argv[option_index]);
    }
    option_index++;
  }
  if (bia_server.robot[0] == 0) EXIT_ON_ERROR("You need to precise the interface. eth0, eth1, ...");

  sprintf(system_command, "ifconfig %s promisc", bia_server.robot);
  printf("%s\n", system_command);
  if (system(system_command) != 0) EXIT_ON_ERROR("Fail to execute: %s", system_command);

  prom_receive_callbacks[SET_POSITION_CHANNEL_ID] = bia_set_positions;
  prom_receive_callbacks[GET_POSITION_CHANNEL_ID] = bia_get_positions;

  bia_connect(&bia_server);
  prom_connection.ip = "127.0.0.1";
  prom_connection.port = 12345;

  value = 0;
  bia_set_positions(&value, 4);
  prom_connection_manager(&prom_connection);
  bia_disconnect();


  return 0;
}

