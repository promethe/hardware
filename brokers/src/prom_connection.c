/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "prom_connection.h"
#include <enet/enet.h>
#include "basic_tools.h"
#include <stdio.h>

#define ENET_UNLIMITED_BANDWITH 0
#define ENET_CLIENTS_MAX 10
#define TIMEOUT 10000

type_prom_receive_callbacks prom_receive_callbacks[CHANNELS_NB] = {};

void prom_connection_manager(type_prom_connection *prom_connection)
{
  ENetAddress address;
  ENetEvent event;
  ENetHost *host;
  ENetPeer *peer;
  int connected = 0;

  if (enet_initialize() != 0)
  {
    EXIT_ON_ERROR("An error occurred while initializing ENet.\n");
  }
  atexit(enet_deinitialize);

  host = enet_host_create(NULL, 1, CHANNELS_NB, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);
  if (!host) EXIT_ON_ERROR("Impossible to create the server");
  if (enet_address_set_host(&address, prom_connection->ip) < 0) EXIT_ON_ERROR("Impossible to set the address %s", prom_connection->ip);
  address.port = prom_connection->port;
  peer = NULL; /* On ne le connaitra qu'a la connection */
  address.host = ENET_HOST_ANY;
  host = enet_host_create(&address, ENET_CLIENTS_MAX, CHANNELS_NB, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);
  if (!host) EXIT_ON_ERROR("Impossible to create the server port %i. \n\t Check that ther is no other promethe using this port and adress. \n It may be the peer open but frozen.", address.port);

  while (1)
  {
    if (enet_host_service(host, &event, TIMEOUT) < 0) EXIT_ON_ERROR("Problem with enet_host_service");

    switch (event.type)
    {
    case ENET_EVENT_TYPE_CONNECT:
      event.peer->data = NULL;
      if (peer == NULL ) peer = event.peer;
      else if (peer != event.peer) EXIT_ON_ERROR("The peer is not the one expected. No more than one connection at a time.");
      printf("Connection \n");
      connected = 1;
      break;

    case ENET_EVENT_TYPE_RECEIVE:
      if (prom_receive_callbacks[event.channelID] != NULL ) prom_receive_callbacks[event.channelID](event.packet->data, event.packet->dataLength);
      else PRINT_WARNING("No prom_receive_callback declared for channel %i. %li bytes received but not treated.", event.channelID, event.packet->dataLength);
      enet_packet_destroy(event.packet);
      break;

    case ENET_EVENT_TYPE_DISCONNECT:
      /* Reset the peer's client information. */
      event.peer->data = NULL;
      peer=NULL;
      connected = 0;
      break;
    case ENET_EVENT_TYPE_NONE:
      PRINT_WARNING("No command for "STRINGIFY_CONTENT(TIMEOUT)" miliseconds.");
      break;
    default:
      break;
    }
  }
}
