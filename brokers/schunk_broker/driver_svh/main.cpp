/*
 *  Created on: Oct 2, 2014
 *      Author: Arnaud Blanchard
 */

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include <driver_svh/SVHFingerManager.h>

#include <limits.h>
#include <getopt.h>
#include <math.h>

#include "blc_tools.h"
#include "blc_channel.h"
#include "fcntl.h"

#define DEFAULT_DEVICE "/dev/ttyUSB0"
#define DEFAULT_BROKER_NAME "prom.schunk.hand"

#define DEFAULT_PORT 4000
#define MOTORS_NB driver_svh::eSVH_DIMENSION

static struct option long_options[] =
  {
    { "device", required_argument, NULL, 'd' },
    { "help", no_argument, NULL, 'h' },
    { "port", required_argument, NULL, 'p' },
    { "", 0, NULL, 0 }
  };

driver_svh::SVHFingerManager hand;
const char *device_name = DEFAULT_DEVICE;
const char *broker_name = DEFAULT_BROKER_NAME;


void usage()
{
  printf("usage: schunk_broker [options]\n");
  printf("  -d\t--device\t\t:device (default "DEFAULT_DEVICE")\n");
  printf("  -h\t--help\t\t:display this help\n");
}

void  quit(int signal)
{
   (void) signal;
   hand.disconnect();
   printf("Quitting broker\n");
   exit(1);
}

int main(int argc, char**argv)
{

   blc_channel *set_positions_channel, *get_positions_channel, *get_torques_channel, *set_torques_channel;
   char channel_name[NAME_MAX];
   float *set_positions_values, *get_positions_values, *get_torques_values, *set_torques_values;
   double current_position, current, value;
   int option, option_index, i;

  while ((option = getopt_long(argc, argv, "d:h", long_options, &option_index)) != -1)
    switch (option)
    {
    case 'd':
      device_name = optarg;
      break;
    case 'h':
      usage();
      exit(0);
      break;
    default:
      usage();
    }

  argc -= optind;
  argv += optind;

  if (argc == 1)  broker_name = argv[0];
  else if (argc > 1)
  {
    usage();
    EXIT_ON_ERROR("Wrong format of parameters.");
  }

  signal(SIGINT, quit);

  printf("device\t%s\nbroker:\t%s\n-h for help\n", device_name, broker_name);
  printf("Initializing the hand ...\n");

  if (!hand.connect(device_name)) EXIT_ON_ERROR("Fail to connect to the hand");
  hand.resetChannel(driver_svh::eSVH_ALL);

  snprintf(channel_name, NAME_MAX, "%s.set_positions-tf",broker_name);
  set_positions_channel = new blc_channel(channel_name, sizeof(float)*MOTORS_NB); // We cannot have writelonly
  set_positions_values=(float*)set_positions_channel->data;
  snprintf(channel_name, NAME_MAX, "%s.get_positions-tf",broker_name);
  get_positions_channel = new blc_channel(channel_name, sizeof(float)*MOTORS_NB);
  get_positions_values=(float*)get_positions_channel->data;
  snprintf(channel_name, NAME_MAX, "%s.get_torques-tf",broker_name);
  get_torques_channel = new blc_channel(channel_name, sizeof(float)*MOTORS_NB);
  get_torques_values=(float*)get_torques_channel->data;
  snprintf(channel_name, NAME_MAX, "%s.set_torques-tf",broker_name);
  set_torques_channel = new blc_channel(channel_name, sizeof(float)*MOTORS_NB);
  set_torques_values=(float*)set_torques_channel->data;

  printf("The hand is ready on path %s.\n", broker_name);

  while(1)
  {
     FOR(i, MOTORS_NB)
     {
        hand.getRelativePosition((driver_svh::SVHChannel)i, value);
        get_positions_values[i] = value;
        hand.getCurrent((driver_svh::SVHChannel)i, value);
        get_torques_values[i] = fabs(value/1000.);
        if (get_torques_values[i] > set_torques_values[i])  set_positions_values[i] =  get_positions_values[i];
        value = set_positions_values[i];
        hand.setRelativePosition((driver_svh::SVHChannel)i, value);
     }
     usleep(20000);
  }

  getchar();
  quit(0);

  return 0;
}
