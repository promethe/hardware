/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
    \file  create_pantilt.c 
    \brief 

    Author: Julien Hirel
    Created: 29/04/2009
    Modified:
    - author: -
    - description: -
    - date: -

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 

    Macro:
    -none 

    Local variables:
    - none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: none (yet!)

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

#include <Aibo.h>
#include <PanTilt.h>

#include "tools/include/local_var.h"

void pantilt_create_pantilt(PanTilt ** pantilt, char *name, char *type)
{

   FILE *fh;
   char lecture[256];
   char tmp[129];
   int tmpi, ret;
   void *lib_handle = NULL;
   Biclops *(*new_biclops)() = NULL;
   int (*biclops_initialize)(Biclops *, const char*) = NULL;
   void (*biclops_setdebuglevel)(Biclops *, int) = NULL;
   int (*biclops_homeall)(Biclops *, long) = NULL;

   *pantilt = (PanTilt *) malloc(sizeof(PanTilt));
   if (*pantilt == NULL)
   {
      fprintf(stderr, "ERROR in pantilt_create_pantilt: malloc failed for pantilt\n");
      exit(1);
   }

   strcpy((*pantilt)->name, name);    /*nom du pantilt */
   tmp[128] = '\0';
    
   (*pantilt)->autocheck = 0;
   /* valeur par defaut au cas ou ils ne sont pas definis */
   strcpy((*pantilt)->config_file, "\0");
   (*pantilt)->type = 1;
   (*pantilt)->debug = 0;

   /* Initializing function pointers */
   (*pantilt)->biclops_getaxisactualposition_rev = NULL;
   (*pantilt)->biclops_moveall = NULL;
   (*pantilt)->biclops_moveaxis = NULL;
   (*pantilt)->biclops_setaxisprofilepos_rev = NULL;
   
   /* recuperation des informations contenu dans le fichier hardware */
   fh = fopen(type, "r");
   if (fh == NULL)
   {
      fprintf(stderr, "ERROR in pantilt_create_pantilt: Cannot open file  %s\n", type);
      exit(1);
   }
    
   while (!feof(fh))
   {
      ret = fscanf(fh, "%s = ", lecture);   /* lecture du premier mot de la ligne */
      
      str_upper(lecture);
      
      if (strcmp(lecture, "CONFIG") == 0)
      {

	 if (fgets(tmp, 129, fh) == NULL || strlen(tmp) > 127)
	 {
	    fprintf(stderr, "ERROR in pantilt_create_pantilt: Error reading config of pantilt %s\n", name);
	    exit(1);
	 }
	 
	 /* delete the end char if it is an end line */
	 if (tmp[strlen(tmp) - 1] == '\n')
	    tmp[strlen(tmp) - 1] = '\0';
	 
	 strcpy((*pantilt)->config_file, tmp);	 
      }
      else if (strcmp(lecture, "TYPE") == 0)
      {
	 ret = fscanf(fh, "%d\n", &tmpi);

	 if (ret != 1)
	 {
	    fprintf(stderr, "ERROR in pantilt_create_pantilt: Error reading type value (int) of pantilt %s\n", name);
	    exit(1);
	 }

	 (*pantilt)->type = tmpi;
      }
      else if (strcmp(lecture, "DEBUG") == 0)
      {
	 ret = fscanf(fh, "%d\n", &tmpi);

	 if (ret != 1)
	 {
	    fprintf(stderr, "ERROR in pantilt_create_pantilt: Error reading debug value (int) of pantilt %s\n", name);
	    exit(1);
	 }

	 (*pantilt)->debug = tmpi;
      }
      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
	 ret = fscanf(fh, "%d\n", &tmpi);

	 if (ret != 1)
	 {
	    fprintf(stderr, "ERROR in pantilt_create_pantilt: Error reading autocheck value (int) of pantilt %s\n", name);
	    exit(1);
	 }

	 (*pantilt)->autocheck = tmpi;
      }
      else
	 lire_ligne(fh);
   }
   fclose(fh);

   if (strlen((*pantilt)->config_file) == 0)
   {
      fprintf(stderr, "ERROR in pantilt_create_pantilt: No config file was specified\n");
      exit(1);
   }

   printf("pantilt_create_pantilt: PanTilt %s \n", (*pantilt)->name);
   printf("\t\t config=%s \n\t\t type=%d \n\t\t debug=%d \n", (*pantilt)->config_file, (*pantilt)->type, (*pantilt)->debug);

   /* Dynamic loading of the Biclops library */
   lib_handle = dlopen("libBiclops_prominterface.so", RTLD_LAZY);
   if (lib_handle == NULL)
   {
      PRINT_WARNING("Could not load Biclops library\n\tError message from dlopen: %s\n", dlerror());
   }
   else
   {
      /* Compiling with -ansi and -pedantic will cause gcc to complain about the conversion from a void * pointer to a function pointer when calling dlsym. This is due to the implementation of dlsym which returns a data pointer and assumes that conversion from a data pointer to a function pointer is supported by the system it is running on (which is pretty much always the case, at least when the system is SUSv3/POSIX compliant). This warning can be avoided by first casting the pointer to an intptr_t and then to a function pointer, but since this is bad practice we leave the warning. According to the dlsym development team, the function might be broken in the future into 2 functions, one returning data pointers and one returning function pointers */

      new_biclops = /*(Biclops *(*)()) (intptr_t)*/ dlsym(lib_handle, "new_biclops");
      if (new_biclops == NULL)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not load new_biclops function\n");
	 fprintf(stderr, "\tError message from dlsym: %s\n", dlerror());
	 exit(1);
      }
      (*pantilt)->biclops = (*new_biclops)();

      biclops_initialize = dlsym(lib_handle, "biclops_initialize");
      if (biclops_initialize == NULL)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not load biclops_initialize function\n");
	 fprintf(stderr, "\tError message from dlsym: %s\n", dlerror());
	 exit(1);
      }
   
      if ((*biclops_initialize)((*pantilt)->biclops, (*pantilt)->config_file) == 0)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not initialize PanTilt\n");
	 exit(1);      
      }

      biclops_setdebuglevel = dlsym(lib_handle, "biclops_setdebuglevel");
      if (biclops_setdebuglevel == NULL)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not load biclops_setdebuglevel function\n");
	 fprintf(stderr, "\tError message from dlsym: %s\n", dlerror());
	 exit(1);
      }

      (*biclops_setdebuglevel)((*pantilt)->biclops, (*pantilt)->debug);

      biclops_homeall = dlsym(lib_handle, "biclops_homeall");
      if (biclops_homeall == NULL)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not load biclops_homeall function\n");
	 fprintf(stderr, "\tError message from dlsym: %s\n", dlerror());
	 exit(1);
      }
      
      if ((*biclops_homeall)((*pantilt)->biclops, 0) == 0)
      {
	 fprintf(stderr, "ERROR in pantilt_create_pantilt: Could not initialize PanTilt\n");
	 exit(1);      
      }
   }

   (*pantilt)->libBiclops_handle = lib_handle;
}
