/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Compass.h>
#include <Serial.h>
#include <ClientHW.h>
#include <unistd.h>
#include <ClientRobubox.h>

#include <termios.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*#define ONLINE_DEBUG*/
/*#define DEBUG*/
#include <net_message_debug_dist.h>

float compass_read(Compass * compass)
{
   if (compass->compass_type == USE_HOLTZ)
      return (float) compass_holtz_read(compass);
   else if (compass->compass_type == USE_STAMP)
      return (float) compass_stamp_query(compass);
   else if (compass->compass_type == USE_CLIENTHW)
      return (float) compass_server_read(compass);
   else if (compass->compass_type == USE_COMPASS_ROBUBOX)
      return (float) compass_robubox_query(compass);
   else if (compass->compass_type == USE_COMPASS_ROBUROC)
      return (float) compass_robuboc_query(compass);
   else if (compass->compass_type == USE_COMPASS_THREAD)
      return (float) compass_thread_query(compass);

   /*else if(COMPASS_TYPE==1) return AngleBoussole_oneb();
       else printf("ERROR on COMPASS_TYPE!"); */
   return -1.;
}

/* fonction de lecture de la valeur de la boussole par requete sur le basic stamp */
int compass_robuboc_query(Compass * compass)
{
    char reponse[400];
    int ret = 0;
    int compass_value;
    char buff[10] ; /* Commande 'C' envoyees a l'arduino pour demander la valeur de la boussole */

  /* dprints("compass_stamp_query: starting \n");*/

    buff[0]='C';
    buff[1]='\n';
    buff[2]=0;
   do
   {
      serial_send_and_receive(compass->port, buff, reponse, 3);
     /* dprints("\n reponse : %s --\n", reponse);*/
	 ret = sscanf(reponse, "-c%d-t%d-r%d-", &compass_value,(int *) &(compass->tilt),(int *) &(compass->roll));

       if (ret != 3)
          PRINT_WARNING("Type=ROBUROC. The value (%s) read by compass_read does not have the required format. Retrying to read ...", reponse);
    }
    while (ret != 3);


   /*dprints("compass_value=%d\n,tilt_value=%d\n,roll_value=%d\n ", compass_value,compass->tilt,compass->roll);*/

   return compass_value;
}

/* fonction de lecture de la valeur de la boussole par requete sur le basic stamp */
int compass_stamp_query(Compass * compass)
{
   char reponse[400];
   int ret = 0;
   int compass_value;
   int length;
   char buff = 'C'; /* Commande 'C' envoyees au basic stamp pour demander la valeur de la boussole */

#ifdef ONLINE_DEBUG
   printf("compass_stamp_query: starting \n");
#endif

   do
   {
      serial_send_and_receive(compass->port, &buff, reponse, 1);

      length = strlen(reponse);
      if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
         reponse[length-1] = '\0';

#ifdef ONLINE_DEBUG
      printf("\n reponse : %s --\n", reponse);
#endif

      if (reponse[0] == 'C')
         ret = sscanf(reponse+1, "-c%d-", &compass_value);
      else
         ret = sscanf(reponse, "-c%d-", &compass_value);

      if (ret != 1)
         PRINT_WARNING("Type=STAMP_QUERY. The value (%s) read by compass_read does not have the required format. Retrying to read ...", reponse);
   }
   while (ret != 1);

#ifdef DEBUG
   printf("compass_value=%d\n", compass_value);
#endif

   return compass_value;
}


/* fonction devenue obsolete lisant les valeurs de la boussole qui sont envoyees continuellement par le basic stamp */
int compass_stamp_read(Compass * compass)
{
   char reponse[400];
   int n, l, m, ret = 0;
   char lu;
   int compass_value;
   int serialport1;
#ifdef ONLINE_DEBUG
   printf("compass stamps _read \n");
#endif
   serialport1 = (serial_get_serial_by_name(compass->port))->desc;

   /*sprintf(buff,"C");
       n = strlen(buff);
       buff[n]=0x0D;
       buff[n+1]=0;
       printf("avant envoie\n");
       tcdrain(serialport1);
       write(serialport1, buff, n); */
   do
   {
      tcdrain(serialport1);
      tcflush(serialport1, TCIOFLUSH);

      l = m = 0;
      do
      {
         n = read(serialport1, &lu, 1);
#ifdef DEBUG
         printf("n=%d\n", n);
#endif
         if (n)
         {
            if (lu > 2)
            {
               reponse[l] = lu;
#ifdef DEBUG
               printf("%c -> %d\n", reponse[l], reponse[l]);
#endif
               l++;
            }
         }
         else
         {
            lu = '\0';
            m++;
         }
      }
      while ((lu != 13) && (l < 100) && (m < 10000));
      n = l;
      reponse[n - 1] = '\0';



#ifdef ONLINE_DEBUG
      printf("\n reponse : %s --\n", reponse);
#endif
      ret = sscanf(reponse, "-c%d-", &compass_value);

      if (ret != 1)
         PRINT_WARNING("Type=STAMP_READ. The value (%s) read by compass_read does not have the required format. Retrying to read ...", reponse);
   }
   while (ret != 1);
#ifdef DEBUG
   printf("compass_value=%d\n", compass_value);
#endif
   return (compass_value * 360. / 356.);
}

int compass_holtz_read(Compass * compass)
{
   (void)compass;
   printf
   ("le code pour les anciennes bossole n'existe pas dans le packege Compass\n");
   exit(0);
}

int compass_server_read(Compass * compass)
{
   char commande[256];
   char result[256];
   int retour = -1;
   ClientHW *chw;
#ifdef DEBUG
   printf("debut compass_server_read\n");
#endif
   chw = clientHW_get_clientHW_by_name(compass->clientHW);
   sprintf(commande, "compass_read %s", compass->name);
   clientHW_transmit_receive(chw, commande, result);
#ifdef DEBUG
   printf("result after transmit: %s\n", result);
#endif
   sscanf(&(result[2]), "%d", &retour);
   return (retour);
}



int compass_robubox_query(Compass *compass)
{
   ClientRobubox *clientRobubox;
   char commande[256];
   char result[256];

   char cmd[256];
   float value = 0;

   clientRobubox = clientRobubox_get_clientRobubox_by_name(compass->clientRobubox);

   sprintf(commande,"%s","Compass");
   clientRobubox_transmit_receive(clientRobubox,commande,result);

   sscanf(result, "%s %f", cmd, &value);

   value /= 1000;
   value = value * 180 / M_PI;

   return value;
}



int compass_thread_query(Compass *compass)
{
   int compass_value;

   //	un seul entier à lire : pas besoin de mutex (en teste)
   //	pthread_mutex_lock(&compass->lock);
   compass_value = compass->compass_thread_value;
   //	pthread_mutex_unlock(&compass->lock);

   return compass_value;
}
