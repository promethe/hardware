/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Compass.h>
#include <Serial.h> 
#include <unistd.h>

void compass_read_thread(Compass *compass)
{
   char reponse[400];
   int ret;
   int size;
   char cmd='C';

   int tmp_value;

   if( compass == NULL )
   {
      dprints("%s : stop thread function, pointer null ",__FUNCTION__);
   }

   while( compass->enabled == 1)
   {
    do
    {
    //printf("sending compass\n");
    //size = serial_send_and_receive(compass->port,&cmd, reponse, 1);
    size = serial_send_and_receive_block(compass->port, &cmd, reponse,1,1000000);
      if(size == 0)
         PRINT_WARNING("Unable to read from port %s\n",compass->port) ;
      //printf("Compass thread : |%s| %d \n", reponse,size);
      //serial_receive(us_serial->port, reponse);

      /*traitement de la réponse */
      // un seul entier à lire => pas besoin de mutex ...
      //pthread_mutex_lock(&compass->lock);
      //printf("receive compass %s\n",reponse);
      ret = sscanf(reponse, "-c%d-", &tmp_value );

      if( ret == 1 ) compass->compass_thread_value = tmp_value;
      else printf("erreur lecture compass %s\n",reponse);
      }
      while(ret!=1);

      		//printf("Compass value %d  %d\n", compass->compass_thread_value,tmp_value);

      //pthread_mutex_unlock(&compass->lock);
     usleep(1000);
   }
}
