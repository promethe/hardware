/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  serial_setparam.c 
\brief Set parameter of serial communication

Author: B. Mariat
Created: 23/03/2005
Modified:
-

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 * set parameter of serial communication
 * eg:9600 8N2
  - speed: 9600
  - bits length: 8
  - parity: N (none), E (even), O (odd)
  - number of stop bit: 2

Macro:
-none 

Local variables:
-none

Global variables:
-oldttio

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include <Serial.h>
#include "tools/include/local_var_serial.h"


int serial_setparam(char *port_name, char *speed, char *bits, char *par,
      char *stopb)
{
   int spd = -1;
   int newbaud;
   int bit = bits[0];
   struct termios tty;
   Serial *s_port;

   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      printf("Serial_setparam: Port %s non trouve\n", port_name);
      return -1;
   }

   /* Get current parameters */
   tcgetattr(s_port->desc, &tty);
   tcgetattr(s_port->desc, &oldttio);

   /* Set speed */
   /* Check if 'speed' is really a number */
   if ((newbaud = (atol(speed) / 100)) == 0 && speed[0] != '0')
      newbaud = -1;

   switch (newbaud)
   {
   case 0:
      spd = B0;
      break;
   case 3:
      spd = B300;
      break;
   case 6:
      spd = B600;
      break;
   case 12:
      spd = B1200;
      break;
   case 24:
      spd = B2400;
      break;
   case 48:
      spd = B4800;
      break;
   default:
   case 96:
      spd = B9600;
      break;
   case 192:
      spd = B19200;
      break;
   case 384:
      spd = B38400;
      break;
   case 576:
      spd = B57600;
      break;
   case 1152:
      spd = B115200;
      break;
#ifndef Darwin      /* n'existe pas sous Darwin ... */
   case 20000:
      spd = B2000000;
      break;
   case 10000:
      spd = B1000000;
      break;
#endif        
   }

   if (spd != -1)
   {
      cfsetospeed(&tty, (speed_t) spd);
      cfsetispeed(&tty, (speed_t) spd);
   }

   switch (bit)
   {
   case '5':
      tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS5;
      break;
   case '6':
      tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS6;
      break;
   case '7':
      tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS7;
      break;
   case '8':
   default:
      tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
      break;
   }

   /* Set into raw, no echo mode */
   tty.c_iflag = IGNBRK;

   tty.c_lflag = 0;
   tty.c_lflag |= s_port->icanon;
   tty.c_oflag = 0;
   tty.c_cflag |= CLOCAL | CREAD;
   tty.c_cflag &= ~CRTSCTS;
   tty.c_cc[VMIN] = s_port->vmin;
   tty.c_cc[VTIME] = s_port->vtime;

   /* No Flow control */
   tty.c_iflag &= ~(IXON | IXOFF | IXANY);

   /* parity */
   tty.c_cflag &= ~(PARENB | PARODD);
   if (par[0] == 'E')
      tty.c_cflag |= PARENB;
   else if (par[0] == 'O')
      tty.c_cflag |= (PARENB | PARODD);

   /* stop */
   if (stopb[0] == '2')
      tty.c_cflag |= CSTOPB;
   else
      tty.c_cflag &= ~CSTOPB;

   /* Apply new settings */
   tcsetattr(s_port->desc, TCSANOW, &tty);

   return 1;
}
