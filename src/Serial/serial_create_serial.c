/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
/* #include <sys/ioctl.h> */
/*#include <fcntl.h> */

#include <Serial.h>
#include <tools.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>
#include <fcntl.h>


/*#include "tools/include/macro.h" */
#include "tools/include/local_var_serial.h"

void serial_create_serial(Serial ** serial, char *name, char *file_hw)
{

   FILE *fh = NULL;
   char lecture[256];
   int ret,ligne;


   *serial = (Serial *) malloc(sizeof(Serial));
   if (*serial == NULL)
   {
      printf("erreur malloc serial_create_serial\n");
      exit(0);
   }
   (*serial)->name = (char *) malloc(strlen(name) + 1);
   if ((*serial)->name == NULL)
   {
      printf("erreur malloc serial_create_serial\n");
      exit(0);
   }
   strcpy((*serial)->name, name);  /*Serial_object name */



   /*Serial_get_info_from_dev_file((*serial)); */

   /* default parameters */


    /*! loading serial parameters */
    (*serial)->vmin=1;
    (*serial)->vtime=0;
    (*serial)->icanon=0;  
    (*serial)->onblock=0;  

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }
   ligne = 0;
   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */
      ligne ++;
      /*printf("lit: %s %d\n",lecture,ret);*/
      if(ret != 1)
         EXIT_ON_ERROR("The hardware file (%s) does not match the expected format at line %d , pour ret = %d , lu = %s !\n",
             file_hw,ligne,ret,lecture ) ;
      str_upper(lecture);

      if ((strcmp(lecture, "PATH") == 0)) /* get the port address */
      {
         ret=fscanf(fh, "%s\n", ((*serial)->path));
      }
      else if (strcmp(lecture, "DEBIT") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serial)->debit));
      }
      else if (strcmp(lecture, "BIT") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serial)->bit));
      }
      else if (strcmp(lecture, "PARITE") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serial)->parite));
      }
      else if (strcmp(lecture, "STOP") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serial)->stop));
      }
      else if (strcmp(lecture, "VMIN") == 0)
      {
         ret=fscanf(fh, "%d\n", &(((*serial)->vmin)));
      }
      else if (strcmp(lecture, "VTIME") == 0)
      {
         ret=fscanf(fh, "%d\n", &(((*serial)->vtime)));
      }
      else if(strcmp(lecture, "ICANON") == 0)
      {
         ret=fscanf(fh, "%d\n", &(((*serial)->icanon)));
         if( (*serial)->icanon)
         {
            (*serial)->icanon = ICANON;
         }
      }
      else if(strcmp(lecture, "NDELAY") == 0)
      {
         ret=fscanf(fh, "%d\n", &(((*serial)->onblock)));
         if( (*serial)->onblock)
         {
            (*serial)->onblock = O_NDELAY;
         }
      }
      else
         lire_ligne(fh);

   }
   fclose(fh);

   if (((*serial)->desc =open((char *) (*serial)->path, O_RDWR |  (*serial)->onblock )) < 0)
   {
      printf("serial_init: ERREUR ouverture du port serie pour l'adresse %s \n",(*serial)->path);
      exit(1);
   }
   if (pthread_mutex_init(&((*serial)->mutex_serial), NULL))
      printf("*** %s : %d : Can't initialise mutex : mutex_list_prom_fft ***", __FUNCTION__, __LINE__);
   printf("\t\t path=%s \n\t\t debit=%s \n\t\t bit=%s \n\t\t parite=%s\n\t\t stop=%s\n",
         ((*serial)->path), ((*serial)->debit), ((*serial)->bit),
         ((*serial)->parite), ((*serial)->stop));


}
