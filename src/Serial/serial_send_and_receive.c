/*
Copyright  ETIS ��� ENSEA, Universit�� de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouz��ne,  
M. Lagarde, S. Lepr��tre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengerv��, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  serial_send_and_receive.c 
\brief Send a command through serial port and wait for an answer

Author: B. Mariat
Created: 23/03/2005

Description: 
 * Send a command through serial port and wait for an answer
 * the end of the answer is detect by '\r' character (10 in the ASCII table)
 * 1 is return if the operation has succed, and -1 if there is a problem



Known bugs: 
peut etre si on ajoute un \n a la fin de la commande: on se retrouve avec 2 \n et il peut y avoir un probleme
*/

#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>


#include <Serial.h>
#include "tools/include/macro_serial.h"

int serial_send_and_receive(char *port_name, char *commande, char *retour,
      int lenght)
{
   int n, /*i, */ l, m,o;
   char lu;
   Serial *s_port;

   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      cprints("serial_send_and_receive: Port %s non trouve\n", port_name);
      return -1;
   }

   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIOFLUSH);
   /* TESTER LE RETOUR !! */
   o = write(s_port->desc, commande, lenght);
   if (o == -1)
   {
      EXIT_ON_ERROR("write serial");
   }

   /* fprintf(stdout, "o = %i\n", o);*/
   tcdrain(s_port->desc);

   /* lecture de la reponse */
   l = m = 0;
   do
   {
      n = read(s_port->desc, &lu, 1);

      dprints("serial_send_and_receive: read -> n = %d %c\n", n,lu);

      if (n > 0)
      {
         retour[l] = lu;
         dprints("serial_send_and_receive: character read = %c -> %d\n", retour[l],(int) retour[l]);
         l++;
      }
      else if (n == 0)               /*the answer is not read. */
      {
         lu = '\0';
         m++;
      }
      else
      {
         EXIT_ON_ERROR("serial_send_and_receive read error (n = %i)\n", n);
      }

   }
   while ((lu != 10) && (lu != 13) && (lu != '\n') && (l < 100) && (m < 1000));
   n = l;

   pthread_mutex_unlock(&((s_port)->mutex_serial));
   if( n > 0 )
   {
      if ((retour[n - 1] == '\n') || (retour[n - 1] == 10) || (retour[n - 1] == 13))
      {
         retour[n] = '\0';
         return n;
      }
   }

   cprints("serial_send_and_receive: ERREUR lors de l'ecriture sur la RS232\npas de ACK , temps depasse\n");
   return -1;

}

int serial_send_and_receive_line(char *port_name, char *commande, char *retour, int lenght)
{
   int n, l, m,o;
   char lu;
   Serial *s_port;

   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      cprints("serial_send_and_receive_line: Port %s non trouve\n", port_name);
      return -1;
   }

   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIOFLUSH);

   o = write(s_port->desc, commande, lenght);
   if (o == -1)
      EXIT_ON_ERROR("write serial");

   /* fprintf(stdout, "o = %i\n", o);*/
   tcdrain(s_port->desc);

   /* lecture de la reponse */
   l = m = 0;
   do
   {
      n = read(s_port->desc, &lu, 1);

      dprints("serial_send_and_receive_line: read -> n = %d %c\n", n,lu);

      if (n > 0)
      {
         retour[l] = lu;
         dprints("serial_send_and_receive_line: character read = %c -> %d\n", retour[l],(int) retour[l]);
         l++;
      }
      else if (n == 0)               /*the answer is not read. */
      {
         lu = '\0';
         m++;
      }
      else
         EXIT_ON_ERROR("serial_send_and_receive_line read error (n = %i)\n", n);

   }
   while (lu != '\n');
   n = l;

   pthread_mutex_unlock(&((s_port)->mutex_serial));
   if( n > 0 )
   {
      if ((retour[n - 1] == '\n') || (retour[n - 1] == 10) || (retour[n - 1] == 13))
      {
         retour[n] = '\0';
         dprints("%s\n", retour);
         return n;
      }
   }

   cprints("serial_send_and_receive_line: ERREUR lors de l'ecriture sur la RS232\npas de ACK , temps depasse\n");
   cprints("lu=%d, l=%d, m=%d\n", lu, l, m);
   return -1;
}

int serial_send_and_receive_nchar(char *port_name, char *commande, char *retour,
      int lenght, int lenght_out)
{
   int n, /*i, */ l, m,o;
   char lu;
   Serial *s_port;

   /*  n = strlen(commande);
      commande[n] = 0x0D;
      commande[n + 1] = 0;*/

   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      cprints("serial_send_and_receive: Port %s non trouve\n", port_name);
      return -1;
   }


   /*    n = strlen(commande);*/
   /*  for(i=0; i<TIMEOUT; i++){*/

   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIOFLUSH);
   /* TESTER LE RETOUR !! */
   o = write(s_port->desc, commande, lenght);
   if (o == -1)
      EXIT_ON_ERROR("write serial");

   /* fprintf(stdout, "o = %i\n", o);*/
   tcdrain(s_port->desc);

   /* lecture de la reponse */
   l = m = 0;
   do
   {
      n = read(s_port->desc, &lu, 1);
      dprints("serial_send_and_receive: read -> n = %d\n", n);

      if (n > 0)
      {
         retour[l] = lu;
         dprints("serial_send_and_receive: character read = %c -> %d\n", retour[l],(int) retour[l]);
         l++;
      }
      else if (n == 0)               /*the answer is not read. */
      {
         lu = '\0';
         m++;
      }
      else
         EXIT_ON_ERROR("serial_send_and_receive read error (n = %i)\n", n);

   }
   while (l < lenght_out && m < 5);
   n = l;

   pthread_mutex_unlock(&((s_port)->mutex_serial));
   /*       retour[n] = '\0';*/
   return n;
}

int serial_send_and_receive_ax12(char *port_name, unsigned char *commande, unsigned char *retour,
      unsigned char lenght_in,unsigned char * length_out)
{
   int n, /*i, */ l, m,o;
   char lu;
   Serial *s_port;

   /*  n = strlen(commande);
      commande[n] = 0x0D;
      commande[n + 1] = 0;*/


   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      cprints("serial_send_and_receive: Port %s non trouve\n", port_name);
      return -1;
   }


   /*    n = strlen(commande);*/
   /*  for(i=0; i<TIMEOUT; i++){*/

   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIOFLUSH);
   /* TESTER LE RETOUR !! */
   o = write(s_port->desc, commande, lenght_in);
   if (o == -1)
      EXIT_ON_ERROR("write serial");

   /* fprintf(stdout, "o = %i\n", o);*/
   tcdrain(s_port->desc);

   *length_out=5;
   /*le 4eme caractere est la taille du message*/
   /* lecture de la reponse */
   l = m = 0;
   do
   {
      n = read(s_port->desc, &lu, 1);
      dprints("serial_send_and_receive: read -> n = %d\n", n);

      if (n > 0)
      {
         retour[l] = lu;
         dprints("serial_send_and_receive: character read = %c -> %d\n", retour[l],(int) retour[l]);
         l++;
      }
      else if (n == 0)               /*the answer is not read. */
      {
         lu = '\0';
         m++;
      }
      else
         EXIT_ON_ERROR("serial_send_and_receive read error (n = %i)\n", n);
      /*au 4eme octet, on a la taille qui suit*/
      if(l==4)
         *length_out=4+lu;
   }
   while (l < *length_out && m < 1000);
   n = l;

   pthread_mutex_unlock(&((s_port)->mutex_serial));
   /*       retour[n] = '\0';*/
   return n;

}


int serial_send_and_receive_block(char *port_name, char *commande, char *retour,
      int lenght, time_t timeout_usec)
{

   fd_set rfds;
   struct timeval tv;
   int retval;
   int o, size_r;

   Serial *s_port;


   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      cprints("serial_send_and_receive: Port %s non trouve\n", port_name);
      return -1;
   }

   tv.tv_sec = 0;
   tv.tv_usec = timeout_usec ;

   FD_ZERO(&rfds);
   FD_SET(s_port->desc, &rfds);


   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIOFLUSH);

   o = write(s_port->desc, commande, lenght);
   if (o < 0)
   {
      perror("write serial");
      exit(42);
   }

   tcdrain(s_port->desc);

   retval = select( s_port->desc+1, &rfds, NULL, NULL, &tv);

   if (retval == -1)
   {
      size_r = 0;
      perror("select()");
   }
   else if (retval)
   {
      size_r = read(s_port->desc, retour, 1000);

      if(size_r > 0 && size_r < 1000)
      {
         retour[size_r-1] = '\0';
      }
      else
      {
         perror("read serial");
         exit(42);
      }
   }
   else{
      size_r = 0;
   }
   pthread_mutex_unlock(&((s_port)->mutex_serial));
   return size_r;
} 

