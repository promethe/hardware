/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
    \file  serial_receive.c 
    \brief read serial port

    Author: C. Hasson
    Created: 23/03/2005
    Modified:
    -

    Theoritical description:
    - \f$  LaTeX equation: none \f$  

    Description: 
 * read serial port and stock what is read from the carriage return.
 * 1 is return if the operation has succed, and -1 if there is a problem

    Macro:
    -none 

    Local variables:
    -none

    Global variables:
    -none

    Internal Tools:
    -none

    External Tools: 
    -none

    Links:
    - type: algo / biological / neural
    - description: none/ XXX
    - input expected group: none/xxx
    - where are the data?: none/xxx

    Comments:

    Known bugs: 
    peut etre si on ajoute un \n a la fin de la commande: on se retrouve avec 2 \n et il peut y avoir un probleme

    Todo:see author for testing and commenting the function

    http://www.doxygen.org
 ************************************************************/

#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

#include <Serial.h>
#include "tools/include/macro_serial.h"

/*#define DEBUG*/

int serial_receive(char *port_name, char *retour)
{
   int n, l, m;
   char lu;
   Serial *s_port;  

   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      printf("serial_receive: Port %s non trouve\n", port_name);
      return -1;
   }

   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIFLUSH);
   /* lecture de la reponse */
   l = m = 0;
   /* do
   {
      n = read(s_port->desc, &lu, 1);
    */    /*printf("nb_read = %i, serial_drop = %c (%i)\n", n, lu, lu);*/
   /*  }
   while ((lu!=10) && (lu!=13) && (lu!='\n') && (lu!='`'));
    */
   do
   {
      n = read(s_port->desc, &lu, 1);

#ifdef DEBUG
      printf("serial_receive: read -> n = %i\n", n);
#endif

      if (n > 0)
      {
         retour[l] = lu;
#ifdef DEBUG 
         printf("serial_receive: character read = %c -> %d\n", retour[l],(int) retour[l]);
#endif
         l++;
      }
      else if (n == 0)               /*the answer is not read. */
      {
         lu = '\0';
         m++;
      }
      else
      {
         EXIT_ON_ERROR("serial_receive read error (n = %i)", n);
      }

   }
   while ((lu != 10) && (lu != 13) && (lu != '\n') && (lu != '`') && (l < 100) && (m < 10000));

   n = l;

   pthread_mutex_unlock(&((s_port)->mutex_serial));
   if ((retour[n - 1] == '\n') || (retour[n - 1] == 10) || (retour[n - 1] == 13) || (retour[n - 1] == '`'))
   {
      retour[n] = '\0';
      return 1;
   }

   EXIT_ON_ERROR("serial_receive: ERREUR lors de la lecture sur la RS232");
   return(-1);
}


int serial_receive_block(char *port_name,  char *retour, time_t timeout_usec)
{
   fd_set rfds;
   struct timeval tv;
   int retval;
   int size_r;

   Serial *s_port;

   /* recherche du port */
   s_port = serial_get_serial_by_name(port_name);
   if (s_port == NULL)
   {
      printf("serial_send_and_receive: Port %s non trouve\n", port_name);
      return -1;
   }

   tv.tv_sec = 0;
   tv.tv_usec = timeout_usec ;

   FD_ZERO(&rfds);
   FD_SET(s_port->desc, &rfds);

   /* ecriture de la commande */
   pthread_mutex_lock(&((s_port)->mutex_serial));
   tcflush(s_port->desc, TCIFLUSH);

   retval = select( s_port->desc+1, &rfds, NULL, NULL, &tv);

   if (retval == -1)
   {
      size_r = 0;
      perror("select()");
   }
   else if (retval)
   {
      size_r = read(s_port->desc, retour, 1000);

      if(size_r > 0 && size_r < 1000)
      {
         retour[size_r-1] = '\0';
      }
      else
      {
         perror("read serial");
         exit(42);
      }
   }
   else{
      size_r = 0;
   }
   pthread_mutex_unlock(&((s_port)->mutex_serial));
   return size_r;
}
