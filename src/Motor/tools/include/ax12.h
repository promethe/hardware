/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#ifndef _AX12_H
#define _AX12_H


void motor_init_USE_AX12_USB2DYN(Motor * motor);

int motor_ax12_transmit_and_receive (Motor * motor,unsigned char instr,unsigned char nb_param,unsigned char * param,unsigned char * retour);

int motor_ax12_ping(Motor* motor);
void motor_ax12_get_allinfo(Motor* motor);
void motor_ax12_led_blinking(Motor * motor,int delay,int nb_blink);
void motor_ax12_set_speed(Motor * motor, int speed);
void motor_ax12_command_position_wait_target(Motor *motor,int position);
void motor_ax12_command_position(Motor*motor,int position);
void motor_ax12_torque_enable(Motor * motor);
int motor_ax12_get_position(Motor *motor);
int motor_ax12_is_moving(Motor *motor);

#define AX12_INST_ping            0x01
#define AX12_INST_read_data        0x02
#define AX12_INST_write_data      0x03
#define AX12_INST_reg_write       0x04
#define AX12_INST_action          0x05
#define AX12_INST_reset           0x06
#define AX12_INST_digital_reset   0x07
#define AX12_INST_system_read     0x0c
#define AX12_INST_system_write    0x0d
#define AX12_INST_sync_write      0x83
#define AX12_INST_sync_reg_write  0x84


#define AX12_DATA_model_number_low        0x00
#define AX12_DATA_model_number_high       0x01
#define AX12_DATA_firmware	          0x02
#define AX12_DATA_id_motor                0x03
#define AX12_DATA_baudrate                0x04
#define AX12_DATA_return_delay_time       0x05
#define AX12_DATA_angle_limit_CW_low      0x06
#define AX12_DATA_angle_limit_CW_high     0x07
#define AX12_DATA_angle_limit_CCW_low     0x08
#define AX12_DATA_angle_limit_CCW_high    0x09
#define AX12_DATA_highest_temp		  0x0b
#define AX12_DATA_lowest_voltage	  0x0c
#define AX12_DATA_highest_voltage         0x0d
#define AX12_DATA_maxtorque_EEPROM_low    0x0e
#define AX12_DATA_maxtorque_EEPROM_high   0x0f
#define AX12_DATA_status_return           0x10
#define AX12_DATA_alarm_led               0x11
#define AX12_DATA_alarm_shutdown          0x12
#define AX12_DATA_calibration_low_low     0x14
#define AX12_DATA_calibration_low_high    0x15
#define AX12_DATA_calibration_high_low    0x16
#define AX12_DATA_calibration_high_high   0x17
#define AX12_DATA_torque_status           0x18
#define AX12_DATA_led                     0x19 
#define AX12_DATA_compliance_margin_CW    0x1a
#define AX12_DATA_complinace_margin_CCW   0x1b
#define AX12_DATA_compliance_slope_CW     0x1c
#define AX12_DATA_compliance_slope_CCW    0x1d
#define AX12_DATA_goal_position_low       0x1e
#define AX12_DATA_goal_position_high      0x1f
#define AX12_DATA_moving_speed_low        0x20
#define AX12_DATA_moving_speed_high       0x21
#define AX12_DATA_maxtorque_RAM_low       0x22
#define AX12_DATA_maxtorque_RAM_high       0x23
#define AX12_DATA_current_pos_low         0x24
#define AX12_DATA_current_pos_high        0x25
#define AX12_DATA_current_speed_low       0x26
#define AX12_DATA_current_speed_high      0x27
#define AX12_DATA_current_load_low        0x28
#define AX12_DATA_current_load_high       0x29
#define AX12_DATA_current_voltage         0x2a
#define AX12_DATA_current_temp            0x2b
#define AX12_DATA_registered_instr        0x2c
#define AX12_DATA_is_moving               0x2e
#define AX12_DATA_lock                    0x2f
#define AX12_DATA_punch_low               0x30
#define AX12_DATA_punch_high              0x31


#endif 
