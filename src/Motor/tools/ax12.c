/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  ax12.c 
\brief 

Author: C. Giovannangeli
Created: 03/09/2009
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-search in the dev file the devices that are Motors
from those it build their structures

Macro:
-none 

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <tools.h>
#include <Motor.h>
#include <Serial.h>
#include "include/ax12.h"
#include <net_message_debug_dist.h>

void motor_init_USE_AX12_USB2DYN(Motor * motor)
{
    /*fonction d'init: ping, set torque, flassh led.*/
    /*Verifie le vitesse de connexion*/


     Serial * serial=serial_get_serial_by_name(motor->port_name);
     if(strcmp(serial->debit,"1000000")!=0||strcmp(serial->bit,"8")!=0||strcmp(serial->parite,"N")!=0||strcmp(serial->stop,"1")!=0)
     {
      printf("bad serial port communication mode, set %s to 1000000-8N1\n",motor->port_name);
      serial_setparam(motor->port_name,(char*)"1000000",(char*)"8",(char*)"N",(char*)"1");
     }
    
    /*   ping   */

    printf("\nping %s ......\n",motor->name);

    motor_ax12_ping(motor);
    motor_ax12_get_allinfo(motor);

/*     printf("\tled blinking\n");
    motor_ax12_led_blinking(motor,250,10);
*/     
    printf("\tenable torque\n");
    motor_ax12_torque_enable(motor);

    printf("\t set maximal speed\n");
    motor_ax12_set_speed(motor,1023);

    printf("\t set middle position\n");
    motor_command_position_wait_target(motor,.5,0.);
    
    printf("\t turn right left middle\n");
    motor_command_position_wait_target(motor,0.01,0.);
    motor_command_position_wait_target(motor,0.99,0.);
    motor_command_position_wait_target(motor,0.5,0.);
  
}

int motor_ax12_transmit_and_receive
	      (Motor * motor,
	       unsigned char instr, 
	       unsigned char nb_param,
	       unsigned char * param,
	       unsigned char * retour) {
  
unsigned char i, checksum;
unsigned char commande[255];
unsigned char length_out;
int resultat=0;
  /* Build the packet header in the transmission buffer */
  commande[0] = 0xff;
  commande[1] = 0xff;
  commande[2] = motor->adress_bus;
  commande[3] = nb_param + 2;
  commande[4] = instr;
  
  /* Fill the transmission buffer with the packet data */
  for(i = 0; i < nb_param; i++)
    commande[i + 5] = param[i];
  
  /* Compute the checksum and append it to the packet */
  checksum = 0;
  for(i = 0; i < 3 + nb_param ; i++)
    checksum += commande[i+2];
  commande[nb_param+5] = ~checksum;

/*   printf("commande = %d-%d-%d-%d-%d-%d\n",commande[0],commande[1],commande[2],commande[3],commande[4],commande[5]);*/
  
  serial_send_and_receive_ax12(motor->port_name,commande,retour,nb_param+6,&length_out);
  
  if(retour[0]!= 0xff || retour[1]!=0xff)
  {
    printf("reception from ax12 error: retour dring dring failed\n");
    resultat=-1;
  }
/*   printf("id is %d\n",retour[2]);
   printf("to read: %d \n",retour[3]);
  printf("length_out=%d\n",length_out);*/
if(retour[4]!=0)
  printf("error = %d \n",retour[4]);


  if(length_out!=(6+retour[3]-2))
  {
    printf("reception from ax12 error: computation of length returned message error\n");
    resultat=-1;
  }
 
/*  for(i=0;i<retour[3]-2;i++)
  printf("param %d is %d\n",i,retour[5+i]);

  printf("checksum %d\n",retour[5+retour[3]-2]);
*/
  checksum = 0;
  for(i = 0; i < 3 + retour[3]-2 ; i++)
    checksum += retour[i+2];
  checksum=~checksum;
   
  /*printf("checksum=%d\n",checksum);*/

  if(checksum!=retour[5+retour[3]-2])
    {
      resultat=-1;
      printf("reception from ax12 error: checksum_incorrect\n");
    }

  if (resultat == 0)
    resultat=length_out;
/*   printf("retour = %d-%d-%d-%d-%d-%d\n",retour[0],retour[1],retour[2],retour[3],retour[4],retour[5]);*/
  
  return resultat;
}

int motor_ax12_ping(Motor* motor)
{
    
    int length_out;
    unsigned char param[255],retour[255];
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_ping,0,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("..............ping ok\n");
    }
    else
    {
      printf("..............ping failed on servo %s\n",motor->name);
    }
    return length_out;
}


/*delay in ms*/
void motor_ax12_led_blinking(Motor * motor,int delay,int nb_blink)
{
    int length_out,i;
    unsigned char param[255],retour[255];
    struct timespec duree_nanosleep, res;
    
    if(delay>1000)
    {
      printf("delay blink tohugh, delay is set to 1000 ms");
      delay=1000;
    }
    duree_nanosleep.tv_sec = 0;
    duree_nanosleep.tv_nsec = delay*1000000;

    for (i=0;i<nb_blink;i++)
    {
      param[0]=AX12_DATA_led;
      if(i%2==0)
	param[1]=0x00;
      else
	param[1]=0x01;
      length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,2,param,retour);
      if(length_out==-1 || retour[2]!=motor->adress_bus)
      {
	printf("\t error write led %s\n",motor->name);
      }   
      nanosleep(&duree_nanosleep, &res);
    }

    param[0]=AX12_DATA_led;
    param[1]=0x00;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,2,param,retour);
    if(length_out==-1 || retour[2]!=motor->adress_bus)
    {
	printf("\t error write led %s\n",motor->name);
    } 
}

void motor_ax12_torque_enable(Motor * motor)
{
    int length_out;
    unsigned char param[255],retour[255];
    
    param[0]=AX12_DATA_torque_status;
    param[1]=0x01;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,2,param,retour);
    if(length_out==-1 || retour[2]!=motor->adress_bus)
    {
      printf("\t error write torque %s\n",motor->name);
    }   
}

void motor_ax12_command_position(Motor*motor,int position)
{
    int length_out;
    unsigned char param[255],retour[255];
  
    param[0]=AX12_DATA_goal_position_low;
    param[1]= position & 0xff;
    param[2]= (position& 0xff00)>>8;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,3,param,retour);
    
    if(length_out==-1 || retour[2]!=motor->adress_bus)
    {
      printf("\t error write led %s\n",motor->name);
    }   
}

void motor_ax12_command_position_wait_target(Motor *motor,int position)
{
    int length_out;
    unsigned char param[255],retour[255];
  
    param[0]=AX12_DATA_goal_position_low;
    param[1]= position & 0xff;
    param[2]= (position& 0xff00)>>8;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,3,param,retour);
    if(length_out==-1 || retour[2]!=motor->adress_bus)
    {
      printf("\t error write led %s\n",motor->name);
    }   

    param[0]=AX12_DATA_is_moving;
    param[1]=0x01;
    do
    {
       length_out = motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    }
    while (retour[5] != 0);
}

void motor_ax12_set_speed(Motor * motor, int speed)
{
  int length_out;
    unsigned char param[255],retour[255];

  param[0]=AX12_DATA_moving_speed_low;
  param[1]= speed & 0xff;
  param[2]= (speed& 0xff00)>>8;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_write_data,3,param,retour);
    if(length_out==-1 || retour[2]!=motor->adress_bus)
    {
      printf("\t error write speed %s\n",motor->name);
    }   
}


void motor_ax12_get_allinfo(Motor* motor)
{
    
    int length_out;
    unsigned char param[255],retour[255];
    /*read datas*/

    printf("\nINFORMATION ON the MOTOR %s\n",motor->name);
    printf("___________________________\n");
/*Model number low and high*/
    param[0]=AX12_DATA_model_number_low;
    param[1]=2;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t model number=%d\n",(int)(retour[5])+(int)(retour[6])*256);
    }
    else
    {
      printf("\t unreadable model number on motor %s\n",motor->name);
    }
/*Firmware*/
    param[0]=AX12_DATA_firmware;
    param[1]=1;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t firmware=%d\n",retour[5]);
    }
    else
    {
      printf("\t unreadable firmware on motor %s\n",motor->name);
    }
    printf("___________________________\n");

/*id_motor*/
    param[0]=AX12_DATA_id_motor;
    param[1]=1;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t id_motor=%d\n",retour[5]);
    }
    else
    {
      printf("\t unreadable id_motor on motor %s\n",motor->name);
    }
/*baudrate*/
    param[0]=AX12_DATA_baudrate;
    param[1]=1;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t baudrate=%d\n",retour[5]);
    }
    else
    {
      printf("\t unreadable baudrate on motor %s\n",motor->name);
    }

/*return delay time*/
    param[0]=AX12_DATA_return_delay_time;
    param[1]=1;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t return delay time =%d us\n",retour[5]*2);
    }
    else
    {
      printf("\t unreadable baudrate on motor %s\n",motor->name);
    }
    printf("___________________________\n");
/*operation_angle_limit*/
    param[0]=AX12_DATA_angle_limit_CW_low;
    param[1]=4;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t CW =%d\n",(int)(retour[5])+(int)(retour[6])*256);
      printf("\t CCW =%d\n",(int)(retour[7])+(int)(retour[8])*256);

    }
    else
    {
      printf("\t unreadable angle limit on motor %s\n",motor->name);
    }
/*physical limit*/

    param[0]=AX12_DATA_highest_temp;
    param[1]=5;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t highest temperature =%d deg C\n",retour[5]);
      printf("\t lowest voltage =%f V\n",(float)(retour[6])/10.);
      printf("\t highest voltage =%f V\n",(float)(retour[7])/10.);
      printf("\t maximum torque EEPROM=%d\n",(int)(retour[8])+(int)(retour[9])*256);
    }
    else
    {
      printf("\t unreadable physical limit EEPROM on motor %s\n",motor->name);
    }

    param[0]=AX12_DATA_maxtorque_RAM_low;
    param[1]=2;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t maximum torque RAM= %d\n",(int)(retour[5])+(int)(retour[6])*256);
    }
    else
    {
      printf("\t unreadable torque limit RAM on motor %s\n",motor->name);
    }


    param[0]=AX12_DATA_current_load_low;
    param[1]=0x04;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t current load=%d\n",(int)(retour[5])+(int)(retour[6])*256);
      printf("\t current voltage=%f V\n",(float)(retour[7])/10.);
      printf("\t current temperature=%d deg C\n",(int)(retour[8]));
    }
    else
    {
      printf("\t unreadable current physical value on motor %s\n",motor->name);
    }
/*Calibration*/
    param[0]=AX12_DATA_calibration_low_low;
    param[1]=0x04;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t calibration low limit = %d\n",(int)(retour[5])+(int)(retour[6])*256);
      printf("\t calibration high limit = %d\n",(int)(retour[7])+(int)(retour[8])*256);
    }
    else
    {
      printf("\t unreadable calibration limit on motor %s\n",motor->name);
    }
/*compliance*/
    param[0]=AX12_DATA_compliance_margin_CW;
    param[1]=0x04;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t compliance margin CW = %d\n",(int)(retour[5]));
      printf("\t compliance margin CCW = %d\n",(int)(retour[6]));
      printf("\t compliance slope CW = %d\n",(int)(retour[7]));
      printf("\t compliance slope CCW = %d\n",(int)(retour[8]));

    }
    else
    {
      printf("\t unreadable compliance parameters on motor %s\n",motor->name);
    }
    printf("___________________________\n");
/*status retrun*/
    param[0]=AX12_DATA_status_return;
    param[1]=0x01;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t status return (0: no response - 1: r-only - 2: all instr. ) = %d\n",retour[5]);
    }
    else
    {
      printf("\t unreadable return status on motor %s\n",motor->name);
    }
    printf("___________________________\n");
/*Led and alarm*/
    param[0]=AX12_DATA_alarm_led;
    param[1]=0x02;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t led alarm = %d\n",retour[5]);
      printf("\t led shutdown = %d\n",retour[6]);
    }
    else
    {
      printf("\t unreadable led and alarm on motor %s\n",motor->name);
    }

    param[0]=AX12_DATA_led;
    param[1]=0x01;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t led alarm = %d\n",retour[5]);
    }
    else
    {
      printf("\t unreadable led bits on motor %s\n",motor->name);
    }

/*torque_status*/
    param[0]=AX12_DATA_torque_status;
    param[1]=0x01;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t torque status = %d\n",(int)(retour[5]));
    }
    else
    {
      printf("\t unreadable led and alarm on motor %s\n",motor->name);
    }

/*goal position and speed*/
    param[0]=AX12_DATA_goal_position_low;
    param[1]=0x08;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t goal position = %d\n",(int)(retour[5])+(int)(retour[6])*256);
      printf("\t goal speed = %d\n",(int)(retour[7])+(int)(retour[8])*256);
      printf("\t present position = %d\n",(int)(retour[9])+(int)(retour[10])*256);
      printf("\t present speed= %d\n",(int)(retour[11])+(int)(retour[12])*256);

    }
    else
    {
      printf("\t unreadable goal and speed on motor %s\n",motor->name);
    }

/*Registered, moving lock and puch*/
    param[0]=AX12_DATA_registered_instr;
    param[1]=0x05;
    length_out=motor_ax12_transmit_and_receive(motor,AX12_INST_read_data,2,param,retour);
    if(length_out!=-1 && retour[2]==motor->adress_bus)
    {
      printf("\t registered instr = %d\n",(int)(retour[5]));
      printf("\t is moving = %d\n",(int)(retour[6]));
      printf("\t lock status = %d\n",(int)(retour[7]));
      printf("\t punch = %d\n",(int)(retour[8])+(int)(retour[9])*256);

    }
    else
    {
      printf("\t unreadable goal and speed on motor %s\n",motor->name);
    }   
    printf("\n");
}

int motor_ax12_get_position(Motor *motor)
{
   unsigned char param[2],retour[8];
   int position = -1;
   int length_out;

   param[0] = AX12_DATA_current_pos_low;
   param[1] = 0x02;
   length_out = motor_ax12_transmit_and_receive(motor, AX12_INST_read_data, 2, param, retour);
   
   if (length_out != -1 && retour[2] == motor->adress_bus)
   {
      position = (int) retour[5] + 256 * (int) retour[6];
      dprints("current position of motor %s = %d\n",motor->name, position);
   }
   else
   {
      PRINT_WARNING("Unreadable position on motor %s",motor->name);
   }
   return position;
}

int motor_ax12_is_moving(Motor *motor)
{
   unsigned char param[2],retour[8];
   int length_out;

   param[0]=AX12_DATA_is_moving;
   param[1]=0x01;
   length_out = motor_ax12_transmit_and_receive(motor, AX12_INST_read_data, 2, param, retour);

   if (length_out != -1 && retour[2] == motor->adress_bus)
   {
      if (retour[5] == 0)
      {
	 dprints("motor %s is not moving\n",motor->name);
	 return 0;
      }
      else
      {
	 dprints("motor %s is moving\n",motor->name);
	 return 1;
      }
   }
   else
   {
      PRINT_WARNING("Unreadable is_moving on motor %s",motor->name);
      return 0;
   }
}
