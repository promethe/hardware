/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  motor_command_position.c 
\brief 

Author: Benoit Mariat
Created: 03/03/2005
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-send the command to motor, the command is a percentage of the
 mouvement capacity define by parameters start and stop

Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <unistd.h>
#include <termio.h> */

#include <Serial.h>
#include <Motor.h>
#include <ClientHW.h>
#include "tools/include/local_var.h"
#include <time.h>
#include <Aibo.h>
/*
	name: nom du motor
	pct: pourcentage de la position
	speed: controle de la vitesse (lorsque c'est possible)
		 valeurs de 1 (vitesse min) a Vmax (vitesse max), 0: pas de controle
*/
void motor_check_hardware(Motor * serv)
{
     int i;
     float pct;
     /*Le check des motor consiste � faire passer le motor par toutes les positions entre start et stop*/

     printf("Check motor %s ....",serv->name);
     printf("Servo %s balaye de gauche � droite.....",serv->name); 	

     for(i=0;i<serv->stop-serv->start;i=i+2)
     {
	pct=i/(float)(serv->stop-serv->start);
	if(serv->rotation != 1)
		pct=1.-pct;
	motor_command_position_wait_target(serv,pct,0);
     }

	if (serv->rotation == 1)
	{
		pct =
		(float) (serv->init -
				serv->start) /
		(float) (serv->stop - serv->start);
	}
	else
	{
		pct =
		(float) (serv->stop -
				serv->init) /
		(float) (serv->stop -
				serv->start);
	}
	/*mise a la position init */
	motor_command_position_wait_target(serv /*->name*/ , pct,
				0);
	printf("..................................ok\n");
}
