/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <Aibo.h>
#include <Motor.h>
#include <net_message_debug_dist.h>
#include "tools/include/ax12.h"
#include "tools/include/local_var.h"

void	motor_get_proprio(Motor *serv)
{
  if(serv == NULL)
    {
      printf("Nom du motor inconnu\n");
      exit(1);
    }
  /*
  ** BLOCK MOTEUR TYPE AIBO
  */
  if(serv->type == 4)
    {
      char	commande[255];
      /*struct timespec ts;*/

      serv->updated = 0;
      snprintf(commande, 255, "val_motor_%s:%s,\n", serv->name, serv->name);
#ifdef DEBUG
      printf("%s", commande);
#endif
      aibo_send_message(commande, strlen(commande));

/*       while (serv->updated != 1) */
/* 	{ */
/* 	  ts.tv_sec = 0; */
/* 	  ts.tv_nsec = 5000000; */
/* 	  nanosleep(&ts, NULL); */
/* 	} */

      sem_wait(&(serv->sem));
      
    }
   else if (serv->type == USE_AX12_USB2DYN)
   {
      int current_pos, distance;

      distance = serv->stop - serv->start;

      if (distance >= 1024 || distance <= 0)
      {
	EXIT_ON_ERROR("probleme of range for ax12 > 1024 ou < 0\ncheck start and stop");	
      }

      current_pos = motor_ax12_get_position(serv);

      if (serv->rotation == 1)
      {
	 serv->proprio = (current_pos - serv->start) / (float) distance;
      }
      else
      {
	 serv->proprio = (serv->stop - current_pos) / (float) distance;
      }
   }
}

int motor_is_moving(Motor *serv)
{
   if (serv->type == USE_AX12_USB2DYN)
   {
      return motor_ax12_is_moving(serv);
   }
   else
   {
      PRINT_WARNING("motor_is_moving only defined for AX12 USB2DYN motors");
      return 0;
   }
}
