/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  motor_speedo_command.c 
\brief 

Author: LAGARDE Matthieu
Created: 18/07/2005
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 


Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Motor.h>
#include <Aibo.h>

/*
**	name: nom du motor
**	speed: controle de la vitesse (lorsque c'est possible) entre -1 et 1
*/
void motor_speedo_command(Motor *serv, float speed)
{
  int position, distance, vitesse=0;
  char commande[255];
  int pct = 0;

 if(serv == NULL)
   {
     printf("Nom du motor inconnu\n");
     exit(1);
   }
 
 /*
 ** BLOCK MOTEUR TYPE AIBO
 */
 if(serv->type == 4)
   {

     if (speed > 1)
       speed = 1;
     else if (speed < -1)
       speed = -1;

     vitesse = speed * serv->Vmax;
     pct = 1;
     
     if (speed < 0)
       {
	 vitesse = -vitesse;
	 pct = 0;
       }

     /* position "virtuelle" entre 0 et 1 */
     serv->proprio = pct;
     
     /* On calcule la valeur reelle selon le sens de rotation */
     distance = serv->stop - serv->start;
     position = serv->start + (int)(pct * distance);

     /* Proprioception qui devrait etre reelle normalement */
     /* 
     ** TODO : Pour obtenir la reelle, il faut attendre que le moteur
     ** ait fini son mouvement ... doit on bloquer l'appli pour attendre
     ** et avoir la reelle proprioception ??
     */
     serv->position = position;

     /* 
     ** On verifie que la vitesse est correcte 
     ** la aussi, evite de casser un moteur
     */
     if(vitesse > serv->Vmax)
       vitesse = serv->Vmax;

     memset(commande, 0, 255);

     snprintf(commande, 255, "val_motor_%s : %s = %i speed : %i,\n", serv->name, serv->name, ((int)serv->position), vitesse);


     printf("%s", commande);
#ifdef DEBUG
#endif

     aibo_send_message(commande, strlen(commande));

     /* attente le temps que la commande s'execute sur Aibo */
/*      usleep(200000); */

     return ;

   } 
   else
   {
	printf("motor_command_speed non devel pour le type %d\n",serv->type);
	exit(0);
   }
}

