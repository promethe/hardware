/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  motor_motor_command.c 
\brief 

Author: Benoit Mariat
Created: 03/03/2005
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-send the command to motor, the command is a percentage of the
 mouvement capacity define by parameters start and stop

Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*#include <unistd.h>
#include <termio.h> */

#include <Serial.h>
#include <Motor.h>
#include <ClientHW.h>
#include "tools/include/local_var.h"
#include "tools/include/ax12.h"
#include <Aibo.h>
/*
	name: nom du motor
	pct: pourcentage de la position
	speed: controle de la vitesse (lorsque c'est possible)
		 valeurs de 1 (vitesse min) a Vmax (vitesse max), 0: pas de controle
*/
void motor_command_position(Motor * serv, float pct, int speed)
{
    /*Motor *serv; */
    int position, distance, vitesse = 0;
    char buff[255];
    char commande[255];
    ClientHW *chw = NULL;
    unsigned char lowbyte_pos,highbyte_pos,id_servo_byte,addr;
    char retour[10];

    /* recuperation du motor */
    /*serv=motor_get_serv_by_name(name); */

    if (serv == NULL)
    {
        printf("Nom du motor inconnu\n");
        exit(1);
    }

 /*
 ** BLOCK MOTEUR TYPE AIBO
 */
 if(serv->type == USE_AIBO)
   {

     /*
     ** On verifie qu'on ne sort pas des valeurs possible
     ** Ca permet d'eviter de griller le moteur
     */
     if(pct < 0) pct = 0;
     if(pct > 1) pct = 1;
     
     /* position "virtuelle" entre 0 et 1 */
     serv->position=pct;
     
     /* On calcule la valeur reelle selon le sens de rotation */
     distance = serv->stop - serv->start;
     position = serv->start + (int)(pct * (float)distance);

     /* Proprioception qui devrait etre reelle normalement */
     /* 
     ** TODO : Pour obtenir la reelle, il faut attendre que le moteur
     ** ait fini son mouvement ... doit on bloquer l'appli pour attendre
     ** et avoir la reelle proprioception ??
     */
     serv->proprio = position;

     /* 
     ** On verifie que la vitesse est correcte 
     ** la aussi, evite de casser un moteur
     */
     if(speed < 0) vitesse = serv->Vdefault;
     else vitesse = (int) ((float)speed * serv->Vmult);
     if(vitesse > serv->Vmax) vitesse = serv->Vmax;
     if( (vitesse < 1) && (speed != 0)  && (serv->Vdefault != 0) ) vitesse = 1;

     memset(commande, 0, 255);

     snprintf(commande, 255, "val_motor_%s : %s = %i speed : %i,\n", serv->name, serv->name, ((int)serv->proprio), vitesse);

#ifdef DEBUG
     printf("%s", commande);
#endif

     aibo_send_message(commande, strlen(commande));

     return ;
   } 
 else if (serv->type == USE_CLIENTHW)
    {
        if (pct < 0.)
            pct = 0.;
        if (pct > 1.)
            pct = 1.;


        serv->position = pct;

        sprintf(commande, "motor_motor_command %s %f %d", serv->name, pct,
                speed);
        chw = clientHW_get_clientHW_by_name(serv->clientHW);
        clientHW_transmit(chw, commande);
        return;
    }
    else if (serv->type== USE_SERIAL_SSC2||serv->type== USE_SERIAL_SSC12||serv->type== USE_SERIAL_SSC32)
    {
        /* calcul de la position absolue */
        if (pct < 0.)
            pct = 0.;
        if (pct > 1.)
            pct = 1.;


        serv->position = pct;


        serv->proprio = pct;    /*la position resultant de la proprio ou directement de la commande envoyee */
	distance = serv->stop - serv->start;
        if (serv->rotation == 1)
        {
            position = serv->start + (int) (pct * (float) distance);
        }
        else
        {
            position = serv->stop - (int) (pct * (float)distance);
        }

#ifdef DEBUG
        printf("POSITION: %d\n", position);
        printf("DISTANCE: %d\n", distance);
        printf("PCT: %f\n", pct);
#endif

        /* ecriture de la position dans le buffer pour l'envoyer vers le port serie */

        if (serv->type==1)
        {
            buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
            buff[1] = (char) serv->adress_bus; /*!< numero du motor [0-254] */
            buff[2] = (char) position;  /*!< position du motor [0-254] */
			buff[3] = '\0';
        }
        else if (serv->type==2)
        {
            if (speed == -1)
                vitesse = serv->Vdefault;
            else
                vitesse = (int) ((float) speed * serv->Vmult);
            if (vitesse > serv->Vmax)
                vitesse = serv->Vmax;
            if ((vitesse < 1) && (speed != 0) && (serv->Vdefault != 0))
                vitesse = 1;
#ifdef DEBUG
            printf("VITESSE: %d\n", vitesse);
            printf("commande d'envoi: %X | %X | %X\n", 255,
                   vitesse * 16 + serv->adress_bus, position);
#endif
            buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
            buff[1] = (char) (vitesse * 16 + serv->adress_bus);    /*!< numero du motor [0-254] */
            buff[2] = (char) position;  /*!< position du motor [0-254] */
			buff[3] = '\0';
        }
	else if (serv->type== 6)
        {
			sprintf(buff,"#%dP%d\r",serv->adress_bus,500+8*position);
	}
	else
        {
            printf
                ("type de materiel (support des motors) non reconnu (motor_motor_command)\n");
            printf("TEST: %d \n", serv->type);
            exit(1);
        }


        /* envoi vers le port serie */
        /*if(serv->clientHW[0]==0)**M.M. : y'a un return avant s'il est different de 0 donc on arrive la que si y'a 0 donc pas de test..." */
	if(strcmp(serv->port_name,"NULL")!=0)
        	serial_send(serv->port_name, buff, strlen(buff));
#ifdef DEBUG
	else
		printf("motor simul\n");
#endif
        /*tcflush(serialport, TCIOFLUSH);  elimine les ecritures en queue et les donnees recues non lues *
           write(serialport, buff, 3);
           tcdrain(serialport);   attend que tout soit transmis */


        
    } 
    else if (serv->type== USE_AX12_2POB)
    {
      if(pct < 0) pct = 0;
      if(pct > 1) pct = 1;

      serv->position = pct;


      serv->proprio = pct;    /*la position resultant de la proprio ou directement de la commande envoyee */
      distance = serv->stop - serv->start;
      if(distance>=1024||distance <=0)
      {
	printf("probleme of range for ax12 > 1024 ou < 0\ncheck start and stop\n");	
	exit(-1);
      }
      if (serv->rotation == 1)
      {
	  position = serv->start + (int) (pct * (float) distance);
      }
      else
      {
	  position = serv->stop - (int) (pct * (float)distance);
      }
   
      lowbyte_pos=position & 0xff;
      highbyte_pos=(position& 0xff00)>>8;
      id_servo_byte=serv->adress_bus;
      addr='D';

      sprintf(commande,"%c%c%c%c\n",addr,id_servo_byte,highbyte_pos,lowbyte_pos);
      
printf("envoie AX12%s \n",commande);
      serial_send_and_receive_nchar(serv->port_name,commande,retour,4,5);
      printf("retour AX12:%c%c%c%c%c\n",retour[0],retour[1],retour[2],retour[3],retour[4]);

    }
    else if (serv->type== USE_AX12_USB2DYN)
    {
      if(pct < 0) pct = 0;
      if(pct > 1) pct = 1;

      serv->position = pct;
      /*serv->proprio = pct; */    /*la position resultant de la proprio ou directement de la commande envoyee */
 
      distance = serv->stop - serv->start;
      if(distance>=1024||distance <=0)
      {
	printf("probleme of range for ax12 > 1024 ou < 0\ncheck start and stop\n");	
	exit(-1);
      }
      if (serv->rotation == 1)
      {
	  position = serv->start + (int) (pct * (float) distance);
      }
      else
      {
	  position = serv->stop - (int) (pct * (float)distance);
      }
   
      motor_ax12_command_position(serv,position);
     
    }
    else
        {
            printf
                ("type de materiel (support des motors) non reconnu (motor_motor_command)\n");
            printf("TEST: %d \n", serv->type);
            exit(1);
        }
return;
}

void motor_command_position_wait_target(Motor * serv, float pct, int speed)
{
	
    /*Motor *serv; */
    int position, distance;
    char buff[255];
    char buff1[7]="q\r\n";
    char retour;
    /* recuperation du motor */
    /*serv=motor_get_serv_by_name(name); */
    (void)speed; /* A supprimer du format de la fonction*/

    if (serv == NULL)
    {
        printf("Nom du motor inconnu\n");
        exit(1);
    }

 /*
 ** BLOCK MOTEUR TYPE AIBO
 */
 if (serv->type==USE_SERIAL_SSC32)
  {
        /* calcul de la position absolue */
        if (pct < 0.)
            pct = 0.;
        if (pct > 1.)
            pct = 1.;


        serv->position = pct;
        serv->proprio = pct;    /*la position resultant de la proprio ou directement de la commande envoyee */
	distance = serv->stop - serv->start;
        if (serv->rotation == 1)
        {
            position = serv->start + (int) (pct * (float) distance);
        }
        else
        {
            position = serv->stop - (int) (pct * (float)distance);
        }

#ifdef DEBUG
        printf("POSITION: %d\n", position);
        printf("DISTANCE: %d\n", distance);
        printf("PCT: %f\n", pct);
#endif

	if(strcmp(serv->port_name,"NULL")!=0)
	{
 		sprintf(buff,"#%dP%d\r\n",serv->adress_bus,500+8*position);
        	serial_send(serv->port_name, buff, strlen(buff));
 		retour = serial_send_and_readchar(serv->port_name, buff1, strlen(buff1));  /*private */
 		while(retour=='+')
	 		retour = serial_send_and_readchar(serv->port_name, buff1, strlen(buff1));  /*private */
		printf("fin cmd %s\n",buff);
	}
	
#ifdef DEBUG
	else
		printf("motor simul\n");
#endif
        /*tcflush(serialport, TCIOFLUSH);  elimine les ecritures en queue et les donnees recues non lues *
           write(serialport, buff, 3);
           tcdrain(serialport);   attend que tout soit transmis */


        
   }
   else if(serv->type==USE_AX12_USB2DYN)
   {
      if(pct < 0) pct = 0;
      if(pct > 1) pct = 1;

      serv->position = pct; /*la position resultant de la proprio ou directement de la commande envoyee */
      /*serv->proprio = pct;    */
 
      distance = serv->stop - serv->start;
      if(distance>=1024||distance <=0)
      {
	printf("probleme of range for ax12 > 1024 ou < 0\ncheck start and stop\n");	
	exit(-1);
      }
      if (serv->rotation == 1)
      {
	  position = serv->start + (int) (pct * (float) distance);
      }
      else
      {
	  position = serv->stop - (int) (pct * (float)distance);
      }
#ifdef DEBUG
      printf("motor goto position %i\n", position);
#endif
   
      motor_ax12_command_position_wait_target(serv,position);
   }
   else 
   {
	printf("fonction motor_command_position_wait_target non definies pour type %d\n",serv->type);
   }

return;
}
