/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  create_motor.c 
\brief 

Author: Benoit Mariat
Created: 03/03/2005
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

#include <Aibo.h>
#include <Motor.h>

#include "tools/include/local_var.h"
#include "basic_tools.h"

void motor_create_motor(Motor ** serv, char *name, char *type)
{

    FILE *fh;
    char lecture[256];
    char tmp[129];
    int tmpi;
    float tmpf;

    *serv = (Motor *) malloc(sizeof(Motor));
    if (*serv == NULL)
    {
        printf("erreur malloc motor_create_motor\n");
        exit(0);
    }
    strcpy((*serv)->name, name);    /*nom du motor */

    tmp[128] = '\0';
    
    (*serv)->autocheck = 0;
    /* valeur par defaut au cas ou ils ne sont pas definis */
    (*serv)->Vmax = 15;         /*initialisation Vmax */
    (*serv)->vitesse = 1000;         /*initialisation Vmax */
    (*serv)->Vdefault = 0;      /*initialisation Vdefault */
    (*serv)->Vmult = 1.;        /*initialisation Vmult */
    (*serv)->type = 1;        /*initialisation Vmult */
    /*    (*serv)->clientHW[0]=(char)0; */
    (*serv)->adress_bus = 0;
    /* recuperation des informations contenu dans le fichier hardware */
    fh = fopen(type, "r");
    if (fh == NULL)
    {
        printf("le fichier %s n'a pas pu etre ouvert\n", type);
        exit(1);
    }

    while (!feof(fh))
    {
        if (fscanf(fh, "%s = ", lecture) != 1) PRINT_WARNING("Read first word.");  /* lecture du premier mot de la ligne */

        str_upper(lecture);

        if ((strcmp(lecture, "PORT") == 0))
        {

            if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
            {
                printf("Erreur de lecture pour le port du motor %s\n", name);
                exit(1);
            }

            /* delete the end char if it is an end line */
            if (tmp[strlen(tmp) - 1] == '\n')
                tmp[strlen(tmp) - 1] = '\0';

            strcpy((*serv)->port_name, tmp);
	    
        }
        else if (strcmp(lecture, "TYPE") == 0)
        {
            if (fscanf(fh, "%d\n", &((*serv)->type)) !=1) PRINT_WARNING("Read TYPE.");
        }
	else if (strcmp(lecture, "ADRESS_BUS") == 0)
        {
            if (fscanf(fh, "%d\n", &((*serv)->adress_bus))  !=1) PRINT_WARNING("Read ADRESS_BUS.");
        }
        else if ((strcmp(lecture, "START") == 0)) 
        {
            if (fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Read START.");
            (*serv)->start = tmpi;
        }
        else if ((strcmp(lecture, "STOP") == 0))
        {
            if(fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Read STOP.");
            (*serv)->stop = tmpi;
        }
        else if ((strcmp(lecture, "INIT") == 0))
        {
            if (fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Read INIT.");
            (*serv)->init = tmpi;
        }
        else if ((strcmp(lecture, "AUTOCHECK") == 0))
        {
            if (fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Error reading AUTOCHECK.");
            (*serv)->autocheck = tmpi;
        }
	else if ((strcmp(lecture, "ZERO") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Error reading ZERO.");;
            (*serv)->zero = tmpi;
        }
        else if ((strcmp(lecture, "RANGE") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi) !=1) PRINT_WARNING("Error reading RANGE.");;
            (*serv)->range = tmpi;
        }
        else if ((strcmp(lecture, "ROTATION") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi)!=1) PRINT_WARNING("Error reading ROTATION.");;
            (*serv)->rotation = tmpi;
        }
        else if ((strcmp(lecture, "VITESSE") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi)!=1) PRINT_WARNING("Error reading VITESSE.");;
            (*serv)->vitesse = tmpi;
        }
        else if ((strcmp(lecture, "VMAX") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi)!=1) PRINT_WARNING("Error reading VMAX.");;
            (*serv)->Vmax = tmpi;
        }
        else if ((strcmp(lecture, "VDEFAULT") == 0))
        {
             if (fscanf(fh, "%d\n", &tmpi)!=1) PRINT_WARNING("Error reading VDEFAULT.");;
            (*serv)->Vdefault = tmpi;
        }
        else if ((strcmp(lecture, "VMULT") == 0))
        {
             if (fscanf(fh, "%f\n", &tmpf)!=1) PRINT_WARNING("Error reading VMULT.");
            (*serv)->Vmult = tmpf;
        }
        else if (strcmp(lecture, "CLIENTHW") == 0)
        {
             if (fscanf(fh, "%s\n", ((*serv)->clientHW))!=1) PRINT_WARNING("Error reading CLIENTHW.");
            (*serv)->type = 3;
        }
	else if (strcmp(lecture,"JOINT_AIBO")==0)
	{
	  if (fscanf(fh,"%d\n",&tmpi) !=1)  PRINT_WARNING("Error reading CLIENTHW.");

	if (tmpi == 1)
	  {
	    char	commande[255];

	    (*serv)->type = 4; /* TYPE AIBO_JOINT */

	    (*serv)->updated = 0;
	    /* mode de fonctionnement des articulation imposee */
	    /* ->cancel : permet de stopper la commande en cours et de lq remplacer par la nouvelle */
	    snprintf(commande, 255, "%s->blend=cancel;\n", name);
	    aibo_send_message(commande, strlen(commande));
	  }
	}
        else
            lire_ligne(fh);

    }
    fclose(fh);

    printf("\t\t port=%s \n\t\t type=%d \n\t\t range=%d\n\t\t start=%d\n\t\t stop=%d\n\t\t init=%d\n",
        (*serv)->port_name, (*serv)->type, (*serv)->range, (*serv)->start, (*serv)->stop,
         (*serv)->init);

    return;
}
