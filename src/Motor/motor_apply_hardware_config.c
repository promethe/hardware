/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  motor_apply_hardware_config.c
 \brief

 Author: Benoit Mariat
 Created: 03/03/2005
 Modified:
 - author: -
 - description: -
 - date: -

 Theoritical description:
 - \f$  LaTeX equation: none \f$  

 Description:
 -search in the dev file the devices that are Motors
 from those it build their structures

 Macro:
 -none

 Local variables:
 - none

 Global variables:
 -none

 Internal Tools:
 -none

 External Tools:
 -none

 Links:
 - type: algo / biological / neural
 - description: none/ XXX
 - input expected group: none/xxx
 - where are the data?: none/xxx

 Comments:

 Known bugs: none (yet!)

 Todo:see author for testing and commenting the function

 http://www.doxygen.org
 ************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <tools.h>
#include <Motor.h>
#include <Serial.h>
#include "tools/include/ax12.h"

void motor_apply_hardware_config(char *argv)
{
  FILE *fl;
  Motor **motor_table = NULL;

  int nb_motor;
  int i;
  char lecture[100];
  char motor_name[256][256];
  char motor_type[256][256];

  nb_motor = 0;

  /*! trying to open appli_name.dev */
  fl = fopen(argv, "r");
  if (fl == NULL)
  {
    PRINT_WARNING("Promethe initialisation: unable to open configuration file %s \n Using default parameters", argv);
  }
  else
  {
    /*! loading parameters */

    while (!feof(fl))
    {
      if (fscanf(fl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%')
      {
        lire_ligne(fl);
      }
      else
      {
        if (strcmp(lecture, "Motor") == 0)
        {
          if (fscanf(fl, " %s is %s", motor_name[nb_motor], motor_type[nb_motor]) != 2)
          {
            EXIT_ON_ERROR("parse error in dev file\n");
          }
          nb_motor++;
        }
        /*printf("%s %s %s\n",camera_name[nb_camera-1],camera_type[nb_camera-1]
         ,camera_port[nb_camera-1]); */

      }

    }
    fclose(fl);
  }

  if (nb_motor > 0)
  {
    kprints("%d motor(s) are defined:\n", nb_motor);
    motor_table = (Motor **) calloc(nb_motor, sizeof(Motor *));
    for (i = 0; i < nb_motor; i++)
    {
      kprints("\t Motor %s which is a %s mapped\n", motor_name[i], motor_type[i]);
      motor_table[i] = NULL;
      motor_create_motor(&motor_table[i], motor_name[i], motor_type[i]);
    }

    motor_create_motor_table(motor_table, nb_motor);
    /* checking des motor */

    /* 	for (i = 0; i < nb_motor; i++)
     {
     if (motor_table[i]->type!=4&&motor_table[i]->type!=5)
     {
     if (motor_table[i]->rotation == 1)
     {
     pct =
     (float) (motor_table[i]->init -
     motor_table[i]->start) /
     (float) (motor_table[i]->stop - motor_table[i]->start);
     }
     else
     {
     pct =
     (float) (motor_table[i]->stop -
     motor_table[i]->init) /
     (float) (motor_table[i]->stop -
     motor_table[i]->start);
     }
     motor_command_position_wait_target(motor_table[i] , pct,
     0);
     }
     }	*/

    for (i = 0; i < nb_motor; i++)
      motor_init(motor_table[i]);

    for (i = 0; i < nb_motor; i++)
      if (motor_table[i]->autocheck == 1) motor_check_hardware(motor_table[i]);

  }

  return;
}

void motor_init(Motor * motor)
{
  if (motor->type == USE_AX12_USB2DYN)
  {
    motor_init_USE_AX12_USB2DYN(motor);
  }
}
