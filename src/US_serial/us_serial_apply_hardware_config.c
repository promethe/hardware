/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tools.h>
#include <US_Serial.h>

#include <tools/include/local_var.h>

void us_serial_apply_hardware_config(char *argv)
{
  FILE * dfl;

  char lecture[100];
  char us_name[256][256];
  char us_type[256][256];
  int i;
  int nb_us = 0;

  dfl = fopen(argv, "r");
  if (dfl == NULL)
  {
    PRINT_WARNING("Unable to open configuration file %s \n\tUsing default parameters", argv);
  }
  else
  {
    while (!feof(dfl))
    {

      if (fscanf(dfl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%') lire_ligne(dfl);
      else
      {
        if (strcmp(lecture, "US_Serial") == 0)
        {
          if (fscanf(dfl, " %s is %s", us_name[nb_us], us_type[nb_us]) != 2)
          {
            EXIT_ON_ERROR("Parse error in dev file");
          }
          nb_us++;
        }
      }
    }
    fclose(dfl);
  }

  if (nb_us > 0)
  {
    kprints("us_serial_apply_hardware_config: %d ultrasonic(s) serial are defined:\n", nb_us);
    for (i = 0; i < nb_us; i++)
    {
      kprints("\t US serial %s is configurated by %s \n", us_name[i], us_type[i]);
    }

    us_serial_table = (US_Serial **) MANY_ALLOCATIONS(nb_us, US_Serial*);

    for (i = 0; i < nb_us; i++)
    {
      us_serial_table[i] = NULL;
      us_serial_create_us_serial(&us_serial_table[i], us_name[i], us_type[i]);
      kprints("%s : US_Serial %s is created \n", __FUNCTION__, us_name[i]);
    }

    us_serial_create_us_serial_table(us_serial_table, nb_us);

    /*checking des us_serial*/
    for (i = 0; i < nb_us; i++)
    {
      us_serial_init(us_serial_table[i]);
      if (us_serial_table[i]->autocheck == 1) us_serial_check_hardware(us_serial_table[i]);
    }
  }
}
