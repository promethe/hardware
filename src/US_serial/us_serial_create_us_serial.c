/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <US_Serial.h>
#include <tools.h>
#include "tools/include/local_var.h"

void us_serial_create_us_serial( US_Serial ** us_serial, char *name, char *file_hw)
{

   FILE *fh;
   char lecture[256];
   char tmp[129];
   int ret;
   int cpt;
   char lettre;

   *us_serial=(US_Serial *)calloc(1,sizeof(US_Serial));
   if (  *us_serial  == NULL)
   {
      printf("erreur malloc %s\n",__FUNCTION__);
      exit(0);
   }

   (*us_serial)->name = (char *) malloc(strlen(name) + 1);
   if ((*us_serial)->name == NULL)
   {
      printf("erreur malloc %s\n",__FUNCTION__);
      exit(0);
   }

   strcpy((*us_serial)->name, name); /*Compass_object name */
   (*us_serial)->port = NULL;
   (*us_serial)->autocheck=  0;
   (*us_serial)->enabled=  0;


   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }

   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */
      /*     printf("lit: %s \n",lecture); */
      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {

         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            printf("Erreur de lecture pour le port de la compass %s\n",
                  name);
            exit(1);
         }

         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         /* verify if it is the first declaration for port */
         if ((*us_serial)->port != NULL)
         {
            dprints("Several declarations of port are declared in %s", file_hw);
            free((*us_serial)->port);
         }
         (*us_serial)->port = (char *) malloc(strlen(tmp) + 1);
         if ((*us_serial)->port == NULL)
         {
            printf("Erreur d'allocation memoire pour le port de compass %s dans compass_create_compass\n",name);
            exit(1);
         }
         strcpy((*us_serial)->port, tmp);
      }
      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
         ret=fscanf(fh, "%d\n", &((*us_serial)->autocheck));
      }
      else if (strcmp(lecture, "NB_US") == 0)
      {
         ret=fscanf(fh, "%d", &((*us_serial)->nb_us));
         if( (*us_serial)->nb_us > MAX_US)  (*us_serial)->nb_us = MAX_US;
         printf("nb_us: %d\n",  (*us_serial)->nb_us );
      }
      else if (strcmp(lecture, "US_ANGLE") == 0)
      {
         printf("%s\n", lecture);
         cpt = 0;
         ret=fscanf(fh, "%f", &((*us_serial)->angle_us[cpt]));
         cpt++;
         do
         {
            ret=fscanf(fh, "%f%c", &((*us_serial)->angle_us[cpt]), &lettre);
            cpt++;
         }
         while (lettre != '\n');
      }
      else if (strcmp(lecture, "SEUIL_US") == 0)
      {
         ret=fscanf(fh, "%f", &((*us_serial)->seuil_us));
         printf(" %f\n", (*us_serial)->seuil_us);
      }
      else if (strcmp(lecture, "ACTMAX_US") == 0)
      {
         ret=fscanf(fh, "%f", &((*us_serial)->actmax_us));
         printf(" %f\n", (*us_serial)->actmax_us);
      }
      else
         lire_ligne(fh);
   }
   fclose(fh);

   if (pthread_mutex_init(&((*us_serial)->lock), NULL) != 0)
   {
      EXIT_ON_ERROR("Can't initialize mutex : us_serial->lock");
   }
   (void)ret;
}
