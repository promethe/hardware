/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <ColorDetector.h>
#include <Serial.h>
#include <ClientHW.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*#define DEBUG*/

/* fonction de lecture de la valeur de contraste du capteur (requete sur le basic stamp)*/
void contrastdetector_read(ColorDetector * colordetector, int * contrastdetector_value)
{
  char reponse[400];
  int ret = 0;
  int length;
  int contrast_detect;
  char buff[255]; /* Commande 'C' envoyees a l'ATMEGA pour demander la valeur de detect_contrast */
  
#ifdef DEBUG
  printf("contrastdetector_query: starting \n");
#endif

  /*initialisation des buffers*/
  memset(reponse, 0, 400 * sizeof(char));
  memset(buff, 0, 255 * sizeof(char));

  sprintf(buff,"V");
  do
    {
      serial_send_and_receive(colordetector->port, (char*)(&buff), reponse, 1);
      length = strlen(reponse);

      if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
	{
	  reponse[length-1] = '\0';
#ifdef DEBUG
	  printf("reponse = --%s--\n",reponse);
#endif
	}
      if (reponse[0] == 'v')
	{
	  if(sscanf(reponse, "v %d", &contrast_detect)!=1)
	    {
	      ret=0;
	    }
	  else ret=1;
	  
	}
      else ret=0;
      
#ifdef DEBUG
      printf("contrast_detect = %d\n", contrast_detect);
#endif
      
      if (ret != 1)
	printf("WARNING: the value read by contrastdetector_read (%s) does not have the required format. Retrying to read ... \n",reponse);
    }
  while (ret != 1);
  
  * contrastdetector_value = contrast_detect;
}
