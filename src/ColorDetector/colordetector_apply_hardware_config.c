/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tools.h>
#include <basic_tools.h>

#include <ColorDetector.h>

void colordetector_apply_hardware_config(char *argv)
{
  FILE *dfl;
  ColorDetector **colordetector_table = NULL;

  int nb_colordetector;
  int i;
  /*	char file_name[255];*/
  char lecture[100];
  char colordetector_name[256][256];
  char colordetector_type[256][256];

  nb_colordetector = 0; /*! trying to open appli_name.dev */
  dfl = fopen(argv, "r");
  if (dfl == NULL)
  {
    PRINT_WARNING("Promethe initialisation: unable to open configuration file %s \n Using default parameters", argv);

  }
  else
  { /*loading parameters*/
    while (!feof(dfl))
    {
      if (fscanf(dfl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%') lire_ligne(dfl);
      else
      {
        if (strcmp(lecture, "ColorDetector") == 0)
        {
          if (fscanf(dfl, " %s is %s", colordetector_name[nb_colordetector], colordetector_type[nb_colordetector]) != 2)
          {
            EXIT_ON_ERROR("parse error in dev file\n");
          }
          nb_colordetector++;
        }
      }
    }
    fclose(dfl);
  }

  if (nb_colordetector > 0)
  {
    kprints("%d colordetector(s) are defined:\n", nb_colordetector);
    for (i = 0; i < nb_colordetector; i++)
    {
      kprints("\t ColorDetector %s is configurated by %s \n", colordetector_name[i], colordetector_type[i]);
    }

    colordetector_table = MANY_ALLOCATIONS(nb_colordetector, ColorDetector*);

    for (i = 0; i < nb_colordetector; i++)
    {
      colordetector_table[i] = NULL;
      colordetector_create_colordetector(&colordetector_table[i], colordetector_name[i], colordetector_type[i]);
    }

    colordetector_create_colordetector_table(colordetector_table, nb_colordetector);

    /*checking des colordetector*/
    for (i = 0; i < nb_colordetector; i++)
      if (colordetector_table[i]->autocheck == 1) colordetector_check_hardware(colordetector_table[i]);
  }

}
