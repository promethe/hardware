/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <AirSensor.h>
#include <Serial.h>
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*#define ONLINE_DEBUG*/
/*#define DEBUG*/

char air_sensor_get_cmd(int type)
{
	switch(type)
  	{
		case TEMPERATURE : return CTEMPERATURE;
		case HUMIDITY : return CHUMIDITY;
		case SMOKE : return CSMOKE;
		case DUST : return CDUST;
	}
	return -1;
}

char air_sensor_get_head(int type)
{
        switch(type)
        {
                case TEMPERATURE : return HTEMPERATURE;
                case HUMIDITY : return HHUMIDITY;
                case SMOKE : return HSMOKE;
                case DUST : return HDUST; 
        }
        return -1;
}

float air_sensor_read(AirSensor * air_sensor)
{
    char reponse[400];
    int ret = 0;


    char cmd,head,rhead;
    if( (cmd = air_sensor_get_cmd(air_sensor->type)) == -1) return -1 ;
    if( (head = air_sensor_get_head(air_sensor->type)) == -1) return -1 ;

	

    serial_send_and_receive_block(air_sensor->port, &cmd, reponse, 1, 1000000);	// precedemment size= . Mais il semble que la valeur de retour est inutilisé ici.

    ret = sscanf(reponse, "-%c%f-", &rhead,  &(air_sensor->value) );

    if( ret == 2 ) 
    {
	if( head == rhead) air_sensor->value = (air_sensor->value - air_sensor->val_min) / air_sensor->val_max ;
	else air_sensor->value = -1;
    }
    return air_sensor->value;
}
