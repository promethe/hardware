/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <dlfcn.h>
#include <stdlib.h>

#include <net_message_debug_dist.h>
#include "prom_tools/include/xml_tools.h"

#include <device.h>
#include <dev.h>
#include <Components/generic.h>
#include <Components/bus.h>

void device_flush(Device *this);

void device_init(Device *device, Node *node_of_device)
{
	const char *node_name, *type_of_component, *type_of_driver, *id_of_bus;
	int i;
	Node *node_of_component;
	type_generic_component *component;
    void *(*constructor_of_specific_driver)() = NULL;

	void (*specific_component_init)(type_generic_component *) = NULL;
	void (*driver_init)(type_generic_component *) = NULL;
	void (*specific_component_start)(type_generic_component *) = NULL;

	/** On recupere le nom du noeud ('device' pour un appareil de plusieurs composants ou directement le nom du composant s'il est unique */
	node_name = xml_get_node_name(node_of_device);

	/** On recupere l'identifiant de l'appareil */
	device->node = node_of_device;
	device->id = xml_try_to_get_string(node_of_device, "id");

	/** Si c'est un appareil de plusieurs composants, on les compte et on pointe sur le premier */
	if (strcmp(node_name, "device") == 0)
	{
		device->number_of_components = xml_get_number_of_childs(node_of_device);
		node_of_component = xml_get_first_child(node_of_device);
	}
	/** Sinon on pointe sur le seul composant.*/
	else
	{
		device->number_of_components = 1;
		node_of_component = node_of_device;
	}

	/** Maintenant qu'on connait le nombre de composants de l'appareil on creer le tableau de composants */
	device->components = MANY_ALLOCATIONS(device->number_of_components, type_generic_component*);

	/** On recupere le type des composants et des drivers de l'appareil */
	device->type_of_the_components = xml_get_node_name(node_of_component);
	device->type_of_the_drivers = xml_get_string(node_of_component, "type");

	/** Si l'appreil n'est pas un bus on recherche le bus qu'on associera a l'appareil sinon bus est NULL. En fait si un bus est precisé, meme un bus peut avoir un bus (i.e. channel)*/
	/** Pour l'instant on suppose qu'il n'y a qu'un bus par appareil*/
	device->number_of_buses = 1;
	device->buses = ALLOCATION(type_bus*);

	id_of_bus = xml_try_to_get_string(node_of_device, "bus");
	if (id_of_bus != NULL /*&& (strcmp(device->type_of_the_components, "bus") != 0)*/)
	{
		device->buses[0] = (type_bus*) (dev_get_device("bus", id_of_bus)->components[0]);
	}
	else
		device->buses[0] = NULL;

	/** On associe les fonctions de construction et d'initialisation a leur bibliotheque. */
#ifdef DEBUG_COMPILATION
	device->lib_handle_of_component = load_libraryf("%s/%s/%s_debug.so", getenv("HOME"), HARDWARD_LIB_PATH, device->type_of_the_components);
  device->lib_handle_of_driver = load_libraryf("%s/%s/%s_%s_debug.so", getenv("HOME"), HARDWARD_LIB_PATH, device->type_of_the_drivers, device->type_of_the_components);
#else
  device->lib_handle_of_component = load_libraryf("%s/%s/%s.so", getenv("HOME"), HARDWARD_LIB_PATH, device->type_of_the_components);
  device->lib_handle_of_driver = load_libraryf("%s/%s/%s_%s.so", getenv("HOME"), HARDWARD_LIB_PATH, device->type_of_the_drivers, device->type_of_the_components);
#endif

	try_to_link_function_with_library(&specific_component_init, device->lib_handle_of_component, "init");
	try_to_link_function_with_library(&specific_component_start, device->lib_handle_of_component, "start");

	try_to_link_function_with_library(&constructor_of_specific_driver, device->lib_handle_of_driver, "constructor");
	try_to_link_function_with_library(&driver_init, device->lib_handle_of_driver, "init");

    /** On associe par défaut les fonctions de l'appareil lui même.*/
      device->flush = device_flush;

      device->total_elements_nb = 0;
	/** On cree chaque composant du pilote */
	for (i = 0; i < device->number_of_components; i++)
	{
		/** On test que tous les composants de l'appareil soient de meme type avec le meme pilote */
		type_of_component = xml_get_node_name(node_of_component);

		type_of_driver = xml_get_string(node_of_component, "type");
		if (strcmp(device->type_of_the_components, type_of_component) != 0) EXIT_ON_ERROR("The component '%s' is not the same as the others ('%s') in the device." , type_of_component, device->type_of_the_components);
		if (strcmp(device->type_of_the_drivers, type_of_driver) != 0) EXIT_ON_ERROR("The component '%s' has not the same type of driver ('%s') than the others ('%s') in the device." , type_of_component, type_of_driver, device->type_of_the_drivers);

		/** On alloue la memoire su composnant (Partie generique + partie specifique (e.g. motor) + driveur (e.g. urbi_driver)*/
		component = constructor_of_specific_driver();
		device->components[i] = component;

		/** Initialisation du composant: composant generique puis pilote puis composant specifique (e.g. motor)*/
		component->device = device;
		component->node = node_of_component;
		component->bus = device->buses[0];
        if (!xml_try_to_get_int(node_of_component, "elements_nb", &component->elements_nb)) component->elements_nb = 1;
        device->total_elements_nb += component->elements_nb;
		specific_component_init(component);
		driver_init(component);

		/** Si elle existe, on appelle la fonction de démarrage du composant */
		if (specific_component_start != NULL) specific_component_start(component);

		/** Positionnement sur le composant suivant */
		node_of_component = xml_get_next_sibling(node_of_component);
	}
}

void device_start(Device *device)
{
	int i = 0;
	void (*driver_start)(type_generic_component *) = NULL;

	try_to_link_function_with_library(&driver_start, device->lib_handle_of_driver, "start");

	for(i = 0; i< device->number_of_components; i++){
		if(driver_start != NULL) driver_start(device->components[i]);
	}
}

void device_stop(Device *device)
{
	int i;
	void (*component_stop)(type_generic_component *) = NULL;
	void (*driver_stop)(type_generic_component *) = NULL;

	/** On recupere la fonction d'arret des composants et pilotes.*/
	try_to_link_function_with_library(&component_stop, device->lib_handle_of_component, "stop");
	try_to_link_function_with_library(&driver_stop, device->lib_handle_of_driver, "stop");

	/** On arrete tous les composants et drivers de l'appareil.*/
	for (i = 0; i < device->number_of_components; i++)
	{
		if (component_stop != NULL) component_stop(device->components[i]);
		if (driver_stop != NULL)
		{
			driver_stop(device->components[i]);
		}
	}

	/** On ferme les bibliotheques des composants et pilotes de l'appareil. */
	close_library(device->lib_handle_of_component);
	close_library(device->lib_handle_of_driver);
}

/** Fonction par défaut. Flush les bus de l'appareil (pour l'instant il n'y en a toujours qu'un).*/
void device_flush(Device *this)
{
	this->buses[0]->flush(this->buses[0]);
}
