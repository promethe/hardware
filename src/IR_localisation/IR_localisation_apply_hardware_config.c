/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tools.h>
#include <basic_tools.h>
#include <IR_localisation.h>

void IR_localisation_apply_hardware_config(char *argv)
{
  FILE *dfl;
  IR_localisation **IR_localisation_table = NULL;

  int nb_IR_localisation;
  int i;

  char lecture[100];
  char IR_localisation_name[256][256];
  char IR_localisation_type[256][256];

  nb_IR_localisation = 0; /*! trying to open appli_name.dev */
  dfl = fopen(argv, "r");
  if (dfl == NULL)
  {
    PRINT_WARNING("Promethe initialisation: unable to open configuration file %s \n Using default parameters", argv);
  }
  else
  { /*loading parameters*/
    while (!feof(dfl))
    {
      if (fscanf(dfl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%') lire_ligne(dfl);
      else
      {
        if (strcmp(lecture, "IR_localisation") == 0)
        {
          if (fscanf(dfl, " %s is %s", IR_localisation_name[nb_IR_localisation], IR_localisation_type[nb_IR_localisation]) != 2)
          {
            EXIT_ON_ERROR("parse error in dev file\n");
          }
          nb_IR_localisation++;
        }
      }
    }
    fclose(dfl);
  }

  if (nb_IR_localisation > 0)
  {
    kprints("%d IR_localisation is defined:\n", nb_IR_localisation);
    for (i = 0; i < nb_IR_localisation; i++)
    {
      kprints("\t IR_localisation %s is configurated by %s \n", IR_localisation_name[i], IR_localisation_type[i]);
    }

    IR_localisation_table = MANY_ALLOCATIONS(nb_IR_localisation, IR_localisation*);

    for (i = 0; i < nb_IR_localisation; i++)
    {
      IR_localisation_table[i] = NULL;
      IR_localisation_create_IR_localisation(&IR_localisation_table[i], IR_localisation_name[i], IR_localisation_type[i]);
    }

    IR_localisation_create_IR_localisation_table(IR_localisation_table, nb_IR_localisation);

    /*checking des IR_localisation*/
    for (i = 0; i < nb_IR_localisation; i++)
      if (IR_localisation_table[i]->autocheck == 1) IR_localisation_check_hardware(IR_localisation_table[i]);
  }
}
