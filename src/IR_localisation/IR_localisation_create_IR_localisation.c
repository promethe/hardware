/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <IR_localisation.h>
#include <tools.h>

#include "tools/include/local_var.h"

void IR_localisation_create_IR_localisation(IR_localisation ** IR_localisation_struct, char *name, char *file_hw)
{
   FILE *fh;
   char lecture[256];
   char tmp[129];

   *IR_localisation_struct = ALLOCATION(IR_localisation);

   (*IR_localisation_struct)->name = (char *) malloc(strlen(name) + 1);
   if ((*IR_localisation_struct)->name == NULL)
   {
      EXIT_ON_ERROR("erreur malloc IR_localisation_create_IR_localisation");
   }

   strcpy((*IR_localisation_struct)->name, name); /*IR_localisation_object name */
   (*IR_localisation_struct)->port = NULL;
   (*IR_localisation_struct)->autocheck = 0;

   /* default parameters */

   /*! loading colordetector parameters */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }

   while (!feof(fh))
   {
      TRY_FSCANF(1, fh, "%s = ", lecture);
      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {

         if (fscanf(fh, "%s\n", tmp) == 0)
         {
            EXIT_ON_ERROR("Erreur de lecture pour le port du IR_localisation %s", name);
         }

         /* verify if it is the first declaration for port */
         if ((*IR_localisation_struct)->port != NULL)
         {
            PRINT_WARNING("several declarations of port are declared in %s", file_hw);
            free((*IR_localisation_struct)->port);
         }

         (*IR_localisation_struct)->port = (char *) malloc(strlen(tmp) + 1);
         if ((*IR_localisation_struct)->port == NULL)
         {
            EXIT_ON_ERROR("Erreur d'allocation memoire pour le port de IR_localisation %s", name);
         }
         strcpy((*IR_localisation_struct)->port, tmp);
      }
      else if (strcmp(lecture, "TYPE") == 0)
      {
         TRY_FSCANF(1, fh, "%d\n", &((*IR_localisation_struct)->IR_localisation_type));
      }
      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
         TRY_FSCANF(1, fh, "%d\n", &((*IR_localisation_struct)->autocheck));
      }
      else lire_ligne(fh);
   }
   fclose(fh);
   printf("\t\t IR type=%d\n", (*IR_localisation_struct)->IR_localisation_type);
}
