/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IR_localisation.h>
#include <Serial.h>
#include <World.h>
#include <Robot.h>

/*#define DEBUG*/

void IR_localisation_read(IR_localisation * IR_localisation, float * IR_localisation_value)
{
   char reponse[400];
   int ret = 0;
   int length;
   int LandmarkID;
   float X, Y, Z, Angle;
  
#ifdef DEBUG
   printf("IR_localisation_query: starting \n");
#endif
      
   do
   {
      serial_receive(IR_localisation->port, reponse);
      length = strlen(reponse);
	  
      if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
      {
	 reponse[length-1] = '\0';
      }

      ret = sscanf(reponse, "~^I%i|%f|%f|%f|%f`", &LandmarkID, &Angle, &X, &Y, &Z);
	  
#ifdef DEBUG
      printf("LandMarkID:%i, X:%f, Y:%f, Angle:%f\n", LandmarkID, X, Y, Angle);

      if (ret != 5)
	 PRINT_WARNING("the values read by IR_localisation_read (%s) do not have the required format. Retrying to read ...", reponse);
#endif
   }
   while (ret != 5);
  
   IR_localisation_value[0] = X;
   IR_localisation_value[1] = Y;
   IR_localisation_value[2] = Angle;
}
