/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <string.h>
#include <sys/ioctl.h>
#ifdef Linux
#include <error.h>
#include <errno.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <Camera.h>
#include <ClientRobubox.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

enum {GREY=0, RGB8, RGB24, BGR24, YUV422, YVU420, YUV420, YUV411, SGBRG8, SGRBG8, SBGGR8, SRGGB8, JPEG,  NB_FORMAT};

int camera_init_capture(Camera * cam)
{
   int i;
   int supported_formats[NB_FORMAT];
   int image_size;

   memset(&supported_formats, 0, sizeof(supported_formats));

   dprints("camera_init_capture: Setting up camera %s for image capture\n", cam->name);

   /*Redefine width and height if scale_factor method is used to capture... */
   if (cam->scale_factor < 1.0) EXIT_ON_ERROR("scale_factor < 1 (%f)\n", cam->scale_factor);

   if (cam->camera_type == USE_CAM_WEBOTS)
   {
      char commande[256];
      char result[256];
      char cmd[256];
      int width = 0;
      int height = 0;
      int type = 0;

      ClientRobubox *clientRobubox;
      clientRobubox = clientRobubox_get_clientRobubox_by_name(cam->clientRobubox);

      sprintf(commande, "%s", "CameraInfo");
      clientRobubox_transmit_receive(clientRobubox, commande, result);

      sscanf(result, "%s %d %d %d", cmd, &width, &height, &type);

      if (width != cam->width_max || height != cam->height_max)
      {
         PRINT_WARNING("Image size (%ix%i) in hardware file differs from webots (%ix%i)", cam->width_max, cam->height_max, width, height);
         cam->width_max = width;
         cam->height_max = height;
      }

      if (cam->color != type)
      {
         PRINT_WARNING("Color parameter in hardware file (%i) differs from Webots (%i)", cam->color, type);
         cam->color = type;
      }

      camera_set_image_size(cam, width, height);
      cam->palette = type;
   }
   else if (cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO || cam->camera_type == USE_V4L2)
   {
#ifdef Linux
      struct v4l2_input input;
      v4l2_std_id std_id;
      struct v4l2_fmtdesc v4l2_fmtd;
      struct v4l2_buffer buf;
      enum v4l2_buf_type type;
      struct v4l2_queryctrl queryctrl;
      int id;

      /* Changes the image size if one was specified in the config file and it is different from the curren size */
      camera_set_image_size(cam, cam->width_max, cam->height_max);

      /* The size the camera has been opened with will be the maximum size (for future scaling) */
      cam->width_max = cam->width;
      cam->height_max = cam->height;

      /* Handles the format for image data */
      memset(&v4l2_fmtd, 0, sizeof(v4l2_fmtd));
      v4l2_fmtd.index = 0;
      v4l2_fmtd.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

      while (!ioctl(cam->desc, VIDIOC_ENUM_FMT, &v4l2_fmtd))
      {
         dprints("camera_init_capture: Format supported by the cam: index=%d, pixelformat=%d, desc=%s\n", v4l2_fmtd.index, v4l2_fmtd.pixelformat, v4l2_fmtd.description);

         if (v4l2_fmtd.pixelformat == cam->v4l2_fmt.fmt.pix.pixelformat)
            dprints("======> format currently used\n");

         switch (v4l2_fmtd.pixelformat)
         {
         case V4L2_PIX_FMT_GREY:
            supported_formats[GREY] = 1;
            break;

         case V4L2_PIX_FMT_BGR24:
            supported_formats[BGR24] = 1;
            break;

         case V4L2_PIX_FMT_RGB24:
            supported_formats[RGB24] = 1;
            break;

         case V4L2_PIX_FMT_YVU420:
            supported_formats[YVU420] = 1;
            break;

         case V4L2_PIX_FMT_YUV420:
            supported_formats[YUV420] = 1;
            break;

         case V4L2_PIX_FMT_YUYV:
            supported_formats[YUV422] = 1;
            break;

         case V4L2_PIX_FMT_SGBRG8: //0
            supported_formats[SGBRG8] = 1;
            break;

         case V4L2_PIX_FMT_SGRBG8: //1
            supported_formats[SGRBG8] = 1;
            break;

         case V4L2_PIX_FMT_SBGGR8: //2
            supported_formats[SBGGR8] = 1;
            break;

         case V4L2_PIX_FMT_SRGGB8: //3
            supported_formats[SRGGB8] = 1;
            break;

         case V4L2_PIX_FMT_JPEG:
               supported_formats[JPEG] = 1;
                break;

         default:
            break;
         }

         v4l2_fmtd.index++;
      }

      /* Tries to switch to a supported format in the order of preference */
      if (cam->color == 1 && supported_formats[RGB24] == 1)
      {
         dprints("camera_init_capture: Switching cam format to RGB24\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
      }
      else if (cam->color == 1 && supported_formats[BGR24] == 1)
      {
         dprints("camera_init_capture: Switching cam format to BGR24\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR24;
      }
      else if (cam->color == 1 && supported_formats[YUV422] == 1)
      {
         dprints("camera_init_capture: Switching cam format to YUV422 (YUYV)\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
      }
      else if (cam->color == 1 && supported_formats[YVU420] == 1)
      {
         dprints("camera_init_capture: Switching cam format to YVU420\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YVU420;
      }
      else if (cam->color == 1 && supported_formats[YUV420] == 1)
      {
         dprints("camera_init_capture: Switching cam format to YUV420\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
      }
      else if (cam->color == 1 && supported_formats[SGBRG8] == 1)
      {
         dprints("camera_init_capture: Switching cam format to SGBRG8\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_SGBRG8;
      }
      else if (cam->color == 1 && supported_formats[SGRBG8] == 1)
      {
         dprints("camera_init_capture: Switching cam format to SGRBG8\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_SGRBG8;
      }
      else if (cam->color == 1 && supported_formats[SBGGR8] == 1)
      {
         dprints("camera_init_capture: Switching cam format to SBGGR8\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_SBGGR8;
      }
      else if (cam->color == 1 && supported_formats[SRGGB8] == 1)
      {
         dprints("camera_init_capture: Switching cam format to SRGGB8\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_SRGGB8;
      }
      else if (supported_formats[JPEG] == 1)
      {
         dprints("camera_init_capture: Switching cam format to JPEG\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_JPEG;
      }
      else if (supported_formats[GREY] == 1)
      {
         dprints("camera_init_capture: Switching cam format to GREY\n");
         cam->v4l2_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY;
         cam->color = 0;
      }
      else EXIT_ON_ERROR("Camera does not handle the pixel format '%4s' (see V4L2)", &v4l2_fmtd.pixelformat);

      cam->palette = cam->v4l2_fmt.fmt.pix.pixelformat;

      /*cam->v4l2_fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;*//* Is it necessary ? */

      /* Setting previously configured format on the camera */
      if (ioctl(cam->desc, VIDIOC_S_FMT, &cam->v4l2_fmt) == -1) EXIT_ON_SYSTEM_ERROR("ioctl (VIDIOC_S_FMT)");

      /* Sets the norm to NTSC or PAL if specified */
      if (cam->camera_type_v4l == CAM_COMPOSITE)
      {
         dprints("camera_init_capture: Setting standard for composite input\n");

         memset (&input, 0, sizeof (input));

         if (-1 == ioctl(cam->desc, VIDIOC_G_INPUT, &input.index))  EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_G_INPUT)");
         if (-1 == ioctl(cam->desc, VIDIOC_ENUMINPUT, &input))  EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_ENUM_INPUT)");

         if (cam->norm == NTSC )
         {
            std_id = V4L2_STD_NTSC;
            dprints("camera_init_capture: Trying to set NTSC standard\n");
         }
         else if (cam->norm == PAL)
         {
            std_id = V4L2_STD_PAL;
            dprints("camera_init_capture: Trying to set PAL standard\n");
         }
         if (0 == (input.std & std_id)) EXIT_ON_ERROR("Standard is not supported by the camera.");
         if (-1 == ioctl(cam->desc, VIDIOC_S_STD, &std_id)) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_ENUM_INPUT)");
      }

      /* Starts configuring the capture using memory mapping for the image buffers */
      memset (&cam->req_buf, 0, sizeof (cam->req_buf));

      cam->req_buf.count = cam->nb_buffers;
      cam->req_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      cam->req_buf.memory = V4L2_MEMORY_MMAP;

      if (-1 == ioctl (cam->desc, VIDIOC_REQBUFS, &cam->req_buf)) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_REQBUFS)");

      dprints("camera_init_capture: Number of buffers allocated for streaming = %d\n", cam->req_buf.count);

      cam->buffers = calloc (cam->req_buf.count, sizeof(struct buffer));

      if (cam->buffers == NULL) EXIT_ON_ERROR("Error allocating memory for buffers\n");
      cam->v4l2_buffers = calloc (cam->req_buf.count, sizeof(struct v4l2_buffer));
      if (cam->v4l2_buffers == NULL) EXIT_ON_ERROR(" Error allocating memory for v4l2_buffers");

      /* Maps the buffers into device memory */
      for (i = 0; i < (int)cam->req_buf.count; i++)
      {
         memset(&buf, 0, sizeof(buf));

         buf.type = cam->req_buf.type;
         buf.memory = cam->req_buf.memory;
         buf.index = i;

         if (-1 == ioctl (cam->desc, VIDIOC_QUERYBUF, &buf)) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QUERYBUF)");

         cam->buffers[i].length = buf.length;
         cam->buffers[i].start = mmap (NULL /* start anywhere */,
               buf.length,
               PROT_READ | PROT_WRITE /* required */,
               MAP_SHARED /* recommended */,
               cam->desc, buf.m.offset);

         if (MAP_FAILED == cam->buffers[i].start) EXIT_ON_SYSTEM_ERROR("mmap");
         if (-1 == ioctl (cam->desc, VIDIOC_QBUF, &buf)) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QBUF)");
      }

      /* Starts streaming on the camera */
      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

      if (-1 == ioctl (cam->desc, VIDIOC_STREAMON, &type))  EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_STREAMON)");

      memset (&queryctrl, 0, sizeof (queryctrl));

      for (id = V4L2_CID_BASE; id < V4L2_CID_LASTP1; id++)
      {
         queryctrl.id = id;
         if (0 == ioctl (cam->desc, VIDIOC_QUERYCTRL, &queryctrl))
         {
            if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
               continue;

            printf ("Control %s , min = %i, max = %i, step = %i, default = %i\n", queryctrl.name, queryctrl.minimum, queryctrl.maximum, queryctrl.step, queryctrl.default_value);
	    
            if( queryctrl.id == V4L2_CID_EXPOSURE ) cam->exposure_mode = V4L2_CID_EXPOSURE;		 
	
         }
         else
         {
            if (errno == EINVAL)
               continue;

            EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QUERYCTRL)");
         }
      }
   
      for (queryctrl.id = V4L2_CID_CAMERA_CLASS_BASE; queryctrl.id < V4L2_CID_AUTO_FOCUS_RANGE  ; queryctrl.id++) {
         if (0 == ioctl (cam->desc, VIDIOC_QUERYCTRL, &queryctrl))
         {
                if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
                        continue;

            printf ("Control %s , min = %i, max = %i, step = %i, default = %i\n", queryctrl.name, queryctrl.minimum, queryctrl.maximum, queryctrl.step, queryctrl.default_value);

            if( queryctrl.id == V4L2_CID_EXPOSURE_ABSOLUTE ) cam->exposure_mode = V4L2_CID_EXPOSURE_ABSOLUTE;		 
        } else {
                if (errno == EINVAL)
                        continue;

            EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QUERYCTRL)");
        }
}


      

#else
      printf("Video pour linux n'est pas defini sur ce systeme %s : %d\n", __FUNCTION__, __LINE__);
#endif
   }
   else if (cam->camera_type == USE_FIREWIRE)
   {

#ifndef FIREWIRE
      EXIT_ON_ERROR("Les API firewires ne sont pas compilees\n");
#else

      dc1394camera_t * camera = NULL;
      int i;
      dc1394featureset_t features;
      dc1394framerates_t framerates;
      dc1394video_modes_t video_modes;
      dc1394framerate_t framerate;
      dc1394video_mode_t video_mode = 0;
      dc1394color_coding_t coding;
      dc1394video_frame_t * converted_frame = NULL;
      /*dc1394_t * d=NULL;*/
      dc1394camera_list_t * list = NULL;
      dc1394error_t err;

      if (firewire_context == NULL)
      {
        printf("INIT ! \n");
         firewire_context = dc1394_new();
      }
      else
      {
        printf("PAS INIT ! \n");
      }
      err = dc1394_camera_enumerate(firewire_context, &list);
      DC1394_ERR_RTN(err,"Failed to enumerate cameras");

      if (list->num == 0)
      {
         dc1394_log_error("");
         EXIT_ON_ERROR("No camera found");
      }
      printf("FIREWIRE: %i cameras found\n", list->num);

      camera = dc1394_camera_new(firewire_context, list->ids[cam->addr_bus_firewire].guid);
      if (camera == NULL) EXIT_ON_ERROR("Could not create firewire camera located on port");
      dc1394_camera_free_list(list);

      printf("Try to init camera on port %d with GUID %d\n",cam->addr_bus_firewire, (int)(camera->guid));

      if(cam->video_mode==-1)
      {
         PRINT_WARNING("the system tries to determine the highest videomode and framerate. ISOSPEED_400 will also be imposed\n You should provide them explicitely (alsothe isospeed), using a test with coriender");
         /*-----------------------------------------------------------------------
          *  get the best video mode and highest framerate. This can be skipped
          *  if you already know which mode/framerate you want...
          *-----------------------------------------------------------------------*/
         /* get video modes:*/
         err = dc1394_video_get_supported_modes(camera,&video_modes);
         DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Can't get video modes");

         /* select highest res mode:*/
         for (i = video_modes.num-1; i >= 0; i--)
         {
            printf("Mode supported: %d \n",video_modes.modes[i]);
            switch (video_modes.modes[i])
            {
            case DC1394_VIDEO_MODE_640x480_MONO8:
               printf("Mode: MONO8 \n");
               supported_formats[GREY] = 1;
               break;

            case DC1394_VIDEO_MODE_640x480_RGB8:
               printf("Mode: RGB8 \n");
               supported_formats[RGB8] = 1;
               break;

            case DC1394_VIDEO_MODE_640x480_YUV422:
               printf("Mode: YUV422 \n");
               supported_formats[YUV422] = 1;
               break;

            case DC1394_VIDEO_MODE_640x480_YUV411:
               printf("Mode: YUV411 \n");
               supported_formats[YUV411] = 1;
               break;

            default:
               break;
            }
         }

         /* Tries to switch to a supported format in the order of preference */
         if (cam->color == 1 && supported_formats[RGB8] == 1)
         {
            printf("camera_init_capture: Switching cam format to RGB8\n");
            video_mode = DC1394_VIDEO_MODE_640x480_RGB8;
         }
         else if (cam->color == 1 && supported_formats[YUV422] == 1)
         {
            printf("camera_init_capture: Switching cam format to YUV422\n");
            video_mode = DC1394_VIDEO_MODE_640x480_YUV422;
         }
         else if (cam->color == 1 && supported_formats[YUV411] == 1)
         {
            printf("camera_init_capture: Switching cam format to YUV411\n");
            video_mode = DC1394_VIDEO_MODE_640x480_YUV411;
         }
         else if (supported_formats[GREY] == 1)
         {
            printf("camera_init_capture: Switching cam format to GREY\n");
            video_mode = DC1394_VIDEO_MODE_640x480_MONO8;
         }
         else
         {
            printf( "ERROR in camera_init_capture: Camera does not handle any of the supported formats\n");
            cleanup_and_exit(camera);
         }

         cam->video_mode=video_mode;

         /* get highest framerate*/
         err = dc1394_video_get_supported_framerates(camera,video_mode,&framerates);
         DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not get framrates");
         for(i = 0; i < (int)framerates.num; i++)
         {
            printf("Framerate supported pour videomode %d: %d\n",video_mode,framerates.framerates[i]);
         }
         framerate = framerates.framerates[framerates.num-1];
         cam->framerate=framerate;
         printf("Framerate selected: %d\n",cam->framerate);

         cam->isospeed=DC1394_ISO_SPEED_400;
         printf("Isospeed selected: %d\n",cam->isospeed);

      }

      /*-----------------------------------------------------------------------
       *  setup capture
       *-----------------------------------------------------------------------*/

      err = dc1394_video_set_iso_speed(camera, cam->isospeed);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not set iso speed");

      err = dc1394_video_set_mode(camera, cam->video_mode);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not set video mode");
      cam->palette = cam->video_mode; /*il faut pas utiliser cette variable qui vien plutot de V4L. Avec Firewire; on parle de videomode*/
      err = dc1394_get_color_coding_from_video_mode(camera, cam->video_mode, &coding);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not get color coding");
      if (coding == DC1394_COLOR_CODING_MONO8)
      {
         cam->color = 0;
      }
      else
      {
         cam->color = 1;
      }

      err = dc1394_video_set_framerate(camera, cam->framerate);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not set framerate");

      err = dc1394_capture_setup(camera, cam->nb_buffers, DC1394_CAPTURE_FLAGS_DEFAULT);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not setup camera-\nmake sure that the video mode and framerate are\nsupported by your camera");

      /*-----------------------------------------------------------------------
       *  report camera's features
       *-----------------------------------------------------------------------*/

      err = dc1394_feature_get_all(camera,&features);
      if (err != DC1394_SUCCESS)
      {
         dc1394_log_warning("Could not get feature set");
      }
      else
      {
         dc1394_feature_print_all(&features, stdout);
      }

      /*-----------------------------------------------------------------------
       *  have the camera start sending us data
       *-----------------------------------------------------------------------*/
      err = dc1394_video_set_transmission(camera, DC1394_ON);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(camera),"Could not start camera iso transmission");

      /*frame de convertion vers le rgb8*/
      cam->cam_firewire = camera;

      converted_frame = ALLOCATION(dc1394video_frame_t);
      converted_frame->color_coding = DC1394_COLOR_CODING_RGB8;
      converted_frame->allocated_image_bytes = cam->width_max * cam->height_max * 3;
      converted_frame->image = malloc(converted_frame->allocated_image_bytes);

      cam->converted_frame_firewire = converted_frame;

      cam->width = (int) (cam->width_max / cam->scale_factor);
      cam->height = (int) (cam->height_max / cam->scale_factor);

#ifdef DEBUG
      printf("width_max:%d,heigth_max:%d,scale:%f\n", cam->width_max, cam->height_max, cam->scale_factor);
      printf("width:%d,height:%d\n", cam->width, cam->height);
#endif

#endif
   }

   if (cam->camera_type == USE_CAM_WEBOTS)  image_size = cam->width_max * cam->height_max * 3 + 1;
   else image_size = cam->width_max * cam->height_max * 3;

   if (cam->img_buff == NULL)  cam->img_buff = MANY_ALLOCATIONS(image_size, unsigned char);

   if (cam->exposure == -1)
   {
      printf("setting exposure to auto mode\n");
      camera_set_exposure_auto(cam);
   }
   else if (cam->exposure >= 0)
   {
      printf("setting exposure to value %i\n", cam->exposure);
      camera_set_exposure_value(cam, cam->exposure);
   }

   if (cam->brightness == -1)
   {
      printf("setting brightness to auto mode\n");
      camera_set_brightness_auto(cam);
   }
   else if (cam->brightness >= 0)
   {
      printf("setting brightness to value %i\n", cam->brightness);
      camera_set_brightness_value(cam, cam->brightness);
   }

   if (cam->gain == -1)
   {
      printf("setting gain to auto mode\n");
      camera_set_gain_auto(cam);
   }
   else if (cam->gain >= 0)
   {
      printf("setting gain to value %i\n", cam->gain);
      camera_set_gain_value(cam, cam->gain);
   }

   if (cam->sharpness == -1)
   {
      printf("setting sharpness to auto mode\n");
      camera_set_sharpness_auto(cam);
   }
   else if (cam->sharpness >= 0)
   {
      printf("setting sharpness to value %i\n", cam->sharpness);
      camera_set_sharpness_value(cam, cam->sharpness);
   }

   if (cam->red_balance == -1 || cam->blue_balance == -1)
   {
      printf("setting white_balance to auto mode\n");
      camera_set_white_balance_auto(cam);
   }
   else if (cam->red_balance >= 0 && cam->blue_balance >= 0)
   {
      printf("setting white_balance (blue,red) to values %i,%i\n", cam->blue_balance, cam->red_balance);
      camera_set_white_balance_values(cam, cam->blue_balance, cam->red_balance);
   }

   if (cam->hue == -1)
   {
      printf("setting hue to auto mode\n");
      camera_set_hue_auto(cam);
   }
   else if (cam->hue >= 0)
   {
      printf("setting hue to value %i\n", cam->hue);
      camera_set_hue_value(cam, cam->hue);
   }

   if (cam->saturation == -1)
   {
      printf("setting saturation to auto mode\n");
      camera_set_saturation_auto(cam);
   }
   else if (cam->saturation >= 0)
   {
      printf("setting saturation to value %i\n", cam->saturation);
      camera_set_saturation_value(cam, cam->saturation);
   }

   if (cam->gamma == -1)
   {
      printf("setting gamma to auto mode\n");
      camera_set_gamma_auto(cam);
   }
   else if (cam->gamma >= 0)
   {
      printf("setting gamma to value %i\n", cam->gamma);
      camera_set_gamma_value(cam, cam->gamma);
   }

   if (cam->shutter == -1)
   {
      printf("setting shutter to auto mode\n");
      camera_set_shutter_auto(cam);
   }
   else if (cam->shutter >= 0)
   {
      printf("setting shutter to value %i\n", cam->shutter);
      camera_set_shutter_value(cam, cam->shutter);
   }

   if (cam->iris == -1)
   {
      printf("setting iris to auto mode\n");
      camera_set_iris_auto(cam);
   }
   else if (cam->iris >= 0)
   {
      printf("setting iris to value %i\n", cam->iris);
      camera_set_iris_value(cam, cam->iris);
   }

   return 0;
}
