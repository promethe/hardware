/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Camera.h>
#include <pthread.h>
#include <ClientHW.h>
#include <Aibo.h>
#include <Nios.h>
#include <ClientRobubox.h>

/*#define DEBUG*/

#include "tools/include/local_var.h"
#include <net_message_debug_dist.h>

/* Resets all the buffers to avoid old frames */
static void flush_buffers(Camera *cam)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      unsigned int j;

      memset(cam->v4l2_buffers, 0, sizeof(*(cam->v4l2_buffers)));

      for (j = 0; j < cam->req_buf.count; j++)
      {
         cam->v4l2_buffers[j].type = cam->req_buf.type;
         cam->v4l2_buffers[j].memory = cam->req_buf.memory;

         /* in order to flush the buffers we must dequeue all of them and requeue them */
         if (-1 == ioctl (cam->desc, VIDIOC_DQBUF, &(cam->v4l2_buffers[j]))) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_DQBUF)");
      }

      for (j = 0; j < cam->req_buf.count; j++)
      {
         if (-1 == ioctl (cam->desc, VIDIOC_QBUF, &(cam->v4l2_buffers[j]))) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_DQBUF)");
      }
#else
      printf("La camera en v4l n'a pas ete compilee, pb OS... %s,%d\n",__FUNCTION__,__LINE__);
#endif
   }
   else if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      dc1394video_frame_t *frame = NULL;

      dc1394_capture_dequeue(cam->cam_firewire, DC1394_CAPTURE_POLICY_POLL, &frame);
      if (frame != NULL)
      {
         /* Here we dequeue and requeue all recursively */
         flush_buffers(cam);
         dc1394_capture_enqueue(cam->cam_firewire, frame);
      }
#endif
   }
   else EXIT_ON_ERROR("Camera type %d not supported.", cam->camera_type);

}

/* Generic image capture function */
/* Stores a new image in the img_buff structure using the camera parameters */
int camera_capture_image(Camera *cam)
{
   if (cam->img_buff == NULL)
   {
      fprintf(stderr, "ERROR in camera_capture_image: NULL pointer for cam->img_buff\n");
      exit(1);
   }

   dprints("camera_capture_image for cam %s\n", cam->name);

   if (cam->camera_type == USE_CAM_WEBOTS)
   {
      char commande[256];      
      ClientRobubox *clientRobubox;

      clientRobubox = clientRobubox_get_clientRobubox_by_name(cam->clientRobubox);

      sprintf(commande,"%s","Camera");
      pthread_mutex_lock(&(cam->mutex_grab_image));
      clientRobubox_transmit_receive(clientRobubox, commande, (char *)cam->img_buff);			
   }
   else if (cam->camera_type == USE_FIREWIRE || cam->camera_type == USE_PLAYER)
   {
#ifdef FIREWIRE
      dc1394video_frame_t *p_frame = NULL;
      dc1394video_frame_t *p_converted_frame;
      dc1394camera_t *p_camera;
      dc1394error_t err;

      p_converted_frame = (dc1394video_frame_t *) cam->converted_frame_firewire;
      p_camera = (dc1394camera_t *) (cam->cam_firewire);

      if (cam->flush_buffers == 1) flush_buffers(cam);

      err = dc1394_capture_dequeue(p_camera, DC1394_CAPTURE_POLICY_WAIT, &p_frame);
      DC1394_ERR_CLN_RTN(err,cleanup_and_exit(p_camera),"Could not capture a frame");

      pthread_mutex_lock(&(cam->mutex_grab_image));

      /* Handling of image formats */
      switch (cam->palette)
      {
      case DC1394_VIDEO_MODE_640x480_MONO8:
         memcpy(cam->img_buff, p_frame->image, cam->width * cam->height);
         break;

      case DC1394_VIDEO_MODE_640x480_RGB8:
         memcpy(cam->img_buff, p_frame->image, cam->width * cam->height * 3);
         break;

      default:
         dc1394_convert_frames(p_frame, p_converted_frame);
         memcpy(cam->img_buff, p_converted_frame->image, cam->width * cam->height * 3);
         break;
      }

      dc1394_capture_enqueue(p_camera, p_frame);		
#endif
   }
   else if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      int sizesortie;
      if (cam->flush_buffers == 1) flush_buffers(cam); //AB: Attention flush_buffers est surement bugué verifier le memset inside

      /* Gets one filled buffer */
      memset(cam->v4l2_buffers, 0, sizeof(cam->v4l2_buffers[0]));
      cam->v4l2_buffers[0].type = cam->req_buf.type;
      cam->v4l2_buffers[0].memory = cam->req_buf.memory;

      if (-1 == ioctl (cam->desc, VIDIOC_DQBUF, &(cam->v4l2_buffers[0]))) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QBUF)");

      dprints("image size %d\n",(cam->v4l2_buffers[0]).bytesused );
      pthread_mutex_lock(&(cam->mutex_grab_image));

      /* Handling of image formats */
      switch (cam->v4l2_fmt.fmt.pix.pixelformat)
      {
       case V4L2_PIX_FMT_JPEG:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, (cam->v4l2_buffers[0]).bytesused);
         sizesortie = cam->width * cam->height*3;
         convertJPEGtoRGB((char *)cam->img_buff, (cam->v4l2_buffers[0]).bytesused, (char *)cam->img_buff, &sizesortie);
         break;

      case V4L2_PIX_FMT_GREY:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, cam->width * cam->height);
         break;

      case V4L2_PIX_FMT_BGR24:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, cam->width * cam->height * 3);
         switch_rb(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_RGB24:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, cam->width * cam->height * 3);
         break;

      case V4L2_PIX_FMT_YVU420:
      case V4L2_PIX_FMT_YUV420:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, cam->width * cam->height * 3 / 2);
         yuv420p_to_rgb24(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_YUYV:
         memcpy(cam->img_buff, cam->buffers[cam->v4l2_buffers[0].index].start, cam->width * cam->height * 2);
         yuv422_to_rgb24(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_SGBRG8:
         bayer_to_rgb24 ( cam->buffers[cam->v4l2_buffers[0].index].start  , cam->img_buff , cam->width, cam->height, 0);
         //flip_bgr_image(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_SGRBG8:
         bayer_to_rgb24 ( cam->buffers[cam->v4l2_buffers[0].index].start  , cam->img_buff , cam->width, cam->height, 1);
         //flip_bgr_image(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_SBGGR8: /*Camera cote saint martin*/
         bayer_to_rgb24 ( cam->buffers[cam->v4l2_buffers[0].index].start  , cam->img_buff , cam->width, cam->height, 1);/*1 ou 2 ?*/
         //flip_bgr_image(cam->img_buff, cam->width, cam->height);
         break;

      case V4L2_PIX_FMT_SRGGB8:
         bayer_to_rgb24 ( cam->buffers[cam->v4l2_buffers[0].index].start  , cam->img_buff , cam->width, cam->height, 3);
         //flip_bgr_image(cam->img_buff, cam->width, cam->height);
         break;

      default:
         EXIT_ON_ERROR("Camera pixelformat '%4s' not supported\n", &cam->v4l2_fmt.fmt.pix.pixelformat);
      }

      if (-1 == ioctl (cam->desc, VIDIOC_QBUF, &(cam->v4l2_buffers[0]))) EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_QBUF)");

#else
      printf("La camera en v4l n'a pas ete compilee, pb OS... %s,%d\n",__FUNCTION__,__LINE__);
#endif
   }

   /* Unlocks the mutex so that the image can be read */
   pthread_mutex_unlock(&(cam->mutex_grab_image));
   return 0;
}


/* Function with hardware scaling and color conversion */
void camera_grabimage_hardscaling(Camera * cam, unsigned char *im_tab_ext,
      int color_wanted, float scale_factor)
{   
   int k = 0, j = 0, val = 0;

   dprints("camera_grabimage_hardscaling: starting capture on cam %p, scale_factor = %f\n", (void *)cam, scale_factor);

   if (cam->color == 0 && color_wanted == 1)  EXIT_ON_ERROR("Color is wanted but the camera runs in B&W mode => Conversion impossible");

   if (isdiff(scale_factor,cam->scale_factor))
   {
      dprints("camera_grabimage_hardscaling: cam->scale_factor(%f) != scale_factor(%f), setting camera scale_factor\n",  cam->scale_factor, scale_factor);
      camera_set_scale_factor(cam, scale_factor);
   }

   if (cam->threading == 0)
   {
      camera_capture_image(cam);
      sem_post(&(cam->sem));
   }
   else  sem_wait(&(cam->sem));

   pthread_mutex_lock(&(cam->mutex_grab_image));

   if (color_wanted == cam->color)
   {
      if (color_wanted == 1)
         memcpy(im_tab_ext, cam->img_buff, cam->width * cam->height * 3);
      else
         memcpy(im_tab_ext, cam->img_buff, cam->width * cam->height);
   }
   else
   {
      for (j = 0; j < (cam->height * cam->width); j++)
      {
         val = 0;
         for (k = 3 * j; k < 3 * j + 3; k++)
            val += cam->img_buff[k];
         im_tab_ext[j] = val / 3;
      }
   }

   pthread_mutex_unlock(&(cam->mutex_grab_image));
}


/* Function with hardware scaling and color conversion */
void camera_grabimage_softscaling(Camera * cam, unsigned char *im_tab_ext, int color_wanted, float scale_factor)
{
   int l;
   int k = 0, j = 0, val = 0, val2 = 0;
   int c, jj, nb_band;;

   dprints("camera_grabimage_softscaling: starting capture on cam %p, scale_factor = %f\n", (void *)cam, scale_factor);

   if (cam->color == 0 && color_wanted == 1)  EXIT_ON_ERROR("Color is wanted but the camera runs in B&W mode => Conversion impossible");

   if (cam->scale_factor < 1. || cam->scale_factor > 1.)
   {
      dprints("camera_grabimage_softscaling: setting cam->scale_factor to 1.\n");
      camera_set_scale_factor(cam, 1);
   }

   if (cam->threading == 0)
   {
      /* Manual image capture */
      camera_capture_image(cam);
   }
   else
   {
      /* If the camera is threaded we just wait for a new image to be captured */
      sem_wait(&(cam->sem));
   }

   /* Protection of the img_buff structure against overwriting be the capture function */
   pthread_mutex_lock(&(cam->mutex_grab_image));

   if (isequal(scale_factor, 1.) && color_wanted == cam->color)
   {
      /* The captured image has the correct format, we just copy it */
      if (color_wanted == 1)  memcpy(im_tab_ext, cam->img_buff, (size_t) (cam->width/scale_factor * cam->height/scale_factor * 3));
      else  memcpy(im_tab_ext, cam->img_buff, (size_t) (cam->width/scale_factor * cam->height/scale_factor));
   }
   else
   {
      int new_width = cam->width / scale_factor;
      int new_height = cam->height /scale_factor;

      if (cam->color == 1)    nb_band = 3;
      else nb_band = 1;

      /* Scaling and color/B&W conversion */ 
      for (j = 0; j < new_height; j++)
      {
         for (jj = 0; jj < new_width; jj++)
         {
            val2 = 0;
            for (c = 0; c < nb_band; c++)
            {
               val = 0;
               for (k = 0; k < (int) scale_factor; k++)
               {
                  for (l = 0; l < (int) scale_factor; l++)
                  {
                     val += cam->img_buff[j * cam->width * nb_band * (int) scale_factor + jj * (int) scale_factor * nb_band + cam->width * k * nb_band + l * nb_band + c];
                  }
               }

               if (cam->color == color_wanted)
               {
                  /* Rescaling only */
                  im_tab_ext[j * new_width * nb_band + jj * nb_band + c] = (unsigned char) (val / scale_factor / scale_factor);
               }
               else
               {
                  val2 += val;
               }
            }

            if (cam->color != color_wanted)
            {
               /* Conversion from color to black and white */
               im_tab_ext[j * new_width + jj] = (unsigned char) (val2 / scale_factor / scale_factor / 3);
            }
         }
      }
   }

   dprints("camera_grabimage_softscaling: Scaling done.\n");
   pthread_mutex_unlock(&(cam->mutex_grab_image));
}


/* Function used for threaded cameras */
int camera_grabimage_thread(Camera * cam)
{
   int value; 

   while (cam->enabled == 1)
   {      
      dprints("camera_grabimage_thread: new capture for cam %s\n", cam->name);
      /* the thread constantly updates the image buffer */
      camera_capture_image(cam);

      /* We use a binary semaphore to notify the presence of a new image */
      sem_getvalue(&(cam->sem), &value); 
      if (value == 0) sem_post(&(cam->sem));
   }

   dprints("camera_grabimage_thread: end of thread for cam %s\n", cam->name);
   return 1;
}



/* Generic function called by promethe to obtain a camera image */
void camera_grabimage(Camera * cam, unsigned char *im_capture,
      int color_wanted, float scale_factor, int hardscale)
{
   if (cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO || cam->camera_type == USE_V4L2)
   {
      if (hardscale == 0)
         camera_grabimage_softscaling(cam, im_capture, color_wanted, scale_factor);
      else
         camera_grabimage_hardscaling(cam, im_capture, color_wanted, scale_factor);
   }
   else if (cam->camera_type == USE_CLIENTHW)
   {
      char commande[10000];
      ClientHW *chw;

      dprints("debut compass_server_read\n");
      chw = clientHW_get_clientHW_by_name(cam->clientHW);
      sprintf(commande, "camera_grabimage %s %d %f", cam->name, color_wanted, scale_factor);
      clientHW_transmit_video(chw, commande, im_capture);
   }
   else if (cam->camera_type == USE_FIREWIRE)    /*firewire */
   {
      if (hardscale == 0)
         camera_grabimage_softscaling(cam, im_capture, color_wanted, scale_factor);
      else
      {
         PRINT_WARNING("Hardware scaling is not supported on firewire cameras yet -> using software scaling");
         camera_grabimage_softscaling(cam, im_capture, color_wanted, scale_factor);
      }
   }
   else if (cam->camera_type == USE_PLAYER)    /*firewire */
   {
#ifdef PLAYER
      camera_grabimage_softscaling(cam, im_capture, color_wanted, scale_factor);
#else
      EXIT_ON_ERROR("les api player ne sont pas compile");
#endif
   }
   else if (cam->camera_type == USE_CAM_AIBO)	/* Camera Aibo */
   {
      char			commande[255];
      unsigned char			temp[128000];
      int size_out = 0;

#ifdef DEBUG
      printf("Debut de la capture de la camera aibo\n");
#endif

      cam->updated = 0;
      memset(commande, 0, 255);
      /* Envoyer la requete pour recuperer une image de la camera */
      snprintf(commande, 255, "val_camera_%s:%s,\n", cam->name, cam->name);

#ifdef DEBUG
      printf("%s", commande);

      printf("Envoie de la requete pour la capture de la camera aibo\n");
#endif

      aibo_send_message_camera(cam, commande, strlen(commande));

      /* En attente que l'image ait ete mise a jour */

#ifdef DEBUG
      printf("En attente de la capture de la camera aibo\n");
#endif

      /*       while (cam->updated != 1) */
      /* 	{ */
      /* 	  ts.tv_sec = 0; */
      /* 	  ts.tv_nsec = 5000000; */
      /* 	  nanosleep(&ts, NULL); */
      /* 	} */
      sem_wait(&(cam->sem));

#ifdef DEBUG
      printf("Image mise a jour de la camera aibo\n");
#endif

      memset(temp, 0, 128000);
      if (cam->format == 0) /* YCBCR  ???*/
      {
         size_out = cam->width * cam->height * 3;
         convertYCrCbtoRGB((char *)cam->grab_buf, cam->size, (char *)temp);
      }
      else if (cam->format == 1) /* JPEG */
      {
         size_out = cam->width * cam->height * 3;
         convertJPEGtoRGB((char *)cam->grab_buf, cam->size, (char *)temp, &size_out);
      }

      /* Niveaux de gris */
      if (color_wanted == 0)
      {
         int n = size_out / 3;
         int i = 0;

         for  (i = 0; i < n; i++)
         {
            im_capture[i] = (unsigned char) ( ( (int)(temp[3 * i] + temp[(3 * i) + 1] + temp[(3 * i) + 2]) / 3 ) );
         }
      }
      /* Couleur RGB */
      else  memcpy(im_capture, temp, cam->width * cam->height * 3);

   }
   else if (cam->camera_type == USE_CAM_NIOS)	/* Camera NIOS */
   {
      unsigned short int			commande;
//    int size_out = 0;

#ifdef DEBUG
      printf("Debut de la capture de la camera nios\n");
#endif

      cam->updated = 0;
      commande=1;
      /*memset(commande, 0, 255);
      Envoyer la requete pour recuperer une image de la camera
      snprintf(commande, 255, "%d",1);

       */

#ifdef DEBUG
      printf("%d", commande);

      printf("Envoie de la requete pour la capture de la camera Nios\n");
#endif


      nios_send_message_camera(cam, (char *)&commande, /*strlen(commande)*/sizeof (unsigned short int));

      /* En attente que l'image ait ete mise a jour */

#ifdef DEBUG
      printf("En attente de la capture de la camera Nios\n");
#endif

      /*       while (cam->updated != 1) */
      /* 	{ */
      /* 	  ts.tv_sec = 0; */
      /* 	  ts.tv_nsec = 5000000; */
      /* 	  nanosleep(&ts, NULL); */
      /* 	} */
      sem_wait(&(cam->sem));

#ifdef DEBUG
      printf("Image mise a jour de la camera Nios\n");
#endif


      /*
      size_out = cam->width * cam->height ;
	  convertJPEGtoRGB((char *)cam->grab_buf, cam->size, (char *)temp, &size_out);*/


      /* Niveaux de gris */


      memcpy(im_capture, (char *)cam->grab_buf, cam->width * cam->height );

   }
   else if (cam->camera_type == USE_CAM_WEBOTS)    /* Webots */
   {
      camera_grabimage_softscaling(cam, im_capture, color_wanted, scale_factor);
   }
}


