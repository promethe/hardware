/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Camera.h>

#include <net_message_debug_dist.h>

/*****************************************************************
 *********** Feature setup functions for V4L2 cameras ************
 ****************************************************************/

#ifdef Linux
int V4L2_get_feature_value(Camera * cam, int feature, unsigned int *value)
{
   (void)cam;
   (void)feature;
   (void)value;
   return 0;
}

int V4L2_get_feature_boundaries(Camera * cam, int feature, unsigned int *min, unsigned int *max)
{
   struct v4l2_queryctrl queryctrl;

   memset (&queryctrl, 0, sizeof (queryctrl));
   queryctrl.id = feature;
  	
   if (-1 == ioctl (cam->desc, VIDIOC_QUERYCTRL, &queryctrl))
   {
		return 1;
   }
   
   if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) return 1;

   *min =  queryctrl.minimum; 
   *max =  queryctrl.maximum;	
   

   return 0;
}
#endif



/*****************************************************************
 ********* Feature setup functions for firewire cameras **********
 ****************************************************************/

#ifdef FIREWIRE
int firewire_get_feature_value(Camera * cam, dc1394feature_t feature, unsigned int *value)
{
   dc1394error_t err;

   err = dc1394_feature_get_value(cam->cam_firewire, feature, value);
   if (err != DC1394_SUCCESS)
   {
      PRINT_WARNING("Could not set value for feature %i", feature);
      return err;
   }
   else
   {
      return 0;
   }
}


int firewire_get_feature_boundaries(Camera * cam, dc1394feature_t feature, unsigned int *min, unsigned int *max)
{
   dc1394error_t err;
   err=dc1394_feature_get_boundaries(cam->cam_firewire, feature, min,max); 	

   if (err != DC1394_SUCCESS)
   {
      PRINT_WARNING("Could not set value for feature %i", feature);
      return err;
   }
   else
   {
      return 0;
   }
}
#endif



/*****************************************************************
 *************** Generic feature setup functions *****************
 ****************************************************************/

/********* Exposure ***********/
int camera_get_exposure_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_EXPOSURE, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_EXPOSURE, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}

int camera_get_exposure_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, cam->exposure_mode , min, max);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_EXPOSURE, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}

/********* Gain ***********/
int camera_get_gain_value(Camera * cam, unsigned int * value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_GAIN, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_GAIN, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for gain control");
      return 1;
   }
   return 0;
}

int camera_get_gain_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_GAIN, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_GAIN, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}



/********* Brightness ***********/
int camera_get_brightness_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_BRIGHTNESS, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_BRIGHTNESS, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for brightness control");
      return 1;
   }
   return 0;
}

int camera_get_brightness_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_BRIGHTNESS, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {  
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_BRIGHTNESS, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}



/********* Sharpness ***********/
int camera_get_sharpness_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_SHARPNESS, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_SHARPNESS, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for sharpness control");
      return 1;
   }
   return 0;
}

int camera_get_sharpness_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_SHARPNESS, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_SHARPNESS, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


/********* White_Balance ***********/
int camera_get_white_balance_values(Camera * cam, unsigned int *BU, unsigned int *RV)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_AUTO_WHITE_BALANCE, 0) +
            V4L2_get_feature_value(cam, V4L2_CID_RED_BALANCE, RV) +
            V4L2_get_feature_value(cam, V4L2_CID_BLUE_BALANCE, BU);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      dc1394error_t err;

      err = dc1394_feature_whitebalance_get_value(cam->cam_firewire, BU, RV);
      DC1394_ERR_CLN_RTN(err, cleanup_and_exit(cam->cam_firewire), "Could not adjust feature value");
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for white_balance control");
      return 1;
   }
   return 0;
}

int camera_get_white_balance_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return 0; 
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam,DC1394_FEATURE_WHITE_BALANCE, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}

/********* Hue ***********/
int camera_get_hue_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_HUE, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_HUE, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for hue control");
      return 1;
   }
   return 0;
}

int camera_get_hue_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_HUE, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {     
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_HUE, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


/********* Saturation ***********/
int camera_get_saturation_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_SATURATION, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_SATURATION, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for saturation control");
      return 1;
   }
   return 0;
}

int camera_get_saturation_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_SATURATION, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_SATURATION, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


/********* Gamma ***********/
int camera_get_gamma_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_value(cam, V4L2_CID_GAMMA, value);
#endif
   } 
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_GAMMA, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for gamma control");
      return 1;
   }
   return 0;
}

int camera_get_gamma_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      return V4L2_get_feature_boundaries(cam, V4L2_CID_GAMMA, min, max);
#endif
   }
   else  if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_GAMMA, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


/********* Shutter ***********/
int camera_get_shutter_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_SHUTTER, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for shutter control");
      return 1;
   }
   return 0;
}


int camera_get_shutter_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_SHUTTER, min, max);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


/********* Iris ***********/
int camera_get_iris_value(Camera * cam, unsigned int *value)
{
   if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_value(cam, DC1394_FEATURE_IRIS, value);
#endif
   }
   else
   {
      PRINT_WARNING("camera type not supported for iris control");
      return 1;
   }
   return 0;
}

int camera_get_iris_boundaries(Camera * cam, unsigned int *min, unsigned int *max)
{
   if (cam->camera_type == USE_FIREWIRE)
   {
#ifdef FIREWIRE
      return firewire_get_feature_boundaries(cam, DC1394_FEATURE_IRIS, min, max);
#endif
   }  
   else
   {
      PRINT_WARNING("camera type not supported for exposure control");
      return 1;
   }
   return 0;
}


