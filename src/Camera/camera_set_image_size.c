/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Camera.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DEBUG

#include <net_message_debug_dist.h>

void camera_set_image_size(Camera * cam, int width, int height)
{
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      int default_width, default_height;
      int choice;
      
      memset(&cam->v4l2_fmt, 0, sizeof(cam->v4l2_fmt));
      cam->v4l2_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    
      if (ioctl(cam->desc, VIDIOC_G_FMT, &(cam->v4l2_fmt)) == -1)
      {
	 perror("ioctl (VIDIOC_G_FMT)");
	 exit(1);
      }
      
      /* Saves previous image size */
      default_width = cam->v4l2_fmt.fmt.pix.width;
      default_height = cam->v4l2_fmt.fmt.pix.height;
      
      dprints("camera_init_capture: Image size returned by G_FMT = %dx%d  %d\n", default_width, default_height, cam->v4l2_fmt.fmt.pix.sizeimage);

      if ((width > 0 || height > 0) && (width != default_width || height != default_height))
      {

	 dprints("camera_set_image_size: Trying to set the image size to %dx%d\n", width, height);
	 cam->v4l2_fmt.fmt.pix.width = width;
	 cam->v4l2_fmt.fmt.pix.height = height;

	 /* Tries to see if the settings can be applied without actually changing them */
	 if (ioctl(cam->desc, VIDIOC_TRY_FMT, &cam->v4l2_fmt) == -1)
	 {
	    perror("ioctl (VIDIOC_TRY_FMT)");
	    exit(1);
	 }
	   
	 dprints("camera_set_image_size: Image size returned by TRY_FMT = %dx%d \n",
	 cam->v4l2_fmt.fmt.pix.width, cam->v4l2_fmt.fmt.pix.height);
	   
	 if ((int)cam->v4l2_fmt.fmt.pix.width == width && (int)cam->v4l2_fmt.fmt.pix.height == height)
	 {
	    /* Image size ok */
	    dprints("camera_set_image_size: Image size %dx%d accepted by the camera\n",width, height);
	 }
	 else if ((int)cam->v4l2_fmt.fmt.pix.width == default_width && (int)cam->v4l2_fmt.fmt.pix.height == default_height)
	 {
	    /* Image size not accepted, TRY_FMT returned the previous size */
	    PRINT_WARNING("Setting image size to %dx%d failed -> keeping default settings (%dx%d)", width, height, default_width, default_height);
	 }
	 else
	 {
	    cprints("camera_set_image_size: Failed to set image size to %dx%d\n",width, height);
	    cprints("Image size proposed by the camera is %dx%d\n", cam->v4l2_fmt.fmt.pix.width, cam->v4l2_fmt.fmt.pix.height);
	    cprints("Enter a number to choose an option:\n");
	    cprints("1 - Keep previous image size (%dx%d):\n",default_width, default_height);
	    cprints("2 - Change to proposed image size (%dx%d):\n", cam->v4l2_fmt.fmt.pix.width, cam->v4l2_fmt.fmt.pix.height);
	    
	    do
	    {
	       cscans("%d", &choice);
	    } while (choice != 1 && choice != 2);
	    
	    if (choice == 1)
	    {
	       cam->v4l2_fmt.fmt.pix.width = default_width;
	       cam->v4l2_fmt.fmt.pix.height = default_height;
	    }
	 }

	 /* Setting previously configured format on the camera */
	 if (ioctl(cam->desc, VIDIOC_S_FMT, &cam->v4l2_fmt) == -1)
	 {
	    if (errno == EBUSY)
	    {
	       fprintf(stderr, "ERROR in camera_set_image_size: Device is busy.\nHardscaling is not available for this camera with v4l2\nMost of the v4l2 cameras don't support changes in the format used while streaming. For the image size to be changed we would have to stop the streaming/reallocate the buffers/close the device depending on the camera. Hardscaling shouldn't be used anyway. If you want to control the size of the image on the hardware side, specify the correct size in the hardware file of the camera and you're done.\n");
	       exit(1);
	    }
	    else
	    {
	       perror("ioctl (VIDIOC_S_FMT)");
	       exit(1);
	    }
	 }
      }

      cam->width = cam->v4l2_fmt.fmt.pix.width;
      cam->height = cam->v4l2_fmt.fmt.pix.height;
#else
	printf("Video pour linux n'est pas defini sur ce systeme %s : %d\n",__FUNCTION__, __LINE__);
#endif
   }
   else
   {
      cam->width = width;
      cam->height = height;
   }
}
