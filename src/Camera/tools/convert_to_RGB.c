/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  convert_to_RGB.c
\brief traite les messages de retour du serveur URBI

Author: A.HIOLLE
Created: 14/04/2005
Modified:
- author: A.HIOLLE
- description: specific file creation
- date: 11/08/2004

Modified : 
- author: Pierre Delarboulas
- description: add Bayer to RGB conversion
- date: 13/05/2014


Thanks to Paulo Assis <pj.assis@gmail.com> for the conversion Bayer to RGB

Theoritical description:
 - \f$  LaTeX equation: none \f$

Description:  aibo_image tool convert image to rgb


Macro:
-none

Local variables:
-none

Global variables:
-none

Internal Tools:
-none

External Tools:
-
Links:
- type: algo
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs:

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jerror.h>
#include <jpeglib.h>
#include <setjmp.h>

//#include <glib.h>
//#include "colorspaces.h"
//#include "v4l2uvc.h"

#include <inttypes.h>

#define SAT(c) if (c & (~255)) { if (c < 0) c = 0; else c = 255; }

typedef struct mem_source_mgr
{
  struct jpeg_source_mgr pub;
  JOCTET eoi[2];
}mem_source_mgr;


struct my_error_mgr {
  struct jpeg_error_mgr pub;	/* "public" fields */

  jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;


static void init_source(j_decompress_ptr cinfo)
{
}

METHODDEF(boolean) fill_input_buffer(j_decompress_ptr cinfo)
{
  mem_source_mgr *src = (mem_source_mgr *) cinfo->src;
  if (src->pub.bytes_in_buffer != 0)
    return TRUE;
  src->eoi[0] = 0xFF;
  src->eoi[1] = JPEG_EOI;
  src->pub.bytes_in_buffer = 2;
  src->pub.next_input_byte = src->eoi;
  return TRUE;
}

METHODDEF(void) term_source(j_decompress_ptr cinfo)
{
}

METHODDEF(void) skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{
  mem_source_mgr *src = (mem_source_mgr *) cinfo->src;
  if (num_bytes <= 0)
    return;
  if (num_bytes > src->pub.bytes_in_buffer)
    num_bytes = src->pub.bytes_in_buffer;
  src->pub.bytes_in_buffer -= num_bytes;
  src->pub.next_input_byte += num_bytes;
}



/*static char clamp(float v)
{
 if (v < 0.)
    return 0;
  if (v > 255.0)
    return (unsigned char) 255;
  return (unsigned char) v;

  }*/

static inline int clamp_fast(int v)/*va avec  convertYCrCbtoRGB*/
{
	if(v<0)
		return 0;
	if(v>261120)/*les valers sont trouves apres des decalages binaires*/
		return 261120;
	return v;
}

/*
 * Here's the routine that will replace the standard error_exit method:
 */


METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message) (cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}

/*! Convert a jpeg image to YCrCb or RGB. Allocate the buffer with malloc.
 */
static void *read_jpeg(char *jpgbuffer, int jpgbuffer_size, int RGB, 
                       int *output_size)  /* ancien int &output_size... */
{
  struct jpeg_decompress_struct cinfo;
  struct my_error_mgr jerr; 
  mem_source_mgr *source;
  /*  struct jpeg_source_mgr *source;*/
  void *buffer;

  /* Initialize the JPEG decompression object with default error handling. */
#ifdef DEBUG
   printf("read_jpeg \n");
#endif

  /* We set up the normal JPEG error routines, then override error_exit. */
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = my_error_exit;
  /* Establish the setjmp return context for my_error_exit to use. */
  if (setjmp(jerr.setjmp_buffer)) {
    /* If we get here, the JPEG code has signaled an error.
     * We need to clean up the JPEG object, close the input file, and return.
     */
    printf("JPEG internal error (read_jpeg / convert_to_RGB.c)\n");
    jpeg_destroy_decompress(&cinfo);
    return 0;
  }
  /* Now we can initialize the JPEG decompression object. */
  jpeg_create_decompress(&cinfo);


  source = (struct mem_source_mgr *)
    (*cinfo.mem->alloc_small) ((j_common_ptr) & cinfo, JPOOL_PERMANENT,
			       sizeof(mem_source_mgr));

  cinfo.src = (struct jpeg_source_mgr *) source;
  source->pub.skip_input_data = skip_input_data;
  source->pub.term_source = term_source;
  source->pub.init_source = init_source;
  source->pub.fill_input_buffer = fill_input_buffer;
  source->pub.resync_to_restart = jpeg_resync_to_restart;
  source->pub.bytes_in_buffer = jpgbuffer_size;
  source->pub.next_input_byte = (JOCTET *) jpgbuffer;
  cinfo.out_color_space = (RGB ? JCS_RGB : JCS_YCbCr);


  jpeg_read_header(&cinfo, TRUE);
  cinfo.out_color_space = (RGB ? JCS_RGB : JCS_YCbCr);

  jpeg_start_decompress(&cinfo);
  *output_size =  cinfo.output_width * cinfo.output_components * cinfo.output_height;
  /*  printf("output size= %d width=%d height=%d nb component= %d \n",
   *output_size, cinfo.output_width,  cinfo.output_height, cinfo.output_components);*/
  buffer = malloc(*output_size);

  while (cinfo.output_scanline < cinfo.output_height) {
    /* jpeg_read_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could ask for
     * more than one scanline at a time if that's more convenient.
     */
    JSAMPROW row_pointer[1];
    row_pointer[0] = (JOCTET *) & ((char *) buffer)[cinfo.output_scanline   * 
                                                    cinfo.output_components * 
                                                    cinfo.output_width];
    jpeg_read_scanlines(&cinfo, row_pointer, 1);
  }

  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  return buffer;
}


/*M.M. : ce code la va 60 fois plus vite sur le routeur... evite les floatants*/
int  convertYCrCbtoRGB(char * sourceImage, 
                  int bufferSize, 
                  char * destinationImage)
{
  unsigned char *in = (unsigned char *) sourceImage;
  unsigned char *out = (unsigned char *) destinationImage;
/*  float y ;
  float cb ;
  float cr ;
  int i=0;
  
  
  for ( i = 0; i < bufferSize - 3; i += 3) {
    
    y = in[i];
    cb = in[i + 1];
    cr = in[i + 2];
    
    out[i] = clamp(1.164 * (y - 16) + 1.596 * (cr - 128));
    out[i + 1] = clamp(1.164 * (y - 16) - 0.813 * (cr - 128) - 
                       0.392 * (cb - 128));
    out[i + 2] = clamp(1.164 * (y - 16) + 2.017 * (cb - 128)); 
  }*/
  
  int y ;/*Mickael : code equivalent a celui en commentaire mais calculs en virgule fixe (et non floatant) avec decalage binaire -> gain de 30 pour le routeur+aibo*/
  int cb ;
  int cr ;
  int i=0;
  
  
  for ( i = 0; i < bufferSize - 3; i += 3) {
    
    y = in[i];
    cb = in[i + 1];
    cr = in[i + 2];
    
    out[i] = (unsigned char)(clamp_fast( (1192 * (y - 16) + 1634 * (cr - 128)) ) >> 10);
    out[i + 1] = (unsigned char)(clamp_fast( (1192 * (y - 16) - 833 * (cr - 128) - 401 * (cb - 128)) ) >> 10);
    out[i + 2] = (unsigned char)(clamp_fast( (1192 * (y - 16) + 2065 * (cb - 128)) ) >> 10); 
  }
  
  return 1;
}

int 
convertJPEGtoYCrCb(char *source, int sourcelen, char *dest, 
                       int *size)
{
  int sz;
  int cplen;
  void *destination;

  destination = read_jpeg((char *) source, sourcelen, 0, &sz);
  if (!destination) {
    *size = 0;
    return 0;
  }
  
  cplen = sz > (*size) ? (*size) : sz;
  memcpy(dest, destination, cplen);
  free(destination);
  (*size) = sz;
  return 1;
}

int convertJPEGtoRGB(char *source, int sourcelen, char * dest, int *size)
{
  int sz;
  int cplen;
  void *destination;
#ifdef DEBUG
  printf("JPEGtoRGB taille de l'image d'entree = %d \n",sourcelen);
#endif
  destination = read_jpeg(source, sourcelen, 1, &sz);
  if (!destination) {
    (*size) = 0;
    return 0;
  }
  
  cplen = sz > (*size) ? (*size) : sz;
  memcpy(dest, destination, cplen);
  free(destination);
  (*size) = sz;
  return 1;
}

void switch_rb(unsigned char *img, int width, int height)
{
    int i;
    unsigned char tmp;

    for (i = 0; i < width * height * 3; i += 3)
    {
        tmp = img[i];
        img[i] = img[i + 2];
        img[i + 2] = tmp;
    }
}

void yuv420p_to_rgb24(unsigned char *img, int width, int height)
{
    int line, col, linewidth;
    int y, u, v, yy, vr, ug, vg, ub;
    int r, g, b;
    const unsigned char *py, *pu, *pv;
    char *src, *dst;

    src = (char *) malloc(width * height * 3 / 2);
    if (src == NULL)
    {
        printf("erreur malloc: internal_yuv420p2rgb24\n");
        exit(0);
    }
    memcpy(src, img, width * height * 3 / 2);
    dst = (char *) img;

    linewidth = width >> 1;
    py = (unsigned char *) src;
    pu = py + (width * height);
    pv = pu + (width * height) / 4;

    y = *py++;
    yy = y << 8;
    u = *pu - 128;
    ug = 88 * u;
    ub = 454 * u;
    v = *pv - 128;
    vg = 183 * v;
    vr = 359 * v;

    for (line = 0; line < height; line++)
    {
        for (col = 0; col < width; col++)
        {
            r = (yy + vr) >> 8;
            g = (yy - ug - vg) >> 8;
            b = (yy + ub) >> 8;

            if (r < 0)
                r = 0;
            if (r > 255)
                r = 255;
            if (g < 0)
                g = 0;
            if (g > 255)
                g = 255;
            if (b < 0)
                b = 0;
            if (b > 255)
                b = 255;

            *dst++ = r;
            *dst++ = g;
            *dst++ = b;

            y = *py++;
            yy = y << 8;
            if (col & 1)
            {
                pu++;
                pv++;

                u = *pu - 128;
                ug = 88 * u;
                ub = 454 * u;
                v = *pv - 128;
                vg = 183 * v;
                vr = 359 * v;
            }
        }                       /* ..for col */
        if ((line & 1) == 0)
        {                       /* even line: rewind */
            pu -= linewidth;
            pv -= linewidth;
        }
    }                           /* ..for line */
    free(src);
}

void yuv422_to_rgb24(unsigned char *img, int width, int height)
{
   unsigned char *s;
   unsigned char *src;
   unsigned char *d;
   int l, c, i;
   int r, g, b, cr, cg, cb, y1, y2;

   s = (unsigned char *) malloc(width * height * 2);
   if (s == NULL)
   {
      fprintf(stderr, "ERROR in yuyv_to_rgb24: malloc for s failed\n");
      exit(1);
   }
   memcpy(s, img, width * height * 2);

   src = s;
   l = height;
   d = img;
   i = 0;

   while (l--) {
      c = width >> 1;
      while (c--) {
         y1 = *s++;
         cb = ((*s  - 128) * 454) >> 8;
         cg = (*s++ - 128) * 88;
         y2 = *s++;
         cr = ((*s  - 128) * 359) >> 8;
         cg = (cg + (*s++ - 128) * 183) >> 8;

         i++;
         if(i%3==2||1){
         r = y1 + cr;
         b = y1 + cb;
         g = y1 - cg;
         }else r=g=b=0;
         SAT(r);
         SAT(g);
         SAT(b);

         *d++ = r;
         *d++ = g;
         *d++ = b;

         if(i%3==2 ||1){
         r = y2 + cr;
         b = y2 + cb;
         g = y2 - cg;
         }else r=g=b=0;
         SAT(r);
         SAT(g);
         SAT(b);

         *d++ = r;
         *d++ = g;
         *d++ = b;
     }
   }
   free(src);
}

void convert_border_bayer_line_to_bgr24(uint8_t * bayer, uint8_t* adjacent_bayer, uint8_t *bgr, int width, int start_with_green, int blue_line)
{
	int t0, t1;

	if (start_with_green) 
	{
	/* First pixel */
		if (blue_line) 
		{
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
			*bgr++ = adjacent_bayer[0];
		} 
		else 
		{
			*bgr++ = adjacent_bayer[0];
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
		}
		/* Second pixel */
		t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
		t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line) 
		{
			*bgr++ = bayer[1];
			*bgr++ = t0;
			*bgr++ = t1;
		} 
		else 
		{
			*bgr++ = t1;
			*bgr++ = t0;
			*bgr++ = bayer[1];
		}
		bayer++;
		adjacent_bayer++;
		width -= 2;
	} 
	else 
	{
		/* First pixel */
		t0 = (bayer[1] + adjacent_bayer[0] + 1) >> 1;
		if (blue_line) 
		{
			*bgr++ = bayer[0];
			*bgr++ = t0;
			*bgr++ = adjacent_bayer[1];
		} 
		else 
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = t0;
			*bgr++ = bayer[0];
		}
		width--;
	}

	if (blue_line) 
	{
		for ( ; width > 2; width -= 2) 
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = bayer[1];
			*bgr++ = t0;
			*bgr++ = t1;
			bayer++;
			adjacent_bayer++;
		}
	} 
	else 
	{
		for ( ; width > 2; width -= 2) 
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = t0;
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = t1;
			*bgr++ = t0;
			*bgr++ = bayer[1];
			bayer++;
			adjacent_bayer++;
		}
	}

	if (width == 2) 
	{
		/* Second to last pixel */
		t0 = (bayer[0] + bayer[2] + 1) >> 1;
		if (blue_line) 
		{
			*bgr++ = t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
		} 
		else 
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = t0;
		}
		/* Last pixel */
		t0 = (bayer[1] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line) 
		{
			*bgr++ = bayer[2];
			*bgr++ = t0;
			*bgr++ = adjacent_bayer[1];
		}
		else 
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = t0;
			*bgr++ = bayer[2];
		}
	} 
	else 
	{
		/* Last pixel */
		if (blue_line) 
		{
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
		} 
		else 
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
		}
	}
}

void bayer_to_rgbbgr24(uint8_t *bayer, uint8_t *bgr, int width, int height,int start_with_green, int blue_line)
{
	/* render the first line */
	convert_border_bayer_line_to_bgr24(bayer, bayer + width, bgr, width,
		start_with_green, blue_line);
	bgr += width * 3;

	/* reduce height by 2 because of the special case top/bottom line */
	for (height -= 2; height; height--) 
	{
		int t0, t1;
		/* (width - 2) because of the border */
		uint8_t *bayerEnd = bayer + (width - 2);

		if (start_with_green) 
		{
			/* OpenCV has a bug in the next line, which was
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1; */
			t0 = (bayer[1] + bayer[width * 2 + 1] + 1) >> 1;
			/* Write first pixel */
			t1 = (bayer[0] + bayer[width * 2] + bayer[width + 1] + 1) / 3;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = t1;
				*bgr++ = bayer[width];
			} 
			else 
			{
				*bgr++ = bayer[width];
				*bgr++ = t1;
				*bgr++ = t0;
			}

			/* Write second pixel */
			t1 = (bayer[width] + bayer[width + 2] + 1) >> 1;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = bayer[width + 1];
				*bgr++ = t1;
			} 
			else 
			{
				*bgr++ = t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = t0;
			}
			bayer++;
		} 
		else 
		{
			/* Write first pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = bayer[width];
				*bgr++ = bayer[width + 1];
			} 
			else 
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width];
				*bgr++ = t0;
			}
		}

		if (blue_line) 
		{
			for (; bayer <= bayerEnd - 2; bayer += 2) 
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = t0;
				*bgr++ = t1;
				*bgr++ = bayer[width + 1];

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = t1;
			}
		} 
		else 
		{
			for (; bayer <= bayerEnd - 2; bayer += 2) 
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = bayer[width + 1];
				*bgr++ = t1;
				*bgr++ = t0;

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = t1;
				*bgr++ = bayer[width + 2];
				*bgr++ = t0;
			}
		}

		if (bayer < bayerEnd) 
		{
			/* write second to last pixel */
			t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
				bayer[width * 2 + 2] + 2) >> 2;
			t1 = (bayer[1] + bayer[width] +
				bayer[width + 2] + bayer[width * 2 + 1] +
				2) >> 2;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = t1;
				*bgr++ = bayer[width + 1];
			} 
			else 
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = t1;
				*bgr++ = t0;
			}
			/* write last pixel */
			t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = bayer[width + 1];
			} 
			else 
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width + 2];
				*bgr++ = t0;
			}
			bayer++;
		} 
		else
		{
			/* write last pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			t1 = (bayer[1] + bayer[width * 2 + 1] + bayer[width] + 1) / 3;
			if (blue_line) 
			{
				*bgr++ = t0;
				*bgr++ = t1;
				*bgr++ = bayer[width + 1];
			} 
			else 
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = t1;
				*bgr++ = t0;
			}
		}

		/* skip 2 border pixels */
		bayer += 2;

		blue_line = !blue_line;
		start_with_green = !start_with_green;
	}

	/* render the last line */
	convert_border_bayer_line_to_bgr24(bayer + width, bayer, bgr, width,
		!start_with_green, !blue_line);
}

/*convert bayer raw data to rgb24
* args: 
*      pBay: pointer to buffer containing Raw bayer data data
*      pRGB24: pointer to buffer containing rgb24 data
*      width: picture width
*      height: picture height
*      pix_order: bayer pixel order (0=gb/rg   1=gr/bg  2=bg/gr  3=rg/bg)
*/
void bayer_to_rgb24(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order)
{
	switch (pix_order) 
	{
		//conversion functions are build for bgr, by switching b and r lines we get rgb
		case 0: /* gbgbgb... | rgrgrg... (V4L2_PIX_FMT_SGBRG8)*/
			bayer_to_rgbbgr24(pBay, pRGB24, width, height, 1, 0);
			break;
		
		case 1: /* grgrgr... | bgbgbg... (V4L2_PIX_FMT_SGRBG8)*/
			bayer_to_rgbbgr24(pBay, pRGB24, width, height, 1, 1);
			break;
		
		case 2: /* bgbgbg... | grgrgr... (V4L2_PIX_FMT_SBGGR8)*/
			bayer_to_rgbbgr24(pBay, pRGB24, width, height, 0, 0);
			break;
		
		case 3: /* rgrgrg... ! gbgbgb... (V4L2_PIX_FMT_SRGGB8)*/
			bayer_to_rgbbgr24(pBay, pRGB24, width, height, 0, 1);
			break;
			
		default: /* default is 0*/
			bayer_to_rgbbgr24(pBay, pRGB24, width, height, 1, 0);
			break;
	}
}

/*fonction de balance des couleurs et des blancs*/
void flip_bgr_image(unsigned char *bgr_buff, int width, int height)
{
    int i =0, j = 0;
    float v;
    /*alloc a temp buffer*/
    unsigned char *tmp_buff = calloc(width*height*3, sizeof(unsigned char));

    int p;
    float K,m00,m01,m02,m10,m11,m12,m20,m21,m22;
		K = -1;
		m00 = 0.299 + 0.701*K;
		m01 = 0.587 * (1-K);
		m02 = 0.114 * (1-K); 
		m10 = 0.299 * (1-K);

		m11 = 0.587 + 0.413*K;
		m12 = 0.114 * (1-K) ;
		m20 = 0.299 * (1-K);
		m21 = 0.587 * (1-K);
		m22 = 0.114 + 0.886*K;

    for(i=0;i<height;i++)
    {
			for(j=0;j<width;j++)
			{
				p = (j+i*width)*3;
			  tmp_buff[p] = (unsigned char) (( m00 * bgr_buff[p] + m01 * bgr_buff[p+1] + m02 * bgr_buff[p+2] ));
			  tmp_buff[p+1] = (unsigned char) (( m10 * bgr_buff[p] + m11 * bgr_buff[p+1] + m12 * bgr_buff[p+2] ));	
			  tmp_buff[p+2] = (unsigned char) (( m20 * bgr_buff[p] + m21 * bgr_buff[p+1] + m22 * bgr_buff[p+2] ));


        /* v = 1. * bgr_buff[p]; //ROUGE
	  if (v<255.) tmp_buff[p]=(unsigned char) v; else tmp_buff[p]=(unsigned char) 255;

          v = .75 * bgr_buff[p+1]; //VERT
	  if (v<255.) tmp_buff[p+1]=(unsigned char) v; else tmp_buff[p+1]=(unsigned char) 255;
          v = 1. * bgr_buff[p+2]; //BLEU
	  if (v<255.) tmp_buff[p+2]=(unsigned char) v; else tmp_buff[p+2]=(unsigned char) 255;*/
			}

   }
    /*get the fliped buffer*/
    memcpy(bgr_buff, tmp_buff, height*width*3);
    free(tmp_buff);
}

