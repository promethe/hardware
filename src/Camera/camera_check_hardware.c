/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Camera.h>
#include <stdio.h>
#include <stdlib.h>
#include <basic_tools.h>

#define IMAGE_FILE_NAME "Image_test.pgm"


int camera_check_hardware(Camera * cam)
{

	int cpt = 0;
	FILE* imagefile;
	unsigned int width = cam->width_max;
	unsigned int height = cam->height_max;
	unsigned char * im_tmp = NULL;
	char nom_image[255];

	if (cam->color == 1)
	{
		im_tmp = (unsigned char *) malloc(3 * width * height * sizeof(char));
	}
	else
	{
		im_tmp = (unsigned char *) malloc(width * height * sizeof(char));
	}

	kprints("Check camera %s ....\n", cam->name);
	while (cpt < 15)
	{
		sprintf(nom_image, "Image_test%d.pgm", cpt);
		imagefile = fopen(nom_image, "wb");

		if (imagefile == NULL) EXIT_ON_SYSTEM_ERROR("Can't create image in camera_check_hardware\n");

		if (cam->color == 1)
		{
			camera_grabimage(cam, im_tmp, 1, 1., 0);
			fprintf(imagefile, "P6\n%u %u 255\n", cam->width, cam->height);
			fwrite(im_tmp, 1, height * width * 3, imagefile);
		}
		else
		{
			camera_grabimage(cam, im_tmp, 0, 1., 0);
			fprintf(imagefile, "P5\n%u %u 255\n", cam->width, cam->height);
			fwrite(im_tmp, 1, height * width, imagefile);
		}

		fprintf(imagefile, "P5\n%u %u 255\n", cam->width, cam->height);
		fwrite(im_tmp, 1, height * width, imagefile);

		fclose(imagefile);
		printf("wrote: " IMAGE_FILE_NAME "%d\n", cpt);
		cpt++;
	}
	free(im_tmp);
	kprints(" .... ok");
	return 0;
}
