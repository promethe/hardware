/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

/*#define DEBUG*/

#include <Camera.h>
#include "tools/include/local_var.h"
#include <net_message_debug_dist.h>

void camera_stop(Camera * cam)
{
   struct timespec duree_nanosleep, res;
   unsigned int i;

   if (cam->threading == 1)
   {
      dprints("camera_stop: Disabling camera %s\n", cam->name);
      /* Sortie propre du thread de capture */
      pthread_mutex_lock(&(cam->mutex_grab_image));
      cam->enabled = 0;
      pthread_mutex_unlock(&(cam->mutex_grab_image));
      
      /* Attend 1 s la fin du thread de capture */
      duree_nanosleep.tv_sec = 1;
      duree_nanosleep.tv_nsec = 0;
      nanosleep(&duree_nanosleep, &res);
   }
   
   if (cam->camera_type == USE_V4L2 || cam->camera_type == USE_CAMERA_CLASSIC || cam->camera_type == USE_CAMPANO)
   {
#ifdef Linux
      enum v4l2_buf_type type;

      type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      
      pthread_mutex_lock(&(cam->mutex_grab_image));
      printf("fermeture de la cam V4L2 %s\n", cam->name);

      if (-1 == ioctl(cam->desc, VIDIOC_STREAMOFF, &type))
      {
	 perror("ioctl(VIDIOC_STREAMOFF)");
	 exit(1);
      }

      for (i = 0; i < cam->req_buf.count; ++i)
	 if (-1 == munmap (cam->buffers[i].start, cam->buffers[i].length))
	 {
	    perror("munmap");
	    exit(1);
	 }

      pthread_mutex_unlock(&(cam->mutex_grab_image));
#else
      printf("Video pour linux n'est pas defini sur ce systeme %s : %d\n",__FUNCTION__, __LINE__);
#endif
   }

#ifdef FIREWIRE
    if (cam->camera_type == USE_FIREWIRE)
    {
       dc1394video_frame_t *p_converted_frame = (dc1394video_frame_t *)cam->converted_frame_firewire;
       /*Le thread etant arret�, on continue la fermeture*/
       pthread_mutex_lock(&(cam->mutex_grab_image));
       printf("fermeture de la cam firewire %s\n", cam->name);
       dc1394_video_set_transmission(cam->cam_firewire, DC1394_OFF);
       dc1394_capture_stop(cam->cam_firewire);
	    
       if (p_converted_frame != NULL)
       {
	  if (p_converted_frame->image != NULL)
	  {
	     free(p_converted_frame->image);		   
	     p_converted_frame->image = NULL;
	  }
	  free(p_converted_frame);		   
	  cam->converted_frame_firewire = NULL;
       }
       dc1394_camera_set_power(cam->cam_firewire, DC1394_OFF);
       
       dc1394_camera_free(cam->cam_firewire);
       pthread_mutex_unlock(&(cam->mutex_grab_image));
    }
#endif /* FIREWIRE */

   if(cam->camera_type != USE_FIREWIRE)
   {
      close(cam->desc);
   }
}

#ifdef FIREWIRE
void cleanup_and_exit(dc1394camera_t *camera)
{
    dc1394_video_set_transmission(camera, DC1394_OFF);
    dc1394_capture_stop(camera);
    dc1394_camera_free(camera);
    exit(1);
}
#endif
