/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Camera.h>

/*#define DEBUG*/

void camera_set_scale_factor(Camera * cam, float scale_p)
{
    int new_width, new_height;
/*    unsigned char *img = NULL;*/

    if (cam->scale_factor < 1.0)
    {
        printf("Error scale_factor=%f\n", cam->scale_factor);
        exit(1);
    }

    /* taille de l'image geree par cam->scale_factor (cam->scale_factor=1 donne w=768 h=576 pixels) */
    cam->scale_factor = scale_p;
    new_width =  (int) (cam->width_max / scale_p);
    new_height =  (int) (cam->height_max / scale_p);

    /* Redefine width and heigth if scale_factor method is used to capture... */
    if (cam->width != new_width || cam->height != new_height)
       camera_set_image_size(cam, new_width, new_height);
    
#ifdef DEBUG
    printf("Size factor is %f, Width:%d, height:%d\n", cam->scale_factor, cam->width, cam->height);
#endif
/*    img = (unsigned char *) malloc(sizeof(char) * cam->width * cam->height * 3);
    if (img == NULL)
    {
        printf("erreur malloc non init: camera_set_scale_factor\n");
        exit(1);
    }
    camera_grabimage_hardscaling(cam, img, 1, scale_p);
      camera_grabimage_hardscaling(cam, img, 1, scale_p);
      free(img);*/
}
