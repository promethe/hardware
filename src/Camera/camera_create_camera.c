/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <Camera.h>
#include <tools.h>
#include <Aibo.h>
#include <Nios.h>
#include "tools/include/macro.h"
#include "tools/include/local_var.h"
#include <basic_tools.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>


void camera_create_camera(Camera ** cam, char *name, char *file_hw)
{

    FILE *fh;
    char lecture[256];
    char tmp[129];
    float tmpf;
    int tmpi, dim_h, dim_w, nrm, frmt,ret;
    char composite[64];
#ifdef FIREWIRE
    dc1394error_t err;
#endif
    strncpy(composite,"Composite1",64);

    *cam = ALLOCATION(Camera);

    memset(*cam, 0, sizeof(Camera));

    (*cam)->name = MANY_ALLOCATIONS(128, char); /*a changer...!!! */
    if (strlen(name) > 127)
	{
		EXIT_ON_ERROR("Name %s too long 128", name);
	}
    strcpy((*cam)->name, name); /*Camera_object name */

    /*Camera_get_info_from_dev_file((*cam)); */

    /* default parameters */
    (*cam)->autocheck = 0;
    (*cam)->camera_type = USE_CAMERA_CLASSIC;
    (*cam)->teta_h = 90.0;
    (*cam)->teta_w = 180.0;
    (*cam)->height = 0;
    (*cam)->width = 0;
    (*cam)->format=0;
    (*cam)->color=1;
    (*cam)->threading=0;
    (*cam)->nb_buffers = 2;
    (*cam)->flush_buffers = 0;

    /* Sensor parameters */
    (*cam)->brightness = -2;
    (*cam)->exposure = -2;
    (*cam)->gain = -2;
    (*cam)->sharpness = -2;
    (*cam)->red_balance = -2;
    (*cam)->blue_balance = -2;
    (*cam)->hue = -2;
    (*cam)->saturation = -2;
    (*cam)->gamma = -2;
    (*cam)->shutter = -2;
    (*cam)->iris = -2;

    /*pour le firewire, par default*/
    (*cam)->addr_bus_firewire = 0;
    (*cam)->video_mode = -1;
    (*cam)->framerate = -1;
    (*cam)->isospeed = -1;

    (*cam)->exposure_mode = 0;


    sem_init(&((*cam)->sem), 1, 0);
    dim_h = -1;
    dim_w = -1;
    nrm = 0;                    /* norm PAL */
    frmt = 1; /* format jpeg */

    /*! loading camera parameters */

    /* get parameters from the hardware file */
    fh = fopen(file_hw, "r");
    if (fh == NULL)
    {
        EXIT_ON_ERROR("le fichier %s n'a pas pu etre ouvert\n", file_hw);
    }

    while (!feof(fh))
    {
        ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */
        str_upper(lecture);

        if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
        {

           (*cam)->port = MANY_ALLOCATIONS(128, char);   /*a changer...!!! */
           ret=fscanf(fh, "%s", (*cam)->port);
        }
        else if (strcmp(lecture, "CLIENTHW") == 0)
        {
            ret=fscanf(fh, "%s", ((*cam)->clientHW));
        }
	else if (strcmp(lecture, "CLIENTROBUBOX") == 0)
        {
            ret=fscanf(fh, "%s", ((*cam)->clientRobubox));
        }
        else if (strcmp(lecture, "AUTOCHECK") == 0)
        {
            ret=fscanf(fh, "%d", &((*cam)->autocheck));
        }
        else if ((strcmp(lecture, "TETA_H") == 0))  /* get the vertical angle view */
        {
            ret=fscanf(fh, "%f\n", &tmpf);
            (*cam)->teta_h = tmpf;
        }
        else if ((strcmp(lecture, "TETA_W") == 0))  /* get the horizontal angle view */
        {
            ret=fscanf(fh, "%f\n", &tmpf);
            (*cam)->teta_w = tmpf;
        }
        else if ((strcmp(lecture, "TYPE") == 0))    /* get the type */
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            (*cam)->camera_type = tmpi;
        }
        else if ((strcmp(lecture, "WIDTH") == 0))   /* get the maximum width */
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            dim_w = tmpi;
        }
        else if ((strcmp(lecture, "HEIGHT") == 0))  /* get the maximum height */
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            dim_h = tmpi;
        }
        else if ((strcmp(lecture, "NORM") == 0))    /* get the camera's norm (norm=1 -> NTSC, else -> PAL) */
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            nrm = tmpi;
        }
        else if ((strcmp(lecture, "COLOR") == 0))    /* get the camera's color mode (1 -> color, 0 -> grey) */
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            (*cam)->color = tmpi;
        }
	/* get the number of buffers for the capture of images */
	/* A large number reduces the risk of losing some frames but
	   increases the lag on captured images */
        else if ((strcmp(lecture, "NB_BUFFERS") == 0))
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            (*cam)->nb_buffers = tmpi;
        }
	/* get the camera's buffer flushing mode (1 -> flush, 0 -> no flush) */
	/* Flushing should be used when the images have to be in real time, each
	   capture corresponding to the latest frame. It can however reduce the
	   framerate of the capture */
        else if ((strcmp(lecture, "FLUSH_BUFFERS") == 0))
        {
            ret=fscanf(fh, "%d\n", &tmpi);
            (*cam)->flush_buffers = tmpi;
        }
	else if( (strcmp(lecture,"FORMAT")==0) ) /* get the camera's format (O-> YCBCR 1-> JPEG) */
	{
	   ret=fscanf(fh,"%d\n",&tmpi);
	   frmt=tmpi;
	}
        else if( (strcmp(lecture,"COMPOSITE")==0) ) /*get the number of the composite input*/
        {
            ret=fscanf(fh,"%d\n",&tmpi);
            snprintf(composite,64,"Composite%d",tmpi);
        }
        else if (strcmp(lecture, "THREADING") == 0)
        {
            ret=fscanf(fh, "%d\n", &((*cam)->threading));
        }
	/*Pour firewire seulement*/
	else if (strcmp(lecture, "ADDR_BUS_FIREWIRE") == 0)
        {
            ret=fscanf(fh, "%d\n", &((*cam)->addr_bus_firewire));
        }
	else if (strcmp(lecture, "VIDEO_MODE") == 0)
        {
            ret=fscanf(fh, "%d\n", &((*cam)->video_mode));
        }
	else if (strcmp(lecture, "FRAMERATE") == 0)
        {
            ret=fscanf(fh, "%d\n", &((*cam)->framerate));
        }
	else if (strcmp(lecture, "ISOSPEED") == 0)
        {
            ret=fscanf(fh, "%d\n", &((*cam)->isospeed));
        }
	else if (strcmp(lecture, "EXPOSURE") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading exposure value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->exposure = -1;
	   }
	   else
	   {
	      (*cam)->exposure = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "BRIGHTNESS") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading brightness value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->brightness = -1;
	   }
	   else
	   {
	      (*cam)->brightness = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "GAIN") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading gain value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->gain = -1;
	   }
	   else
	   {
	      (*cam)->gain = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "SHARPNESS") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading sharpness value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->sharpness = -1;
	   }
	   else
	   {
	      (*cam)->sharpness = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "WHITE_BALANCE") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading white_balance value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->blue_balance = -1;
	      (*cam)->red_balance = -1;
	   }
	   else
	   {
	      ret = sscanf(tmp, "%i,%i", &(*cam)->blue_balance, &(*cam)->red_balance);
	      if (ret != 2)
	      {
		 EXIT_ON_ERROR("Error reading white_balance value of camera %s", name);
	      }
	   }
	}
	else if (strcmp(lecture, "HUE") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading hue value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->hue = -1;
	   }
	   else
	   {
	      (*cam)->hue = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "SATURATION") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading saturation value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->saturation = -1;
	   }
	   else
	   {
	      (*cam)->saturation = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "GAMMA") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading gamma value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->gamma = -1;
	   }
	   else
	   {
	      (*cam)->gamma = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "SHUTTER") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading shutter value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->shutter = -1;
	   }
	   else
	   {
	      (*cam)->shutter = atoi(tmp);
	   }
	}
	else if (strcmp(lecture, "IRIS") == 0)
	{
	   ret = fscanf(fh, "%s\n", tmp);

	   if (ret != 1)
	   {
	      EXIT_ON_ERROR("Error reading iris value of camera %s", name);
	   }

	   if (strcmp(tmp, "auto") == 0)
	   {
	      (*cam)->iris = -1;
	   }
	   else
	   {
	      (*cam)->iris = atoi(tmp);
	   }
	}
        else
            lire_ligne(fh);


    }
    fclose(fh);

   if ((*cam)->camera_type == USE_FIREWIRE) kprints("\t\t type= 4->firewire \n\t\t port_on_bus=%d\n\t\t video_mode=%d \n\t\t framerate=%d \n\t\t isospeed=%d \n\t\t nb_buffers=%i\n\t\t angular_width=%f \n\t\t angular_height=%f\n\t\t threading=%i\n\t\t ", (*cam)->addr_bus_firewire, (*cam)->video_mode, (*cam)->framerate, (*cam)->isospeed, (*cam)->nb_buffers, (*cam)->teta_w, (*cam)->teta_h, (*cam)->threading);
	else kprints("\t\t type=%d \n\t\t port=%s \n\t\t angular_width=%f \n\t\t angular_height=%f\n\t\t width=%i\n\t\t height=%i\n\t\t nb_buffers=%i\n\t\t flush_buffers=%i\n\t\t color=%i\n\t\t threading=%i\n", (*cam)->camera_type, (*cam)->port, (*cam)->teta_w, (*cam)->teta_h, dim_w, dim_h, (*cam)->nb_buffers, (*cam)->flush_buffers, (*cam)->color, (*cam)->threading);

    /* grayscale=1;  NC var utilisateur??? */
    /*Image size (PAL) w=768 h=576 pixels */
    (*cam)->scale_factor = 1.;

    /* Ouverture du device bttv */
    (*cam)->desc = -1;          /*pour etre sur... */

    /* Recherche de l'entree video webcam ou composite */
    /* and Init of cam object variable by autodection */


    if ((*cam)->camera_type == USE_CLIENTHW || (*cam)->camera_type == USE_CAM_WEBOTS)
    {
        (*cam)->width_max = dim_w;
        (*cam)->height_max = dim_h;
    }
    else if ((*cam)->camera_type == USE_FIREWIRE)
    {
    /*Recuperation de la taille du buffer*/
#ifdef FIREWIRE
	if( (*cam)->video_mode==-1 && ( (dim_w==-1)|| (dim_h==-1) )  )
	{
	 EXIT_ON_ERROR("Undefined firewire camera size\n Please, provide either the video_mode or the explicit size of the camera frame\n");
	}
	else if ((*cam)->video_mode!=-1)
	{
	  err=dc1394_get_image_size_from_video_mode((*cam)->cam_firewire, (*cam)->video_mode, (unsigned int*)&((*cam)->width_max),(unsigned int*)&((*cam)->height_max));
	  if(err!=DC1394_SUCCESS)
	   PRINT_WARNING("Cannot get image size from videmode (explicit size has been ignored)\n ");
	}
	else
	{
	  (*cam)->width_max = dim_w;
	  (*cam)->height_max = dim_h;
	}
#endif
#ifndef FIREWIRE
        EXIT_ON_ERROR("les api firewire ne sont pas compile\n");
#endif

    }
    else if ((*cam)->camera_type == USE_PLAYER)
    {
#ifdef PLAYER
        (*cam)->width_max = dim_w;
        (*cam)->height_max = dim_h;
#endif
#ifndef PLAYER
       EXIT_ON_ERROR("les api player ne sont pas compile\n");
#endif
    }
    else if((*cam)->camera_type == USE_CAM_AIBO)
    {
       char *ip = NULL;
       char *port = NULL;
       int i;
       pthread_t	tid;

       (*cam)->width_max = dim_w;
       (*cam)->height_max = dim_h;
       (*cam)->format = frmt;

       /* Init de la connexion pour la camera Aibo */
       ip = (*cam)->port;
       for (i = 0; (*cam)->port[i]; i++)
		  if ((*cam)->port[i] == ':')
		  {
		     (*cam)->port[i] = 0;
		     port = &((*cam)->port[i + 1]);
		     break;
		  }

       (*cam)->desc = connect_Aibo_Client(ip, port);

       /* Campatibilite avec les anciens scripts */
       (*cam)->updated = 0;

       if (pthread_create(&tid, NULL, aibo_recv_tagged_message, (void *)&((*cam)->desc)) != 0)
       {
	  		EXIT_ON_ERROR("Erreur de creation de la thread de reception des messages URBI-Aibo\n");
       }

    }
    else if((*cam)->camera_type == USE_CAM_NIOS)
       {
          char *ip = NULL;
          char *port = NULL;
          int i;
          pthread_t	tid;

          (*cam)->width_max = dim_w;
          (*cam)->height_max = dim_h;
          (*cam)->format = frmt;

          /* Init de la connexion pour la camera Aibo */
          ip = (*cam)->port;
          for (i = 0; (*cam)->port[i]; i++)
		   	  if ((*cam)->port[i] == ':')
		   	  {
		   	     (*cam)->port[i] = 0;
		   	     port = &((*cam)->port[i + 1]);
		   	     break;
		   	  }

          (*cam)->desc = connect_nios_Client(ip, port);

          /* Campatibilite avec les anciens scripts */
          (*cam)->updated = 0;

          if (pthread_create(&tid, NULL, nios_recv_message, (void *)&((*cam)->desc)) != 0)
          {
   	 		 EXIT_ON_ERROR("connect_nios_Client : Erreur de creation de la thread de reception des messages NIOS\n");
          }

       }
    else if ((*cam)->camera_type == USE_CAMERA_CLASSIC || (*cam)->camera_type == USE_CAMPANO || (*cam)->camera_type == USE_V4L2)
    {

       if ((*cam)->camera_type == USE_CAMERA_CLASSIC || (*cam)->camera_type == USE_CAMPANO)
       {
	  PRINT_WARNING("v4l (video for linux) is now deprecated in newer linux kernels. Cameras types 1 and 2 are now using v4l2. Switch the camera type to 7 (v4l2 cameras) to turn off this warning");
       }

#ifdef Linux
       int input_index = -1;
       struct v4l2_control v4l2_ctrl;
       struct v4l2_input v4l2_in;
       struct v4l2_queryctrl v4l_queryctrl;

       dprints("camera_create_camera: opening port %s\n", (*cam)->port);
       (*cam)->desc = open((*cam)->port, O_RDWR);
       if ((*cam)->desc == -1)
       {
			 EXIT_ON_SYSTEM_ERROR("Cannot open port %s\n", (*cam)->port);
       }

       /* Gets the capabilities of the camera */
       if (-1 == ioctl ((*cam)->desc, VIDIOC_QUERYCAP, &((*cam)->v4l2_caps)))
       {
	  if (EINVAL == errno)
	  {
	    EXIT_ON_SYSTEM_ERROR("Camera not supported by V4L2 \n");
	  }
	  else
	  {
	     EXIT_ON_SYSTEM_ERROR("VIDIOC_QUERYCAP");
	  }
       }

       if (!((*cam)->v4l2_caps.capabilities & V4L2_CAP_VIDEO_CAPTURE))
       {
			EXIT_ON_ERROR ("Camera is not a video capture device\n");
       }

       dprints("camera_create_camera: Capabilty of the cam:\ndriver=%s\ncard=%s\nbus=%s,version=%d\n",
       (*cam)->v4l2_caps.driver, (*cam)->v4l2_caps.card, (*cam)->v4l2_caps.bus_info, (*cam)->v4l2_caps.version);

       /* Test of the output modes supported by the camera */
       if (!((*cam)->v4l2_caps.capabilities & V4L2_CAP_STREAMING))
       {
	 		PRINT_WARNING("Camera does not support streaming i/o\n");
			if (((*cam)->v4l2_caps.capabilities & V4L2_CAP_READWRITE))
				EXIT_ON_ERROR("NOTICE in camera_create_camera: Camera supports read/write i/o (not implemented yet)\n");
       }

       /* Mutes the audio input if the camera supports audio */
       if ((*cam)->v4l2_caps.capabilities & V4L2_CAP_AUDIO)
       {
		  memset(&v4l2_ctrl, 0, sizeof(v4l2_ctrl));

		  v4l2_ctrl.id = V4L2_CID_AUDIO_MUTE;
		  v4l2_ctrl.value = 1;

		  if (ioctl((*cam)->desc, VIDIOC_S_CTRL, &v4l2_ctrl) == -1)
		     perror("ioctl (VIDIOC_S_CTRL)");
       }

       memset(&v4l2_in, 0, sizeof(v4l2_in));
       v4l2_in.index = 0;
       (*cam)->camera_type_v4l = CAM_WEBCAM;

       while (0 == ioctl((*cam)->desc, VIDIOC_ENUMINPUT, &v4l2_in))
       {
	  if (v4l2_in.type == V4L2_INPUT_TYPE_CAMERA)
	  {
	     dprints("camera_create_camera: Camera input detected -> %s\n", v4l2_in.name);
	     if (strstr((char *)v4l2_in.name, composite))
	     {
		input_index = v4l2_in.index;
		(*cam)->camera_type_v4l = CAM_COMPOSITE;
		break;
	     }
	  }

	  v4l2_in.index++;
       }

       if (input_index >= 0)
	  if (-1 == ioctl ((*cam)->desc, VIDIOC_S_INPUT, &input_index))
	  {
	     EXIT_ON_SYSTEM_ERROR("ioctl(VIDIOC_S_INPUT)");
	  }


       /* Saves wanted image format for future setup */
       (*cam)->width_max = dim_w;
       (*cam)->height_max = dim_h;

       if (nrm == 1)
	  (*cam)->norm = NTSC;   /* norm NTSC */
       else
	  (*cam)->norm = PAL; /* norm PAL */
#else
	kprints("Video pour linux n'est pas defini sur ce systeme.");
#endif
    }

    if (pthread_mutex_init(&((*cam)->mutex_grab_image), NULL))
    {
       PRINT_WARNING("Can't initialise mutex : mutex_grab_image");
    }
}
