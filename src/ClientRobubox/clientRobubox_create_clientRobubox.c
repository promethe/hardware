/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <string.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <net_message_debug_dist.h>

#include <ClientRobubox.h>
#include <tools.h>

#include "tools/include/local_var.h"

/*numero de la premiere trame*/
#define CPT_TRAME_INIT 11 

void clientRobubox_create_clientRobubox(ClientRobubox ** clientRobubox, char *name, char *file_hw)
{
   FILE *fh;
   char lecture[256];
   char tmp[129];

   *clientRobubox = (ClientRobubox *) malloc(sizeof(ClientRobubox));
   if (*clientRobubox == NULL) EXIT_ON_ERROR("erreur malloc clientRobubox_create_clientRobubox\n");

   (*clientRobubox)->name = (char *) malloc(strlen(name) + 1);
   if ((*clientRobubox)->name == NULL) EXIT_ON_ERROR("erreur malloc clientRobubox_create_clientRobubox\n");

   strcpy((*clientRobubox)->name, name); /*ClientRobubox_object name */
   (*clientRobubox)->host = NULL;
   (*clientRobubox)->timeout_delay = 50000;
   (*clientRobubox)->compteur = CPT_TRAME_INIT;


   /* default parameters */


   /*! loading clientRobubox parameters */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }

   while (!feof(fh))
   {
      if (fscanf(fh, "%s = ", lecture) != 1) PRINT_WARNING("Eroor reading firs word.");   /* Reading the first line's word */
      /*     printf("lit: %s \n",lecture); */
      str_upper(lecture);

      if ((strcmp(lecture, "HOST") == 0)) /* get the port address */
      {
         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            printf("Erreur de lecture pour le port de la clientRobubox %s\n",name);
            exit(1);
         }

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         /* verify if it is the first declaration for port */
         if ((*clientRobubox)->host != NULL)
         {
            PRINT_WARNING("Several declarations of port are declared in %s",file_hw);
            free((*clientRobubox)->host);
         }

         (*clientRobubox)->host = (char *) malloc(strlen(tmp) + 1);
         if ((*clientRobubox)->host == NULL)
         {
            printf("Erreur d'allocation memoire pour le port de clientRobubox %s dans clientRobubox_create_clientRobubox\n",name);
            exit(1);
         }
         strcpy((*clientRobubox)->host, tmp);
      }
      else if (strcmp(lecture, "PORT") == 0)
      {
         if (fscanf(fh, "%d\n", &((*clientRobubox)->port))!=1) PRINT_WARNING("Error reading port.");
      }
      else if (strcmp(lecture, "TIMEOUT") == 0)
      {
          if (fscanf(fh, "%d\n", &((*clientRobubox)->timeout_delay)) != 1 ) PRINT_WARNING("Error reading port.");
      }
      else
         lire_ligne(fh);

   }
   fclose(fh);

   printf("\t\t host=%s\n\t\t port=%d\n", (*clientRobubox)->host, (*clientRobubox)->port);

   if (pthread_mutex_init(&((*clientRobubox)->mutex_socket), NULL))   EXIT_ON_ERROR("Can't initialise mutex : mutex_client_read ***\n");

   clientRobubox_connect(*clientRobubox);
}
