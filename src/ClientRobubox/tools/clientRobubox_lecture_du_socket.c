/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <net_message_debug_dist.h>
#include <ClientRobubox.h>
#include <errno.h>

/*#define DEBUG*/

int clientRobubox_lecture_du_socket(ClientRobubox * chw)
{
	int n;
	int result_select;
	struct timeval wait;
	fd_set set;
	int biggest_fd;
	char reponse[MAXLINE];
	long cpt;
	struct sockaddr_in from;
	socklen_t salong=sizeof(from);

	wait.tv_sec = 0;
	wait.tv_usec = chw->timeout_delay; /* 25ms */

	FD_ZERO(&set);
	FD_SET(chw->sockfd, &set);

	biggest_fd=chw->sockfd+1;

	result_select=select(biggest_fd, &set, NULL, NULL, &wait);
	if ( result_select < 0 )
	{
		if (errno != EINTR) {
			perror("select");
			exit(0);
		}
	}
	else if (result_select==0) {
		/*timeout*/
		printf("TIMEOUT dans %s\n",__FUNCTION__);
		sprintf(chw->buff,"%s","");

		return 1;
	}
	else
	{

		if ( (n=recvfrom(chw->sockfd, reponse, MAXLINE, 0,(struct sockaddr *)&from,&salong)) < 0 ) {
			perror("recvfrom");
			exit(0);
		}
		reponse[n] = (char) 0;

		sscanf(reponse,"[%ld]",&cpt);/* lecture du numero de la trame */
		sprintf(chw->buff,"%s",strstr(reponse," ")+1); /* lecture de la commande */

		if( cpt < (chw->compteur) )
		{
			PRINT_WARNING("Le numero de la trame est incorrect : %s",reponse);
			/*             	return clientRobubox_lecture_du_socket(chw);*/
			return 2;
		}
		else if ( cpt > (chw->compteur) )
			EXIT_ON_ERROR("Le numero de la trame est incorrect : %s",reponse);

		/* On test si la reponse correspond a l'un des messages d'erreur */
		if( strcmp(chw->buff,"Error")==0 )
			EXIT_ON_ERROR("ERREUR dans l'application de la commande sur le Roburoc4 : %s\n",reponse);

		else if ( strcmp(chw->buff,"UnknownCommand")==0 )
			EXIT_ON_ERROR("ERREUR clientRobubox : %s\n",reponse);

		else if ( strcmp(chw->buff,"BadSyntax")==0 )
			EXIT_ON_ERROR("ERREUR clientRobubox : %s\n",reponse);

		dprints("clientRobubox_lecture_du_socket : [%ld] = '%s' - size : %d\n",cpt,chw->buff,n);

	}

	return 0;
}
