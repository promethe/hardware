/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mxml.h>

#include <World.h>
#include <tools.h>
#include <time.h>

/*#define DEBUG*/

#include "tools/include/local_var.h"
#include "net_message_debug_dist.h"

void world_create_world(World ** world, char *name, char *file_hw)
{

   FILE *fh;
   int i = 0;

   int nb_resources = 0, nb_landmarks = 0, nb_obstacles = 0, nb_doors = 0;
   mxml_node_t *tree = NULL;
   mxml_node_t *node = NULL;
   mxml_node_t *node_sec = NULL;
   mxml_node_t * child = NULL;


   *world = (World *) malloc(sizeof(World));

   if (*world == NULL)
   {
      EXIT_ON_ERROR("ERROR in world_create_world: malloc failed for world\n");
      
   }

   (*world)->name = (char *) malloc(strlen(name) + 1);

   if ((*world)->name == NULL)
   {
      dprints("ERROR in world_create_world: malloc failed for world name\n");
      
   }

   strcpy((*world)->name, name);   /*World_object name */

   /* default parameters */


   /*! loading world parameters */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      EXIT_ON_ERROR("ERROR in world_create_world: File %s cannot be opened\n", file_hw);
      
   }

   tree = mxmlLoadFile(NULL, fh, MXML_OPAQUE_CALLBACK); /* "OPAQUE" pour avoir l'ensemble du texte contenu dans un noeud */
   fclose(fh);
   
   for (node = mxmlFindElement(tree, tree, "Landmark", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Landmark", NULL, NULL,MXML_DESCEND))
   {
      nb_landmarks++;
   }
   
   for (node = mxmlFindElement(tree, tree, "Obstacle", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Obstacle", NULL, NULL,MXML_DESCEND))
   {
      nb_obstacles++;
   }

   for (node = mxmlFindElement(tree, tree, "Door", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Door", NULL, NULL,MXML_DESCEND))
   {
      nb_doors++;
   }
   
   for (node = mxmlFindElement(tree, tree, "Resource", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Resource", NULL, NULL, MXML_DESCEND))
   {
      nb_resources++;
   }
   
   dprints("nb_obstacles = %d, nb_landmarks = %d, nb_resources = %d\n", nb_obstacles, nb_landmarks, nb_resources);
   
   /* Creating tables */
   (*world)->landmarks = (Landmark *) malloc(nb_landmarks * sizeof(Landmark));
   (*world)->nb_landmarks = nb_landmarks;

   (*world)->obstacles = (Obstacle *) malloc(nb_obstacles * sizeof(Obstacle));
   (*world)->nb_obstacles = nb_obstacles;

   (*world)->doors = (Door *) malloc(nb_doors * sizeof(Door));
   (*world)->nb_doors = nb_doors;

   (*world)->resources = (Resource *) malloc(nb_resources * sizeof(Resource));
   (*world)->nb_resources = nb_resources;

   node = NULL;
   node = mxmlFindElement(tree, tree, "time_constant", NULL, NULL, MXML_DESCEND);
   if (node != NULL)
   {
      child = NULL;   
      child = node->child;
      if (child == NULL)
      {   
         EXIT_ON_ERROR("ERROR in world_create_world: No child node for time_constant\n");
         
      }
      dprints("time_constat = %s \n",child->value.opaque);
      (*world)->time_constant = atof(child->value.opaque);
   }   
   else  
      (*world)->time_constant = 1.;
   
   
   node = NULL;
   node = mxmlFindElement(tree, tree, "spatial_constant", NULL, NULL, MXML_DESCEND);
   if (node != NULL)
   {
      child = NULL;   
      child = node->child;
      if (child == NULL)
      {   
         EXIT_ON_ERROR("ERROR in world_create_world: No child node for spatial_constant\n");
         
      }
      dprints("spatial_constant = %s \n",child->value.opaque);
      (*world)->spatial_constant = atof(child->value.opaque);
   }   
   else  
      (*world)->spatial_constant = 1.;

   node = NULL;
   node = mxmlFindElement(tree, tree, "width", NULL, NULL, MXML_DESCEND);
   if (node != NULL)
   {
      child = NULL;   
      child = node->child;
      if (child == NULL)
      {   
         EXIT_ON_ERROR("ERROR in world_create_world: No child node for width\n");
         
      }
      dprints("width = %s \n",child->value.opaque);
      (*world)->width = atoi(child->value.opaque);
   }   
   else  
      (*world)->width = 500;
      
   node = NULL;   
   node = mxmlFindElement(tree, tree, "width", NULL, NULL, MXML_DESCEND);
   if (node != NULL)
   {
      child = NULL;   
      child = node->child;
      if (child == NULL)
      {   
         EXIT_ON_ERROR("ERROR in world_create_world: No child node for height\n");
         
      }
      dprints("height = %s \n", child->value.opaque);
      (*world)->height= atoi(child->value.opaque);
   }   
   else  
      (*world)->height = 500;
   
   /* Creation of obstacles */   
   
   i = 0;
   for (node = mxmlFindElement(tree, tree, "Obstacle", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Obstacle", NULL, NULL,MXML_DESCEND))
   {
      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "xa", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for xa\n");
	    
	 }
	 dprints("xa = %s \n", child->value.opaque);
         (*world)->obstacles[i].xa = atoi(child->value.opaque);
      }   
      else  
         (*world)->obstacles[i].xa = 0;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "ya", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for ya\n");
	    
	 }
	 dprints("ya = %s \n", child->value.opaque);
         (*world)->obstacles[i].ya = atoi(child->value.opaque);
      }   
      else  
         (*world)->obstacles[i].ya = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "xb", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for xb\n");
	    
	 }
	 dprints("xb = %s \n", child->value.opaque);
         (*world)->obstacles[i].xb = atoi(child->value.opaque);
      }   
      else  
         (*world)->obstacles[i].xb = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "yb", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for yb\n");
	    
	 }
	 dprints("yb = %s \n", child->value.opaque);
         (*world)->obstacles[i].yb = atoi(child->value.opaque);
      }   
      else  
         (*world)->obstacles[i].yb = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "high", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for high\n");
	    
	 }
	 dprints("high = %s \n", child->value.opaque);
         (*world)->obstacles[i].high = atoi(child->value.opaque);
      }   
      else  
         (*world)->obstacles[i].high = 1;
      
      i++;
   }

   /* Creation of doors */   
   
   i = 0;
   
   for (node = mxmlFindElement(tree, tree, "Door", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Door", NULL, NULL,MXML_DESCEND))
   {
	  node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "xa", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for xa in the doors\n");
	 }
	 dprints("xa = %s \n", child->value.opaque);
         (*world)->doors[i].xa = atoi(child->value.opaque);
      }   
      else  
         (*world)->doors[i].xa = 5;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "ya", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for ya in the doors\n");
	    
	 }
	 dprints("ya = %s \n", child->value.opaque);
         (*world)->doors[i].ya = atoi(child->value.opaque);
      }   
      else  
         (*world)->doors[i].ya = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "xb", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for xb in the doors\n");
	    
	 }
	 dprints("xb = %s \n", child->value.opaque);
         (*world)->doors[i].xb = atoi(child->value.opaque);
      }   
      else  
         (*world)->doors[i].xb = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "yb", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for yb in the doors\n");
	    
	 }
	 dprints("yb = %s \n", child->value.opaque);
         (*world)->doors[i].yb = atoi(child->value.opaque);
      }   
      else  
         (*world)->doors[i].yb = 10;
         
      if (((*world)->doors[i].ya > (*world)->doors[i].yb) && ((*world)->doors[i].xa > (*world)->doors[i].xb)) 
      {
		  EXIT_ON_ERROR("ERROR in world_create_world: incorrect x or y values in door %d, xa must be <= xb and ya <= yb\n",i);
	  }

      node_sec = NULL;     
      node_sec = mxmlFindElement(node, tree, "direction", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	  if (child == NULL)
	  {   
	     EXIT_ON_ERROR("ERROR in world_create_world: No child node for high in the doors\n");
	    
	  }

	  if ((atoi(child->value.opaque) != 1) && (atoi(child->value.opaque) != 2) && (atoi(child->value.opaque) != 3) && (atoi(child->value.opaque) != 4))
	  {

	     EXIT_ON_ERROR("ERROR in world_create_world: No child node or incorrect value for direction in the doors, directions are : 1 for north->south, 2 east->west, 3 for south->north and 4 for west->east\n");
	  }
	  dprints("direction = %s \n", child->value.opaque);
          (*world)->doors[i].direction = atoi(child->value.opaque);
	
      if ((((*world)->doors[i].direction == 1) || ((*world)->doors[i].direction == 3)) && ((*world)->doors[i].ya != (*world)->doors[i].yb))
      {
		  EXIT_ON_ERROR("ERROR in world_create_world: incorrect values for y in door %d, ya and yb must be equal if the door is horizontal\n",i);
	  }

	  if ((((*world)->doors[i].direction == 2) || ((*world)->doors[i].direction == 4)) && ((*world)->doors[i].xa != (*world)->doors[i].xb))
      {
		  EXIT_ON_ERROR("ERROR in world_create_world: incorrect values for x in door %d, xa and xb must be equal if the door is vertical\n",i);
	  }
	  
      }   
      else  
         (*world)->doors[i].direction = 1;
         

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "high", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for high in the doors\n");
	    
	 }
	 dprints("high = %s \n", child->value.opaque);
         (*world)->doors[i].high = atoi(child->value.opaque);
      }   
      else  
         (*world)->doors[i].high = 1;
   
      i++;
   }

   /* Creation of resources */
   i = 0;
   for (node = mxmlFindElement(tree, tree, "Resource", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Resource", NULL, NULL, MXML_DESCEND))
   {
      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "x", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for x\n");
	    
	 }
	 dprints("x = %s \n", child->value.opaque);
         (*world)->resources[i].x = atof(child->value.opaque);
      }   
      else  
         (*world)->resources[i].x = 50;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "y", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for y\n");
	    
	 }
	 dprints("y = %s \n", child->value.opaque);
         (*world)->resources[i].y = atof(child->value.opaque);
      }   
      else  
         (*world)->resources[i].y = 50;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "radius", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for radius\n");
	    
	 }
	 dprints("radius = %s \n", child->value.opaque);
         (*world)->resources[i].radius = atof(child->value.opaque);
      }   
      else  
         (*world)->resources[i].radius = 20;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "red", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for red\n");
	    
	 }
	 dprints("red = %s \n", child->value.opaque);
         (*world)->resources[i].red = atoi(child->value.opaque);
      }   
      else  
         (*world)->resources[i].red = 1;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "green", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for green\n");
	    
	 }
	 dprints("green = %s \n", child->value.opaque);
         (*world)->resources[i].green = atoi(child->value.opaque);
      }   
      else  
         (*world)->resources[i].green = 1;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "blue", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for blue\n");
	    
	 }
	 dprints("blue = %s \n", child->value.opaque);
         (*world)->resources[i].blue = atoi(child->value.opaque);
      }   
      else  
         (*world)->resources[i].blue = 1;
      
      i++;
   }
   
   
   /*Crestion des Landmarks*/
   
   i = 0;
   for (node = mxmlFindElement(tree, tree, "Landmark", NULL, NULL, MXML_DESCEND);
        node != NULL;
        node = mxmlFindElement(node, tree, "Landmark", NULL, NULL,MXML_DESCEND))
   {
      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "x", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for x\n");
	    
	 }
	 dprints("x = %s \n", child->value.opaque);
         (*world)->landmarks[i].x = atoi(child->value.opaque);
      }   
      else  
         (*world)->landmarks[i].x = 0;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "y", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for y\n");
	    
	 }
	 dprints("y = %s \n", child->value.opaque);
         (*world)->landmarks[i].y = atoi(child->value.opaque);
      }   
      else  
         (*world)->landmarks[i].y = 10;

      node_sec = NULL;   
      node_sec = mxmlFindElement(node, tree, "always_visible", NULL, NULL, MXML_DESCEND);
      if (node_sec != NULL)
      {
         child = NULL;   
         child = node_sec->child;
	 if (child == NULL)
	 {   
	    EXIT_ON_ERROR("ERROR in world_create_world: No child node for always_visible\n");
	    
	 }
	 dprints("always_visible = %s \n", child->value.opaque);
         (*world)->landmarks[i].always_visible = atoi(child->value.opaque);
      }   
      else  
         (*world)->landmarks[i].always_visible = 1;
      
      i++;
   }
}
