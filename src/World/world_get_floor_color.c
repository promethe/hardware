/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>
#include <World.h>

void world_get_floor_color(World *world, float pos_x, float pos_y, int *colors)
{
   int i;
   
   if (world == NULL)
   {
      fprintf(stderr, "ERROR in world_get_floor_color: NULL pointer for world\n");
      exit(1);
   }
   
   colors[0] = colors[1] = colors[2] = 0;
   dprints("world_get_floor_color: pos_x = %f, pos_y = %f\n", pos_x, pos_y);

   for (i = 0; i < world->nb_resources; i++)
   {
      dprints("world_get_floor_color: resource %d => pos_x = %f, pos_y = %f\n", i, world->resources[i].x, world->resources[i].y);
      if (sqrt(pow(world->resources[i].x - pos_x, 2) + pow(world->resources[i].y - pos_y, 2)) <= world->resources[i].radius)
      {
	 dprints("world_get_floor_color: resource %d detected !!! color = (%d,%d,%d)\n", i, 
	         world->resources[i].red, world->resources[i].green, world->resources[i].blue);
	 colors[0] = world->resources[i].red;
	 colors[1] = world->resources[i].green;
	 colors[2] = world->resources[i].blue;
	 break;
      }
   }

}
