/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Command_Serial.h>
#include "tools/include/local_var.h"


void cmd_serial_set_cmd(CMD_Serial * cmd_serial, int * VReacteur, int *VParfum)
{
	
	char commande[20];
	char reponse[200];

	if(cmd_serial == NULL)
	{
	  EXIT_ON_ERROR("cmd_serial_get_cmd : NULL pointer for cmd_serial");
	}
	if(  VReacteur == NULL || VParfum == NULL)
	{
	  EXIT_ON_ERROR("cmd_serial_get_cmd : NULL pointer for cmd_serial");
	}

	if( (*VReacteur) > 0 ) cmd_serial->vr_value = CMD_ON;
        else  cmd_serial->vr_value = CMD_OFF;

	if( (*VParfum) > 0 ) cmd_serial->vp_value = CMD_ON;
        else  cmd_serial->vp_value = CMD_OFF;

	//sprintf(commande, "R-%c-%c-%c-", cmd_serial->l_value , cmd_serial->vr_value ,cmd_serial->vp_value);
	sprintf(commande, "A%c%c", cmd_serial->vr_value ,cmd_serial->vp_value);
	serial_send_and_receive_block(cmd_serial->port, commande, reponse, 3, 1000000);
//	serial_send(cmd_serial->port, commande,4);
/*
// Code de lecture des valeurs des cmds (avoir un vrai retour de l'arduino)
// Refaire envoie + lecture tant qu'on a pas des valeurs correctes
// Surement mieux de ne pas faire l'envoie + lecture dans la meme fonction => threader le tout pour etre le max asynchrone

	commande[0] = 'L';
	serial_send_and_receive_block(us_serial->port, commande, reponse, 1, 1000000);

	entete = strstr(reponse,"-l-");
        if( entete != NULL )
        {
	   i = 0;
           read = 0;
           for(j = 0 ; i < 3 && j < 200 ;j++)
           {
           	read = sscanf(reponse+j,"-%d-",&val);
                if( read > 0)
                {
			switch(i)
			{
				case 0 : cmd_serial->r = val; break;
				case 1 : cmd_serial->g = val; break;
				case 2 : cmd_serial->b = val; break;
			}
                        i++;
                }
	  }
	}
	
	*R = cmd_serial->r / MAXCMD;
	*G = cmd_serial->g / MAXCMD;
	*B = cmd_serial->b / MAXCMD;
	
	if( *R < 0 ) *R = 0; 
	if( *G < 0 ) *G = 0; 
	if( *B < 0 ) *B = 0; 

	if( *R > 1. ) *R = 1.; 
	if( *G > 1. ) *G = 1.; 
	if( *B > 1. ) *B = 1.; 
*/	
	//pthread_mutex_lock(&cmd_serial->lock);
	//pthread_mutex_unlock(&cmd_serial->lock);
}


void cmd_serial_set_cmd_reset(CMD_Serial * cmd_serial, int SSC)
{
	char commande[20];
        char reponse[200];

        if(cmd_serial == NULL)
        {
          EXIT_ON_ERROR("cmd_serial_get_cmd : NULL pointer for cmd_serial");
        }

	if( (SSC) > 0 ) cmd_serial->reset = CMD_ON;
        else  cmd_serial->reset = CMD_OFF;

	sprintf(commande, "P%c", cmd_serial->reset);
	serial_send_and_receive_block(cmd_serial->port, commande, reponse, 2, 1000000);
}

