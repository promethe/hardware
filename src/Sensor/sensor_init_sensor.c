/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  sensor_sens_command.c 
\brief 

Author: de RENGERVE Antoine
Created: 20/07/2010
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-
Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Sensor.h>
#include <Serial.h>
#include "tools/local_var_Sensor.h"
#include "tools/error_management_Sensor.h"
#include <net_message_debug_dist.h>

#include <Aibo.h>
#include <ClientKatana.h>

/*define DEBUG */

/*
** Initialise les senseurs (possibilite d'autocheck)
*/
void sensor_init_sensor(Sensor * sens)
{
  char commande[255];
  char reponse[400];	
  int length,ret;
  int err;
  int val1,val2,val3,val4;
  int longueur=-1;
  /*Attention myclient est en dur dans le code 
   *il faut le mettre dans la structure
   *Sensor
   */
  /*char client[256]="myclient";*/
  /*ClientKatana *katana=NULL;
  char tab[16][256];
  char *t;
  */

  memset(reponse, 0, 400 * sizeof(char));
  memset(commande, 0, 255 * sizeof(char));

 if(sens == NULL)
   {
     fprintf(stderr, "Sensor_sens_command : Nom du sensor inconnu\n");
     exit(1);
   }
 
 /*
 ** BLOCK SENSOR TYPE AIBO
 */
 if(sens->type == TYPE_AIBO)
   {
     printf("No initialization implemented for TYPE_AIBO\n");
     return;
   } 
 else
   {
     if(sens->type == 2) /* type Katana */
       {
	 printf("No initialization implemented for TYPE_KATANA\n");
	 return;
       }
     else if (sens->type == TYPE_JOYSTICK) 
     {
       printf("initialisation pour type joystick\n");
       
       sprintf(commande,"IJ %d.%d %d.%d %d\r",sens->periode,0,0,100,sens->autocheck); /*modifier pour avoir les bonnes valeurs plutot que des valeurs par defaut*/
       longueur = strlen(commande);
       do
	 {
#ifdef DEBUG
     printf("Commande : %s\n", commande);
#endif
	   serial_send_and_receive(sens->port, commande, reponse, longueur);
#ifdef DEBUG
	   printf("reponse :%s\n",reponse);
#endif
	   length = strlen(reponse);
	   if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
	     {
	       reponse[length-1] = '\0';
	     }
	   if (reponse[0] == 'i' && reponse[1] == 'j')
	     {
	       if(sscanf(reponse, "ij %d.%d %d.%d %d",&val1,&val2,&val3,&val4, &err)!=5)
		     {
		        ret=0;
		     }
	       else ret=1;
	       
	     }
	   else ret=0;
	   
     if(reponse[0]=='E') {
       error_management_Sensor(reponse);
     }
	   if (ret != 1)
	     printf("WARNING: the values read by sensor_init_sensor_read (%s) do not have the required format. Retrying to read ... \n", reponse);
     else {
#ifdef DEBUG
	      printf("Initialisation : retour :%d\n", err);
#endif


        /*gestion du checkout a faire/completer*/
        switch(err)
        {
          case -1:
            printf("No autocheck\n");
            break;
          case 0:
            printf("Autocheck did not found any error\n");
            break;
          default:
            printf("Unknown autocheck return!!\n");
        }	   
      }
      }      
       while (ret != 1);

       return;
     }
     else if (sens->type == TYPE_FORCE)
     {
       printf("initialisation pour type force sensor\n");
       
       sprintf(commande,"IF %d.%d %d.%d %d\r",sens->periode,0,0,100,sens->autocheck); /*modifier pour avoir les bonnes valeurs plutot que des valeurs par defaut*/
       longueur = strlen(commande);   

     do
	 {
#ifdef DEBUG
       printf("Commande : %s\n", commande);
#endif
	   serial_send_and_receive(sens->port, commande, reponse, longueur);
#ifdef DEBUG
	   printf("reponse :%s\n",reponse);
#endif
	   length = strlen(reponse);
	   if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
	     {
	       reponse[length-1] = '\0';
	     }
	   if (reponse[0] == 'i' && reponse[1] == 'f')
	     {
	       if(sscanf(reponse, "if %d.%d %d.%d %d",&val1,&val2,&val3,&val4, &err)!=5)
		      {
		        ret=0;
		      }
	       else ret=1;
	       
	     }
	   else ret=0;
     if(reponse[0]=='E') {
       error_management_Sensor(reponse);
     }
	   if (ret != 1)
	     printf("WARNING: the values read by sensor_init_sensor_read (%s) do not have the required format. Retrying to read ... \n", reponse);
     else {
#ifdef DEBUG
	      printf("Initialisation : retour :%d\n", err);
#endif

        /*gestion du checkout a faire/completer*/
        switch(err)
        {
          case -1:
            printf("No autocheck\n");
            break;
          case 0:
            printf("Autocheck did not found any error\n");
            break;
          default:
            printf("Unknown autocheck return!!\n");
        }	   
      }
      }
       while (ret != 1);

       return;
     }
     else {
       EXIT_ON_ERROR("Unknown sensor type\n");
     }
       
   }
 return;
}
