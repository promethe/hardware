/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
 \file  sensor_apply_hardware_config.c
 \brief

 Author: LAGARDE Matthieu

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tools.h>
#include <basic_tools.h>
#include <Sensor.h>

#include "./tools/local_var_Sensor.h"

void sensor_apply_hardware_config(char *argv)
{
  FILE *fl;
  Sensor **sens_table = NULL;

  int nb_sensor;
  int i;
  char lecture[100];

  char sensor_name[256][256];
  char sensor_type[256][256];

  nb_sensor = 0;

  /*! trying to open appli_name.dev */
  fl = fopen(argv, "r");
  if (fl == NULL)
  {
    PRINT_WARNING("Promethe initialisation: unable to open configuration file %s \n Using default parameters", argv);
  }
  else
  {
    /*! loading parameters */

    while (!feof(fl))
    {
      if (fscanf(fl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%')
      {
        lire_ligne(fl);
      }
      else
      {
        if (strcmp(lecture, "Sensor") == 0)
        {
          if (fscanf(fl, " %s is %s", sensor_name[nb_sensor], sensor_type[nb_sensor]) != 2)
          {
            EXIT_ON_ERROR("parse error in dev file\n");
          }
          nb_sensor++;
        }
      }

    }
    fclose(fl);
  }

  if (nb_sensor > 0)
  {
    kprints("%d sensor(s) are defined:\n", nb_sensor);
    sens_table = (Sensor **) calloc(nb_sensor, sizeof(Sensor *));
    for (i = 0; i < nb_sensor; i++)
    {
      kprints("\t Sensor %s which is a %s mapped\n", sensor_name[i], sensor_type[i]);
      sens_table[i] = NULL;
      sensor_create_sensor(&sens_table[i], sensor_name[i], sensor_type[i]);
    }

    sensor_create_sensor_table(sens_table, nb_sensor);
  }
  return;
}
