/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  sensor_sens_command.c 
\brief 

Author: LAGARDE Matthieu
Created: 02/07/2006
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-
Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Sensor.h>
#include <Serial.h>
#include "tools/local_var_Sensor.h"
#include "tools/error_management_Sensor.h"

#include <Aibo.h>
#include <ClientKatana.h>

/*#define DEBUG */

/*
** Renvoie la vaeur du capteur normalisee entre 0 et 1.
*/
float sensor_sens_command(Sensor * sens)
{
  char commande[255];
  char reponse[255];
  char *buff=NULL;
  int i, val, len=-1, count=-1, ret=-1, length;
  /*Attention myclient est en dur dans le code 
   *il faut le mettre dans la structure
   *Sensor
   */
  char tab[16][256];
  /*char client[256]="myclient";*/
  char *t;
  ClientKatana *katana=NULL;

 if(sens == NULL)
   {
     fprintf(stderr, "Sensor_sens_command : Nom du sensor inconnu\n");
     exit(1);
   }
 
 /*
 ** BLOCK SENSOR TYPE AIBO
 */
 if(sens->type == TYPE_AIBO)
   {
     /*struct timespec	ts;*/
     char		commande[255];

     memset(commande, 0, 255);
     snprintf(commande, 255, "val_sensor_%s:%s,\n", sens->name, sens->name);
#ifdef DEBUG
     printf("%s", commande);
#endif
     sens->updated = 0;
     aibo_send_message(commande, strlen(commande));
/*      while (sens->updated != 1) */
/*        { */
/* 	 ts.tv_sec = 0; */
/* 	 ts.tv_nsec = 5000000; */
/* 	 nanosleep(&ts, NULL); */
/*        } */
     sem_wait(&(sens->sem));
#ifdef DEBUG
     printf("sensor_sens_command : \t\tsensor %s = %f\n", sens->name, sens->valn);
#endif
     return(sens->valn);
   } 
 else
   {
     if(sens->type == 2)
       {
	 memset(commande, 0, 255);
	 snprintf(commande, 255, "sensor\n");
	 katana=clientKatana_get_clientKatana_by_name(sens->client);  
	 clientKatana_transmit_receive(katana,commande,reponse);
	 /*printf("reponse=%s\n",reponse);*/
	 
	 
	 if(tab == NULL)
	   {
	     exit(1);
	   }
	 
	 t = reponse;
	 /*sscanf(t," %s",tab[0]);*/
	 for(i=0 ; i< /*nb de sensor*/ sens->nb_sensor ; i++)
	   {
	     sscanf(t," %s",tab[i]);
	     t=strchr(t+1,' ');
	     sens->vecteur_sensor[i] = (atof(tab[i]) - sens->val_start)/(sens->val_stop - sens->val_start);
	   }
	 
	 return 2;
  }
	else if(sens->type == TYPE_JOYSTICK || sens->type == TYPE_FORCE) {
#ifdef DEBUG
	  printf("lecture des donnees en cours\n");  
#endif
   	  if(sens->type == TYPE_JOYSTICK)
        sprintf(commande,"J \r");
	    else
        sprintf(commande,"F \r");/* TYPE_FORCE */
#ifdef DEBUG
      printf("Commande : %s\n", commande);   
#endif

      do
	    {
	      serial_send_and_receive(sens->port, commande, reponse, strlen(commande));
	      length = strlen(reponse);
#ifdef DEBUG
	      printf("reponse est :%s\n",reponse);
#endif
	      if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
	      {
	        reponse[length-1] = '\0';
	      }
	      if (reponse[0] == 'j' || reponse[0] == 'f')
	      {

	        buff = reponse;
	        len = 1;
          count=0;
	        for(i=0; i<sens->nb_sensor; i++) {
	          buff = buff+len+1;
/*   	        printf("buff :%s\n",buff);*/
	          count  = count + sscanf(buff,"%d ",&val);
/*	          printf("val %d",val);*/
		        sens->vecteur_sensor[i] = (float)(val - sens->val_start)/(float)(sens->val_stop - sens->val_start);
	          len = strcspn(buff," ");
	        }
	        /*printf("count %d \n",count);*/


	        if(count!=sens->nb_sensor) /* on doit recuperer autant de valeurs que de capteurs*/
		      {
		        ret=0;
		      }
	        else ret=1;
	       
	      }
	      else ret=0;

        if(reponse[0]=='E') {
          error_management_Sensor(reponse);
        }
        else {	
   
  	      if (ret != 1)
	          printf("WARNING: the values read by sensor_sens_command (%s) do not have the required format. Retrying to read ... \n", reponse);
	      }
      }
      while (ret != 1);	
    }
  }
  return -1;
}

