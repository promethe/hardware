/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <libhardware.h>

#include <stdio.h>
#include <dev.h>
#include <locale.h>

void libhardware_open(char * filename)
{
	setlocale(LC_ALL, "C");

	printf("Début du chargement Hardware (tout n'est pas forcement chargé)... \n");
	printf("Chargement AIBO en cours ...\n");
	aibo_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement nios en cours ...\n");
	nios_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement clientHW en cours ...\n");
	clientHW_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement clientPlayer en cours ...\n");
	clientPlayer_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement clientRobubox en cours ...\n");
	clientRobubox_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement camera en cours ...\n");
	camera_apply_hardware_config(filename);  /* recherche dans le .dev des declarations de Camera */
	printf("Finis ... \n");
	printf("Chargement serial en cours ...\n");
	serial_apply_hardware_config(filename); /* recherche dans le .dev des declarations d'ouverture du port serie */
	printf("Finis ... \n");
	printf("Chargement joint en cours ...\n");
	joint_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement motor en cours ...\n");
	motor_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement sensor en cours ...\n");
	sensor_apply_hardware_config(filename); /* recherche dans le .dev des declarations de Capteurs */
	printf("Finis ... \n");
	printf("Chargement robot_apply en cours ...\n");
	robot_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement IR_localisation en cours ...\n");
	IR_localisation_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement world_apply en cours ...\n");
	world_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement compass_apply en cours ...\n");
	compass_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement gps_apply en cours ...\n");
	gps_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement clientKatana en cours ...\n");
	clientKatana_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement laser_apply en cours ...\n");
	laser_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement accelero_apply en cours ...\n");
	accelero_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement colordetector en cours ...\n");
	colordetector_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement pantilt en cours ...\n");
	pantilt_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement joystick en cours ...\n");
	joystick_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement us_serial en cours ...\n");
	us_serial_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement air_sensor en cours ...\n");
	air_sensor_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement led_serial en cours ...\n");
	led_serial_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement cmd_serial en cours ...\n");
	cmd_serial_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("dev_load_file ...\n");
	dev_load_file(filename);
	printf("Finis ... \n");
	printf("dev_create_all_devices ...\n");
	dev_create_all_devices();
	printf("Finis ... \n");
	printf("Chargement mouse en cours ...\n");
	mouse_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Chargement pressure en cours ...\n");
	pressure_apply_hardware_config(filename);
	printf("Finis ... \n");
	printf("Fin du chargement Hardware ... \n");
}


void libhardware_start(){
	printf("Starting all devices...\n");
	dev_start_all_devices();
}