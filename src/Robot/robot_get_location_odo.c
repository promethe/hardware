/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Robot.h>
#include <ClientRobubox.h>
#include <math.h>
#include "tools/include/local_var.h"

/*#define DEBUG*/
#include <net_message_debug_dist.h>

void robot_get_location_odo(Robot * robot)
{
  float posX, posY, angle;
  static int reset = 1;

  if (robot == NULL)
  {
    fprintf(stderr, "ERROR in robot_get_location_odo: NULL pointer for robot\n");
    exit(1);
  }

  if (robot->robot_type == 4 )
  {
    /* Nothing to do: location is updated in the movement functions */
  }
  else if (robot->robot_type == 6 || robot->robot_type == USE_ROBUBOX_V2)
  {
    char commande[256];
    char resultat[256];
    char cmd[1024];
    ClientRobubox *crb;

    crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);

    if(reset == 1)
    {
      sprintf(commande,"%s","ResetLoc");
      clientRobubox_transmit_receive(crb,commande,resultat);
      reset = 0;
    }

    sprintf(commande,"%s","Loc");
    clientRobubox_transmit_receive(crb,commande,resultat);
    sscanf(resultat,"%s %f %f %f", cmd, &posX, &posY, &angle);
    dprints("%s %f %f %f\n", cmd, posX, posY, angle);
    fflush(stdout);
    /*correction avec le firmware 3.0 : un demi-tour egal 324 au lieu de 314*/
    if( robot->robot_type == USE_ROBUBOX_V2)  angle = -angle * M_PI / 324;
    robot->posx_odo = posX;
    robot->posy_odo = posY;
    robot->orientation_odo = angle;
  }
  else if (robot->robot_type == USE_MAYA )
  {
    pthread_mutex_lock(&robot->lock);

    robot->posx_odo =  robot->posx_odo_th;
    robot->posy_odo =  robot->posy_odo_th;
    robot->orientation_odo = robot->orientation_odo_th;

    //	  printf(" pox X : %f || pos Y : %f || Angle : %f \n\n",robot->posx_odo , robot->posy_odo, robot->orientation_odo);

    pthread_mutex_unlock(&robot->lock);
  }
  else
  {
    fprintf(stderr, "ERROR in robot_get_location_odo: Robot type %d is not supported\n", robot->robot_type);
    exit(1);
  }
}
