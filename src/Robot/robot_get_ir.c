/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Robot.h>
#include <World.h>
#include <ClientHW.h>
#include <ClientRobubox.h>
#include <ClientPlayer.h>
#include <reseau.h>
#include <net_message_debug_dist.h>

#include "tools/include/local_var.h"

#ifdef PLAYER
#include <libplayerc/playerc.h>
#endif

/*#define DEBUG*/
void robot_get_ir(Robot * robot, float *IR)
{
    static int *nothing = 0;
    static char data[80];
    static int d[16];
    int i;
    char commande[256];
    char result[256];
    char us[256];
#ifdef PLAYER
    ClientPlayer  * pc;   
    playerc_sonar_t * player_sonar_proxy ;

#endif

    dprints("robot_type:%d\n", robot->robot_type);

    if (robot->robot_type == 1 || robot->robot_type == 2)
    {
        dprints("avant commande_koala\n");
        robot_commande_koala(robot, COM_N, nothing, data);
        dprints("apres commande_koala\n");
        if (robot->nb_ir == 16)
            sscanf(data, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                   d, d + 1, d + 2, d + 3, d + 4, d + 5, d + 6, d + 7, d + 8,
                   d + 9, d + 10, d + 11, d + 12, d + 13, d + 14, d + 15);
        else if (robot->nb_ir == 10)
            sscanf(data, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                   d, d + 1, d + 2, d + 3, d + 4, d + 5, d + 6, d + 7, d + 8,
                   d + 9);

        for (i = 0; i < robot->nb_ir; i++)
            IR[i] = (float) d[i] / robot->actmax_ir;
    }
    else if (robot->robot_type == 3)
    {
        ClientHW *chw;
        dprints("debut get_ir rsx\n");

        chw = clientHW_get_clientHW_by_name(robot->clientHW);
        sprintf(commande, "robot_get_ir %s", robot->name);
        clientHW_transmit_receive(chw, commande, result);
        dprints("result after transmit: %s\n", result);

        sscanf(result, "v %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
               IR, IR + 1, IR + 2, IR + 3, IR + 4, IR + 5, IR + 6, IR + 7,
               IR + 8, IR + 9, IR + 10, IR + 11, IR + 12, IR + 13, IR + 14,
               IR + 15);

        /*printf("lecture done\n"); */

/*	printf("Valeur de sortie: %f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f \n",
	 IR[0], IR[1], IR[2], IR[3], IR[4], IR[5], IR[6], IR[7], IR[8], IR[9], IR[10],IR[11],
	 IR[12], IR[13], IR[14], IR[15]);*/
        /*IR entre 0 et 1: 0 pas d'obstacle, 1 : obstacle */

        for (i = 0; i < robot->nb_ir; i++)
            IR[i] = IR[i] / robot->actmax_ir;



    }
    else if (robot->robot_type == 4)
    {
       int nb_obstacles = 0, i, j, obs_invisible, nb_doors = 0;
       Obstacle *the_obstacles = NULL;
       Door *the_doors = NULL;
       float x_agent,y_agent,theta_agent, radius_proximity;
       float Ax,Ay,Bx,By,Cx,Cy,Dx,Dy,r,s,den,num_r,num_s,px,py,dir_laser,act,act_max,Ix,Iy,D,CALBG_x,CALBG_y,COBG_x,COBG_y,CALHD_x,CALHD_y,COHD_x,COHD_y,door_direction;
       World *world = world_get_first_world();

       if (world == NULL)
       {
	  EXIT_ON_ERROR("ERROR in robot_get_ir: Robot or World is NULL, cannot simulate proximity sensors\n");
       }

       nb_obstacles = world->nb_obstacles;
       the_obstacles = world->obstacles;

       nb_doors = world->nb_doors;
       the_doors = world->doors;

       robot_get_location_abs(robot);
       x_agent = robot->posx_abs;
       y_agent = robot->posy_abs;
       theta_agent = robot->orientation_abs;
       radius_proximity = robot->actmax_ir;
       /*Pour toutes les direction, on calcule l'intersection des segment */
   
       for(i = 0; i < robot->nb_ir; i++)      
       {
          /*calcul du point lointain*/
	  dir_laser = robot->angle_ir[i] * 2. * M_PI / 360.;
       
	  px = x_agent + radius_proximity * cos(dir_laser + theta_agent) / world->spatial_constant;
	  py = y_agent + radius_proximity * sin(dir_laser + theta_agent) / world->spatial_constant;

	  act = 0.0;
	  act_max = 0.;

	  for(j = 0; j < nb_obstacles; j++)
	  {
	     /*Intersection AB et CD ?*/
	     Ax = the_obstacles[j].xa;
	     Ay = the_obstacles[j].ya; 
	     Bx = the_obstacles[j].xb;
	     By = the_obstacles[j].yb;
                    
	     Cx = px;
	     Cy = py;
	     Dx = x_agent;
	     Dy = y_agent;
                
	     den = (Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
	     num_r = (Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
	     num_s = (Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);

	     if(isdiff(den, 0.)) /*sinon droite parallele*/
	     {
		r = num_r/den;
		s = num_s/den;
		/*Segment croises*/
		if(r >= 0. && r <= 1. && s >= 0. && s <= 1.)
		{
		   Ix = Ax+r*(Bx-Ax);
		   Iy = Ay+r*(By-Ay);
		   D = sqrt((Ix-Dx)*(Ix-Dx) + (Iy-Dy)*(Iy-Dy));
		   act = 1.-D/(radius_proximity/world->spatial_constant);
		}
	     }    
	     else
	     {
		/*Segmant parallele*/
		/*cohincidence*/
		if(isequal(num_r, 0.))
		{
		   /*Si O1 ou O2 dans rect A L*/
		   if(Cx >= Dx)
		   {
		      CALBG_x = Dx;
		      CALHD_x = Cx;
		   }
		   else
		   {
		      CALBG_x = Cx;
		      CALHD_x = Dx;
		   }
                              
		   if(Cy >= Dy)
		   {
		      CALBG_y = Dy;
		      CALHD_y = Cy;
		   }
		   else
		   {
		      CALBG_y = Cy;
		      CALHD_y = Dy;
		   }

		   if((Ax >= CALBG_x && Ax <= CALHD_x) && (Ay >= CALBG_y && Ay <= CALHD_y))
		      obs_invisible = 1;

		   if((Bx >= CALBG_x && Bx <= CALHD_x) && (By >= CALBG_y && By <= CALHD_y))
		      obs_invisible = 1;

		   /*si A ou L dans rect O1 O2*/
		   if (Ax >= Bx)
		   {
		      COBG_x = Bx;
		      COHD_x = Ax;
		   }
		   else
		   {
		      COBG_x = Ax;
		      COHD_x = Bx;
		   }
                              
		   if(Ay >= By)
		   {
		      COBG_y = By;
		      COHD_y = Ay;
		   }
		   else
		   {
		      COBG_y = Ay;
		      COHD_y = By;
		   }

		   if((Cx >= COBG_x && Cx <= COHD_x) && (Cy >= COBG_y && Cy <= COHD_y))
		      obs_invisible = 1;
		   
		   if((Dx >= COBG_x && Dx <= COHD_x) && (Dy >= COBG_y && Dy <= COHD_y))
		      obs_invisible = 1;
		}
	     }
	     if(act >= act_max)
			act_max = act;

	     IR[i] = act_max;
	  }

	  for(j = 0; j < nb_doors; j++)
	  {
	     /*Intersection AB et CD ?*/
	     Ax = the_doors[j].xa;
	     Ay = the_doors[j].ya; 
	     Bx = the_doors[j].xb;
	     By = the_doors[j].yb;
	     
	     door_direction = the_doors[j].direction;
                    
	     Cx = px;
	     Cy = py;
	     Dx = x_agent;
	     Dy = y_agent;
                
	     den = (Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
	     num_r = (Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
	     num_s = (Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);

		if(((isequal(door_direction,1) || isequal(door_direction,2)) && (den > 0.)) || ((isequal(door_direction,3) || isequal(door_direction,4)) && (den < 0.)))
		{
	     if(isdiff(den, 0.)) /*sinon droite parallele*/
	     {
		r = num_r/den;
		s = num_s/den;
		/*Segment croises*/
		if(r >= 0. && r <= 1. && s >= 0. && s <= 1.)
		{
		   Ix = Ax+r*(Bx-Ax);
		   Iy = Ay+r*(By-Ay);
		   D = sqrt((Ix-Dx)*(Ix-Dx) + (Iy-Dy)*(Iy-Dy));
		   act = 1.-D/(radius_proximity/world->spatial_constant);
		}
	     }
	     
	     if(act >= act_max)
			act_max = act;

	     IR[i] = act_max;

	 }
	  }
  }
    (void) obs_invisible;
    }
    else if (robot->robot_type == 5)
    {
#ifdef PLAYER
        dprints("debut GetUS\n");
        
        pc= clientPlayer_get_clientPlayer_by_name(robot->clientPlayer);
	playerc_client_read(pc->player_client);
	player_sonar_proxy=pc->player_sonar_proxy;
	for (i = 0; i < player_sonar_proxy->scan_count; i++)
	  {
		IR[i]=player_sonar_proxy->scan[i];
	    printf("[%6.3f] ", IR[i]);
                
	  }
	printf("\n");
	for (i = 0; i < robot->nb_ir; i++)
	  {
	    IR[i] = 1 - (IR[i] / robot->actmax_ir);
	    if (IR[i]<0)
		IR[i] = 0;
	    printf("[%6.3f] ", IR[i]);
	  }
	printf("\n");
#endif
    }
    else if ( robot->robot_type == 6 || robot->robot_type == USE_ROBUBOX_V2 )
    {
        ClientRobubox *crb;
        dprints("debut GetUS\n");

        crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);
        sprintf(commande, "%s", "GetUS");
        clientRobubox_transmit_receive(crb, commande, result);
        dprints("result after transmit: %s\n", result);

	sscanf(result, "%s %f %f %f %f %f %f %f %f %f", us, &IR[0], &IR[1], &IR[2], &IR[3], &IR[4], &IR[5], &IR[6], &IR[7], &IR[8]);

        for (i = 0; i < robot->nb_ir; i++)
	{
	   /*sscanf(result + 2 + (i * 6), " %f", &(IR[i]));*/
	   if( IR[i] > 0 ) IR[i] = 1 - (IR[i] / robot->actmax_ir);
	   if (IR[i] < 0)
	   {
	      dprints("Ultrasound sensor %i returned a negative value (%f) => Setting to 0", i, IR[i]);
	      IR[i] = 0;
	   }
	}
    }

#ifdef DEBUG
    if (robot->nb_ir == 16)
    {
/*printf("Valeur brute: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",
	 d, d+1, d+2, d+3, d+4, d+5, d+6, d+7, d+8, 	d+9, d+10, d+11,
	 d+12, d+13, d+14, d+15);*/
        dprints
            ("Valeur de sortie: %f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f \n",
             IR[0], IR[1], IR[2], IR[3], IR[4], IR[5], IR[6], IR[7], IR[8],
             IR[9], IR[10], +IR[11], IR[12], IR[13], IR[14], IR[15]);
    }
    else if (robot->nb_ir == 10)
    {
/*	printf("Valeur brute: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
	 d, d+1, d+2, d+3, d+4, d+5, d+6, d+7, d+8, d+9);*/
        dprints("Valeur de sortie: %f,%f,%f,%f,%f,%f,%f,%f,%f,%f \n",
               IR[0], IR[1], IR[2], IR[3], IR[4], IR[5], IR[6], IR[7], IR[8],
               IR[9]);
    }
    else if (robot->nb_ir == 9)
      {
	dprints("Valeur de sortie: %f,%f,%f,%f,%f,%f,%f,%f,%f \n", IR[0], IR[1], IR[2], IR[3], IR[4], IR[5], IR[6], IR[7], IR[8]);
      }
#endif
}
