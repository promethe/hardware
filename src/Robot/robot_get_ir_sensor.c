/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Robot.h>
#include <World.h>
#include <ClientHW.h>
#include <ClientRobubox.h>
#include <ClientPlayer.h>
#include <reseau.h>
#include <net_message_debug_dist.h>

#include "tools/include/local_var.h"

#ifdef PLAYER
#include <libplayerc/playerc.h>
#endif

/*#define DEBUG*/
void robot_get_ir_sensor(Robot * robot, float *IR)
{
    int i;
    char commande[256];
    char result[256];
    char ir[256];
#ifdef PLAYER
    ClientPlayer  * pc;   
    playerc_sonar_t * player_sonar_proxy ;

#endif

    dprints("robot_type:%d\n", robot->robot_type);

    if ( robot->robot_type == USE_ROBUBOX_V2 )
    {
        ClientRobubox *crb;
        /*dprints("debut GetIR\n");*/
        dprints("debut GetIR\n");

        crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);
        sprintf(commande, "%s", "GetIR");
        clientRobubox_transmit_receive(crb, commande, result);
        dprints("result after transmit: %s\n", result);

	sscanf(result, "%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", ir, &IR[0], &IR[1], &IR[2], &IR[3], &IR[4], &IR[5], &IR[6], &IR[7], &IR[8],&IR[9],&IR[10],&IR[11],&IR[12],&IR[13],&IR[14],&IR[15]);

        for (i = 0; i < robot->nb_ir_sensor; i++)
	{
	   if( IR[i] <= robot->seuil_ir_sensor ) IR[i]=0;
	   /*sscanf(result + 2 + (i * 6), " %f", &(IR[i]));*/
	   if( IR[i] > 0 ) IR[i] = 1 - (IR[i] / robot->actmax_ir_sensor);

	   if (IR[i] < 0 )
	   {
	      dprints("InfraRed sensor %i returned a negative value (%f) => Setting to 0", i, IR[i]);
	      IR[i] = 0;
	   }
	}
    }
}
