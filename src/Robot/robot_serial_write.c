/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <stdio.h>
#include <Robot.h>
#include <Serial.h>

int robot_serial_write(Robot * robot, char *buff, char c, char *retour)
{
    int n, t, j = 0, i /*, l, m,serialport */ ;
/*  char lu;*/
/*  Serial* this_serial;
  n = strlen(buff);
  buff[n] = 0x0D;
  buff[n + 1] = 0;

  this_serial = serial_get_serial_by_name(robot->port);
  if(this_serial==NULL)
  {
	  printf("robot_serial_write : le port serie %s n'existe pas\n",robot->port);
	  exit(1);
  }
  
serialport=this_serial->desc;*/
    for (t = 0; t < TIMEOUT; t++)
    {
        serial_send_and_receive(robot->port, buff, retour, strlen(buff));
        /*on teste le formatage de la chaine de caractere de retour */
        if ((retour[0] == c)
            || (c == 'n' && retour[0] == 'N' && robot->robot_type == 2))
            /*En fait si on envoie D,0,0 la caractere de retour attendu est 'd', qui se trouve dans 'char c'. Mais comme le retour du labo3 a la commande N est aussi N (va savoir pq..), il faut bien traiter le cas pour que ca marche: Encore une preuve qui faut etre standart sous peine de faire des codes de schizo */
        {
            n = strlen(retour);
            for (i = 0, j = 0; i < n; i++)
                if (retour[i] == ',')
                    j++;

            switch (buff[0])
            {
            case COM_B:
                if (j == 2)
                    return (1);
            case COM_E:
                if (j == 2)
                    return (1);
            case COM_H:
                if (j == 2)
                    return (1);

            case COM_I:
                if (j == 1)
                    return (1);
            case COM_T:
                if (j == 1)
                    return (1);
            case COM_R:
                if (j == 1)
                    return (1);
            case COM_Y:
                if (j == 1)
                    return (1);

            case COM_K:
                if (j == 6)
                    return (1);

            case COM_N:
                if ((j == 16 && robot->robot_type == 1)
                    || (j == 10 && robot->robot_type == 2))
                    return (1);
            case COM_O:
                if (j == 16)
                    return (1);


            case COM_Z:
            case COM_A:
            case COM_C:
            case COM_D:
            case COM_F:
            case COM_G:
            case COM_J:
            case COM_L:
            case COM_Q:
            case COM_P:
            case COM_W:
                return (1);

            default:
                printf
                    ("ERREUR lors de l'ecriture sur la RS232 : chaine renvoyee mal formatee : %s\n",
                     retour);
                exit(0);

            }
        }
    }
    printf
        ("ERREUR lors de l'ecriture sur la RS232 : code renvoye faux : %s\n",
         retour);
    exit(0);
}
