/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Robot.h>
#include <Serial.h> 
#include <unistd.h>   /* For open(), creat() */
#include <sys/time.h>
#include <termios.h>
#include <fcntl.h>    /* For O_RDWR */ 

#define DEBUG

#define RAYON 0
#define NBT 360
#define ENTRAX 0

void robot_get_odometry_thread(Robot *robot)
{
   Serial *s_port;
   unsigned char reponse[10];
   char cmd_odo[10];
   char cmd_rst[10];
   int tic_droit,tic_gauche;
   float dist_droit,dist_gauche;
   float dAlpha,dDelta;
   float dX,dY;
   int ret;
   int cumul_problem=0;



   if( robot == NULL )
   {
      dprints("%s : stop thread function, pointer null ",__FUNCTION__);
      EXIT_ON_ERROR(" stop thread function, pointer null ");
   }

   if( robot->robot_type != USE_MAYA )
   {
      EXIT_ON_ERROR(" thread odometry Only available for MAYA ");
   }

   sprintf(cmd_odo,"%c%c", SYNC_BYTE,GET_ENCODERS );
   sprintf(cmd_rst,"%c%c", SYNC_BYTE,RESET_ENCODERS);

   serial_send(robot->port, cmd_rst, 2);

   while( robot->enable == 1)
   {
      ret = serial_send_and_receive_nchar(robot->port, cmd_odo, (char*)reponse,2,8);

      if(ret == 0)  /*Aucun retour d'Odometrie.*/
      {
         printf("robot_get_odometry_thread: [%s] : ret = %d // %s \n", robot->name, ret, reponse);
         cumul_problem ++;
         if(cumul_problem > 2)   /*On attend 2 itérations pour être sur que le probleme est justifié*/
         {
            cumul_problem = 0;
            printf("\nOdometry Problem: resetting serial com...\n");

            /* recherche du port */
            s_port = serial_get_serial_by_name(robot->port);
            if (s_port == NULL)
            {
               printf("serial_send : Port %s non trouve\n", robot->port);
               exit(1);
            }
            /*fermeture liaison serie*/
            if (close(s_port->desc) != 0)
            {
               printf("serial_close : ERREUR fermeture du port serie\n");
               exit(1);
            }
            usleep(5000);
            /*ouverture liaison serie*/
            if ((s_port->desc =open((char *) (s_port)->path, O_RDWR |  (s_port)->onblock )) < 0)
            {
               printf("serial_init: ERREUR ouverture du port serie pour l'adresse %s \n",(s_port)->path);
               exit(1);
            }
            usleep(5000);
            /*retablissement des parametres initiaux.*/
            if (serial_setparam(robot->port, s_port->debit, s_port->bit, s_port->parite, s_port->stop) == -1) printf("f_serial_init: port %s inconnu!\n", robot->port);
            usleep(5000);
         }
      }

      dprints("ODO : TIME %ld  RET %d  REP : %d %d %d %d %d %d %d %d \n",t2.tv_usec - t.tv_usec, ret, reponse[0],reponse[1],reponse[2],reponse[3],reponse[4],reponse[5],reponse[6],reponse[7]);

      /*traitement de la réponse */
      if( ret == 8 )
      {
         pthread_mutex_lock(&robot->lock);

         tic_droit = (reponse[0] << 24) + (reponse[1] << 16) + (reponse[2] << 8) + reponse[3];
         tic_gauche= (reponse[4] << 24) + (reponse[5] << 16) + (reponse[6] << 8) + reponse[7];

	 
         dist_droit = tic_droit - robot->nb_clics_droits ;
         dist_gauche = tic_gauche - robot->nb_clics_gauches ;

	 if( (dist_droit + dist_gauche) < 0) 
	 {
		robot->mvt_sens  = -1;
	 }
	 else
	 {
		robot->mvt_sens = 1;
	 }


         robot->nb_clics_droits = tic_droit;
         robot->nb_clics_gauches = tic_gauche;

         // distance sur chaque roue en metre

         dist_droit=((dist_droit * 2 * M_PI * robot->wheel_radius) / robot->nbt_encoder);
         dist_gauche=((dist_gauche* 2 * M_PI * robot->wheel_radius) / robot->nbt_encoder);

         dAlpha=atan((dist_droit-dist_gauche)/robot->largeur_robot); //variation de l'angle
         dDelta=(dist_droit + dist_gauche)/2;   //variation de l'avancement

         //calcul des dÃ©calages selon X et Y
         dX = cos(robot->orientation_odo_th + dAlpha/2) * dDelta;
         dY = sin(robot->orientation_odo_th + dAlpha/2) * dDelta;
         robot->orientation_odo_th += dAlpha;

         if(robot->orientation_odo_th >= 2 * M_PI) robot->orientation_odo_th -=  2 * M_PI;
         if(robot->orientation_odo_th < 0) robot->orientation_odo_th +=  2 * M_PI;

         robot->posx_odo_th+=dX;
         robot->posy_odo_th+=dY;

         pthread_mutex_unlock(&robot->lock);

         if( abs(robot->nb_clics_droits)>robot->nb_clics_max|| abs(robot->nb_clics_gauches)>robot->nb_clics_max)
         {
            serial_send(robot->port, cmd_rst, 2);
            robot->nb_clics_droits = 0;
            robot->nb_clics_gauches = 0;
         }
      }
      else {
         cprints("%s : erreur de lecture \n",__FUNCTION__);
      }
      usleep(5000);
   }
}
