/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Robot.h>
#include <stdio.h>
#include <stdlib.h>
#include <ClientHW.h>
void robot_go_by_position(Robot * robot, float Left, float Right)
{
    int parm[2];
    char *noth = 0;
    int *noth_in = 0;
    char data[50];
    int d[2];
    float rapport;
    char commande[255];
    ClientHW *chw;
    if (robot->robot_type == 1)
    {
        /* Set actual position to 0,0 */
        parm[0] = 0;
        parm[1] = 0;
        robot_commande_koala(robot, COM_G, parm, noth);

        /* Read actual position
           commande_koala(COM_H, nothing, data);
           sscanf(data,"%d,%d",d ,d+1);
           printf("last position %d %d \n",parm[0], parm[1]);
           parm[0]=d[0];
           parm[1]=d[1]; */

        /* go to position dist,dist */
        parm[0] += (int) (Left * 333.0);
        parm[1] += (int) (Right * 333.0);
        robot_commande_koala(robot, COM_C, parm, noth);
#ifdef ONLINE_DEBUG
        printf("new position %d %d \n", parm[0], parm[1]);
#endif
        while (!robot_precise_last_target_reached(robot)) ;
    }
    else if (robot->robot_type == USE_LABO3)
    {
        printf("demarrage, r: %f, l : %f\n", Right, Left);
        parm[1] = (int) (-333. * Right);
        parm[0] = (int) (-333. * Left);
        robot_commande_koala(robot, COM_G, parm, noth);
        printf("demarrage, r: %d, l : %d\n", parm[1], parm[0]);

        if (abs((int) Left) > abs((int) Right))
        {
            printf("passage0\n");
            rapport = Right / Left;
            if (Left < 0.)
            {
                parm[1] = (int) (-5);
                parm[0] = (int) (-5 * rapport);
            }
            else
            {
                parm[1] = (int) (5);
                parm[0] = (int) (5 * rapport);
            }
            robot_commande_koala(robot, COM_D, parm, noth);
            robot_commande_koala(robot, COM_H, noth_in, data);
            sscanf(data, "%d,%d", d, d + 1);
            while (abs(d[0]) > 30)
            {
                robot_commande_koala(robot, COM_H, noth_in, data);
                sscanf(data, "%d,%d", d, d + 1);
                printf("data0:%d\n", d[1]);
            }
            parm[0] = 0;
            parm[1] = 0;
            robot_commande_koala(robot, COM_D, parm, noth);
        }
        else
        {
            printf("passage1\n");
            rapport = Left / Right;
            if (Right < 0.)
            {
                parm[0] = (int) (-5);
                parm[1] = (int) (-5 * rapport);
            }
            else
            {
                parm[0] = (int) (5);
                parm[1] = (int) (5 * rapport);
            }

            robot_commande_koala(robot, COM_D, parm, noth);

            robot_commande_koala(robot, COM_H, noth_in, data);
            sscanf(data, "%d,%d", d, d + 1);
            while (abs(d[1]) > 30)
            {
                robot_commande_koala(robot, COM_H, noth_in, data);
                sscanf(data, "%d,%d", d, d + 1);
                printf("data1_0:%d --- data1_1:%d\n", d[0], d[1]);
            }
            parm[0] = 0;
            parm[1] = 0;
            robot_commande_koala(robot, COM_D, parm, noth);
        }
    }
    else if (robot->robot_type == 3)
    {
        sprintf(commande, "robot_go_by_position %s %f %f", robot->name, Left,
                Right);
        chw = clientHW_get_clientHW_by_name(robot->clientHW);
        clientHW_transmit(chw, commande);
        return;
    }


}

void robot_go_by_position_with_odo(Robot * robot, float Left, float Right)
{
    int parm[2];
    char *noth = 0;
    int *noth_in = 0;
    char data[50];
    int d[2];
    float rapport;
    char commande[255];
    int consigne0, consigne1;
    ClientHW *chw;
    if (robot->robot_type == 1)
    {
        /* Set actual position to 0,0 */
        robot_commande_koala(robot, COM_H, noth_in, data);
        sscanf(data, "%d,%d", d, d + 1);


        parm[0] = d[0];
        parm[1] = d[1];
        /*     robot_commande_koala(robot,COM_G, parm, noth); */

        /* Read actual position
           commande_koala(COM_H, nothing, data);
           sscanf(data,"%d,%d",d ,d+1);
           printf("last position %d %d \n",parm[0], parm[1]);
           parm[0]=d[0];
           parm[1]=d[1]; */

        /* go to position dist,dist */
        parm[0] += (int) (Left);
        parm[1] += (int) (Right);
        robot_commande_koala(robot, COM_C, parm, noth);
#ifdef ONLINE_DEBUG
        printf("new position %d %d \n", parm[0], parm[1]);
#endif
        while (!robot_precise_last_target_reached(robot)) ;
    }
    else if (robot->robot_type == USE_LABO3)
    {
        printf("demarrage, r: %f, l : %f\n", Right, Left);
        robot_commande_koala(robot, COM_H, noth_in, data);
        sscanf(data, "%d,%d", d, d + 1);
        consigne0 = (int) (d[0] + Left);
        consigne1 = (int) (d[1] + Right);


        printf("etat:  %d   %d\n", d[0], d[1]);
        printf("consigne:  %d   %d\n", consigne0, consigne1);

#ifdef DEBUG
        printf("demarrage, r: %d, l : %d\n", parm[1], parm[0]);
#endif

        if (abs((int) Left) > abs((int) Right))
        {
            printf("passage0\n");
            rapport = Right / Left;
            printf("rapport:%f\n", rapport);
            if (Left < 0.)
            {
                parm[1] = (int) (-5);
                parm[0] = (int) (-5 * rapport);
            }
            else
            {
                parm[1] = (int) (5);
                parm[0] = (int) (5 * rapport);
            }
            robot_commande_koala(robot, COM_D, parm, noth);
            robot_commande_koala(robot, COM_H, noth_in, data);
            sscanf(data, "%d,%d", d, d + 1);
            while (d[1] < consigne0 - 30 || d[1] > consigne0 + 30)
            {
                robot_commande_koala(robot, COM_H, noth_in, data);
                sscanf(data, "%d,%d", d, d + 1);
#ifdef DEBUG
                printf("data0:%d\n", d[1]);
#endif
            }
            parm[0] = 0;
            parm[1] = 0;
            robot_commande_koala(robot, COM_D, parm, noth);
        }
        else
        {
            printf("passage1\n");
            rapport = Left / Right;
            printf("rapport:%f\n", rapport);
            if (Right < 0.)
            {
                parm[0] = (int) (-5);
                parm[1] = (int) (-5 * rapport);
            }
            else
            {
                parm[0] = (int) (5);
                parm[1] = (int) (5 * rapport);
            }

            robot_commande_koala(robot, COM_D, parm, noth);

            robot_commande_koala(robot, COM_H, noth_in, data);
            sscanf(data, "%d,%d", d, d + 1);
            while (d[0] < consigne1 - 30 || d[0] > consigne1 + 30)
            {
                robot_commande_koala(robot, COM_H, noth_in, data);
                sscanf(data, "%d,%d", d, d + 1);
#ifdef DEBUG
                printf("data1_0:%d --- data1_1:%d\n", d[0], d[1]);
#endif
            }
            parm[0] = 0;
            parm[1] = 0;
            robot_commande_koala(robot, COM_D, parm, noth);
        }
    }
    else if (robot->robot_type == 3)
    {
        printf("passage1\n");
        sprintf(commande, "robot_go_by_position %s %f %f", robot->name, Left,
                Right);
        chw = clientHW_get_clientHW_by_name(robot->clientHW);
        printf("avant transmit\n");
        clientHW_transmit(chw, commande);
        return;

    }


}
