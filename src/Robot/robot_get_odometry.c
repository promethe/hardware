/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*#define DEBUG*/

#include <Robot.h>
#include <ClientRobubox.h>
#include "tools/include/local_var.h"
#include <net_message_debug_dist.h>


void robot_get_odometry(Robot * robot)
{
  static int reset = 1;
  if (robot == NULL)
    EXIT_ON_ERROR("ERROR in robot_get_odometry: NULL pointer for robot\n");

  if (robot->robot_type == 1 || robot->robot_type == 2 || robot->robot_type == 3){
#ifdef TIME_TRACE
    static struct timeval FunctionTimeTrace_before, FunctionTimeTrace_after, new_FunctionTimeTrace_after ;
    char MessageFunctionTimeTrace[256];
    long SecondesFunctionTimeTrace;
    long MicroSecondesFunctionTimeTrace;
#endif

    int inverse ;
    int dist_gauche_clic, dist_droite_clic, dist_temp, largeur_clic = robot->largeur_robot,
        nb_clics_gauches, nb_clics_droits ;
    double distance_clic, distance, angle ;

#ifdef TIME_TRACE
    /* Recuperation de l'heure avant la lecture des compteurs de clics des roues */
    gettimeofday(&FunctionTimeTrace_before, (void *) NULL);
#endif

    /* Recuperation des valeurs des compteurs de clics des roues*/
    robot_read_wheels_counter(robot, &nb_clics_gauches, &nb_clics_droits) ;

#ifdef TIME_TRACE
    /* Recuperation de l'heure apres la lecture des compteurs de clics des roues */
    gettimeofday(&new_FunctionTimeTrace_after, (void *) NULL);

    /* Calcul du temps passe a l'exterieur de la lecture des compteurs de clic des roues */
    if (FunctionTimeTrace_before.tv_usec >= FunctionTimeTrace_after.tv_usec)
    {
      SecondesFunctionTimeTrace = FunctionTimeTrace_before.tv_sec - FunctionTimeTrace_after.tv_sec ;
      MicroSecondesFunctionTimeTrace = FunctionTimeTrace_before.tv_usec - FunctionTimeTrace_after.tv_usec ;
    }
    else
    {
      SecondesFunctionTimeTrace = FunctionTimeTrace_before.tv_sec - FunctionTimeTrace_after.tv_sec - 1 ;
      MicroSecondesFunctionTimeTrace =
          1000000 + FunctionTimeTrace_before.tv_usec - FunctionTimeTrace_after.tv_usec ;
    }
    sprintf(MessageFunctionTimeTrace, "%4ld.%06ld",
        SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace) ;
    printf("Temps passe a l'exterieur de la lecture : %s\n", MessageFunctionTimeTrace);

    FunctionTimeTrace_after = new_FunctionTimeTrace_after ;

    /* Calcul du temps passe a l'interieur de la lecture des compteurs de clic des roues */
    if (FunctionTimeTrace_after.tv_usec >= FunctionTimeTrace_before.tv_usec)
    {
      SecondesFunctionTimeTrace = FunctionTimeTrace_after.tv_sec - FunctionTimeTrace_before.tv_sec ;
      MicroSecondesFunctionTimeTrace = FunctionTimeTrace_after.tv_usec - FunctionTimeTrace_before.tv_usec ;
    }
    else
    {
      SecondesFunctionTimeTrace = FunctionTimeTrace_after.tv_sec - FunctionTimeTrace_before.tv_sec - 1 ;
      MicroSecondesFunctionTimeTrace =
          1000000 + FunctionTimeTrace_after.tv_usec - FunctionTimeTrace_before.tv_usec ;
    }
    sprintf(MessageFunctionTimeTrace, "%4ld.%06ld",
        SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace) ;
    printf("Temps passe a l'interieur de la lecture : %s\n", MessageFunctionTimeTrace);
#endif

    /* Calcul des distances parcourues (gauche et droite) en clic */
    dist_gauche_clic = nb_clics_gauches - robot->nb_clics_gauches ;
    dist_droite_clic = nb_clics_droits - robot->nb_clics_droits ;

    /* Mise a zero des compteurs de clics des roues en cas de depassement de la valeur max */
    if ((nb_clics_gauches > robot->nb_clics_max) || (nb_clics_droits > robot->nb_clics_max))
    {
      robot_init_wheels_counter(robot);
      nb_clics_gauches = 0 ;
      nb_clics_droits = 0 ;
    }

    /* Mise a jour des valeurs des compteurs de clics des roues dans la structure Robot */
    robot->nb_clics_gauches = nb_clics_gauches ;
    robot->nb_clics_droits = nb_clics_droits ;

    /* Calcul des valeurs d'odometrie */
    if (dist_gauche_clic == dist_droite_clic)
    {
      angle = 0. ;
      distance_clic = dist_gauche_clic ;
    }
    else
    {
      /* On se ramene toujours au cas ou dist_gauche est superieure a dist_droite pour simplifier */
      if (dist_gauche_clic < dist_droite_clic)
      {
        dist_temp = dist_gauche_clic ;
        dist_gauche_clic = dist_droite_clic ;
        dist_droite_clic = dist_temp ;

        inverse = 1 ;
      }
      else /* dist_gauche_clic > dist_droite_clic */
        inverse = 0 ;

      /* VERSION A */
      /* cas ou dist_droite est nulle */
      /*if (dist_droite_clic == 0)
       {
       angle = (float)(dist_gauche_clic) / (float)(largeur_clic) ;
       distance_clic = largeur_clic * sin(angle/2.) ;
       }*/
      /*else*/ /* cas general */
      /*{
       l_clic = (float)(dist_droite_clic * largeur_clic)/(float)(dist_gauche_clic - dist_droite_clic) ;
       angle = (float)(dist_droite_clic)/l_clic ;

       distance_clic = 2*((float)(largeur_clic)/2. + l_clic)*sin(angle/2.) ;
       }*/

      /* VERSION B */
      angle = (float)(dist_gauche_clic - dist_droite_clic)/(float)(largeur_clic) ;
      distance_clic = (float)(dist_gauche_clic + dist_droite_clic)/2. ;

      if (inverse)
        angle = -angle;
    }

    /* Conversion des clics vers les metres */
    /* ATTENTION : le coefficient peut varier selon le robot utilise */
    distance = distance_clic / 222. ;

    /* Conversion des radians vers les degres */
    angle = (angle / (2. * M_PI)) * 360. ;


    /*Mise a jour des valeurs d'odometrie du robot */
    robot->distance = distance ;
    robot->angle = angle ;
  }
  else if (robot->robot_type == 4)
  {
    float distX = 0., distY = 0., angle = 0.;

    robot_get_location_abs(robot);

    if (robot->last_posx_abs < -99999 || robot->last_posy_abs < -99999 || robot->last_orientation_abs < -99999)
    {
      robot->distance = 0; /* distance en metres */
      robot->angle = 0; /* distance en metres */
    }
    else
    {
      distX = robot->posx_abs - robot->last_posx_abs;
      distY = robot->posy_abs - robot->last_posy_abs;
      angle = robot->orientation_abs - robot->last_orientation_abs;

      while(angle < 0 || angle > 2.*M_PI)
      {
        if(angle < 0)
          angle += 2.*M_PI;
        else if (angle > 2*M_PI)
          angle -= 2.*M_PI;
      }

      robot->distance = sqrt(distX * distX + distY * distY);
      robot->angle = (angle / (2 * M_PI)) * 360.; /* angle en degres */
    }

    robot->last_posx_abs = robot->posx_abs;
    robot->last_posy_abs = robot->posy_abs;
    robot->last_orientation_abs = robot->orientation_abs;
  }
  else if (robot->robot_type == 6 || robot->robot_type == USE_ROBUBOX_V2)
  {
    char commande[256];
    char resultat[256];
    char cmd[1024];
    ClientRobubox *crb;
    float distX = 0., distY = 0., angle = 0.;
    float pos_x, pos_y, new_angle;

    crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);

    if(reset == 1)
    {
      sprintf(commande,"%s","ResetOdo");
      clientRobubox_transmit_receive(crb,commande,resultat);
      reset = 0;
    }

    sprintf(commande,"%s","Odo");
    memset(resultat,0,256*sizeof(char));
    clientRobubox_transmit_receive(crb,commande,resultat);
    sscanf(resultat,"%s %f %f %f", cmd, &pos_x, &pos_y, &new_angle);
    dprints("%s %f %f %f\n", cmd, pos_x, pos_y, new_angle);
    fflush(stdout);

    /*if( robot->robot_type == USE_ROBUBOX_V2)  new_angle = -new_angle * M_PI / 376.3;*//*Ancienne correction avec le premier firmware*/
    /*correction avec le firmware 3.0 : un demi-tour egal 324 au lieu de 314*/
    if( robot->robot_type == USE_ROBUBOX_V2)  new_angle = -new_angle * M_PI / 324;

    if (robot->last_posx_abs < -99999 || robot->last_posy_abs < -99999 || robot->last_orientation_abs < -99999)
    {
      robot->distance = 0; /* distance en metres */
      robot->angle = 0; /* distance en metres */
    }
    else
    {
      distX = pos_x - robot->last_posx_abs;
      distY = pos_y - robot->last_posy_abs;
      angle = new_angle - robot->last_orientation_abs;
      robot->distance = sqrt(distX * distX + distY * distY); /* distance en metres */

      // sur Berenson la distance retournee est en centimetre !
      if( robot->robot_type == USE_ROBUBOX_V2)   robot->distance /= 100.0;

      while(angle <= -M_PI || angle > M_PI)
      {
        if(angle <= -M_PI)
          angle += 2.*M_PI;
        else if (angle > M_PI)
          angle -= 2.*M_PI;
      }
      robot->angle = (angle / (2 * M_PI)) * 360.; /* angle en degres */
    }
    robot->last_posx_abs = pos_x;
    robot->last_posy_abs = pos_y;
    robot->last_orientation_abs = new_angle;

  }
  else if (robot->robot_type == USE_MAYA)
  {
    float distX,distY;
    pthread_mutex_lock(&robot->lock);

    distX = robot->posx_odo_th - robot->last_posx_abs;
    distY = robot->posy_odo_th - robot->last_posy_abs;
    robot->angle = ((robot->orientation_odo_th - robot->last_orientation_abs) / M_PI) * 180.0;
    robot->distance = robot->mvt_sens * sqrt(distX * distX + distY * distY);

    robot->last_posx_abs =  robot->posx_odo_th ;
    robot->last_posy_abs =  robot->posy_odo_th ;
    robot->last_orientation_abs= robot->orientation_odo_th ;

    pthread_mutex_unlock(&robot->lock);
  }
  else
  {
    fprintf(stderr, "ERROR in robot_get_odometry: Robot type %d is not supported\n", robot->robot_type);
    exit(1);
  }
}
