/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  GoToLeftRightBySpeedUncond.c
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: N.Cuperlier
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
  The robot move with the given left-motor speed and
  right-motor speed, unconditional

Macro:
-COM_G
-COM_C
-COM_H

Local variables:
-nnoe

Global variables:
- created 
- used:		

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Com_Koala/commande_koala()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <math.h>
#include <stdio.h>
/*#include <Com_Koala.h> */
/*#include "../tools/Com_koala/tools/include/macro.h" */
#include <stdlib.h>
#include <Robot.h>
#include <World.h>
#include <ClientHW.h>
#include <ClientRobubox.h>
#include <Aibo.h>
#include <Serial.h>

#ifdef PLAYER
#include <ClientPlayer.h>
#endif

/*#define DEBUG*/

#include <net_message_debug_dist.h>

void robot_set_velocity(Robot * robot, float speed, float sideway,
      float angular_speed, int state)
{
   char commande[256];
   char resultat[256];
   ClientHW *chw;
   ClientRobubox *crb;

#ifdef PLAYER
   ClientPlayer  * pc;
   playerc_position2d_t * player_position_proxy ;
#endif

   if (robot->robot_type == 3)
   {
      sprintf(commande, "robot_set_velocity %s %f %f %f %d", robot->name,
            speed, sideway, angular_speed, state);
      chw = clientHW_get_clientHW_by_name(robot->clientHW);
      clientHW_transmit(chw, commande);

      return;
   }
   else if (robot->robot_type == 4) 
   {
      float angle, dx, dy, prec_x, prec_y, prec_theta;
      World *world = world_get_first_world();

      if (world == NULL)
      {
         fprintf(stderr, "ERROR in robot_set_velocity: No World is defined, cannot simulate movement\n");
         exit(1);
      }

      dprints("robot_set_velocity: linear_speed = %f, rotational_speed = %f\n", speed, angular_speed);

      prec_x = robot->posx_odo;
      prec_y = robot->posy_odo;
      prec_theta = robot->orientation_odo;

      dprints("robot_set_velocity: previous robot position => x = %f, y = %f, theta = %f\n", prec_x, prec_y, prec_theta);

      angle = angular_speed * robot->RS_max * world->time_constant;

      dx = cos(angle+prec_theta) * speed * robot->speed_max * world->time_constant/world->spatial_constant;
      dy = sin(angle+prec_theta) * speed * robot->speed_max * world->time_constant/world->spatial_constant;

      robot->posx_odo += dx;
      robot->posy_odo += dy;

      robot->orientation_odo = angle + prec_theta;

      dprints("robot_set_velocity: new robot position => x = %f, y = %f, theta = %f\n", robot->posx_odo, robot->posy_odo, robot->orientation_odo);

      while(robot->orientation_odo <= 0. || robot->orientation_odo > 2.*M_PI)
      {      
         if(robot->orientation_odo <= 0.)
            robot->orientation_odo += 2.*M_PI;
         else if (robot->orientation_odo > 2*M_PI)
            robot->orientation_odo -= 2.*M_PI;   
      }

      (void)prec_x;
      (void)prec_y;
      return;
   }
#ifdef PLAYER 
   else if (robot->robot_type == 5) 
   {
      /*  vitesse lineaire : cm/s
            vitesse angulaire : mrad/s
       */
      pc= clientPlayer_get_clientPlayer_by_name(robot->clientPlayer);
      playerc_client_read(pc->player_client);
      player_position_proxy=pc->player_position_proxy;
      playerc_position2d_set_cmd_vel(player_position_proxy, speed,sideway, angular_speed, state);

      return;
   }
#endif
   else if (robot->robot_type == 6 || robot->robot_type == USE_ROBUBOX_V2)
   {
      /*  vitesse lineaire : cm/s
            vitesse angulaire : mrad/s
       */

      double the_speed;
      double the_angular_speed;

      /* bidouillage pour repasser en m/s et rad/s */
      the_speed = robot->speed_max * speed * 100.;
      the_angular_speed = robot->RS_max * angular_speed * 100. * 13./8.;

      /*printf("send on socket : Drive %f %f\n", the_speed, the_angular_speed);*/
      sprintf(commande, "Drive %f %f", the_speed, the_angular_speed);
      crb = clientRobubox_get_clientRobubox_by_name(robot->clientRobubox);
      clientRobubox_transmit_receive(crb, commande,resultat);

      return;
   }
   else if (robot->robot_type == 7)
   {                         /* Nao */
      /* la vitesse de marche est inversement prop. a la valeur envoyee */
      int nao_speed = (int) 40 * (1 - speed);
      /* angular_speed dans [0, 2PI], sens trigo: PI/2 =  1/4 tour a gauche */
      double nao_angle = angular_speed;
      puts("NAO detected");
      /*
          printf("angular_speed = %f, nao_speed = %d\n", nao_speed);
          sleep(3);
          printf("angular_speed = %f, nao_angle = %f\n", angular_speed, nao_angle);
          sleep (3);
       */
      /*snprintf(commande, 255, "walk << motion.turn(%f,%d);", nao_angle, nao_speed);*/
      snprintf(commande, 255, "echo(\"walk << \" + ALMotion.turn(%f,%d));", nao_angle, nao_speed);
      aibo_send_message(commande, strlen(commande));
      /*snprintf(commande, 255, "walk << motion.walkStraight(%f,%d);", 0.3, nao_speed);*/
      snprintf(commande, 255, "echo(\"walk << \" + ALMotion.walkStraight(%f,%d));", 0.3, nao_speed);
      aibo_send_message(commande, strlen(commande));
      /* snprintf(commande, 255, "walk << motion.waitUntilWalkIsFinished();"); */
      snprintf(commande, 255, "echo(\"walk << \" + ALMotion.waitUntilWalkIsFinished());");
      aibo_send_message(commande, strlen(commande));
   }
   else if (robot->robot_type == USE_MAYA)
   {
      unsigned char mspeed;
      unsigned char mangular_speed;
      dprints("\n  speed :   %f  angular speed  %f || ",speed, angular_speed);

      if( speed > 1 ) speed = 1;
      if( speed < -1 ) speed = -1;

      if( angular_speed > 2 * M_PI) angular_speed = 2 * M_PI;
      if( angular_speed < -(2 * M_PI)) angular_speed = -(2 * M_PI);

			if( speed>-0.0001 && speed<0) speed = 0;
			if( speed < -0.0001 )	angular_speed = -angular_speed;

      mspeed = (unsigned char)(roundf( ((robot->speed_max * speed) + 1.0) * ((float)MPSPEED) / 2.0));
			mangular_speed =(unsigned char)( roundf((( robot->RS_max * angular_speed) + (2 * M_PI)) * ZSPEED / (2.0 * M_PI)));

      dprints(" mspeed :   %u  mangular speed  %u \n  ",mspeed, mangular_speed);
      sprintf(commande, "%c%c%c%c%c%c", SYNC_BYTE,SET_SPEED_1, mspeed , SYNC_BYTE ,SET_SPEED_2, mangular_speed);

      serial_send(robot->port, commande , 6);
   }
   else
   {
      printf("Unknown client\n");
      exit(0);
   }

}
