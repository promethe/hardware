/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Robot.h>
 /*#include <ClientHW.h> */
#include <stdio.h>
int robot_commande_koala(Robot * robot, char cmd, int *param, char *data)
{
    static char retour[100];    /*!< Retour de la commande */
    static char command[100];   /*!< Buffer de commande */
    int ok = 1;          /*!< Tout s'est bien passe */
    /*     ClientHW * chw; */
    /*int           n_parm;         !< Nombre de parametres */

    /*int           n_lus;                  !< Nombre de caracteres lus */

    switch (cmd)                /*<! Fabrique la commande en fonction des parametres */
    {
        /* Pas de parametre */
    /* B */ case COM_B:
    /* H */ case COM_H:
    /* O */ case COM_O:
    /* N */ case COM_N:
    /* E */ case COM_E:
    /* K */ case COM_K:
        sprintf(command, "%c\n", cmd);
        break;
        /* Un parametre */
    /* I */ case COM_I:
    /* Y */ case COM_Y:
    /* R */ case COM_R:
        sprintf(command, "%c,%d\n", cmd, param[0]);
        break;
        /* Deux parametres */
    /* D */ case COM_D:
    /* C */ case COM_C:
    /* W */ case COM_W:
    /* G */ case COM_G:
    /* L */ case COM_L:
    /* Q */ case COM_Q:
     /*P*/ case COM_P:
    /* T */ case COM_T:
        sprintf(command, "%c,%d,%d\n", cmd, param[0], param[1]);
        break;
        /* Trois parametres */
    /* F */ case COM_F:
    /* A */ case COM_A:
        sprintf(command, "%c,%d,%d,%d\n", cmd, param[0], param[1], param[2]);
        break;
        /* Quatre parametres */
    /* J */ case COM_J:
        sprintf(command, "%c,%d,%d,%d,%d\n",
                cmd, param[0], param[1], param[2], param[3]);
        break;
    }

/*	printf("commande:%s\n", command);*/
    /*Envoi de la commande jusqu'a satisfaction */
    /*     if(robot->robot_type!=3) */
    robot_serial_write(robot, command, (cmd - 'A') + 'a', retour);
/*	else
	{
		chw=clientHW_get_clientHW_by_name(robot->clientHW);
		clientHW_transmit(chw,command);
		sprintf(retour,"%s",chw->buff);
	}*/
    /*printf("com:%s retour:%s\n", command,retour); */

    switch (cmd)
    {
        /* Parametres en sortie */
    case COM_B:
    case COM_E:
    case COM_H:
    case COM_I:
    case COM_K:
    case COM_N:
    case COM_O:
    case COM_T:
    case COM_Y:
    case COM_R:
        sscanf(&(retour[2]), "%s", data);
        break;
        /* Pas de parametres */
    case COM_A:
    case COM_C:
    case COM_D:
    case COM_F:
    case COM_G:
    case COM_J:
    case COM_L:
    case COM_Q:
    case COM_P:
    case COM_W:
        break;

    default:
        ok = 0;
        printf("Faux commande: %d\n", retour[0]);
        break;
    }

    return ok;
}
