/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <Robot.h>
#include <World.h>
#include <reseau.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>

void robot_get_one_landmark(Robot * robot, int *landmark_id, float *azimuth, float *elevation)
{
   if (robot == NULL)
   {
      fprintf(stderr, "ERROR in robot_get_one_landmark: NULL pointer for robot\n");
      exit(1);
   }

   if (robot->robot_type == 4)
   {
      float dx, dy = 0.;
      int i, j;
   
      int land_to_process = 0, land_invisible;
      World *world = NULL;
      Obstacle *the_obstacles = NULL;
      int nb_obstacles, nb_landmarks; 
      float the_agent_x;
      float the_agent_y;
   
      float Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, r, s, den, num_r, num_s;
      float CALBG_x, CALBG_y, CALHD_x, CALHD_y, COBG_x, COBG_y, COHD_x, COHD_y;


      world = world_get_first_world();

      if (world == NULL)
      {
	 fprintf(stderr, "ERROR in robot_get_one_landmark: No World defined, cannot simulate landmark perception\n");
	 exit(1);
      }

      nb_landmarks = world->nb_landmarks;
      nb_obstacles = world->nb_obstacles;
      the_obstacles = world->obstacles;

      the_agent_x = robot->posx_abs;
      the_agent_y = robot->posy_abs;
 
      land_invisible = 1;
      i =  robot->current_landmark;

      while (i < nb_landmarks && land_invisible == 1)      
      {

	 /* A landmark is visible by default unless a wall blocks the view */
	 land_invisible = 0;

	 /* Checking if the robot is on the landmark */
	 if (fabs(the_agent_x - world->landmarks[i].x) > 1. || fabs(the_agent_y - world->landmarks[i].y) > 1.)
	 {
	    if (world->landmarks[i].always_visible == 0)
	    {
	       /* Calculating if walls are intersecting the line between the robot and the landmark */
	       for (j = 0; j < nb_obstacles && land_invisible == 0; j++)
	       {
		  /* High obstacle */
		  if (the_obstacles[j].high == 1)
		  {
		     /*Intersection of AB and CD ?*/
		     Ax = the_obstacles[j].xa;
		     Ay = the_obstacles[j].ya;	
		     Bx = the_obstacles[j].xb;
		     By = the_obstacles[j].yb;
					   
		     Cx = world->landmarks[i].x;
		     Cy = world->landmarks[i].y;
		     Dx = the_agent_x;
		     Dy = the_agent_y;
				   
		     den = (Bx-Ax)*(Dy-Cy)-(By-Ay)*(Dx-Cx);
		     num_r = (Ay-Cy)*(Dx-Cx)-(Ax-Cx)*(Dy-Cy);
		     num_s = (Ay-Cy)*(Bx-Ax)-(Ax-Cx)*(By-Ay);

		     if (isdiff(den, 0.))
		     {
			r = num_r/den;
			s = num_s/den;

			/* Crossing segments */
			if (r >= 0. && r <= 1. && s >= 0. && s <= 1.)
			{
			   land_invisible = 1;	
			}
		     }	
		     else
		     {
			/* Parralel segments */
			if (isequal(num_r, 0.))
			{
			   /* If O1 or O2 in rect A L*/
			   if (Cx >= Dx)
			   {
			      CALBG_x = Dx;
			      CALHD_x = Cx;
			   }
			   else
			   {
			      CALBG_x = Cx;
			      CALHD_x = Dx;
			   }
                                 
			   if (Cy >= Dy)
			   {
			      CALBG_y = Dy; 
			      CALHD_y = Cy;
			   }
			   else
			   {
			      CALBG_y = Cy;
			      CALHD_y = Dy;
			   }

			   if ((Ax >= CALBG_x && Ax <= CALHD_x) && (Ay >= CALBG_y && Ay <= CALHD_y))
			      land_invisible=1;   

			   if ((Bx >= CALBG_x && Bx <= CALHD_x) && (By >= CALBG_y && By <= CALHD_y))
			      land_invisible=1;

			   /* If A or L in rect O1 O2*/
			   if (Ax >= Bx)
			   {
			      COBG_x = Bx;
			      COHD_x = Ax;
			   }
			   else
			   {
			      COBG_x = Ax;
			      COHD_x = Bx;
			   }
                                 
			   if (Ay >= By)
			   {
			      COBG_y = By;
			      COHD_y = Ay;
			   }
			   else
			   {
			      COBG_y = Ay;
			      COHD_y = By;
			   }

			   if ((Cx >= COBG_x && Cx <= COHD_x) && (Cy >= COBG_y && Cy <= COHD_y))
			      land_invisible=1;

			   if ((Dx >= COBG_x && Dx <= COHD_x) && (Dy >= COBG_y && Dy <= COHD_y))
			      land_invisible=1;
			}
		     }
		  }

		  if (land_invisible == 1)
		     break;				   
	       }
	    }

	    /* No increment on i if the landmark is visible */
	    if (land_invisible == 0)
	       break;
	 }
	 else
	    land_invisible = 1;

	 i++;
      }
     
      land_to_process = i;

      /* Testing if we processed all the landmarks */
      if (land_to_process == nb_landmarks)
      {
	 *landmark_id = -1;
	 *azimuth = 0.;
	 *elevation = 0.;
	 land_to_process = 0;
      }
      else
      {
	 dx = world->landmarks[land_to_process].x - the_agent_x ; 
	 dy = world->landmarks[land_to_process].y - the_agent_y ; 
		
	 *elevation = 0.;
	 *azimuth = atan2(dy, dx);
	
	 if (*azimuth <= 0)
	    *azimuth += 2. * M_PI;

	 *landmark_id = land_to_process;
	 land_to_process++;
      }

      dprints("robot_get_one_landmark: LM_id = %d, azim = %f, elev = %f\n", *landmark_id, *azimuth, *elevation);

      robot->current_landmark = land_to_process;
   }
}
