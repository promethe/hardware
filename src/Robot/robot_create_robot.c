/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mxml.h>
/* #include <sys/ioctl.h> */
/*#include <fcntl.h> */

#define DEBUG
#include <net_message_debug_dist.h>

#include <Robot.h>
#include <tools.h>
#include <time.h>
#include <Serial.h>

/*#include "tools/include/macro.h" */
#include "tools/include/local_var.h"
void robot_create_robot(Robot ** robot, char *name, char *file_hw)
{

   FILE *fh;
   char lecture[256];
   char commande[10];
   int i = 0;
   char lettre;
   int cpt = 0,ret;

   *robot = (Robot *) calloc(1,sizeof(Robot));
   if (*robot == NULL)
   {
      printf("erreur malloc robot_create_robot\n");
      exit(0);
   }

   (*robot)->name = (char *) malloc(strlen(name) + 1);
   if ((*robot)->name == NULL)
   {
      printf("erreur malloc create_robot\n");
      exit(0);
   }
   strcpy((*robot)->name, name);   /*Robot_object name */


   (*robot)->clientHW[0] = (char) 0;
   (*robot)->clientRobubox[0] = (char) 0;
   (*robot)->clientPlayer[0] = (char) 0;
   (*robot)->autocheck =  0;
   (*robot)->speed_max = 1.;
   (*robot)->RS_max = 1.;
   (*robot)->default_posx = 0.;
   (*robot)->default_posy = 0.;
   (*robot)->default_orientation = 0.;
   (*robot)->last_posx_abs = -99999999;
   (*robot)->last_posy_abs = -99999999;
   (*robot)->last_orientation_abs = -99999999;
   (*robot)->angle90=0;
   (*robot)->largeur_robot= 0;
   (*robot)->wheel_radius= 0;
   (*robot)->nbt_encoder= 0;
   (*robot)->nb_clics_max = 0 ;
   (*robot)->last_orientation_abs_rb2 = -99999999;
   (*robot)->angle_rb2=0;
   (*robot)->mvt_sens=1;
   (*robot)->current_divider=1;

   /*Robot_get_info_from_dev_file((*robot)); */

   /* default parameters */


   /*! loading robot parameters */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }

   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */

      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {
         ret=fscanf(fh, "%s", ((*robot)->port));
         dprints("port: %s\n", (*robot)->port);
      }
      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
         ret=fscanf(fh, "%d", &((*robot)->autocheck));
         dprints("autocheck %d\n", (*robot)->autocheck);
      }
      else if (strcmp(lecture, "NB_IR") == 0)
      {
         ret=fscanf(fh, "%d", &((*robot)->nb_ir));
         dprints("nb_ir: %d\n", i);
      }
      else if (strcmp(lecture, "IR_ANGLE") == 0)
      {
         dprints("%s\n", lecture);
         cpt = 0;
         ret=fscanf(fh, "%f", &((*robot)->angle_ir[cpt]));
         cpt++;
         do
         {
            ret=fscanf(fh, "%f%c", &((*robot)->angle_ir[cpt]), &lettre);
            cpt++;
         }
         while (lettre != '\n');
      }
      else if (strcmp(lecture, "SEUIL_IR") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->seuil_ir));
         dprints(" %f\n", (*robot)->seuil_ir);
      }
      else if (strcmp(lecture, "ACTMAX_IR") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->actmax_ir));
         dprints(" %f\n", (*robot)->actmax_ir);
      }
      else if (strcmp(lecture, "NB_IR_SENSOR") == 0)
      {
         ret=fscanf(fh, "%d", &((*robot)->nb_ir_sensor));
         dprints("nb_ir: %d\n", i);
      }
      else if (strcmp(lecture, "IR_ANGLE_SENSOR") == 0)
      {
         dprints("%s\n", lecture);
         cpt = 0;
         ret=fscanf(fh, "%f", &((*robot)->angle_ir_sensor[cpt]));
         cpt++;
         do
         {
            ret=fscanf(fh, "%f%c", &((*robot)->angle_ir_sensor[cpt]), &lettre);
            cpt++;
         }
         while (lettre != '\n');
      }
      else if (strcmp(lecture, "SEUIL_IR_SENSOR") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->seuil_ir_sensor));
         dprints(" %f\n", (*robot)->seuil_ir_sensor);
      }
      else if (strcmp(lecture, "ACTMAX_IR_SENSOR") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->actmax_ir_sensor));
         dprints(" %f\n", (*robot)->actmax_ir_sensor);
      }
      else if (strcmp(lecture, "ANGLE90") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->angle90));
         dprints(" %f\n", (*robot)->angle90);
      }
      else if (strcmp(lecture, "LARGEUR_ROBOT") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->largeur_robot));
         dprints("largeur_robot : %f\n", (*robot)->largeur_robot);
      }
      else if (strcmp(lecture, "NBT_ENCODER") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->nbt_encoder));
         dprints("nbt_encoder : %f\n", (*robot)->nbt_encoder);
      }
      else if (strcmp(lecture, "WHEEL_RADIUS") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->wheel_radius));
         dprints("wheel_radius : %f\n", (*robot)->wheel_radius);
      }
      else if (strcmp(lecture, "CLICS_MAX") == 0)
      {
         ret=fscanf(fh, "%d", &((*robot)->nb_clics_max));
         dprints("nb_clics_max : %d\n", (*robot)->nb_clics_max);
      }
      else if (strcmp(lecture, "TYPE") == 0)
      {
         ret=fscanf(fh, "%d", &((*robot)->robot_type));
         dprints("type_robot : %d\n", (*robot)->robot_type);
      }
      else if (strcmp(lecture, "CLIENTHW") == 0)
      {
         ret=fscanf(fh, "%s", ((*robot)->clientHW));
      }
      else if (strcmp(lecture, "CLIENTROBUBOX") == 0)
      {
         ret=fscanf(fh, "%s", ((*robot)->clientRobubox));
      }
      else if (strcmp(lecture, "CLIENTPLAYER") == 0)
      {
         ret=fscanf(fh, "%s", ((*robot)->clientPlayer));
      }
      else if (strcmp(lecture, "LINEAR_SPEED_MAX") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->speed_max));
      }
      else if (strcmp(lecture, "ROTATIONAL_SPEED_MAX") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->RS_max));
      }
      else if (strcmp(lecture, "INIT_POSX") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->default_posx));
      }
      else if (strcmp(lecture, "INIT_POSY") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->default_posy));
      }
      else if (strcmp(lecture, "INIT_ORIENTATION") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->default_orientation));
      }
      else if (strcmp(lecture, "MAX_CURRENT") == 0)
      {
         ret=fscanf(fh, "%f", &((*robot)->current_divider));
      }
      else
         lire_ligne(fh);
   }
   fclose(fh);
   printf("\t\t type=%d \n\t\t angle90=%f \n\t\t largeur_robot=%f \n\t\t nb ir=%d\n", (*robot)->robot_type, (*robot)->angle90, (*robot)->largeur_robot,(*robot)->nb_ir);
   printf("\t\t position des ir:");
   for (i = 0; i < (*robot)->nb_ir; i++)
   {
      printf(" %f", (*robot)->angle_ir[i]);
   }
   printf("\n");
   printf("\t\t seuil_ir=%f\n", (*robot)->seuil_ir);

   if((*robot)->robot_type==1)
   {
      /* Ajout Vincent */
      /* Initialisation des valeurs d'odometrie */
      (*robot)->distance = 0. ;
      (*robot)->angle = 0. ;
      /* Initialisation du nombre de clics max */
      /* (le nombre de clics max pourra par la suite etre fixe dans le fiche .hwr */
      (*robot)->nb_clics_max = 1000000 ;
      /* Mise a zero des compteurs de clics des roues */
      robot_init_wheels_counter(*robot) ;
      (*robot)->nb_clics_gauches = 0 ;
      (*robot)->nb_clics_droits = 0 ;
   }
   else if((*robot)->robot_type== USE_MAYA)
   {
      pthread_t robot_thread;

      (*robot)->last_posx_abs = 0;
      (*robot)->last_posy_abs = 0;
      (*robot)->last_orientation_abs =0;

      if(  (*robot)->nb_clics_max == 0)
      {
         (*robot)->nb_clics_max = 100000000;
      }
      sprintf(commande, "%c%c%c", SYNC_BYTE,SET_MODE, MODE_TURN);
      serial_send((*robot)->port, commande , 3);

	/* ACCELERATION */ 
      sprintf(commande, "%c%c%c", SYNC_BYTE, SET_ACCELERATION , 5  );
      serial_send((*robot)->port, commande , 3);
	

      // initialisation mutex
      if (pthread_mutex_init(&((*robot)->lock), NULL) != 0)
      {
         EXIT_ON_ERROR("Can't initialize mutex : us_serial->lock");
      }

      (*robot)->enable = 1;
      // lancement thread
      if (pthread_create(&robot_thread, NULL, (void *(*)(void*))robot_get_odometry_thread , *robot) != 0)
      {
         EXIT_ON_ERROR("robot_create_robot(hardware): Cannot create thread for robot");
      }
   }
   /*
    else if((*robot)->robot_type==USE_ROBUBOX_V2 )
    {
	pthread_t robot_thread;
         (*robot)->enable = 1;
         // lancement thread
         if (pthread_create(&robot_thread, NULL, (void *(*)(void*))robot_get_odometry_thread , *robot) != 0)
         {
                 EXIT_ON_ERROR("robot_create_robot(hardware): Cannot create thread for robot");
         }

    }*/
   else
   {
      (*robot)->distance = 0. ;
      (*robot)->angle = 0. ;
   }
   (void)ret;
}
