/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

/*#define DEBUG*/
#include <Aibo.h>
#include <pressure.h>

#include "tools/include/local_var.h"

void pressure_create_pressure(Pressure ** pressure, char *name, char *type)
{

	FILE *fh;
	char lecture[256];
	char tmp[129];
	int  ret;
	float tmpi;

	*pressure = (Pressure *) malloc(sizeof(Pressure));
	if (*pressure == NULL)
	{
		EXIT_ON_ERROR("malloc failed in %s",__FUNCTION__);
	}

	strcpy((*pressure)->name, name);    /*nom du capteur de pression */
	tmp[128] = '\0';

	/* Default values */
	strcpy((*pressure)->port, "/dev/ttyACM0");
	(*pressure)->enabled = 1;

	/* recuperation des informations contenu dans le fichier hardware */
	fh = fopen(type, "r");
	if (fh == NULL)
		EXIT_ON_ERROR("Cannot open file  %s", type);

	while (!feof(fh))
	{
		ret = fscanf(fh, "%s = ", lecture);   /* lecture du premier mot de la ligne */

		str_upper(lecture);

		if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
		{
			ret = fscanf(fh, "%s", (*pressure)->port);
			
		}
		else if (strcmp(lecture, "ENABLED") == 0)
		{
			ret = fscanf(fh, "%d\n", &tmpi);

			if (ret != 1)
				EXIT_ON_ERROR("Error reading enable value (int) of pressure %s", name);

			(*pressure)->enabled = tmpi;
		}
		/*Le code est fait pour un seul capteur par arduino. Pour utiliser plusieurs capteurs continuer le code
		 * else if (strcmp(lecture, "NB_CAPT") == 0)
		{
			ret = fscanf(fh, "%d\n", &tmpi);

			if (ret != 1)
				EXIT_ON_ERROR("Error reading nb of capteur value (int) of pressure %s", name);

			(*pressure)->nb_capt = tmpi;
		}*/
		else
			lire_ligne(fh);
	}
	fclose(fh);

	dprints("pressure_create_pressure: pressure %s\tport=%s\n", (*pressure)->name, (*pressure)->port); //coooooooool
	/*printf("\t\t port=%s \n\t\t type=%d \n\t\t autocheck=%d \n", (*pressure)->port, (*pressure)->type, (*pressure)->autocheck);*/
	if (pthread_mutex_init(&((*pressure)->lock), NULL) != 0)
		EXIT_ON_ERROR("Can't initialize mutex : in %s",__FUNCTION__);
}
