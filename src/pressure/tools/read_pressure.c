/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

/*#define DEBUG*/
/*#define ONLINE_DEBUG*/

#include <net_message_debug_dist.h>
#include <pressure.h>
#include <Serial.h>
#include <termios.h>
#include <string.h>
float read_pressure(Pressure *pressure)
{
	/*Lecture des donnees retournees par le capteur de pression
	 *  Les donnees sont inversement proportionnelles a la pression (decroissante entre 1023 et 0)
	 * Les donnees sont retraitees dans cette fonction pour etre proportionnelles a la pression et mise entre 0 et 1*/ 
#ifdef Linux
	char reponse[400];
    int ret = 0;
    int pressure_value;
    int length;
    char buff = 'P'; /* Commande 'P' envoyees a l'arduino pour demander la valeur du capteur de pression */
	float value = 0.;
	int tmp = 0;
	dprints("reading pressure value = %i  au port : %s \n", pressure->enabled,pressure->port);

	do
    {
      serial_send_and_receive(pressure->port, &buff, reponse, 1);
      //serial_receive(pressure->port, reponse);
      
      length = strlen(reponse);
      if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
		reponse[length-1] = '\0';

#ifdef ONLINE_DEBUG
       printf("reponse press : %s\n", reponse);
#endif
		ret = sscanf(reponse, "-p%d-", &pressure_value);
		pressure->value =  -((float)pressure_value - 1023)/1023;
       //if (ret != 1)
		//PRINT_WARNING("Type=STAMP_QUERY. The value (%s) read by pressure_read does not have the required format. Retrying to read ...", reponse);
    }
    while (ret != 1);

    dprints("pressure_value=%f  avant %d : \n", pressure->value,pressure_value);
    return pressure->value;
#else
	printf("ERROR, Non Linux OS cannot use functions %s ",__FUNCTION__);
#endif
}
