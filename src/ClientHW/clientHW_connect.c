/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <ClientHW.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>

/*#define DEBUG*/
void clientHW_connect(ClientHW * chw)
{
   int sockfd;
   struct sockaddr_in serv_addr;
   char buff[MAXLINE];


   struct hostent *serverHostEnt;
   long hostAddr;


   /* La structure "serv_addr" doit etre remplie avec l'adresse du serveur */
   bzero((char *) &serv_addr, sizeof(serv_addr));
   hostAddr = inet_addr(chw->host);


   if ((long) hostAddr != (long) -1)
      bcopy(&hostAddr, &serv_addr.sin_addr, sizeof(hostAddr));
   else
   {
      serverHostEnt = gethostbyname(chw->host);
      if (serverHostEnt == NULL)
      {
         printf("ca chie gethost\n");
         exit(0);
      }
      bcopy(serverHostEnt->h_addr, &serv_addr.sin_addr,
            serverHostEnt->h_length);
   }
   serv_addr.sin_port = htons(chw->port);
   serv_addr.sin_family = AF_INET;

   /* creation de la socket */
   if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
   {
      printf("ca chie creation socket client\n");
      exit(0);
   }
   /* requete de connexion */
   if (connect(sockfd,
         (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
   {
      printf("ca chie demande de connection\n");
      exit(0);
   }

   clientHW_init_connection(sockfd, (struct sockaddr *) &serv_addr,
         sizeof(serv_addr));
#ifdef DEBUG
   printf("init marche => message de controle\n");
#endif
   sprintf(buff, "CONTROLE_TRANSMISSION\n");

   clientHW_ecriture_vers_socket(buff, sockfd,
         (struct sockaddr *) &serv_addr,
         sizeof(serv_addr));
#ifdef DEBUG
printf("msg envoyer\n");
#endif
clientHW_lecture_du_socket(buff, sockfd, (struct sockaddr *) &serv_addr,
      sizeof(serv_addr));
#ifdef DEBUG
printf(">>>> %s\n", buff);
printf("controle effectue\n");  /* ne sert pas vraiment ici... */
#endif
chw->sockfd = sockfd;
chw->serv_addr = serv_addr;
printf("\t\tconnection_status=OK\n");
}
