/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
/* #include <sys/ioctl.h> */
/*#include <fcntl.h> */

#include <Gps.h>
#include <tools.h>

/*#include "tools/include/macro.h" */
#include "tools/include/local_var.h"

#include <net_message_debug_dist.h>

void gps_create_gps(Gps **gps,char *name, char *file_hw)
{

   FILE *fh;
   char lecture[256];
   char tmp[129];
   int ret=-1;

   *gps = ALLOCATION(Gps);

   (*gps)->name = (char *) malloc(strlen(name) + 1);
   if ((*gps)->name == NULL)
   {
      EXIT_ON_ERROR("Error when allocating gps");
   }
   strcpy((*gps)->name, name); /*Gps_object name */

   /* default parameters */

   (*gps)->port = NULL;
   (*gps)->clientHW[0] = (char) 0 ;
   (*gps)->udphost = NULL;
   (*gps)->udpport = 0 ;
   (*gps)->sockfd = -1 ;
   (*gps)->buff[0] = (char) 0 ;

   /*Gps_get_info_from_dev_file((*gps)); */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");
   if (fh == NULL)
   {
      EXIT_ON_ERROR("Unable to open file %s \n", file_hw);
   }

   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */
      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {
         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            EXIT_ON_ERROR("Error in gps port %s\n", name);
         }

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         /* verify if it is the first declaration for port */
         if ((*gps)->port != NULL)
         {
            PRINT_WARNING("Warning: several declarations of port in %s\n", file_hw);
            free((*gps)->port);
         }

         (*gps)->port = MANY_ALLOCATIONS(strlen(tmp) + 1, char) ;
         strcpy((*gps)->port, tmp);
      }
      else if (strcmp(lecture, "TYPE") == 0)
      {
         ret=fscanf(fh, "%d\n", &((*gps)->gps_type));
         if((*gps)->gps_type == USE_DSGPMS_OD)
            cprints("  (!)  Note: GPM.S max frequency is 400 KHz\n");
      }
      else if (strcmp(lecture, "CLIENTHW") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*gps)->clientHW));
      }
      else if (strcmp(lecture, "UDPHOST") == 0)
      {
         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            EXIT_ON_ERROR("Error when reading the %s gps address (UDP HOST)\n",name);
         }

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         /* verify if it is the first declaration for port */
         if ((*gps)->udphost != NULL)
         {
            PRINT_WARNING("Warning: several declarations in %s\n",file_hw);
            free((*gps)->udphost);
         }

         (*gps)->udphost = MANY_ALLOCATIONS(strlen(tmp) + 1, char) ;
         strcpy((*gps)->udphost, tmp);
      }
      else if (strcmp(lecture, "UDPPORT") == 0)
      {
         ret=fscanf(fh, "%d\n", &((*gps)->udpport));
      }
      else
         lire_ligne(fh);
   }
   fclose(fh);
   dprints("\t\t type=%d\n", (*gps)->gps_type);

   (void)ret;
}
