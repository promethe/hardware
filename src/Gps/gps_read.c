/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Gps.h>
#include <ClientHW.h>
#include <Serial.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <net_message_debug_dist.h>



void gps_read(Gps * gps)
{
  dprints("Entering gps_read() \n");

  if (gps->gps_type == USE_GARMIN)
    gps_garmin_read_threaded(gps);
  else if (gps->gps_type == USE_CLIENTHW)
    gps_server_read(gps);
  else if (gps->gps_type == USE_WEBOTS_GPS)
    gps_webots_read_threaded(gps);
  else if (gps->gps_type == USE_DSGPMS_OD)
	  gps_dsgpms_od_read(gps);
}

void gps_dsgpms_od_read(Gps * gps)
{
	char lecture[1024];
	char valeurs[13][15];
	Gps_value *gps_value;
	int lenght = 0;
	char buff = 'g';
	int i = 0, j = 0, l = 0;

	gps_value = ALLOCATION(Gps_value) ;
	gps->gps_value=gps_value;

	lenght = serial_send_and_receive_line(gps->port, &buff, lecture, 1);

	if (lenght != -1)
	{
		i = 0, j = 0, l = 0;

		for(l = 0; l < lenght; l++)
		{
			if((lecture[l] == ';') || (lecture[l] == '\0'))
			{
				i++;
				j = 0;
			}
			else
			{
				valeurs[i][j] = lecture[l];
				j++;
			}
		}

	gps->gps_value->nord = atof(valeurs[0]) + (atof(valeurs[1]) / 60);
	gps->gps_value->est = atof(valeurs[2]) + (atof(valeurs[3]) / 60);
	gps->gps_value->altitude = atof(valeurs[4]);
	gps->gps_value->compass = atof(valeurs[5]);
	gps->gps_value->speed = atof(valeurs[6]);
	gps->gps_value->day = atof(valeurs[7]);
	gps->gps_value->month = atof(valeurs[8]);
	gps->gps_value->year = atof(valeurs[9]);
	gps->gps_value->hour = atof(valeurs[10]);
	gps->gps_value->minute = atof(valeurs[11]);
	gps->gps_value->second = atof(valeurs[12]);


	}
}

void gps_garmin_read_threaded(Gps * gps)
{
  pthread_t my_thread;
  static int already_init = 0;

  dprints("Entering gps_garmin_read_threaded ()\n");

  if (already_init == 0)
  {
    if (pthread_create(&my_thread, NULL, (void *(*)(void *))gps_garmin_read, gps) != 0)
    {
      EXIT_ON_ERROR("Error when creating GPS thread\n");
    }
    already_init = 1;
    dprints("GPS Thread created\n");
  }
}

void gps_server_read(Gps * gps)
{
  char commande[256];
  char result[256];
  int retour = -1;
  ClientHW *chw;

  dprints("Entering gps_server_read\n");

  chw = clientHW_get_clientHW_by_name(gps->clientHW);
  sprintf(commande, "gps_read %s", gps->name);
  clientHW_transmit_receive(chw, commande, result);

  dprints("Result after transmit: %s\n", result);

  sscanf(&(result[2]), "%d", &retour);
}



void gps_webots_read_threaded(Gps * gps) 
{
  pthread_t my_thread;

  dprints("Entering gps_webots_read_threaded() \n");


  if(gps->sockfd < 0) { //socket has not been initialized yet (first call of gps_webots_read_threaded())
    gps->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (gps->sockfd < 0)
    {
      EXIT_ON_ERROR("Error when creating the socket for webots gps (%s)\n", gps->name);
    }
    memset(&gps->serv_addr, 0, sizeof(struct sockaddr_in));
    gps->serv_addr.sin_family = AF_INET;
    gps->serv_addr.sin_addr.s_addr = inet_addr(gps->udphost);
    gps->serv_addr.sin_port = htons(gps->udpport);

    if (connect(gps->sockfd,&gps->serv_addr,sizeof(gps->serv_addr)) < 0)
    {
      EXIT_ON_ERROR("Error when connecting to the webots gps socket (%s)\n", gps->name);
    }

    if (pthread_create(&my_thread, NULL, (void *(*)(void *))gps_webots_read, gps) != 0)
    {
      EXIT_ON_ERROR("Error when creating the thread for the GPS\n");
    }
  }

  dprints("GPS thread created \n");
}

