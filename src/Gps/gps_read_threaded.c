/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Gps.h>
#include <Serial.h>
#include <signal.h>
#include <unistd.h>

#include <termios.h> 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

#include <net_message_debug_dist.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

/*#define ONLINE_DEBUG*/
/*#define DEBUG*/

float gps_parse( char *trame, int length, int numero_option ) {

   char valeur[255];
   char tmp=' ';
   int i,j;
   float resultat=-1;


   /* On passe les options inutiles*/
   j=0;
   for(i=0;i<numero_option;i++) {
      while ( tmp != '\n' && j < length ) {
         if ( j == length-1 ) {
            EXIT_ON_ERROR("Parsing error\n");
         }

         tmp=trame[j++];
         if ( tmp == ',' )
            break;
      }
   }

   /* On recupere la valeur de l'option*/
   i=0;
   tmp=' ';
   while ( tmp != '\n' && tmp!= ',' && j < length) {
      tmp=trame[j++];
      valeur[i++]=tmp;
      if ( tmp == ',' ) {
         valeur[i]='\0';
         resultat=atof(valeur);
#ifdef DEBUG
         dprints("option numero %d : %f\n",numero_option,resultat);
#endif
         break;
      }
   }

   /* Cas ou il n'y a aucune valeur*/
   if ( i==1 )
      return -1;

   return resultat;

}


/*#define DEBUG */
void gps_garmin_read(Gps * gps)
{
   char lecture[1024];
   char tmp;
   int length=-1;
   int cpt=-1;
   Gps_value *gps_value;
   int serialport1;

#ifdef ONLINE_DEBUG
   dprints("gps_garmin_read \n");
#endif

   if ( gps == NULL ) {
      EXIT_ON_ERROR("Error unknown GPS\n");
   }

   gestion_mask_signaux_hardware(); /*Gestion du thread */

   serialport1 = (serial_get_serial_by_name(gps->port))->desc;
   tcdrain(serialport1);
   tcflush(serialport1, TCIOFLUSH);

   gps_value = ALLOCATION(Gps_value) ;
   gps->gps_value=gps_value;

   while ( 1 ) {
      cpt=-1;

      /*Lecture de la trame GPS*/
      do {
         length = read(serialport1, &tmp, 1);
         cpt+=length;

         if( cpt == -1)
            break;

         lecture[cpt]=tmp;
      } while ( tmp != '\n' && length > 0 && cpt < 1023 );

      /*Parsing de la trame GPS*/
      if ( cpt > 6 ) {
         if ((strncmp(lecture, "$PGRMF",6) == 0))
         {
#ifdef DEBUG
            dprints("%s \n",lecture);
#endif

            /* Date */
            gps_value->day=gps_parse(lecture,cpt,1);
            gps_value->month=gps_parse(lecture,cpt,2);
            gps_value->year=gps_parse(lecture,cpt,3);

            /*Heure*/
            gps_value->hour=gps_parse(lecture,cpt,4);
            gps_value->minute=gps_parse(lecture,cpt,5);
            gps_value->second=gps_parse(lecture,cpt,6);

            /* Position */
            gps_value->nord=gps_parse(lecture,cpt,7);
            gps_value->est=gps_parse(lecture,cpt,8);

            /* Vitesse */
            gps_value->speed=gps_parse(lecture,cpt,9);

            /* Compas */
            gps_value->compass=gps_parse(lecture,cpt,10);

#ifdef DEBUG
            dprints("UTC date : %f/%f/%f \n",gps->gps_value->day,gps->gps_value->month,gps->gps_value->year);
            dprints("UTC time : %f h %f min %f s \n",gps->gps_value->hour,gps->gps_value->minute,gps->gps_value->second);
            dprints("Nord : %f \n",gps->gps_value->nord);
            dprints("Est : %f \n",gps->gps_value->est);
            dprints("Speed : %f \n",gps->gps_value->speed);
            dprints("Compass : %f \n",gps->gps_value->compass);
#endif			
         }
      }
   }

   return;
}


void gps_webots_read(Gps * gps)
{
   char command[16];
   int command_len ;
   int resp_len=-1;
   double tmp[3] ;
   socklen_t saddr_len;
   Gps_value *gps_value;

#ifdef DEBUG
   dprints("Entering gps_webots_read() \n");
#endif

   if ( gps == NULL ) {
      EXIT_ON_ERROR("Error unknown GPS\n");
   }

   gps_value = ALLOCATION(Gps_value) ;
   gps->gps_value=gps_value;

   sprintf(command, "-gpsvalues") ;
   command_len = strlen(command);
   saddr_len = sizeof(gps->serv_addr);

   while ( 1 ) {
      if ( sendto(gps->sockfd, command, command_len, 0, (struct sockaddr *) &(gps->serv_addr), saddr_len) < 0) {
         EXIT_ON_ERROR("Error when sending command to webots gps (%s)\n", gps->name) ;
      }
      bzero(gps->buff, MAXLINE) ;
      if ( (resp_len=recvfrom(gps->sockfd, gps->buff, MAXLINE, 0,(struct sockaddr *) &(gps->serv_addr),&saddr_len)) < 0 ) {
         EXIT_ON_ERROR("Error when receiving data from webots gps (%s)\n", gps->name);
      }

      sscanf(gps->buff, "%lf %lf %lf", &tmp[0], &tmp[1], &tmp[2]);

      /* Date */
      gps_value->day = 0 ;
      gps_value->month = 0 ;
      gps_value->year = 0 ;

      /*Heure*/
      gps_value->hour = 0 ;
      gps_value->minute = 0 ;
      gps_value->second = 0 ;

      /* Position */
      gps_value->est = (float)tmp[0];
      gps_value->nord = (float)tmp[2];

      /* Vitesse */
      gps_value->speed = 0 ;

      /* Compas */
      gps_value->compass = 0 ;
   }

   return;
}
