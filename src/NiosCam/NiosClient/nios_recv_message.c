/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define DEBUG 1
#include <semaphore.h>
#include <Camera.h>
#include <Nios.h>
#include <tools.h>
#include <net_message_debug_dist.h>
#define	LA_VIE	42



/**
 * Format des messages:
 * "[time:"camera_"+name] *** %s << "
 * "size sx sy\n"  (size en octets)
 * ensuite l'image...
 *
 */

int read_socket(int sock, void * buffer, int nb_octet_to_read)
{

   int n; /*nb octet restant a lire*/

   n = read(sock, buffer, nb_octet_to_read);
   while (n < nb_octet_to_read)
   {
      n += read(sock, buffer + n, nb_octet_to_read - n);
   }
   return n;
}

void *nios_recv_message(void *ptr)
{
   unsigned int i, /*j, k,*/ x, y = 0;
   unsigned int n = 0;
  /* int size = 16 * 16;pour le moment en dur... a modifier*/
   float pix;
   alt_u8 nb_kp;
   alt_u32 num_frame = 0;
   keypoint_t keypoint;
   alt_u16 min, max;

   Nios_Client * nios_client = (Nios_Client *) ptr;
   int sock = (nios_client)->socket; /*int	sock = *((int *)ptr);*/
   Nios_pt_carac* nios_pt_carac = ((nios_client)->nios_pt_carac);
   alt_u32* dim = (nios_client)->dim;

   /* Gestion des signaux pour promethe */
   gestion_mask_signaux_hardware();
   dprints("Entering in nios_recv_message... \n");
   /* On ouvre la socket en flux ... c'est plus simple pour le parsing :P */

   n = read_socket(sock, dim, 2 * sizeof(alt_u32));
   if (n != 2 * sizeof(alt_u32))
   {
      perror("(nios_recv_message) ERROR reading from socket");
      exit(-1);
   }
   dprints("%d octets read when requesting image size\n", n);
   dprints("Dim Nios image = %dx%d\n", dim[0], dim[1]);

   (nios_client)->buffer = (int16_t *) malloc(dim[0] * dim[1] * sizeof(int16_t));
   if ((nios_client)->buffer == NULL)
   {

      perror("nios_recv_message allocation error of buffer image : ");

      dprints("in %s at %d \n", __FUNCTION__, __LINE__);

      exit(-1);

   }
   while (LA_VIE)
   {
      usleep(100000);

      /* recupere num frame */
      n = read_socket(sock, (void *) (&num_frame), sizeof(alt_u32));
      if (n != sizeof(alt_u32))
      {
         perror("(nios_recv_message) error when reading num frame");
         dprints("%d\n", n);
         exit(-1);
      }
      nios_pt_carac->dim_image = num_frame;
      dprints("	-		num frame readed: %d for request type:%d \n", nios_pt_carac->dim_image, (nios_client)->request);

      if ((nios_client)->request & VIDEO_MASK)
      {
         /* recupere l'image*/
         n = read_socket(sock, (void *) ((nios_client)->buffer), dim[0] * dim[1] * sizeof(int16_t));
         if (n != dim[0] * dim[1] * sizeof(int16_t))
         {
            perror("(nios_recv_message) error when reading image");
            dprints("%d\n", n);
            exit(-1);
         }
         /* search min and max */
         max = 0;
         min = 0xFFFF;
         for (y = 0; y < dim[1]; y++)
         {
            for (x = 0; x < dim[0]; x++)
            {

               pix = ((nios_client)->buffer[y * dim[0] + x]) & 0xFFFF;
               if (pix > max) max = pix;
               else if (pix < min) min = pix;
            }
         }
         /* convert the image to float */
         for (y = 0; y < dim[1]; y++)
         {
            for (x = 0; x < dim[0]; x++)
            {

               pix = ((nios_client)->buffer[y * dim[0] + x]) & 0xFFFF;
               if (max == min) pix = 0;
               else (nios_client)->image[y * dim[0] + x] = (float) (pix - min) / (max - min);

            }
         }

      }
      if ((nios_client)->request & KP_MASK)
      {
         /* recupere le nb de pt carac*/
         n = read_socket(sock, (void *) (&nb_kp), sizeof(alt_u8));
         if (n != sizeof(alt_u8))
         {
            perror("(nios_recv_message) error when reading nb keypoints");
            dprints("%d\n", n);
            exit(-1);
         }
         nios_pt_carac->nb_pt = nb_kp;
         dprints("	*		nb pt readed: %d \n", nios_pt_carac->nb_pt);

         /*recupere les points un a un*/
         for (i = 0; i < nb_kp; i++)
         {
            uint16_t act;
            NIOS_pt_carac * pt_carac = &(nios_pt_carac->pts_carac[i]);

            n = read_socket(sock, (void *) (&keypoint), sizeof(keypoint_t));

            while (n != sizeof(keypoint_t))
            {
               perror("(nios_recv_message) error when reading keypoints");
               dprints("%d octets recu au lieu de %ld\n", n, sizeof(keypoint_t));
               exit(-1);
            }
            /* recupere la pos de pt carac*/
            pt_carac->posx = keypoint.x;
            pt_carac->posy = keypoint.y;

            /*recupere l'activite*/
            act = keypoint.v;

            /*la convertie en float*/
            pt_carac->intensity = (float) act / 65535.;

            /*recupere l'imagette*/
            if (!memcpy(pt_carac->image, keypoint.logpol, sizeof(uint16_t) * 16 * 16))
            {
               perror("(nios_recv_message) error when memcpy lopol imagette");
               exit(-1);
            }
            pt_carac->sx = pt_carac->sy = 16;
            pt_carac->size = 16 * 16;
            dprints("	*		pt readed: x=%d, y=%d intensity=%f \n", pt_carac->posx, pt_carac->posy, pt_carac->intensity);
            /*if (i == 0)
             {
             for (j = 0; j < 16; j++)
             {
             for (k = 0; k < 16; k++)
             dprints("%d\t", pt_carac->image[k + j * 16]);

             dprints("\n");
             }
             }*/
         }
         sem_post(&(nios_pt_carac->sem));
      }

      dprints("	- 	nios_recv_message : frame :%d processed!\n-------------------------------------\n", nios_pt_carac->dim_image);

   }
   pthread_exit(NULL);
}
