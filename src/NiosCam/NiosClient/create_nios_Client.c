/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <pthread.h>
#include <tools.h>
#include <Nios.h>
#include "tools/include/local_var.h"
#include <semaphore.h>
#include <net_message_debug_dist.h>
Nios_Client *client_nios;

void create_nios_Client(char *name, char *file)
{

	FILE *fh;
	char lecture[256];
	pthread_t tid;

	dprints("Creation de l'objet nios Client ...\n");
	/* get parameters from the hardware file */

	/*  dprints("file opened \n");*/

	if ((client_nios = (Nios_Client *) malloc(sizeof(Nios_Client))) == NULL)
	{

		perror("client_nios allocation error : ");

		dprints("in %s at %d \n", __FUNCTION__, __LINE__);

		exit(-1);

	}
	strcpy(client_nios->name, name);
	/*memset(client_nios->buffer, 0, 4000);
	memset(client_nios->recvbuffer, 0, 4000);
	memset(client_nios->binary_buffer, 0, 128000);
	memset(client_nios->request, 0, 4000);*/

	dprints("	*		Memory for NIOS Client allocated!\n");

	dprints("	*	*		Reading config file %s,  nios client name is %s\n", file, name);
	if ((fh = fopen(file, "r")) == NULL)
	{

		perror("Unable to open file...\n");
		dprints("Quitting from %s at %d's line", __FUNCTION__, __LINE__);

		exit(-1);

	}

	while (!feof(fh))
	{
		fscanf(fh, "%s = ", lecture); /* Reading the first line's word */

		str_upper(lecture);

		if ((strcmp(lecture, "IP") == 0)) /* get the IP address */
		{
			fscanf(fh, "%s ", client_nios->IP);

			/* dprints("	*		IP in file is %s", client_nios->IP);*/
		}
		else if (strcmp(lecture, "PORT") == 0) /* get the port number  */
		{
			fscanf(fh, " %s ", client_nios->port);
			/*   dprints("	*		port in file is %s", client_nios->port);*/
		}
		else lire_ligne(fh);

	}
	fclose(fh);
	/*allocate the struct for pt_carac*/
	client_nios->nios_pt_carac = (Nios_pt_carac *) malloc(sizeof(Nios_pt_carac));
	/*allocate inner fields of the struct  pt_carac*/
	create_nios_Cam((client_nios->nios_pt_carac));

	dprints("	*		nios IP= %s nios port=%s\n", client_nios->IP, client_nios->port);
	client_nios->socket = connect_nios_Client(client_nios->IP, client_nios->port);
	if (pthread_create(&tid, NULL, nios_recv_message, (void *) client_nios) != 0)
	{
		dprints( "connect_nios_Client : Erreur de creation de la thread de reception des messages\n");
		exit(-1);
	}
}
