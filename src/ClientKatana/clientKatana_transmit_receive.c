/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <ClientKatana.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define NB_SEND 10
/*#define SIMULATION*/
#define TRACE

#ifdef TRACE
#include <sys/time.h>
#endif

int clientKatana_transmit_receive(ClientKatana * chw, char *commande, char *resultat)
{
   int valeur;
   int i;
#ifdef TRACE
   /*long Secondes;
   long MicroSecondes;*/
   struct timeval Input;
   struct timeval Output;
#endif

#ifdef SIMULATION
   return 10;
#endif
   if (pthread_mutex_lock(&(chw->mutex_socket)) != 0)
   {
      perror("pthread_mutex_lock : transmit_receive");
   }
#ifdef DEBUG
   printf("Locked %p %s\n", &(chw->mutex_socket), commande);
#endif
   for(i=0;i< NB_SEND;i++) {

      sprintf(chw->buff,"%s", commande);
#ifdef TRACE
      gettimeofday(&Input, (void *) NULL);
#endif
      clientKatana_ecriture_vers_socket(chw);
      valeur=clientKatana_lecture_du_socket(chw);

#ifdef TRACE
      gettimeofday(&Output, (void *) NULL);

      /* Secondes = Output.tv_sec - Input.tv_sec;
      MicroSecondes = 1000000*Secondes + Output.tv_usec - Input.tv_usec;

        printf("%s respond in\t %ld microsec\n",commande, MicroSecondes); */
#endif

      if ( valeur == 0 ) {

         if ( resultat!=NULL )
            sprintf(resultat, "%s", chw->buff);
#ifdef DEBUG
         printf("Unlock %p %s\n", &(chw->mutex_socket), commande);
#endif
         pthread_mutex_unlock(&(chw->mutex_socket));

         return valeur;
      }

   }
#ifdef DEBUG
   printf("Unlock %p %s\n", &(chw->mutex_socket), commande);
#endif
   pthread_mutex_unlock(&(chw->mutex_socket));

   printf("Ah ba mince alors, je crois qu'on a perdu la connexion !!\n");
   exit(0);
}
