/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <ClientPlayer.h>
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>


#ifdef PLAYER
#include <libplayerc/playerc.h>
#endif

/*#define DEBUG*/
void clientPlayer_connect(ClientPlayer * cp)
{
#ifdef PLAYER
	int i,j;
        float rate =0.0; /*PUSHMODE*/	
        playerc_client_t *player_client;
	playerc_sonar_t * sp;
	playerc_position2d_t * pp;	
	
	printf("Connecting to [%s:%d]\n",cp->host, cp->port);
	player_client = playerc_client_create(NULL, cp->host, cp->port);
	if (playerc_client_connect(player_client) != 0)
	{
		printf("n'arrive pas a se connecter a player");
		exit(-1);
	}

 	
	printf("Setting delivery mode to PLAYER_DATAMODE_PULL\n");
		/* Change the server's data delivery mode.*/
	if (playerc_client_datamode (player_client, PLAYERC_DATAMODE_PULL) != 0)
	{
		printf("error: PULL_MODE\n");
		exit(-1);
	}
	if (playerc_client_set_replace_rule (player_client, -1, -1, PLAYER_MSGTYPE_DATA, -1, 1) != 0)
	{
		printf(stderr, "error:set_replace_rule\n");
		exit(-1);
	}
	
	cp->player_client=player_client;
	
	if (playerc_client_get_devlist(player_client) < 0)
        {
                    printf("playerc_client_get_devlist: error\n");
                    exit(-1);
        }

	printf("connection completed\n");

  	pp = playerc_position2d_create(player_client, 0);
            /*tp=playerc_truth_create(pc,0); */
        sp = playerc_sonar_create(player_client, 0);
            /*p3dp=playerc_position3d_create(pc,0); */
            /*playerc_laser_t * lp= playerc_laser_create(pc,0);;*/
       	cp->player_sonar_proxy = (playerc_sonar_t *) sp;
            /*robot->player_laser_proxy=(playerc_laser_t *)lp; */
            /*robot->player_truth_proxy=(playerc_truth_t*)tp; */
        cp->player_position_proxy = (playerc_position2d_t *) pp;
            /*robot->player_position_proxy=(playerc_position3d_t*)p3dp; */

	printf("proxy creation completed\n");
	/***********************************************/
            /*            SUSCRIBE POSITION_PROXY          */
       /***********************************************/

            printf("subscribing position_proxy (read/write)\n");
            if (playerc_position2d_subscribe(pp, PLAYERC_OPEN_MODE) < 0)
            {
                printf("failed suscribing position_proxy\n");
                exit(-1);
            }
            printf("\t status position_proxy: OK\n");
            if (playerc_position2d_get_geom(pp) == 0)
                printf
                    ("\t position geom: [%6.3f %6.3f %6.3f] [%6.3f %6.3f]\n",
                     pp->pose[0], pp->pose[1], pp->pose[2], pp->size[0],
                     pp->size[1]);
            if (playerc_position2d_enable(pp, 1) != 0)
                printf("failed enable motor\n");
/*             pthread_mutex_lock(&mutex_client_read);*/
            playerc_client_read_nonblock(player_client);
/*            pthread_mutex_unlock(&mutex_client_read);*/
            printf("\t position: [%14.3f] [%6.3f] [%6.3f] [%6.3f] [%d]\n",
                   pp->info.datatime, pp->px, pp->py, pp->pa, pp->stall);

       /***********************************************/
            /*            SUSCRIBE SNAR_PROXY          */
       /***********************************************/

            printf("subscribing sonar_proxy (read)\n");
            if (playerc_sonar_subscribe(sp, PLAYERC_OPEN_MODE) != 0)
            {
                printf("failed suscribing sonar_proxy\n");
                exit(-1);
            }
            printf("\t status prosition_proxy: OK\n");

            printf("getting geometry\n");
            if (playerc_sonar_get_geom(sp) != 0)
            {
                printf("failed");
            }
            printf("sonar geom: ");
            for (i = 0; i < sp->pose_count; i++)
                printf("[%6.3f %6.3f %6.3f] ", sp->poses[i].px,
                       sp->poses[i].py, sp->poses[i].pz);
            printf("\n");
/*	playerc_client_addcallback (pc, (playerc_device_t *) sp,(void*) playerrobot_read_ir,(void *)robot);*/
/*             if (pthread_create
                 (&sonar_thread, NULL, playerrobot_read_ir,
                  (void *) robot) != 0)
             {
                 printf("%s : Erreur de creation du thread pour le sonar\n",
                        __FUNCTION__);
                 exit(0);
             }
 #ifdef DEBUG          */
            for (j = 0; j < 10; j++)
            {
                printf("sonar range: [%d] ", sp->scan_count);
/*                 pthread_mutex_lock(&mutex_client_read);*/
                playerc_client_read_nonblock(player_client);
/*                 pthread_mutex_unlock(&mutex_client_read);*/

                for (i = 0; i < sp->scan_count ; i++)
                    printf("[%6.3f] ", sp->scan[i]);
                printf("\n");
            }
/* #endif*/
#endif
	printf("subscription and control completed\n");


#ifndef PLAYER

	printf("les api player se sont pas demande\n utilise --enable-player lors de la creation du makefile\n");
	exit(-1);

#endif

}

void player_client_read_nonblock(ClientPlayer * cp)
{
#ifdef PLAYER
printf("fonction player_client_read_nonblock a coder\n");
exit(-1);
#endif    
}
