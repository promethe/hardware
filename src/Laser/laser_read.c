/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Laser.h>
#include "tools/include/local_var.h"

/*#define DEBUG*/
int  laser_read_USE_SICK_LMS(Laser *laser);
int  laser_read_USE_URG_04LX(Laser *laser);

int  laser_read(Laser * laser)
{
   int ret=0;
   if(laser->laser_type==USE_SICK_LMS)
   {
      ret = laser_read_USE_SICK_LMS(laser);
   }
   else if(laser->laser_type==USE_URG_04LX)
   {
      ret = laser_read_USE_URG_04LX(laser);
   }
   return ret;
}

int laser_read_USE_SICK_LMS(Laser * laser) {

   int datalen,i,cpt=0;
   double val;
   uchar buf[MAXNDATA];
   int fd=laser->fd;

   datalen = readLMSdata(fd, buf);
   if (datalen != 0) 
   {
      pthread_mutex_lock(&(laser->mutex_laser_capture));
      for(i=7; i<datalen+7; i=i+2,cpt++) 
      {

         val=(double)( (buf[i+1] & 0x1f) <<8  |buf[i]);
         laser->values[cpt]=val;

#ifdef DEBUG
         if((cpt % 40) ==0) 
            printf("\n%d:",cpt);

         printf("%5.0f ", val);
#endif
      }
      pthread_mutex_unlock(&(laser->mutex_laser_capture));

      /* si le status n'est pas bon, on affiche le message correspondant */
      if ( (buf[datalen+7] & 0x07) != 0)
         chkstatus(buf[datalen+7]);
   }

#ifdef DEBUG
   for(i=0;i<laser->nb_values-20;i=i+20)
      printf("%5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld mm\n",laser->values[i],laser->values[i+1],laser->values[i+2],laser->values[i+3],laser->values[i+4],laser->values[i+5],laser->values[i+6],laser->values[i+7],laser->values[i+8],laser->values[i+9],laser->values[i+10],laser->values[i+11],laser->values[i+12],laser->values[i+13],laser->values[i+14],laser->values[i+15],laser->values[i+16],laser->values[i+17],laser->values[i+18],laser->values[i+19]);
   printf("=================================================================\n");
#endif

   return 180;
}

int laser_read_USE_URG_04LX(Laser * laser) 
{
#ifdef DEBUG
   int i;
#endif
   int ret,n;
   /* Request for GD data */

   ret = (*lh_urg_requestData)(laser->urg, URG_GD, URG_FIRST, URG_LAST);
   if (ret < 0) {
      urg_exit(laser->urg, "lh_urg_requestData()");
   }

   pthread_mutex_lock(&(laser->mutex_laser_capture));
   /* Reception */
   n = (*lh_urg_receiveData)(laser->urg, laser->values, laser->nb_values);
   if (n < 0) {
      urg_exit(laser->urg, "lh_urg_receiveData()");
   }
   pthread_mutex_unlock(&(laser->mutex_laser_capture));

   /*sem_post(&(laser->laser_sem));*/

#ifdef DEBUG
   for(i=0;i<laser->nb_values-20;i=i+20)
      printf("%5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld %5ld mm\n",laser->values[i],laser->values[i+1],laser->values[i+2],laser->values[i+3],laser->values[i+4],laser->values[i+5],laser->values[i+6],laser->values[i+7],laser->values[i+8],laser->values[i+9],laser->values[i+10],laser->values[i+11],laser->values[i+12],laser->values[i+13],laser->values[i+14],laser->values[i+15],laser->values[i+16],laser->values[i+17],laser->values[i+18],laser->values[i+19]);
   printf("=================================================================\n");
#endif

   return n;

}
