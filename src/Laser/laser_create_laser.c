/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Laser.h>
#include <tools.h>
#include "tools/include/local_var.h"
#include <net_message_debug_dist.h>

void laser_create_laser(Laser ** laser, char *name, char *file_hw)
{
   FILE *fh;
   char lecture[256];
   char tmp[129];
   int val,ret;

   *laser = (Laser *) malloc(sizeof(Laser));
   if (*laser == NULL)
   {
      printf("erreur malloc laser_create_laser\n");
      exit(0);
   }
   (*laser)->name = (char *) malloc(strlen(name) + 1);
   if ((*laser)->name == NULL)
   {
      printf("erreur malloc laser_create_laser\n");
      exit(0);
   }
   strcpy((*laser)->name, name); /*Laser_object name */
   (*laser)->port = NULL;
   (*laser)->seuil_min = -1.;
   (*laser)->seuil_max = -1.;
   (*laser)->laser_type = USE_SICK_LMS;  /*Par defaut,le premier type a savoir USE_LMS_SICK*/
   (*laser)->autocheck = 0;
   /* default parameters */


   /*! loading laser parameters */

   /* get parameters from the hardware file */
   fh = fopen(file_hw, "r");

   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", file_hw);
      exit(1);
   }

   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */
      /*printf("lit: %s \n",lecture);*/

      if(ret != 1)
         EXIT_ON_ERROR("The hardware file (%s) does not match the expected format !\n", file_hw) ;
      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {
         if ((val=fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            printf("Erreur de lecture pour le port du laser %s\n",name);
            printf("%d\n",val);
            exit(1);
         }

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         /* verify if it is the first declaration for port */
         if ((*laser)->port != NULL)
         {
            printf("Warning: several declarations of port are declared in %s\n",file_hw);
            free((*laser)->port);
         }

         (*laser)->port = (char *) malloc(strlen(tmp) + 1);
         if ((*laser)->port == NULL)
         {
            printf("Erreur d'allocation memoire pour le port de compass %s dans compass_create_compass\n",name);
            exit(1);
         }
         strcpy((*laser)->port, tmp);
      }
      else if (strcmp(lecture, "TYPE") == 0)    /* get the type */
      {
         ret=fscanf(fh, "%d\n", &((*laser)->laser_type));
      }
      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
         ret=fscanf(fh, "%d\n", &((*laser)->autocheck));
      }
      else if (strcmp(lecture, "MIN") == 0)
      {
         ret=fscanf(fh, "%f\n", &((*laser)->seuil_min));
      }
      else if (strcmp(lecture, "MAX") == 0)
      {
         ret=fscanf(fh, "%f\n", &((*laser)->seuil_max));
      }
      else {
         lire_ligne(fh);
      }

      lecture[0]='\0';

      /*	fscanf(fh,"%s\n",lecture); 	*/
   }

   fclose(fh);

   if ((*laser)->seuil_min < 0.) {
      printf("argument 'MIN' non definit dans %s\n",file_hw);
      exit(0);
   }
   if ((*laser)->seuil_max < 0.) {
      printf("argument 'MAX' non definit dans %s\n",file_hw);
      exit(0);
   }

   printf("\t\t type=%d \n\t\t port=%s\n\t\t min=%f max=%f\n\t\t",(*laser)->laser_type, (*laser)->port,(*laser)->seuil_min,(*laser)->seuil_max);

   if (pthread_mutex_init(&((*laser)->mutex_laser_start), NULL))
   {
      printf("*** %s : %d : Can't initialise mutex : mutex_laser ***",
            __FUNCTION__, __LINE__);
   }

   if (pthread_mutex_init(&((*laser)->mutex_laser_capture), NULL))
   {
      printf("*** %s : %d : Can't initialise mutex : mutex_laser ***",
            __FUNCTION__, __LINE__);
   }



}
