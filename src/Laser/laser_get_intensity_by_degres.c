/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Laser.h>
#include "tools/include/local_var.h"

/*#define DEBUG*/
#include <net_message_debug_dist.h>

long  laser_get_intensity_by_degres_USE_SICK_LMS(Laser *laser,float deg);
long  laser_get_intensity_by_degres_USE_URG_04LX(Laser *laser,float deg);

long  laser_get_intensity_by_degres(Laser * laser,float deg)
{
   double ret= 0.;
   if(laser->laser_type==USE_SICK_LMS)
   {
      ret = laser_get_intensity_by_degres_USE_SICK_LMS(laser,deg);
   }
   else if(laser->laser_type==USE_URG_04LX)
   {
      ret = laser_get_intensity_by_degres_USE_URG_04LX(laser,deg);
   }
   return ret;
}

long laser_get_intensity_by_degres_USE_SICK_LMS(Laser * laser,float deg) 
{

   int index;
   double intensity;
   index = (int)deg + 90;
   if(index>=laser->nb_values||index<0)
      intensity = 0.;
   else
      intensity = laser->values[index];

   return intensity;
}

long laser_get_intensity_by_degres_USE_URG_04LX(Laser * laser, float deg) 
{
   int index;
   long intensity;
#ifdef DEBUG
   int min_distance,max_distance;
#endif
   /* Request for GD data */
   index = (*lh_urg_deg2index)(laser->urg, (int)deg);

#ifdef DEBUG
   min_distance = (*lh_urg_minDistance)(laser->urg);
   max_distance = (*lh_urg_maxDistance)(laser->urg);
   dprints("min_distance=%d, max_distance=%d \n",min_distance,max_distance);
#endif

   dprints("deg= %f\tindex = %d\namax = %d\tamin = %d\ndmax = %ld\tdmin = %ld\n\n", deg, index, laser->urg_parameters->area_max_, laser->urg_parameters->area_min_, laser->urg_parameters->distance_max_, laser->urg_parameters->distance_min_);

   if (index >= laser->urg_parameters->area_max_ || index < laser->urg_parameters->area_min_)
      intensity = laser->urg_parameters->distance_max_;
   else
      intensity = laser->values[index];

   if (intensity <= laser->urg_parameters->distance_min_)
      intensity = laser->urg_parameters->distance_max_;

   return intensity;
}
