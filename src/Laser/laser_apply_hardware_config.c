/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <tools.h>
#include <Laser.h>
#include "tools/include/local_var.h"
#include <net_message_debug_dist.h> 
#include <limits.h>

/*function apres ajout des camera dans le .dev, on y retriuve le parsage de la phrase type:
 camera titi is camera.hwc */
void laser_apply_hardware_config(char *argv)
{
   FILE *dfl;
   Laser **laser_table = NULL;

   int nb_laser;
   int i;
   char lecture[100];
   char laser_name[256][256];
   char laser_type[256][256];
   pthread_t my_thread;
   void * module;
   char path_urg[PATH_MAX + 1];

   nb_laser = 0; /*! trying to open appli_name.dev */

   /*Initialisation du buffer contenant le chemin pour acceder a la librairie URG.so*/
   snprintf(path_urg,PATH_MAX,"%s/bin_leto_prom/URG.so",getenv("HOME"));


   dfl = fopen(argv, "r");
   if (dfl == NULL)
   {
      PRINT_WARNING("Promethe initialisation: unable to open configuration file %s \n Using default parameters", argv);

   }
   else
   { /*! loading parameters */
      while (!feof(dfl))
      {
         if (fscanf(dfl, "%s", lecture) == EOF) break;
         if (lecture[0] == '%')
         {
            lire_ligne(dfl);
         }
         else
         {
            if (strcmp(lecture, "Laser") == 0)
            {
               if (fscanf(dfl, " %s is %s", laser_name[nb_laser], laser_type[nb_laser]) != 2)
               {
                  EXIT_ON_ERROR("parse error in dev file\n");
               }
               nb_laser++;
            }

         }

      }
      fclose(dfl);
   }
   if (nb_laser > 0)
   {
      kprints("%d laser(s) are defined:\n", nb_laser);
      for (i = 0; i < nb_laser; i++)
      {
         kprints("\t Laser %s is configurated by %s \n", laser_name[i], laser_type[i]);
      }

      laser_table = (Laser **) MANY_ALLOCATIONS(nb_laser, Laser*);

      for (i = 0; i < nb_laser; i++)
      {
         laser_table[i] = NULL;
         laser_create_laser(&laser_table[i], laser_name[i], laser_type[i]);
      }

      laser_create_laser_table(laser_table, nb_laser);

      /*Charge URG.so si un des laser est urg*/
      for (i = 0; i < nb_laser; i++)
      {
         if (laser_table[i]->laser_type == USE_URG_04LX)
         {
            module = dlopen(path_urg, RTLD_LAZY | RTLD_GLOBAL);
            if (!module)
            {
               PRINT_WARNING("PB ouverture librairie URG.so:  le chemin pour acceder a la librairie est le suivant ( %s )\n erreur=%s\n",path_urg, dlerror());
            }
            else
            {
               link_function_with_library(&lh_urg_connect, module, "urg_connect");
               link_function_with_library(&lh_urg_disconnect, module, "urg_disconnect");
               link_function_with_library(&lh_urg_error, module, "urg_error");
               link_function_with_library(&lh_urg_dataMax, module, "urg_dataMax");
               link_function_with_library(&lh_urg_requestData, module, "urg_requestData");
               link_function_with_library(&lh_urg_receiveData, module, "urg_receiveData");
               link_function_with_library(&lh_urg_maxDistance, module, "urg_maxDistance");
               link_function_with_library(&lh_urg_minDistance, module, "urg_minDistance");
               link_function_with_library(&lh_urg_deg2index, module, "urg_deg2index");
               link_function_with_library(&lh_urg_parameters, module, "urg_parameters");
            }
            break;
         }
      }

      /*init des laser*/
      for (i = 0; i < nb_laser; i++)
         laser_init(laser_table[i]);

      /*Lance le thread d'acquisition des lasers*/
      for (i = 0; i < nb_laser; i++)
         pthread_mutex_lock(&(laser_table[i]->mutex_laser_start));

      if (pthread_create(&my_thread, NULL, (void *(*)(void*)) laser_capture_all_laser, laser_table) != 0)
      {
         EXIT_ON_ERROR("laser_capture_all_laser(hardware) : Cannot create thread for capture laser\n");
      }

      /*Autocheck*/
      for (i = 0; i < nb_laser; i++)
         if (laser_table[i]->autocheck == 1) laser_check_hardware(laser_table[i]);

   }
}
