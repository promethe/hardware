/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_LASER_LOCAL_VAR_H
#define _HARDWARE_LASER_LOCAL_VAR_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1
#define MAXRETRY 25
#define MAXNDATA 802
#define STX 0x02   /*every PC->LMS packet is started by STX*/ 
#define ACKSTX 0x06 /*every PC->LMS packet is started by ACKSTX*/
#define BAUD_500000 512
#define BAUD_38400 256
#define BAUD_19200 128
#define BAUD_9600 64
#define RANGE_100 32
#define RANGE_180 16
#define RES_1_DEG 8
#define RES_0_5_DEG 4
#define RES_0_25_DEG 2 
#define MMMODE 1
#define CMMODE 0
typedef unsigned char uchar ;


/*the cmd and ack packets for the 5 different range/resolution modes*/
extern const unsigned char PCLMS_RES1[9];
extern const unsigned char PCLMS_RES2[9];
extern const unsigned char PCLMS_RES3[9];
extern const unsigned char PCLMS_RES4[9];
extern const unsigned char PCLMS_RES5[9];
extern const unsigned char LMSPC_RES1_ACK[12];
extern const unsigned char LMSPC_RES2_ACK[12];
extern const unsigned char LMSPC_RES3_ACK[12];
extern const unsigned char LMSPC_RES4_ACK[12];
extern const unsigned char LMSPC_RES5_ACK[12];

/*the cmd and ack packets different measurement unit modes*/
extern const unsigned char PCLMS_SETMODE[14];
extern const unsigned char PCLMS_MM[39];/* Block A4 */
extern const unsigned char LMSPC_MM_ACK[42];

/*the cmd packets for setting transfer speed and  controlling the start and stop of measurement*/
extern const unsigned char PCLMS_STATUS[5];
extern const unsigned char PCLMS_START[6];
extern const unsigned char PCLMS_STOP[6];
extern const unsigned char PCLMS_B9600[6];
extern const unsigned char PCLMS_B19200[6];
extern const unsigned char PCLMS_B38400[6];
extern const unsigned char PCLMS_B500000[6];
/*the ack packet for the above*/
extern const unsigned char LMSPC_CMD_ACK[8];

extern Laser **laser_table;
extern int nb_laser;

void chkstatus(uchar c);

#endif
