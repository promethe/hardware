/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Laser.h>
#include "tools/include/local_var.h"

#include <net_message_debug_dist.h>


#include <termios.h>
#ifdef Darwin  /* mac os */
#define  B500000  0010005 /* pas defini en standart sur le mac ! */
#endif


#define NB_SEND 20
/*#define SIMULATION*/
#define TRACE

#ifdef TRACE
#include <sys/time.h>
#endif

/*The only 2 global variable used by the signal handler so that
  the program can reset to 9600 bps mode before unexpected termination*/
static int LMS_FD;
static struct termios OLDTERMIOS;

/* Calculates the CRC for packets sent to/from the LMS */
unsigned short LMSCRC(unsigned char* theBytes, int lenBytes)
{
   unsigned char xyz1 = 0;
   unsigned char xyz2 = 0;
   unsigned short uCrc16 = 0;
   int i;
   for (i = 0; i < lenBytes; i++)
   {
      xyz2 = xyz1;
      xyz1 = theBytes[i];

      if (uCrc16 & 0x8000) {
         uCrc16 = (uCrc16 & 0x7fff) << 1;
         uCrc16 = uCrc16 ^ 0x8005;
      }
      else {
         uCrc16 = uCrc16 << 1;
      }
      uCrc16 = uCrc16 ^ (xyz1 | (xyz2 << 8));
   }
   return uCrc16;
}

void printError(const char * msg)
{
   printf("%s", msg);
   /*fprintf(stderr, "%s", msg);*/
}

void printError1(const char * msg, int val1)
{
   printf("%s 0x%x\n", msg, val1);
   /*fprintf(stderr, "%s 0x%x\n", msg, val1);*/
}

void printError2(const char * msg, int val1, int val2)
{
   printf("%s 0x%x 0x%x\n", msg, val1, val2);
   /*fprintf(stderr, "%s 0x%x 0x%x\n", msg, val1, val2);*/
}


/*Compares two messages*/
uchar msgcmp(int len1, const uchar *s1, int len2, const uchar *s2)
{
   int i;
   unsigned short crcval;
   if (len1 != len2) {
      printError2("msgcmp - message lengths didn't match", len1, len2);
      return FALSE;
   }
   for(i=0; i<len1; i++) {
      if (s1[i] != s2[i]) {
         printError2("msgcmp - character didn't match", s1[i], s2[i]);
         return FALSE;
      }
   }
   /*printf("trame reçu : %x\n",s1);*/
   crcval = LMSCRC((unsigned char*)s1, len1);
   if ((crcval & 0xff) != s2[len2]) {
      printError2("msgcmp - CRC didn't match1", crcval & 0xff, s2[len2-2]);
      return FALSE;
   }
   if (((crcval >> 8) & 0xff) != s2[len2+1]) {
      printError2("msgcmp - CRC didn't match2",(crcval >> 8) & 0xff, s2[len2-1]);
      return FALSE;
   }

   return TRUE;
}

/*Sleeps 55uS (minimum LMS interval between bytes)*/
void sleep55us()
{
   /*This only works if the program priority is raised to real time.
  Otherwise the usleep of 55uS actually takes 1 or more scheduling
  intervals which can be more than the 14mS maximum interval between
  characters for the LMS.
  usleep(55);*/
}


/*Writes a message to the LMS*/
void wtLMSmsg(int fd, int len, const uchar *msg)
{
   int ret,i;
   unsigned short crcval = LMSCRC((unsigned char*)msg, len);
   unsigned char sendchar;

   /*printf("trame envoyee : %x\n",msg);*/

   for (i = 0; i < len; i++) {
      sleep55us();
      ret=write(fd,(const void*) (msg+i),1);
      if (ret == -1)
      {
         EXIT_ON_ERROR("write serial");
      }
   }

   sendchar = crcval & 0xff;
   sleep55us();
   ret=write(fd, (const void *) &sendchar, 1);
   if (ret == -1)
   {
      EXIT_ON_ERROR("write serial");
   }
   sendchar = (crcval >> 8) & 0xff;
   sleep55us();
   ret=write(fd, (const void *) &sendchar, 1);
   if (ret == -1)
   {
      EXIT_ON_ERROR("write serial");
   }


#ifdef DEBUG
   printf("write msg: ");
   for(i=0;i<len; i++) printf("%02x ",msg[i]);
   printf("%02x %02x\n", crcval & 0xff, (crcval >> 8) & 0xff);
#endif
}


int rdLMSmsg(int fd, int len, const uchar *buf)
{
   int sumRead=0,nRead=0,toRead=len,n;
   static struct timeval tv0;
   static fd_set rfds;
   tv0.tv_sec = 0;
   tv0.tv_usec = 500000;
   FD_ZERO (&rfds);
   FD_SET(fd, &rfds);

#ifdef DEBUG
   int i;
   printf("read msg: ");
#endif
   while(toRead>0){
      n=toRead>255 ? 255:toRead;
      toRead-=n;
      if (select(fd+1, &rfds, NULL, NULL, &tv0)) {
         nRead=read(fd,(void *)(buf+sumRead),n);
      }
      else {
         nRead = 20; /*just to keep it going*/
         printError("rdLMSmsg - timeout error in multi-byte read\n");
      }
#ifdef DEBUG
      for(i=0;i<nRead; i++) printf("%02x ",buf[i]);
#endif
      sumRead+=nRead;
      if(nRead!=n) break;
   }
#ifdef DEBUG
   printf("\n");
#endif
   return nRead;
}

uchar rdLMSbyte(int fd)
{
   uchar buf;
   int ret;
   static struct timeval tv0;
   static fd_set rfds;
   tv0.tv_sec = 0;
   tv0.tv_usec = 100000;

   FD_ZERO (&rfds);
   FD_SET(fd, &rfds);
   if (select(fd+1, &rfds, NULL, NULL, &tv0)) {
      ret=read(fd,&buf,1);
#ifdef DEBUG
      printf("read byte: %02x\n", buf);
#endif
      if (ret < 0)
      {
         EXIT_ON_ERROR("read serial");
      }
   }
   else {
      printError("rdLMSbyte - timeout Error\n");
      buf = 0;
   }
   return buf;
}


/*procedure that shows the laser measurements. Format: 
Timestamp
v0  v1  v2  ... v19
v10 v21 v22 ... v39
...
The total number of measurements is dependent on the resolution
and angular range selected*/
void showdata(int len, uchar *buf)
{
   struct timeval now;
   struct timezone tz;
   int i,hr,min,sec;
   double val;

   gettimeofday(&now,&tz);
   hr=(now.tv_sec/3600 % 24 - tz.tz_minuteswest/60) % 24;
   min=(now.tv_sec % 3600)/60;
   sec=now.tv_sec % 60;
   printf("\n%02d:%02d:%02d.%04ld  ",hr,min,sec,now.tv_usec/1000);

   /*each value are represented by 16 bits*/
   /*only the lower 12 bits are significant*/

   for(i=0; i<len; i=i+2) {
      if((i % 40) ==0) printf("\n%d:",i/2);
      val=(double)( (buf[i+1] & 0x1f) <<8  |buf[i]);
      printf("%5.0f ", val);
   }
   printf("\n");
}


/*return true if the ACK packet is as expected*/
uchar chkAck(int fd, int ackmsglen, const uchar *ackmsg)
{
   int i,buflen;
   uchar buf[MAXNDATA];
   uchar c;

   /*the following is to deal with a possibly timing issue*/
   /*the communication occasionally breaks without this*/
   usleep(100000);
   for(i=0;i<MAXRETRY;i++) {
      c=rdLMSbyte(fd);
      if(c==ackmsg[0])
         break;
   }
   buflen=rdLMSmsg(fd,ackmsglen+1,buf);
   return msgcmp(ackmsglen-1,ackmsg+1,buflen-2,buf);
}

/*set the communication speed and terminal properties*/
int initLMS(const char *serialdev, int baud_sel)
{
   int fd;
   const uchar *msg;
   tcflag_t cflagval;
   struct termios newtio_struct, *newtio=&newtio_struct;
   fd = open(serialdev, O_RDWR | O_NOCTTY );
   if (fd <0) {
      printError1("initLMS - Error opening 1", fd);
      perror(serialdev);
      exit(-1);
   }

   tcgetattr(fd,&OLDTERMIOS); /* save current port settings */

   /*after power up, the laser scanner will reset to 9600bps*/
   memset(newtio, 0, sizeof(struct termios));
   newtio->c_cflag = B9600 | CS8 | CLOCAL | CREAD;
   newtio->c_iflag = IGNPAR;
   newtio->c_oflag = 0;

   /* set input mode (non-canonical, no echo,...) */
   newtio->c_lflag = 0;
   newtio->c_cc[VTIME]    = 10;   /* inter-character timer unused */
   newtio->c_cc[VMIN]     = 255;   /* blocking read until 1 chars received */
   tcflush(fd, TCIFLUSH);
   tcsetattr(fd,TCSANOW,newtio);

   if (baud_sel == BAUD_500000) { /*step to the 500000bps mode*/
      msg = PCLMS_B500000;
      cflagval = B500000 | CS8 | CLOCAL | CREAD;
   }
   else if (baud_sel == BAUD_38400) { /*step to the 38400bps mode*/
      msg = PCLMS_B38400;
      cflagval = B38400 | CS8 | CLOCAL | CREAD;
   }
   else if (baud_sel == BAUD_19200) { /*step to the 19200bps mode*/
      msg = PCLMS_B19200;
      cflagval = B19200 | CS8 | CLOCAL | CREAD;
   }
   else {
      /*Do nothing - goal is 9600 and already at 9600*/
      return fd;
   }

   wtLMSmsg(fd,sizeof(PCLMS_B500000)/sizeof(uchar),msg);
   if(!chkAck(fd,sizeof(LMSPC_CMD_ACK)/sizeof(uchar),LMSPC_CMD_ACK))
      printError("initLMS - Baud rate changes failure\n");

   /*set the PC side as well*/
   tcflush(fd, TCIFLUSH);
   close(fd);
   usleep(400000);  /*This is needed on the faster kernels otherwise the*/
   /*open statement on the next line fails*/
   fd = open(serialdev, O_RDWR | O_NOCTTY );
   if (fd <0) {
      printError1("initLMS - Error opening 2", fd);
      perror(serialdev);
      exit(-1);
   }
   newtio->c_cflag = cflagval;

   tcflush(fd, TCIFLUSH);
   tcsetattr(fd,TCSANOW,newtio);
   tcflush(fd, TCIFLUSH);
   return fd;
}

/*Sets the sweep width and resolution*/
int setRangeRes(int fd, int res)
{
   const uchar *msg, *ackmsg;
   int retval = 0;

   /*change the resolution*/
   switch(res){
   case (RES_1_DEG | RANGE_100):
			            msg=PCLMS_RES1;
   ackmsg=LMSPC_RES1_ACK;
   break;
   case (RES_0_5_DEG | RANGE_100):
			            msg=PCLMS_RES2;
   ackmsg=LMSPC_RES2_ACK;
   break;
   case (RES_0_25_DEG | RANGE_100):
			            msg=PCLMS_RES3;
   ackmsg=LMSPC_RES3_ACK;
   break;
   case (RES_1_DEG | RANGE_180):
			            msg=PCLMS_RES4;
   ackmsg=LMSPC_RES4_ACK;
   break;
   case (RES_0_5_DEG | RANGE_180):
			            msg=PCLMS_RES5;
   ackmsg=LMSPC_RES5_ACK;
   break;
   default:
      printError("Invalid resolution selected. Drop back to default\n");
      msg=PCLMS_RES1;
      ackmsg=LMSPC_RES1_ACK;
      break;
   }

   /*the following two line works only because msg & ackmsg are const uchar str*/
   wtLMSmsg(fd,sizeof(PCLMS_RES1)/sizeof(uchar),msg);
   if(!chkAck(fd,sizeof(LMSPC_RES1_ACK)/sizeof(uchar),ackmsg)) {
      printError("setRangeRes - Resolution mode setting failure\n");
      retval = 1;
   }
   return retval;
}

/*Sends the password to enable changing the mode*/
int sendPassword(int fd)
{
   /*invoking setting mode*/
   int retval = 0;
   wtLMSmsg(fd,sizeof(PCLMS_SETMODE)/sizeof(uchar),PCLMS_SETMODE);
   if(!chkAck(fd,sizeof(LMSPC_CMD_ACK)/sizeof(uchar),LMSPC_CMD_ACK)) {
      printError("sendPassword - Measurement mode setting failure\n");
      retval = 1;
   }
   return retval;
}

/*Selects mm/cm
This message causes the red signal to light on an LMS291 - reason unknown*/
int setUnits(int fd, int unit)
{
   int retval = 0;
   uchar msg[100], ackmsg[100];
   memcpy(msg, PCLMS_MM, sizeof(PCLMS_MM));
   memcpy(ackmsg, LMSPC_MM_ACK, sizeof(LMSPC_MM_ACK));
   /*change the measurement unit*/
   /*may need to increase the timeout to 7sec here*/
   if (unit == MMMODE) {
      /*No change is required since MM is the default*/
   }
   else if (unit == CMMODE) {
      msg[11] = 0x00;
   }
   else {
      printError("setUnits - Invalid units specified\n");
   }

   /*The ACK contains the original message data*/
   memcpy(ackmsg+7, msg+5, sizeof(PCLMS_MM) - 5);

   /*the following two line works only because msg & ackmsg are const uchar str*/
   wtLMSmsg(fd,sizeof(PCLMS_MM)/sizeof(uchar),msg);
   if(!chkAck(fd,sizeof(LMSPC_MM_ACK)/sizeof(uchar),ackmsg)) {
      printError("setUnits - Measurement mode setting failure2\n");
      retval = 1;
   }
   return retval;
}


/*tell the scanner to enter the continuous measurement mode*/
int startLMS(int fd)
{
   int retval = 0;
   wtLMSmsg(fd,sizeof(PCLMS_START)/sizeof(uchar),PCLMS_START);
   if(!chkAck(fd,sizeof(LMSPC_CMD_ACK)/sizeof(uchar),LMSPC_CMD_ACK)) {
      printError("startLMS - LMS fails to start\n");
      retval = 1;
   }
   return retval;
}

/*stop the continuous measurement mode*/
void stopLMS(int fd)
{
   /* Mutex */
   Laser *laser=laser_get_first_laser();
   if ( laser == NULL )	 {
      printf("ERREUR : Laser inexistant\n");
      exit(0);
   }
   pthread_mutex_lock(&(laser->mutex_laser_capture));

   wtLMSmsg(fd,sizeof(PCLMS_STOP)/sizeof(uchar),PCLMS_STOP);
   if(!chkAck(fd,sizeof(LMSPC_CMD_ACK)/sizeof(uchar),LMSPC_CMD_ACK))
      printError("stopLMS - LMS fails to stop\n");
   else
      printf("stopLMS OK\n");

   pthread_mutex_unlock(&(laser->mutex_laser_capture));
}


/*check the status bit of the measurement*/
void chkstatus(uchar c)
{
   switch(c & 0x07){
   case 0: printError("chkstatus - no error\n"); break;
   case 1: printError("chkstatus - info\n"); break;
   case 2: printError("chkstatus - warning\n"); break;
   case 3: printError("chkstatus - error\n"); break;
   case 4: printError("chkstatus - fatal error\n"); break;
   default: printError1("chkstatus - unknown error", c); break;
   }
   if(c & 0x40)
      printError("chkstatus - implausible measured value\n");
   if(c & 0x80)
      printError("chkstatus - pollution\n");
}

/*reset terminal and transfer speed of laser scanner before quitting*/
void resetLMS(int fd)
{
   wtLMSmsg(fd,sizeof(PCLMS_B9600)/sizeof(uchar),PCLMS_B9600);
   if(!chkAck(fd,sizeof(LMSPC_CMD_ACK)/sizeof(uchar),LMSPC_CMD_ACK))
      printError("resetLMS - Baud rate changes failure\n");

   tcflush(fd, TCIFLUSH);
   tcsetattr(fd,TCSANOW,&OLDTERMIOS);
   close(fd);
}

/*trap a number of signals like SIGINT, ensure the invoke
  of resetLMS() before quitting*/
static void sig_trap(int sig)
{
   printError("sig_trap - Premature termination\n");
   stopLMS(LMS_FD);
   resetLMS(LMS_FD);
   exit(sig);
}

int connectToLMS(int range_mode, int res_mode, int unit_mode,
      char * port, int baud_sel)
{
   static int fd = -1;
   int retval = 0;
   int numTrys = 1;


   /*initialisation*/
   for (numTrys = 1; numTrys < 26; numTrys++) {
      retval = 0;


      /* #ifdef DEBUG*/
      printf("\n\ttrying to connect %d:\n", numTrys);
      /* #endif*/

      /*Set the baud rate of the PC and the LMS*/
      fd=initLMS(port, baud_sel);
      LMS_FD=fd;

      if  ((signal(SIGTERM, sig_trap) == SIG_ERR) ||
            (signal(SIGHUP, sig_trap)  == SIG_ERR) ||
            (signal(SIGINT, sig_trap)  == SIG_ERR) ||
            (signal(SIGQUIT, sig_trap) == SIG_ERR) ||
            (signal(SIGABRT, sig_trap) == SIG_ERR))
      {
         printError("connectToLMS - Cannot catch the signals\n");
      }


      /*mode setting*/
      retval |= setRangeRes(fd, range_mode | res_mode);
      retval |= sendPassword(fd); /*this enables the following command*/
      retval |= setUnits(fd, unit_mode);
      retval |= startLMS(fd);

      if (retval == 0) {
         /*LMS is working*/
         printf("connected to LMS\n");
         return fd;
      }
      else {
         /*Tell the LMS to stop sending and set baud to 9600*/

         /*Reset the LADAR to put it into a known state*/
         stopLMS(fd);
         resetLMS(fd);
         printError2("connectToLMS - Failed to connect", retval, numTrys);
      }
   }
   printError1("\n\tFailed to connect to ladar after many tries!", numTrys);
   exit(0);
}


int readLMSdata(int fd, uchar* buf)
{
   int lenBytes;
   unsigned short CRCcalculated;
   unsigned short realCRC = 0;
   int datalen;

   /* Mutex */
   Laser *laser=laser_get_first_laser();
   if ( laser == NULL )	 {
      printf("ERREUR : Laser inexistant\n");
      exit(0);
   }
   pthread_mutex_lock(&(laser->mutex_laser_capture));


   buf[0] = 0;

   while (buf[0] != 0x02) {
      buf[0] = rdLMSbyte(fd);
   }

   buf[1] = rdLMSbyte(fd); /*should be the ADR byte, ADR=0X80 here
  LEN refers to the packet length in bytes, datalen 
  refers to the number of measurements, each of them are 16bits long*/
   buf[2] = rdLMSbyte(fd); /*should be the LEN low byte*/
   buf[3] = rdLMSbyte(fd); /*should be the LEN high byte*/
   buf[4] = rdLMSbyte(fd); /*should be the CMD byte, CMD=0xb0 in this mode*/
   buf[5] = rdLMSbyte(fd); /*samples low byte*/
   buf[6] = rdLMSbyte(fd); /*samples high byte*/

   /*only lower 12 bits of high byte are significant */
   datalen = buf[5] | ((buf[6] & 0x1f) << 8);
   datalen = datalen * 2; /*each reading is 2 bytes*/

   /*Check that we have a valid ADR byte, valid data length and valid CMD byte*/
   if ((datalen > MAXNDATA) || (0x80 != buf[1]) || (0xb0 != buf[4])) {
      return 0;
   }

   rdLMSmsg(fd,datalen,buf+7);

   buf[datalen + 7] = rdLMSbyte(fd); /*Status byte*/
   buf[datalen + 8] = rdLMSbyte(fd); /*should be CRC low byte*/
   buf[datalen + 9] = rdLMSbyte(fd); /*should be CRC high byte*/
   realCRC = buf[datalen+8] | (buf[datalen+9] << 8);


   lenBytes = datalen+8;

   CRCcalculated = LMSCRC(buf, lenBytes);


   if (CRCcalculated != realCRC) {
      printError2("readLMSdata - CRC Error ", CRCcalculated, realCRC);
      return 0;
   }

   pthread_mutex_unlock(&(laser->mutex_laser_capture));

   return datalen; /*return 0 on error (0 valid samples)*/
}
