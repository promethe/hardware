/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Led_Serial.h>
#include "tools/include/local_var.h"


void led_serial_set_led(LED_Serial * led_serial, float *R, float *G, float *B)
{
	
	char commande[20];
	char reponse[200];
	char * entete;
	int val;

	if(led_serial == NULL)
	{
	  EXIT_ON_ERROR("led_serial_get_led : NULL pointer for led_serial");
	}
	if(R == NULL || G == NULL || B == NULL)
	{
	  EXIT_ON_ERROR("led_serial_get_led : NULL pointer for led_serial");
	}

	led_serial->r = (*R) * MAXLED; 
	led_serial->g = (*G) * MAXLED; 
	led_serial->b = (*B) * MAXLED; 
/*
	if(  led_serial->r  > MAXLED ) led_serial->r = MAXLED;
	if(  led_serial->g  > MAXLED ) led_serial->g = MAXLED;
	if(  led_serial->b  > MAXLED ) led_serial->b = MAXLED;

	if(  led_serial->r  < MINLED ) led_serial->r = MINLED;
	if(  led_serial->g  < MINLED ) led_serial->g = MINLED;
	if(  led_serial->b  < MINLED ) led_serial->b = MINLED;
*/

	sprintf(commande, "L%c%c%c", led_serial->r , led_serial->g,led_serial->b);
	serial_send_and_receive_block(led_serial->port, commande, reponse, 4, 1000000);
/*
// Code de lecture des valeurs des leds (avoir un vrai retour de l'arduino)
// Refaire envoie + lecture tant qu'on a pas des valeurs correctes
// Surement mieux de ne pas faire l'envoie + lecture dans la meme fonction => threader le tout pour etre le max asynchrone

	commande[0] = 'L';
	serial_send_and_receive_block(us_serial->port, commande, reponse, 1, 1000000);

	entete = strstr(reponse,"-l-");
        if( entete != NULL )
        {
	   i = 0;
           read = 0;
           for(j = 0 ; i < 3 && j < 200 ;j++)
           {
           	read = sscanf(reponse+j,"-%d-",&val);
                if( read > 0)
                {
			switch(i)
			{
				case 0 : led_serial->r = val; break;
				case 1 : led_serial->g = val; break;
				case 2 : led_serial->b = val; break;
			}
                        i++;
                }
	  }
	}
	
	*R = led_serial->r / MAXLED;
	*G = led_serial->g / MAXLED;
	*B = led_serial->b / MAXLED;
	
	if( *R < 0 ) *R = 0; 
	if( *G < 0 ) *G = 0; 
	if( *B < 0 ) *B = 0; 

	if( *R > 1. ) *R = 1.; 
	if( *G > 1. ) *G = 1.; 
	if( *B > 1. ) *B = 1.; 
*/	
	//pthread_mutex_lock(&led_serial->lock);
	//pthread_mutex_unlock(&led_serial->lock);
}
