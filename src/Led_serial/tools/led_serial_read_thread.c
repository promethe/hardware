/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Led_Serial.h>
#include <Serial.h> 
#include <unistd.h>

void led_serial_read_thread(LED_Serial *led_serial)
{
	char reponse[400];
	char * entete;
	char cmd = 'L';
//	int i, j; 
//	int read;
//	int val;

	if( led_serial == NULL )
	{
		dprints("%s : stop thread function, pointer null ",__FUNCTION__);
	}

	while( led_serial->enabled == 1)
	{
		serial_send_and_receive_block(led_serial->port, &cmd, reponse, 1, 1000000);
		//serial_receive(led_serial->port, reponse);

		/*traitement de la réponse */
		pthread_mutex_lock(&led_serial->lock);

		entete = strstr(reponse,"-l-");

		// TODO extraction (voir led_get)


		pthread_mutex_unlock(&led_serial->lock);
		usleep(5000);
	}
}
