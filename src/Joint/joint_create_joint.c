/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  create_joint.c 
\brief 

Author: Benoit Mariat
Created: 03/03/2005
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-search in the file linked to the devices what are the servo's parameters
these parameters are saved in a structure Joint

Macro:
-none 

Local variables:
- none

Global variables:
-none

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

#include <Aibo.h>
#include <Joint.h>

#include "tools/include/local_var.h"

void joint_create_joint(Joint ** serv, char *name, char *type)
{

   FILE *fh;
   char lecture[256];
   char tmp[129];
   int tmpi,ret;
   float tmpf;

   printf("============ ATTENTION: >= libhardware_5.0 ===========\n\tJoint is now deprecated\n\t\tYou should use Motor");


   *serv = (Joint *) malloc(sizeof(Joint));
   if (*serv == NULL)
   {
      printf("erreur malloc joint_create_joint\n");
      exit(0);
   }
   strcpy((*serv)->name, name);    /*nom du servo */

   tmp[128] = '\0';

   (*serv)->autocheck = 0;
   /* valeur par defaut au cas ou ils ne sont pas definis */
   (*serv)->ssc = -1;          /*initialisation num desc */
   (*serv)->Vmax = 15;         /*initialisation Vmax */
   (*serv)->Vdefault = 0;      /*initialisation Vdefault */
   (*serv)->Vmult = 1.;        /*initialisation Vmult */
   (*serv)->rotation = 1.;        /*initialisation rotation */

   /*    (*serv)->clientHW[0]=(char)0; */
   (*serv)->type = 1;
   /* recuperation des informations contenu dans le fichier hardware */
   fh = fopen(type, "r");
   if (fh == NULL)
   {
      printf("le fichier %s n'a pas pu etre ouvert\n", type);
      exit(1);
   }

   while (!feof(fh))
   {
      ret=fscanf(fh, "%s = ", lecture);   /* lecture du premier mot de la ligne */

      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0))
      {

         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
         {
            printf("Erreur de lecture pour le port du servo %s\n", name);
            exit(1);
         }

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         strcpy((*serv)->port_name, tmp);

      }
      else if ((strcmp(lecture, "TYPE") == 0))
      {
         if ((fgets(tmp, 129, fh) == NULL) || (strlen(tmp) > 127))
            EXIT_ON_ERROR("Erreur de lecture pour le type de communication du servo %s\n",name);

         /* delete the end char if it is an end line */
         if (tmp[strlen(tmp) - 1] == '\n')
            tmp[strlen(tmp) - 1] = '\0';

         strcpy((*serv)->type_controlleur, tmp);
      }
      else if ((strcmp(lecture, "START") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->start = tmpi;
      }
      else if ((strcmp(lecture, "STOP") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->stop = tmpi;
      }
      else if ((strcmp(lecture, "INIT") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->init = tmpi;
      }
      else if ((strcmp(lecture, "AUTOCHECK") == 0))
      {
         ret= fscanf(fh, "%d\n", &tmpi);
         (*serv)->autocheck = tmpi;
      }
      else if ((strcmp(lecture, "ZERO") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->zero = tmpi;
      }
      else if ((strcmp(lecture, "RANGE") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->range = tmpi;
      }
      else if ((strcmp(lecture, "ROTATION") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->rotation = tmpi;
      }
      else if ((strcmp(lecture, "VMAX") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->Vmax = tmpi;
      }
      else if ((strcmp(lecture, "VDEFAULT") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->Vdefault = tmpi;
      }
      else if ((strcmp(lecture, "VMULT") == 0))
      {
         ret=fscanf(fh, "%f\n", &tmpf);
         (*serv)->Vmult = tmpf;
      }
      else if ((strcmp(lecture, "SSC") == 0))
      {
         ret=fscanf(fh, "%d\n", &tmpi);
         (*serv)->ssc = tmpi;
      }
      else if (strcmp(lecture, "TYPE_REEL") == 0)
      {
         ret=fscanf(fh, "%d\n", &((*serv)->type));
      }
      else if (strcmp(lecture, "CLIENTHW") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serv)->clientHW));
         (*serv)->type = USE_CLIENTHW;
      }
      else if (strcmp(lecture, "CLIENTROBUBOX") == 0)
      {
         ret=fscanf(fh, "%s\n", ((*serv)->clientRobubox));
         (*serv)->type = USE_JOINT_WEBOTS;
      }
      else if (strcmp(lecture,"JOINT_AIBO")==0)
      {
         ret=fscanf(fh,"%d\n",&tmpi);

         if (tmpi == 1)
         {
            char	commande[255];

            (*serv)->type = USE_AIBO; /* TYPE AIBO_JOINT */

            (*serv)->updated = 0;
            /* mode de fonctionnement des articulation imposee */
            /* ->cancel : permet de stopper la commande en cours et de lq remplacer par la nouvelle */
            snprintf(commande, 255, "%s->blend=cancel;\n", name);
            aibo_send_message(commande, strlen(commande));
         }
      }
      else
         lire_ligne(fh);

   }
   fclose(fh);

   printf
   ("\t\t port=%s \n\t\t type=%d \n\t\t range=%d\n\t\t start=%d\n\t\t stop=%d\n\t\t init=%d\n",
         (*serv)->port_name, (*serv)->type, (*serv)->range, (*serv)->start, (*serv)->stop,
         (*serv)->init);
   (void)ret;
   return;
}
