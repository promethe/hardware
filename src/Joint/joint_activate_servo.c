/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  joint_servo_command.c 
\brief 

Author: Antoine de Rengerve
Created: 22/05/2009
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-activates or desactivates the servo.
(seuil a 0.5)
 < : desactivation
 > : activation
  the activ parameter (0/1) gives the order.
  1 stands for activating
  0 stands for desactivating

Float value could correspond to stiffness.

Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <unistd.h>
#include <termio.h> */

#include <Serial.h>
#include <Joint.h>
#include <ClientHW.h>
#include "tools/include/local_var.h"
#include <ClientKatana.h> /* currently the only one implemented */
#include <Aibo.h>


#define DEBUG
#define ACTIV_SEUIL 0.5

#include <net_message_debug_dist.h>

void joint_activate_servo(Joint * serv, float activ)
{
/*     char buff[255]; */
/*     ClientHW *chw = NULL; */
    ClientKatana *katana = NULL;
    char reponse[255];
    char commande[255];
    int activ_seuil;
    printf("activ %f %d\n",activ, (int)activ);

      if(activ>ACTIV_SEUIL)
	activ_seuil = 1;
      else
	activ_seuil = 0;

    if (serv == NULL)
    {
        cprints("Nom du servo inconnu\n");
        exit(1);
    }
 /*
 ** BLOCK MOTEUR TYPE AIBO
 */ 
    else if(serv->type == USE_AIBO)
      {/* BLOCK MOTEUR TYPE AIBO */
     
     memset(commande, 0, 255); 

     /* remplacement de ":" par "<<" */
     snprintf(commande, 255, "switch_joint_%s : %s.load = %d,\n", serv->name, serv->name, activ_seuil);

#ifdef DEBUG
     printf("%s", commande);
#endif

     aibo_send_message(commande, strlen(commande));

     return ;
   } 
 /**
  * BLOCK MOTEUR KATANA
  */
   else if (serv->type == 5){
    dprints("type=%d\n",serv->type);
      
    memset(commande, 0, 255);
      
    snprintf(commande, 255, "switchMotor = %s,%d\n", serv->name, activ_seuil);
    dprints("Dans %s, la commande envoyer est:%s\n",__FUNCTION__,commande);
    dprints("%s", commande);
    dprints("servo->clientHW = %s",serv->clientHW);
    katana=clientKatana_get_clientKatana_by_name(serv->clientHW);  
    clientKatana_transmit_receive(katana,commande,reponse);
      
    return ;

  }
 else if (serv->type == 3)
    {
      cprints("Not implemented yet !\n");

      return;
    }
    else if (serv->type == 1)
    {
      cprints("Not implemented yet !\n");
      return;
    }
}
