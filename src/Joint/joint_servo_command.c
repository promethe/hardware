/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
\file  joint_servo_command.c 
\brief 

Author: Benoit Mariat
Created: 03/03/2005
Modified:
- author: -
- description: -
- date: -

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
-send the command to servo, the command is a percentage of the
 mouvement capacity define by parameters start and stop

Macro:
-none 

Local variables:
- none

Global variables:
-undirectly

Internal Tools:
-none

External Tools: 
-none

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
 ************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net_message_debug_dist.h>
/*#include <unistd.h>
#include <termio.h> */

#include <Serial.h>
#include <Joint.h>
#include <ClientHW.h>
#include "tools/include/local_var.h"
#include <ClientKatana.h>
#include <Aibo.h>
#include <ClientRobubox.h>
/*
	name: nom du servo
	pct: pourcentage de la position
	speed: controle de la vitesse (lorsque c'est possible)
		 valeurs de 1 (vitesse min) a Vmax (vitesse max), 0: pas de controle
 */
void joint_servo_command(Joint * serv, float pct, int speed)
{
  /*Joint *serv; */
  int distance, vitesse = 0;
  float  position;
  char buff[255];
  char commande[255];
  ClientHW *chw = NULL;
  ClientKatana *katana = NULL;
  char reponse[255];
  /* recuperation du servo */
  /*serv=joint_get_serv_by_name(name); */


  if (serv == NULL)
    EXIT_ON_ERROR("Nom du servo inconnu\n");

  if(serv->type == USE_JOINT_WEBOTS)
  {
    ClientRobubox *clientRobubox;
    char commande[256];
    char result[256];

    clientRobubox = clientRobubox_get_clientRobubox_by_name(serv->clientRobubox);

    if (serv->rotation != 1)
    {
      pct = 1 - pct;
    }

    sprintf(commande,"Servo %f", pct);
    clientRobubox_transmit_receive(clientRobubox,commande,result);
  }
  else if(serv->type == USE_AIBO)
  {/* BLOCK MOTEUR TYPE AIBO */
    /*
     ** On verifie qu'on ne sort pas des valeurs possible
     ** Ca permet d'eviter de griller le moteur
     */
    if(pct < 0) pct = 0;
    if(pct > 1) pct = 1;

    /* position "virtuelle" entre 0 et 1 */
    serv->position=pct;

    /* On calcule la valeur reelle selon le sens de rotation */
    distance = serv->stop - serv->start;
    position = serv->start + (pct * (float)distance);

    /* Proprioception qui devrait etre reelle normalement */
    /*
     ** TODO : Pour obtenir la reelle, il faut attendre que le moteur
     ** ait fini son mouvement ... doit on bloquer l'appli pour attendre
     ** et avoir la reelle proprioception ??
     */
    serv->proprio = position;

    /*
     ** On verifie que la vitesse est correcte
     ** la aussi, evite de casser un moteur
     */
    if(speed < 0) vitesse = serv->Vdefault;
    else vitesse = (int) ((float)speed * serv->Vmult);
    if(vitesse > serv->Vmax) vitesse = serv->Vmax;
    if( (vitesse < 1) && (speed != 0)  && (serv->Vdefault != 0) ) vitesse = 1;

    memset(commande, 0, 255);

    /* remplacement de ":" par "<<" */
    snprintf(commande, 255, "val_joint_%s : %s.val = %f speed : %i,\n", serv->name, serv->name, position, vitesse);

    dprints("%s", commande);

    aibo_send_message(commande, strlen(commande));

    return ;
  }
  else if (serv->type == USE_PLAYER)
  {
    /* printf("type=%d\n",serv->type); */

    if(pct < 0) pct = 0;
    if(pct > 1) pct = 1;

    /* position "virtuelle" entre 0 et 1 */
    serv->position=pct;

    /* On calcule la valeur reelle selon le sens de rotation */
    printf("serv->name=%s\n",serv->name);
    printf("serv->stop=%d\n",serv->stop);
    printf("serv->start=%d\n",serv->start);
    distance = serv->stop - serv->start;
    position = serv->start + (int)(pct * (float)distance);
    printf("position arriver int=%d\n",(int)position);
    printf("position arriver float=%f\n",position);
    /* Proprioception qui devrait etre reelle normalement */
    /*
     ** TODO : Pour obtenir la reelle, il faut attendre que le moteur
     ** ait fini son mouvement ... doit on bloquer l'appli pour attendre
     ** et avoir la reelle proprioception ??
     */
    /*serv->proprio = position;*/

    /*
     ** On verifie que la vitesse est correcte
     ** la aussi, evite de casser un moteur
     */
    /*  if(speed < 0) vitesse = serv->Vdefault;
	else vitesse = (int) ((float)speed * serv->Vmult);
	if(vitesse > serv->Vmax) vitesse = serv->Vmax;
	if( (vitesse < 1) && (speed != 0)  && (serv->Vdefault != 0) ) vitesse = 1;
     */

    memset(commande, 0, 255);

    snprintf(commande, 255, "val_joint_position%s:%s = %d\n", serv->name, serv->name, (int)position);
    printf("Dans %s, la commande envoyer est:%s\n",__FUNCTION__,commande);
#ifdef DEBUG
    printf("%s", commande);
#endif
    printf("servo->clientHW = %s",serv->clientHW);
    katana=clientKatana_get_clientKatana_by_name(serv->clientHW);
    clientKatana_transmit_receive(katana,commande,reponse);

    return ;


  }
  else if (serv->type == USE_CLIENTHW)
  {
    if (pct < 0.)
      pct = 0.;
    if (pct > 1.)
      pct = 1.;


    serv->position = pct;

    sprintf(commande, "joint_servo_command %s %f %d", serv->name, pct,speed);
    chw = clientHW_get_clientHW_by_name(serv->clientHW);
    clientHW_transmit(chw, commande);
    return;
  }
  else if (serv->type == USE_SERIAL)
  {
    /* calcul de la position absolue */
    if (pct < 0.)
      pct = 0.;
    if (pct > 1.)
      pct = 1.;


    serv->position = pct;


    serv->proprio = pct;    /*la position resultant de la proprio ou directement de la commande envoyee */
    distance = serv->stop - serv->start;
    if (serv->rotation == 1)
    {
      position = serv->start + (int) (pct * (float) distance);
    }
    else
    {
      position = serv->stop - (int) (pct * (float)distance);
    }

    dprints("POSITION: %d\n", position);
    dprints("DISTANCE: %d\n", distance);
    dprints("PCT: %f\n", pct);

    /* ecriture de la position dans le buffer pour l'envoyer vers le port serie */

    if ((strcmp(serv->type_controlleur, "ssc2") == 0) && (serv->ssc != -1))
    {
      buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
      buff[1] = (char) serv->ssc; /*!< numero du servo [0-254] */
      buff[2] = (char) position;  /*!< position du servo [0-254] */
      buff[3] = '\0';
    }
    else if (strcmp(serv->type_controlleur, "ssc12") == 0 && serv->ssc != -1)
    {
      if (speed == -1)
        vitesse = serv->Vdefault;
      else
        vitesse = (int) ((float) speed * serv->Vmult);
      if (vitesse > serv->Vmax)
        vitesse = serv->Vmax;
      if ((vitesse < 1) && (speed != 0) && (serv->Vdefault != 0))
        vitesse = 1;
      dprints("VITESSE: %d\n", vitesse);
      dprints("commande d'envoi: %X | %X | %X\n", 255, vitesse * 16 + serv->ssc, position);
      buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
      buff[1] = (char) (vitesse * 16 + serv->ssc);    /*!< numero du servo [0-254] */
      buff[2] = (char) position;  /*!< position du servo [0-254] */
      buff[3] = '\0';
    }
    else if (strcmp(serv->type_controlleur, "ssc32") == 0 && serv->ssc != -1)
    {
      sprintf(buff,"#%d P%d S%d\r",serv->ssc,(int) (500+8*position),2500/* vitesse qu'il faudra modifier*/);
      dprints("%s\n", buff);
    }
    else
      EXIT_ON_ERROR("type de materiel (support des servos) non reconnu (joint_servo_command)\nTEST: %s / %d\n", serv->type_controlleur, serv->ssc);

    /* envoi vers le port serie */
    /*if(serv->clientHW[0]==0)**M.M. : y'a un return avant s'il est different de 0 donc on arrive la que si y'a 0 donc pas de test..." */
    //printf("Envoie commande vers le port %s \n",serv->port_name);
    if(strcmp(serv->port_name,"NULL")!=0)
      serial_send(serv->port_name, buff, strlen(buff));
    else
      dprints("joint simul\n");
    /*tcflush(serialport, TCIOFLUSH);  elimine les ecritures en queue et les donnees recues non lues *
           write(serialport, buff, 3);
           tcdrain(serialport);   attend que tout soit transmis */


    return;
  }
  else if (serv->type == USE_BERENSON)
    {
    /*
     * Pour le cou de Berenson (ou tout autre moteur CC geres par un arduino et une roboclaw)
     *  le fichier hardware doit contenir obligatoirement les lignes
     * TYPE_REEL = 6    --- C'est le type MCC
     * Type = ssc2      --- Simple convention
     * SSC = 4          --- Simple convention
     * Pour la suite il serait interessant de supprimer le recours au type ssc2 et au port 4
    */
      /* calcul de la position absolue */
      if (pct < 0.)        pct = 0.;
      if (pct > 1.)        pct = 1.;

      /*la position resultant de la proprio ou directement de la commande envoyee */
      /*Enorme bug, la commande motrice ne doit pas modifier la proprio. Il existe une fonction dediee à cela
      serv->position = pct;
      serv->proprio = pct;
      */


      distance = serv->stop - serv->start;
      if (serv->rotation == 1)
      {
        position = serv->start + (int) (pct * (float) distance);
      }
      else
      {
        position = serv->stop - (int) (pct * (float)distance);
      }

      dprints("POSITION: %d\n", position);
      dprints("DISTANCE: %d\n", distance);
      dprints("PCT: %f\n", pct);

      /* ecriture de la position dans le buffer pour l'envoyer vers le port serie */

      if ((strcmp(serv->type_controlleur, "ssc2") == 0) && (serv->ssc != -1))
      {
        buff[0] = (char) 255;   /*!< marqueur, toujours a 255 */
        /*buff[1] = (char) serv->ssc;*/ /*!< numero du servo [0-254] */
        buff[1] = (char) 4; /*toujours egal a 4*/
        buff[2] = (char) position;  /*!< position du servo [0-254] */
        buff[3] = '\0';
      }
      else
        EXIT_ON_ERROR("type de materiel (support des servos) non reconnu (joint_servo_command)\nTEST: %s / %d\n", serv->type_controlleur, serv->ssc);

      /* envoi vers le port serie */
      /*if(serv->clientHW[0]==0)**M.M. : y'a un return avant s'il est different de 0 donc on arrive la que si y'a 0 donc pas de test..." */
      //printf("Envoie commande vers le port %s \n",serv->port_name);
      if(strcmp(serv->port_name,"NULL")!=0)
        serial_send(serv->port_name, buff, strlen(buff));
      else
        dprints("joint simul\n");
      /*tcflush(serialport, TCIOFLUSH);  elimine les ecritures en queue et les donnees recues non lues *
             write(serialport, buff, 3);
             tcdrain(serialport);   attend que tout soit transmis */
      return;
    }

}
