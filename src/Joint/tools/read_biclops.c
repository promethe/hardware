/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*
 * read_biclops.c
 *
 *  Created on: Apr 10, 2014
 *      Author: ali
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

/*#define DEBUG*/
/*#define TIME_TRACE*/
#ifdef TIME_TRACE
#include <sys/time.h>

long SecondesFunctionTimeTrace;
long MicroSecondesFunctionTimeTrace;
struct timeval InputFunctionTimeTrace;
struct timeval OutputFunctionTimeTrace;
#endif

#include <net_message_debug_dist.h>
#include <Joint.h>
#include <Serial.h>
#include <termios.h>
#include <string.h>
float read_biclops(Joint *serv)
{
  /*Lecture des donnees retournees par la biclops*/
#ifdef Linux
  char reponse[64];
  int ret = 0;
  int serv_proprio;
  int length;
  char buff[2] = {'-','o'}; /* Commande '-o' envoyees a l'arduino pour demander la valeur du capteur */
  //char buff = 'o';
  float start , stop, distance;
#ifdef TIME_TRACE
  char MessageFunctionTimeTrace[256];
#endif
#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  start = (float)serv->start/254.;
  stop = (float)serv->stop /254.;
  distance = stop - start ;
  //	do
  //	{
  length = serial_send_and_receive_block(serv->port_name, buff, reponse, 2,100000);
#ifdef TIME_TRACE
  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
    SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
    MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  sprintf(MessageFunctionTimeTrace,"Tine in function_acquisition\t%4ld.%06d\n",SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
  printf("read biclops : %s\n", MessageFunctionTimeTrace);
#endif

  ret = sscanf(reponse, "-O%d-", &serv_proprio);
  dprints("message : taille  %d | message %s | retour %d\n",length,reponse,ret);
  if(ret != 1)
  {
    return serv->proprio;
  }

  //	}
  //	while (ret != 1);
  serv->proprio =  ((float)serv_proprio + 2600 )/5200 ; /*Un tour complet de la biclops fait 5200 tics, reponse entre -2600 et +2600*/
  serv->proprio = (serv->proprio - start)/distance ;
  if (serv->rotation == -1 ) serv->proprio = 1 - serv->proprio;


  dprints("reponse = %s proprio=%f \n", reponse,serv->proprio);
  (void)length;
  return serv->proprio;
#else
  printf("ERROR, Non Linux OS cannot use functions %s ",__FUNCTION__);
#endif
}



void read_biclops_thread(Joint *serv)
{
  /*Lecture des donnees retournees par la biclops*/
#ifdef Linux
  char reponse[64];
  int ret = 0;
  int serv_proprio;
  int length;
  char buff[2] = {'-','o'}; /* Commande '-o' envoyees a l'arduino pour demander la valeur du capteur */
  float start , stop, distance;

#ifdef TIME_TRACE
  char MessageFunctionTimeTrace[256];
#endif
#ifdef TIME_TRACE
  gettimeofday(&InputFunctionTimeTrace, (void *) NULL);
#endif

  start = (float)serv->start/254.;
  stop = (float)serv->stop /254.;
  distance = stop - start ;
  if( serv == NULL )
  {
    EXIT_ON_ERROR("%s : stop thread function, pointer null ",__FUNCTION__);
  }
  while(serv->enabled == 1)
  {
    length = serial_send_and_receive_block(serv->port_name, buff, reponse, 2,100000);
#ifdef TIME_TRACE
    gettimeofday(&OutputFunctionTimeTrace, (void *) NULL);
    if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
    {
      SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
      MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    else
    {
      SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
      MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
    }
    sprintf(MessageFunctionTimeTrace,"Tine in function_acquisition\t%4ld.%06d\n",SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace);
    printf("read biclops : %s\n", MessageFunctionTimeTrace);
#endif

    ret = sscanf(reponse, "-O%d-", &serv_proprio);
      dprints("taille recu %d  %s retour %d\n",length,reponse,ret);
      if(ret == 1)
    {
         /*Un tour complet de la biclops fait 5200 tics, reponse entre -2600 et +2600*/
         serv->proprio = ((((float)serv_proprio+ 2600 )/5200)-start)/distance ;
		if (serv->rotation == -1 ) serv->proprio = 1 - serv->proprio;
    }
    
    printf("reponse = %s proprio=%f \n", reponse,serv->proprio);
  }
  (void)length;
  //return serv->proprio;
#else
  printf("ERROR, Non Linux OS cannot use functions %s ",__FUNCTION__);
#endif
}
