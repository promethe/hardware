/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <time.h>
#include <Aibo.h>
#include <ClientKatana.h>
#include <Joint.h>
#include <ClientRobubox.h>

#include <sys/time.h>

#include "tools/include/local_var.h"

 /*#define DEBUG*/
#include <net_message_debug_dist.h>


void joint_get_proprio(Joint *serv)
{	

	if(serv == NULL)
		EXIT_ON_ERROR("Nom du servo inconnu\n");
	dprints("type pan tilt : %d\n",serv->type);

	if(serv->type == USE_JOINT_WEBOTS)
	{
		ClientRobubox *clientRobubox;
		char commande[256];
		char result[256];
		char cmd[256];
		float value = 0;

		clientRobubox = clientRobubox_get_clientRobubox_by_name(serv->clientRobubox);
		sprintf(commande,"%s","Proprio");
		clientRobubox_transmit_receive(clientRobubox,commande,result);

		sscanf(result, "%s %f", cmd, &value);

		if (serv->rotation != 1)
		{
			serv->proprio = 1 - value;
		}
		else
		{
			serv->proprio = value;
		}
	}
	else if(serv->type == USE_AIBO)
	{/* BLOCK MOTEUR TYPE AIBO */

		char	commande[255];
		/*struct timespec ts;*/

		serv->updated = 0;
		snprintf(commande, 255, "val_joint_%s:%s,\n", serv->name, serv->name);
		dprints("%s", commande);
		aibo_send_message(commande, strlen(commande));

		/*       while (serv->updated != 1) */
		/* 	{ */
		/* 	  ts.tv_sec = 0; */
		/* 	  ts.tv_nsec = 5000000; */
		/* 	  nanosleep(&ts, NULL); */
		/* 	} */

		sem_wait(&(serv->sem));

	}
	else
	{

		if(serv->type==USE_PLAYER)
		{
			char commande[255];
			char reponse[255];
			ClientKatana *katana=NULL;
			int distance;
			float position;
			snprintf(commande,255,"proprio_%s:%s,\n",serv->name,serv->name);
			/* dprints("%s", commande); */

			/*	printf("servo->clientHW = %s\n",serv->clientHW);*/
			katana=clientKatana_get_clientKatana_by_name(serv->clientHW);
			clientKatana_transmit_receive(katana,commande,reponse);
			/*	printf("reponse=%s\n",reponse);*/
			/*convertion entre O et 1*/


			/* On calcule la valeur reelle selon le sens de rotation */
			dprints("serv->name=%s\n",serv->name);
			dprints("serv->stop=%d\n",serv->stop);
			dprints("serv->start=%d\n",serv->start);
			dprints("reponse=%s\n",reponse);
			/* printf("reponse=%s\n",reponse); */
			distance = serv->stop - serv->start;
			position = ((float)(atoi(reponse) - serv->start)) / (float)distance;

			serv->proprio=position;

		}

		else if (serv->type==USE_BERENSON)
		{

			serv->proprio = read_biclops(serv);
			dprints("serv->type=%d serv->name=%s proprio=%f \n",serv->type,serv->name,serv->proprio);

		}
		else
		{
			/*fprintf(stderr, "joint_get_proprio : type de joint inconnu\n");
	exit(-1);*/
		}
	}
}
