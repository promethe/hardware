/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#define DEBUG

#include <Joystick.h>
#include <net_message_debug_dist.h>

/* Joystick initialization*/
void joystick_init(Joystick *joystick)
{
   int fd;
   int i;
   unsigned char axes = 0;
   unsigned char buttons = 0;
   int version = 0;
   char name[256] = "Unknown";
   pthread_t joystick_thread;

   if (joystick == NULL)
   {
      EXIT_ON_ERROR("joystick_init: NULL pointer for joystick");
   }

   if ((fd = open(joystick->port, O_RDONLY)) < 0)
   {
      EXIT_ON_ERROR("joystick_init: Cannot open device %s", joystick->port);
   }

#ifdef Linux
   ioctl(fd, JSIOCGVERSION, &version);
   ioctl(fd, JSIOCGAXES, &axes);
   ioctl(fd, JSIOCGBUTTONS, &buttons);
   ioctl(fd, JSIOCGNAME(255), name);
#endif

   printf("joystick (%s) a  %d axes, %d boutons | version du driver :%d.%d.%d.\n",
      name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);

   if (joystick->type == -1)
   {
      dprints("joystick_init: Joystick type is auto => Detecting type\n");
      if (strcmp(name, JOYSTICK_TYPE1_NAME) == 0)
      {
	 dprints("joystick_init: Joystick is type 1 (%s)\n", JOYSTICK_TYPE1_NAME);
	 joystick->type = 1;
      }
      else if (strcmp(name, JOYSTICK_TYPE2_NAME) == 0)
      {
	 joystick->type = 2;
	 dprints("joystick_init: Joystick is type 2 (%s)\n", JOYSTICK_TYPE2_NAME);
      }
      else
      {
	 PRINT_WARNING("Joystick type unkwown (%s)", name);
      }
   }

   joystick->fd = fd;
   joystick->nb_axes = axes;
   joystick->nb_buttons = buttons;
   joystick->axes = (float *) calloc(axes, sizeof(float));
   joystick->buttons = (int *) calloc(buttons, sizeof(int));
   if ((joystick->axes == NULL) || (joystick->buttons == NULL))
   {
      EXIT_ON_ERROR("joystick_init: malloc failed for axes|buttons");
   }

   /* Initialization of axes value to -1 in order to detect if we have read a value or not (allows the specification af a default value on the user side to avoid non-zero default values for robot speed). */
   for (i = 0; i < axes; i++)
   {
      joystick->axes[i] = -1;
   }

   joystick->enabled = 1;
   
   if (pthread_create(&joystick_thread, NULL, (void *(*)(void*)) joystick_read_thread, joystick) != 0)
   {
      EXIT_ON_ERROR("joystick_init(hardware): Cannot create thread for joystick");
   }
}
