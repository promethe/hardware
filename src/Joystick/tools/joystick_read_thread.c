/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>
#include <Joystick.h>
#include <Robot.h>


void joystick_read_thread(Joystick *joystick)
{
#ifdef Linux
   Robot * robot;
   struct js_event js;
   int joystick_threshold, val_rescaled;

   dprints("joystick_read_thread: Start thread enabled = %i\n", joystick->enabled);
   
   while (joystick->enabled == 1)
   {
      if (read(joystick->fd, &js, sizeof(struct js_event)) == sizeof(struct js_event))	 
      {
	 dprints("joystick_read_thread: value_read, type = %i, number = %i\n", js.type, js.number);
	 pthread_mutex_lock(&joystick->lock);

	 switch (js.type)
	 {
            case JS_EVENT_BUTTON:
	       joystick->buttons[js.number] = js.value;
	       dprints("joystick_read_thread: button %i has value %i\n", js.number, joystick->buttons[js.number]);
	       if (joystick->buttons[6] != 0 && joystick->use_emergency == 1 )
	       {
		  printf("***ARRET D'URGENCE***\n");
		  
		  robot = robot_get_first_robot();
		  robot_set_velocity(robot, 0, 0, 0, 0);
		  robot_go_by_speed(robot, 0, 0);
		  exit(1);
	       }
	       break;

            case JS_EVENT_AXIS:
	       joystick_threshold = joystick->sensitivity * JOYSTICK_MAX_VALUE;

	       if (abs(js.value) < joystick_threshold)
	       {
		  joystick->axes[js.number] = 0.5;
	       }
	       else
	       {
		  if (js.value < 0)
		  {
		     val_rescaled = js.value + joystick_threshold;
		  }
		  else
		  {
		     val_rescaled = js.value - joystick_threshold;
		  }

		  joystick->axes[js.number] = 0.5 * val_rescaled / (JOYSTICK_MAX_VALUE - joystick_threshold) + 0.5;
	       }
	       dprints("joystick_read_thread: axis %i has value %f\n", js.number, joystick->axes[js.number]);
	       break;
	 }
	 pthread_mutex_unlock(&joystick->lock);
      }
   }
#else
   printf("ERROR, Non Linux OS cannot us joystick functions %s ",__FUNCTION__);
#endif
}
