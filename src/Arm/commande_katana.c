/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/** ***********************************************************
\file  commande_katana.c 
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
 Dialog function with the katana robot arm
 *  according to the  K-net protocol (the Koala
 *  robot communication protocol).
 *  send to the serial port a command always starting
 *  with 'T', indicating communication with
 *  the turret
 *   - <b>cmd</b> is the letter defining the comand
 *   - <b>param</b> is the list of integer used to command
 *     the arm (see Katana user manual for details)
 *   - <b>data</b> is a string with the  answer of the turret
 
Macro:
-DATA_MAX
-COM_B
-COM_C
-COM_D
-COM_E
-COM_K

Local variables:
-none

Global variables:
-none	

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Serial/serial_write()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include "tools/include/arm_local_var.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Robot.h>

void commande_katana(char cmd, int *param, char *data)
{


    static char retour[DATA_MAX];   /*< command return */
    static char command[DATA_MAX];  /*< Buffer  */
    switch (cmd)
    {
    case COM_B:
        sprintf(command, "T,24,%c\n", cmd);
        break;
    case COM_C:
        sprintf(command, "T,24,%c,%d,%d,%d,%d\n", cmd, param[0], param[1],
                param[2], param[3]);
        break;
    case COM_D:
        sprintf(command, "T,24,%c,%d\n", cmd, param[0]);
        break;
    case COM_E:
        sprintf(command, "T,24,%c,%d\n", cmd, param[0]);
        break;
    case COM_K:
        sprintf(command, "T,24,%c,%d,%d,%d,%d,%d,%d,%d,%d,0,0\n", cmd,
                param[0], param[1], param[2], param[3], param[4], param[5],
                param[6], param[7]);
        break;
    default:
        printf("wrong turret indentifier: %c \n try 'T' \n", cmd);
    }

    printf
        ("La commande_katana n'a pas ete envoye car le protocole est celui de l'ancienne libhardware\n");
    exit(0);
/*
 serial_write(command, 't', retour);
*/


    sscanf(&(retour[2]), "%s", data);

}
