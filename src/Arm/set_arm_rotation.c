/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\file
\brief 

Author: xxxxxxxx
Created: XX/XX/XXXX
Modified:
- author: C.Giovannangeli
- description: specific file creation
- date: 11/08/2004

Theoritical description:
 - \f$  LaTeX equation: none \f$  

Description: 
   Set the arm position in function of the five teta values
 * according a given reseted  position of the arm
 * teta valus are in [0.0,1.0]
 *
 * - motor 1: shoulder, rotation         (teta0)
 * - motor 2: shoulder, lift             (teta1)
 * - motor 3: elbow, lift                (teta2)
 * - motor 4: forearm, rotation          (teta3)
 * - motor 5: gripper                    (teta4)
 *
 * Sendind -1 to a motor does not produce any moves.
 
Macro:
-MAx_DOF_KATANA_ARM
-Free
-DATA_MAX 

Local variables:
-none

Global variables:
-int logical_DOF[MAX_DOF_KATANA_ARM]	

Internal Tools:
-none

External Tools: 
-tools/IO_Robot/Arm/posHigh()
-tools/IO_Robot/Arm/posLow()
-tools/IO_Robot/Arm/commande_katana()

Links:
- type: algo / biological / neural
- description: none/ XXX
- input expected group: none/xxx
- where are the data?: none/xxx

Comments:

Known bugs: none (yet!)

Todo:see author for testing and commenting the function

http://www.doxygen.org
************************************************************/
#include "tools/include/arm_local_var.h"
#include <Arm.h>

void set_arm_rotation(float teta0, float teta1, float teta2, float teta3,
                      float teta4)
{

    static float teta[MAX_DOF_KATANA_ARM];
    static int param[4], cpt;
    static char data[DATA_MAX];



    teta[0] = teta0;
    teta[1] = teta1;
    teta[2] = teta2;
    teta[3] = teta3;
    teta[4] = teta4;

    for (cpt = 0; cpt < MAX_DOF_KATANA_ARM; cpt++)
    {
        if ((logical_DOF[cpt] == Free) && (teta[cpt] >= 0.0)
            && (teta[cpt] <= 1.0))
        {
            param[0] = cpt + 1;
            param[1] = 24;
            param[2] = posHigh(cpt, teta[cpt]);
            param[3] = posLow(cpt, teta[cpt]);


            commande_katana('C', param, data);
        }
    }
}
