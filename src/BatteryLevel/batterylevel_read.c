/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include <BatteryLevel.h>
#include <Serial.h>
#include <World.h>
#include <Robot.h>

#include <ClientRobubox.h>

/*#define DEBUG*/


/* fonction de lecture des valeurs du niveau de tension de la batterie */
//void batterylevel_read(BatteryLevel * batterylevel, int * batterylevel_value)
//{
//
//   if (batterylevel->batterylevel_type == USE_ATMEGA_V1)
//   {
//      char reponse[400];
//      int ret = 0;
//      int length;
//      //int red,green,blue,yellow,all;
//      int level;
//      char buff[255]; /* Commande 'B' envoyees a l'ATMEGA pour demander la valeur du niveau de tension de la batterie */
//
//#ifdef DEBUG
//      printf("batterylevel_query: starting \n");
//#endif
//
//      memset(reponse, 0, 400 * sizeof(char));
//      memset(buff, 0, 255 * sizeof(char));
//      sprintf(buff,"B");
//
//      do
//      {
//	 serial_send_and_receive(batterylevel->port, buff, reponse, 1);
//	 length = strlen(reponse);
//	 if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
//	 {
//	    reponse[length-1] = '\0';
//	 }
//	 if (reponse[0] == '-')
//	 {
//	    if(sscanf(reponse, "-l%d ", &level)!=1)
//	    {
//	       ret=0;
//	    }
//	    else ret=1;
//
//	 }
//	 else ret=0;
//
//#ifdef DEBUG
//	 printf("level:%d\n", level);
//#endif
//
//	 if (ret != 1)
//	    printf("WARNING: the value read by batterylevel_read (%s) do not have the required format. Retrying to read ... \n", reponse);
//      }
//      while (ret != 1);
//
//      batterylevel_value[0] = level;
//
//   }
//   else if (batterylevel->batterylevel_type == USE_ATMEGA_V2)
//   {
//      char reponse[400];
//      int ret = 0;
//      int length;
//      int level;
//      char buff[255]; /* Commande 'B' envoyee a l'ATMEGA pour demander la valeur du niveau de tension de la batterie */
//
//#ifdef DEBUG
//      printf("batterylevel_query: starting \n");
//#endif
//
//      memset(reponse, 0, 400 * sizeof(char));
//      memset(buff, 0, 255 * sizeof(char));
//      sprintf(buff,"B");
//
//      do
//      {
//	// printf("sending color\n");
//	 //serial_send_and_receive(batterylevel->port, buff, reponse, 1);
//	 serial_send_and_receive_block(batterylevel->port, buff, reponse,1,1000000);
//	 length = strlen(reponse);
//	// printf("receive color %s\n",reponse);
//	 if (reponse[length-1] == '\n' || reponse[length-1] == '\r')
//	 {
//	    reponse[length-1] = '\0';
//	 }
//	 if (reponse[0] == '-')
//	 {
//	    if(sscanf(reponse, "-l%d", &level)!=1)
//	    {
//	       ret=0;
//
//	    }
//	    else ret=1;
//
//	 }
//	 else ret=0;
//      //   usleep(1000);
//
//#ifdef DEBUG
//	 printf("level:%d\n", level);
//#endif
//
//	 if (ret != 1)
//	    printf("WARNING: the values read by batterylevel_read (%s) do not have the required format. Retrying to read ... \n", reponse);
//      }
//      while (ret != 1);
//
//
//      batterylevel_value[0] = level;
//
//   }
//   else if (batterylevel->batterylevel_type == USE_WORLD)
//   {
//      Robot *robot = NULL;
//      World *world = NULL;
//
//      robot = robot_get_first_robot();
//
//      if (robot == NULL)
//      {
//	 fprintf(stderr, "ERROR in batterylevel_read: No robot defined\n");
//	 exit(1);
//      }
//
//      world = world_get_first_world();
//
//      if (world == NULL)
//      {
//	 fprintf(stderr, "ERROR in batterylevel_read: No world defined\n");
//	 exit(1);
//      }
//
//      robot_get_location_abs(robot);
//      world_get_floor_color(world, robot->posx_abs, robot->posy_abs, batterylevel_value);
//
///*      batterylevel_value[3] = 0;
//      batterylevel_value[4] = 0;*/
//   }
//	else if(batterylevel->batterylevel_type == USE_BATTERYLEVEL_WEBOTS)
//	{
//		char commande[256];
//		char result[256];
//
//      ClientRobubox *clientRobubox;
//
//      clientRobubox = clientRobubox_get_clientRobubox_by_name(batterylevel->clientRobubox);
//
//      sprintf(commande,"%s","BatteryLevel");
//      clientRobubox_transmit_receive(clientRobubox, commande, result);
//
//		sscanf(result, "batterylevel %d", &batterylevel_value[0]);
//
//	/*   batterylevel_value[3] = 0;
//      batterylevel_value[4] = 0;*/
//	}
//      usleep(1000);
//
//
//}
