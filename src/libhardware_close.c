/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Camera.h>
#include "Camera/tools/include/local_var.h"
#include <Robot.h>
#include "Robot/tools/include/local_var.h"
#include <PanTilt.h>
#include "PanTilt/tools/include/local_var.h"
#include <Laser.h>
#include "Laser/tools/include/local_var.h"
#include <Joystick.h>
#include "Joystick/tools/include/local_var.h"
#include <US_Serial.h>
#include "US_serial/tools/include/local_var.h"
#include <Led_Serial.h>
#include "Led_serial/tools/include/local_var.h"
#include <Command_Serial.h>
#include "Command_serial/tools/include/local_var.h"
#include <Mouse.h>
#include "Mouse/tools/include/local_var.h"
#include <pressure.h>
#include "pressure/tools/include/local_var.h"
#include <dev.h>
#include <Compass.h>
#include "Compass/tools/include/local_var.h"
#include <Joint.h>
#include "Joint/tools/include/local_var.h"

void libhardware_close(void)
{
	int i;

	/*Arret des robots*/
	for (i = 0; i < nb_robot; i++)
	{
		robot_set_velocity(robot_table[i], 0, 0, 0, 0);
		robot_go_by_speed(robot_table[i], 0., 0.);
		robot_delete(robot_table[i]);
	}

	/* Fermeture des cameras*/
	for (i = 0; i < nb_camera; i++)
	{
		camera_stop(cam_table[i]);
	}

#ifdef FIREWIRE
	if (firewire_context != NULL)
	{
		dc1394_free(firewire_context);
		firewire_context = NULL;
	}
#endif

	/*Fermeture pan-tilt*/

	for (i = 0; i < nb_pantilt; i++)
	{
		pantilt_delete(pantilt_table[i]);
	}

	/*Fermeture Laser*/
	for (i = 0; i < nb_laser; i++)
	{
		laser_stop(laser_table[i]);
	}

	/*Fermeture Joystick*/
	for (i = 0; i < nb_joystick; i++)
	{
		joystick_delete(joystick_table[i]);
	}

	/*Fermeture US_Serial*/
	for (i = 0; i < nb_us_serial; i++)
	{
		us_serial_delete(us_serial_table[i]);
	}

	/*Fermeture Led_Serial*/
	for (i = 0; i < nb_led_serial; i++)
	{
		led_serial_delete(led_serial_table[i]);
	}
	/*Fermeture Command_Serial*/
	for (i = 0; i < nb_cmd_serial; i++)
	{
		cmd_serial_delete(cmd_serial_table[i]);
	}

	/* Fermeture ClientKatana*/
	arm_speed_init_command(); /*bras katana remis dans l'etat initialise en attente.*/
	dev_destroy_all_devices();

	/*Fermeture Mouse*/
	for (i = 0; i < nb_mouse; i++)
	{
		mouse_delete(mouse_table[i]);
	}
	/*Fermeture Pressure*/
	for (i = 0; i < nb_pressure; i++)
	{
		pressure_delete(pressure_table[i]);
	}

	/*Fermeture Compass*/
	for (i = 0; i < nb_compass; i++)
	{
		compass_delete(compass_table[i]);
	}

	/*Fermeture de joint*/
	for (i = 0; i < nb_servo; i++)
	{
		joint_delete(serv_table[i]);
	}
	/*  for (i = 0; i < device_count; i++)
	 {
	 device = devices + i;
	 if (device->proxy)
	 (*(device->fndestroy)) (device->proxy);
	 free(device->drivername);
	 }

	 printf("Disconnect from server\n");
	 if (playerc_client_disconnect(client) != 0)
	 {
	 PRINT_ERR1("%s", playerc_error_str());
	 return -1;
	 }
	 playerc_client_destroy(client);*/

}
