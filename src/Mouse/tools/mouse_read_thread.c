/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

/*#define DEBUG*/

#include <net_message_debug_dist.h>
#include <Mouse.h>
#include <Robot.h>

#define RIGHT_BUTTON 273
#define LEFT_BUTTON 272
#define MIDDLE_BUTTON 274
#define MOLETTE 8

void mouse_read_thread(Mouse *mouse)
{
#ifdef Linux
   /*Robot * robot;*/
   struct input_event ms;
   /*int mouse_threshold, val_rescaled;*/
   int boutton = 0;
   int axe;
   float tmp;

   dprints("mouse_read_thread: Start thread enabled = %i\n", mouse->enabled);

   while (mouse->enabled == 1)
   {
      if (read(mouse->fd, &ms, sizeof(struct input_event)) == sizeof(struct input_event))
      {
         dprints("mouse_read_thread: value_read, type = %i, number = %i\n", ms.type, ms.code);
         pthread_mutex_lock(&mouse->lock);

         switch (ms.type)
         {
         case 1: /*Clic souris*/
            if (ms.code == LEFT_BUTTON )
               boutton = 0;
            if (ms.code == RIGHT_BUTTON)
               boutton = 1;
            if (ms.code == MIDDLE_BUTTON)
               boutton = 2;
            mouse->buttons[boutton] = ms.value;
            dprints("%s : button %i has value %i\n", __FUNCTION__, boutton, mouse->buttons[boutton]);
            break;
         case 2 : /*Molette souris*/
            if (ms.code == MOLETTE)
            {
               axe = 0;
               tmp = mouse->axes[axe] + (ms.value * mouse->sensitivity) ;

               if (tmp > 1.0)
               {
                  tmp = 1.0;
               }
               else if (tmp < 0.0)
               {
                  tmp = 0.0;
               }
               mouse->axes[axe] = tmp;
               printf ("read thread %f \n", mouse->axes[axe]);
            }
            break;
         }
         pthread_mutex_unlock(&mouse->lock);
      }
   }
#else
   printf("ERROR, Non Linux OS cannot us mouse functions %s ",__FUNCTION__);
#endif
}
