/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>

#include <Aibo.h>
#include <Mouse.h>

#include "tools/include/local_var.h"

void mouse_create_mouse(Mouse ** mouse, char *name, char *type)
{

   FILE *fh;
   char lecture[256];
   /*char tmp[129];*/
   int  ret;
   float tmpf;
   int tmpi;

   *mouse = (Mouse *) malloc(sizeof(Mouse));
   if (*mouse == NULL)
   {
      EXIT_ON_ERROR("malloc failed for mouse");
   }

   strcpy((*mouse)->name, name);    /*nom de la souris */
   /*tmp[128] = '\0';*/

   /* Default values */
   strcpy((*mouse)->port, "/dev/input/event5");
   (*mouse)->autocheck = 0;
   /*(*mouse)->type = -1;*/
   (*mouse)->sensitivity = 0.05;
   /*(*mouse)->use_emergency = 0;*/

   /* recuperation des informations contenu dans le fichier hardware */
   fh = fopen(type, "r");
   if (fh == NULL)
   {
      EXIT_ON_ERROR("Cannot open file  %s", type);
   }

   while (!feof(fh))
   {
      ret = fscanf(fh, "%s = ", lecture);   /* lecture du premier mot de la ligne */

      str_upper(lecture);

      if ((strcmp(lecture, "PORT") == 0)) /* get the port address */
      {
         ret = fscanf(fh, "%s", (*mouse)->port);
      }
      /*else if (strcmp(lecture, "TYPE") == 0)
		{
			ret = fscanf(fh, "%s\n", tmp);

			if (ret != 1)
			{
				EXIT_ON_ERROR("Error reading type value of mouse %s", name);
			}

			if (strcmp(tmp, "auto") == 0)
			{
				(*mouse)->type = -1;
			}
			else
			{
				(*mouse)->type = atoi(tmp);
			}
		}*/
      else if (strcmp(lecture, "SENSITIVITY") == 0)
      {
         ret = fscanf(fh, "%f\n", &tmpf);

         if (ret != 1)
         {
            EXIT_ON_ERROR("Error reading sensitivity value (int) of mouse %s", name);
         }

         (*mouse)->sensitivity = tmpf;
      }

      else if (strcmp(lecture, "AUTOCHECK") == 0)
      {
         ret = fscanf(fh, "%d\n", &tmpi);

         if (ret != 1)
         {
            EXIT_ON_ERROR("Error reading autocheck value (int) of mouse %s", name);
         }

         (*mouse)->autocheck = tmpi;
      }
      /*else if(strcmp(lecture, "USE_EMERGENCY") == 0)
		{
			ret = fscanf(fh, "%d\n", &tmpi);

			if (ret != 1)
			{
				EXIT_ON_ERROR("Error reading use_emergency value (int) of joystick %s", name);
			}

			(*joystick)->use_emergency = tmpi;

		}*/
      else
         lire_ligne(fh);
   }
   fclose(fh);

   printf("mouse_create_mouse: mouse %s \n", (*mouse)->name);
   /*printf("\t\t port=%s \n\t\t type=%d \n\t\t autocheck=%d \n", (*mouse)->port, (*mouse)->type, (*mouse)->autocheck);*/
   printf("\t\t port=%s\n\t\t autocheck=%d \n\t\t sensitivity=%f\n", (*mouse)->port,(*mouse)->autocheck,(*mouse)->sensitivity);
   if (pthread_mutex_init(&((*mouse)->lock), NULL) != 0)
   {
      EXIT_ON_ERROR("Can't initialize mutex : joystick->lock");
   }
}
