/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>

#define DEBUG

#include <Mouse.h>
#include <net_message_debug_dist.h>

/* Mouse initialization*/
void mouse_init(Mouse *mouse)
{
	int fd;
	int i;
	unsigned char axes = 3;
	unsigned char buttons = 4;
	int version = 0;
	char name[256] = "Unknown";
	pthread_t mouse_thread;

	if (mouse == NULL)
	{
		EXIT_ON_ERROR("mouse_init: NULL pointer for mouse");
	}

	if ((fd = open(mouse->port, O_RDONLY | O_NONBLOCK)) < 0)
	{
		EXIT_ON_ERROR("mouse_init: Cannot open device %s", mouse->port);
	}

#ifdef Linux
	ioctl(fd, EVIOCGVERSION, &version);
	/*ioctl(fd, JSIOCGAXES, &axes);
	ioctl(fd, JSIOCGBUTTONS, &buttons);*/
	ioctl(fd, EVIOCGNAME(255), name);
#endif

	printf("mouse (%s) a  %d axes, %d boutons | version du driver :%d.%d.%d.\n",
			name, axes, buttons, version >> 16, (version >> 8) & 0xff, version & 0xff);

	/*if (mouse->type == -1)
	{
		dprints("mouse_init: Mouse type is auto => Detecting type\n");
		if (strcmp(name, MOUSE_TYPE1_NAME) == 0)
		{
			dprints("mouse_init: Mouse is type 1 (%s)\n", MOUSE_TYPE1_NAME);
			mouse->type = 1;
		}
		else if (strcmp(name, MOUSE_TYPE2_NAME) == 0)
		{
			mouse->type = 2;
			dprints("mouse_init: Mouse is type 2 (%s)\n", MOUSE_TYPE2_NAME);
		}
		else
		{
			PRINT_WARNING("Mouse type unkwown (%s)", name);
		}
	}*/

	mouse->fd = fd;
	mouse->nb_axes = axes;
	mouse->nb_buttons = buttons;
	mouse->axes = (float *) calloc(axes, sizeof(float));
	mouse->buttons = (int *) calloc(buttons, sizeof(int));
	if ((mouse->axes == NULL) || (mouse->buttons == NULL))
	{
		EXIT_ON_ERROR("mouse_init: malloc failed for axes|buttons");
	}

	for (i = 0; i < axes; i++)
	{
		mouse->axes[i] = 0;
	}

	mouse->enabled = 1;

	if (pthread_create(&mouse_thread, NULL, (void *(*)(void*)) mouse_read_thread, mouse) != 0)
	{
		EXIT_ON_ERROR("mouse_init(hardware): Cannot create thread for mouse");
	}
}
