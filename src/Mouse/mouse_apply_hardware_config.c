/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tools.h>
#include <Mouse.h>

/* Parsing of mouse parameters, eg:
 Mouse simple is mouse.hwm */
void mouse_apply_hardware_config(char *argv)
{
  FILE *dfl;
  Mouse **mouse_table = NULL;

  int nb_mouse;
  int i;
  char lecture[100];
  char mouse_name[256][256];
  char mouse_type[256][256];

  nb_mouse = 0; /*! trying to open appli_name.dev */
  dfl = fopen(argv, "r");
  if (dfl == NULL)
  {
    PRINT_WARNING("Unable to open configuration file %s \n\tUsing default parameters", argv);
  }
  else
  { /*! loading parameters */
    while (!feof(dfl))
    {

      if (fscanf(dfl, "%s", lecture) == EOF) break;
      if (lecture[0] == '%') lire_ligne(dfl);
      else
      {
        if (strcmp(lecture, "Mouse") == 0)
        {
          if (fscanf(dfl, " %s is %s", mouse_name[nb_mouse], mouse_type[nb_mouse]) != 2)
          {
            EXIT_ON_ERROR("Parse error in dev file");
          }
          nb_mouse++;
        }
      }
    }
    fclose(dfl);
  }

  if (nb_mouse > 0)
  {
    kprints("%s: %d mouse(s) are defined:\n", __FUNCTION__,nb_mouse);
    for (i = 0; i < nb_mouse; i++)
    {
      kprints("\t Mouse %s is configurated by %s \n", mouse_name[i], mouse_type[i]);
    }

    mouse_table = (Mouse **) malloc(nb_mouse * sizeof(Mouse *));
    if (mouse_table == NULL)
    {
      EXIT_ON_ERROR("malloc failed for mouse_table");
    }

    for (i = 0; i < nb_mouse; i++)
    {
      mouse_table[i] = NULL;
      mouse_create_mouse(&mouse_table[i], mouse_name[i], mouse_type[i]);
      mouse_init(mouse_table[i]);
    }

    mouse_create_mouse_table(mouse_table, nb_mouse);
  }
}
