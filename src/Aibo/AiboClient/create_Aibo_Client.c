/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <pthread.h>
#include <tools.h>
#include <Aibo.h>
#include "tools/include/local_var.h"


Aibo_Client *client_aibo;

void create_Aibo_Client(char *name, char *file)
{

    FILE *fh;
    char lecture[256];
    pthread_t	tid;
    int ret;

    printf("Creation de l'objet Aibo Client\n");
    /* get parameters from the hardware file */
    printf("file name is %s, aibo name is %s\n", file, name);

    if ((fh = fopen(file, "r")) == NULL)
    {

        perror("Unable to open file...\n");
        printf("Quitting from %s at %d's line", __FUNCTION__, __LINE__);

        exit(-1);

    }

    printf("file opened \n");

    if ((client_aibo = (Aibo_Client *) malloc(sizeof(Aibo_Client))) == NULL)
    {

        perror("Aibo_Client allocation error : ");

        printf("in %s at %d \n", __FUNCTION__, __LINE__);

        exit(-1);

    }
    strcpy(client_aibo->name, name);
    memset(client_aibo->buffer, 0, 4000);
    memset(client_aibo->recvbuffer, 0, 4000);
    memset(client_aibo->binary_buffer, 0, 128000);
    memset(client_aibo->request, 0, 4000);

    printf(" Client allocated\n");

    while (!feof(fh))
    {
      ret = fscanf(fh, "%s = ", lecture);   /* Reading the first line's word */

        str_upper(lecture);

        printf("Reading...\n");

        if ((strcmp(lecture, "IP") == 0))   /* get the IP address */
        {
          ret = fscanf(fh, "%s ", client_aibo->IP);

            printf("IP in file is %s", client_aibo->IP);
        }
        else if (strcmp(lecture, "PORT") == 0)  /* get the port number  */
        {
          ret = fscanf(fh, " %s ", client_aibo->port);
            printf("port in file is %s", client_aibo->port);
        }
        else
            lire_ligne(fh);

    }
    fclose(fh);

    (void )ret;

    printf("IP= %s port=%s\n",client_aibo->IP,client_aibo->port);    
    client_aibo->socket = connect_Aibo_Client(client_aibo->IP, client_aibo->port);
    if (pthread_create(&tid, NULL, aibo_recv_tagged_message, (void *)&client_aibo->socket) != 0)
      {
	fprintf(stderr, "connect_Aibo_Client : Erreur de creation de la thread de reception des messages URBI-Aibo\n");
	exit(2);
      }
}
