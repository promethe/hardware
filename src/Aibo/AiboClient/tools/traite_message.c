/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include<Aibo.h>

/*#define DEBUG 1 */
char dev_name[50];
int traite_Reponse(char *tag, char *reponse)
{
    char *info = NULL;
    int i = 0;
    Aibo_Joint *joint = NULL;
    Aibo_Sensor *sensor = NULL;

#ifdef DEBUG
    printf("debut traite_Reponse tag=%s, reponse=%s\n", tag, reponse);
#endif

    if (tag[0] == '\0')
    {
        printf
            ("Erreur dans appel hardware:Aibo:traite_Reponse tag vide. reponse = %s \n",
             reponse);
        exit(1);
    }

    if (strncmp(tag, "power", 4) == 0)
    {                           /*Message d'info sur la batterie */
        i = 1;
        while (reponse[i] != '%')
        {
            info[i] = reponse[i];
            i++;
        }

        info[i] = 0;
        printf("power info received--> 0.%s %s \n", info, "%");
    }
    else
    {/** Message concernant les device(joint ou sensors)***/
#ifdef DEBUG
        printf("Message concernant les device(joint ou sensors)\n");
#endif
        info = strstr(tag, "_");
        i = 1;
        while (info[i] != '\0')
        {
            dev_name[i - 1] = info[i];
            i++;
        }
        dev_name[i - 1] = 0;
#ifdef DEBUG
        printf("traite_Reponse dev_name=%s\n", dev_name);
#endif
      /**on se place sur la device concernee  ********/
        joint = get_Aibo_Joint_by_name(dev_name);
        if (joint == NULL)
        {
            sensor = get_Aibo_Sensor_by_name(dev_name);
            if (sensor == NULL)
            {
#ifdef DEBUG
                printf("No joint or sensor named %s trashimg message  \n",
                       dev_name);
#endif
                return -1;
            }
        }

        if (strncmp(tag, "val_", 4) == 0)
        {
    /**Pour le moment aucune autre device que camera ou micro ne renvoi des messages binaire par l'intermediaire
	   du champ val, donc pas de test*****/

            printf
                ("PB c'est un message binaire non traite (traite_message.c)....\n");
            exit(1);

            /*    return traite_reponse_binaire(reponse,liste_device);
             */
        }
        else if (strncmp(tag, "valn_", 5) == 0)
        {
            if (joint != NULL)
            {
#ifdef DEBUG
                printf("joint tag=%s reponse=%s \n", tag, reponse);
#endif
                joint->valn =
                    (strtof(reponse, NULL) > 0. ? strtof(reponse, NULL) : 0.);
                if (joint->next == NULL)
                {
                    joint = joint->first;
                    joint->updated = 1;
#ifdef DEBUG
                    printf("all joints updated!!!\n");
#endif
                    return 1;
                }
            }
            else
            {
#ifdef DEBUG
                printf("sensor tag=%s reponse=%s \n", tag, reponse);
#endif
                sensor->valn =
                    (strtof(reponse, NULL) > 0. ? strtof(reponse, NULL) : 0.);
                /* printf("device %s updated\n",liste_device->name); */
                if (sensor->next == NULL)
                {
                    sensor = sensor->first;
                    sensor->updated = 1;
#ifdef DEBUG
                    printf("update==1 all sensors updated!!!\n");
#endif
                    return 1;
                }
            }
        }
        else
        {
            printf("Error no valid command recognized\n");
            return 0;
        }
    }
    return 1;
}


/*****Pour le micro ou la camera*************/
int traite_Reponse_Binaire(char *reponse)
{
    char type[64];
    int p1, p2, p3, p4, p5;     /* Parametres du flot binaire, image-son, format etc...* */
    int count = 0;
    Aibo_Cam *cam;
    Aibo_Micro *micro;
    Aibo_Client *client;

    cam = get_Aibo_Cam();
    micro = get_Aibo_Micro();
    client = get_Aibo_Client();

    if (client == NULL)
    {
        printf(" No client defined or found .....Quitting from %s,at %d  \n",
               __FUNCTION__, __LINE__);
    }

    memset(type, 0, 64);
  /**** Reading the header of binary data......**********/
#ifdef DEBUG
    printf("reponse= %s\n", reponse);
#endif
    count =
        sscanf(reponse, "BIN %d %63s %d %d %d %d", &p1, type, &p2, &p3, &p4,
               &p5);
#ifdef DEBUG
    fprintf(stderr, "et count=%d -> p1= %d type=%s p2=%d p3=%d p4=%d p5=%d\n",
            count, p1, type, p2, p3, p4, p5);
#endif

/*	printf("recvd binary buffer==  %s  \n",client->binary_buffer);*/
#ifdef DEBUG
    printf("STRLEN binary buffer is %d  \n", strlen(client->binary_buffer));
#endif
  /****Reception d'une image************/

    if (!strcmp(type, "jpeg") || !strcmp(type, "YCbCr"))
    {
#ifdef DEBUG
        printf("Camera type=%s, size=%d\n", type, p1);
#endif
        if (type[0] == 'j')
            cam->format = 1;
        else
            cam->format = 0;
#ifdef DEBUG
        printf("format = %d \n", cam->format);
#endif
        cam->size = p1;
        if ((cam = get_Aibo_Cam()) != NULL)
        {
      /*****Si premiere capture********/
            if ((cam->image = realloc(cam->image, p1)) == NULL)
            {
                get_Aibo_Cam()->updated = 1;
                perror("Allocation error in ");
                printf("%s at   \n", __FUNCTION__);
                return -1;
            }
            else
            {
                memset(cam->mode, 0, 20);
                get_Aibo_Cam()->updated = 1;
                cam->sx = p2;
                cam->sy = p3;
                strncpy(cam->mode, type, strlen(type));
                memcpy(cam->image, client->binary_buffer, p1);
#ifdef DEBUG
                printf("fin traitement message binaire camera\n");
#endif
                return 1;
            }
        }
        else
        {
#ifdef DEBUG
            printf("Error in %s at  %d\n", __FUNCTION__, __LINE__);
            printf("No Aibo Cam defined or found....trashing binary data \n");
#endif
            return -1;
        }
    }

  /****Reception d'une trame sonore******/
    if (strcmp(type, "raw") == 0)
    {
#ifdef DEBUG
        printf("Sound bin acquisition....\n");
#endif
        if ((micro = get_Aibo_Micro()) == NULL)
        {           /*****Si premiere capture********/
#ifdef DEBUG
            printf("Error in %s at  %d\n", __FUNCTION__, __LINE__);
            printf
                ("No Aibo Micro defined or found check dev file ....trashing binary data \n");
#endif
            return -1;

        }
        else
        {
            memcpy(micro->trame, client->binary_buffer, p1);
            micro->updated = 1;
            printf("fin traitement message binaire micro\n");
            return 1;
        }
    }
    printf("donnees binaires non reconnues (type=%s)\n", type);
    return -1;
}
