/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include<Aibo.h>

/*#define DEBUG*/

/********reconnaissance du tag************/
char token[10];
int get_Tag(char *mes, char *tag)
{
    char *info = NULL;
    /*char * name;
       char dev_name[50]; */

    int i, n = 1;
    int longueur;
#ifdef DEBUG
    printf("get_Tag mes=%s\n", mes);
#endif

    strcpy(token, ":");
    if ((info = strstr(mes, token)) == NULL)
    {
        printf("Hardware:Aibo:get_Tag\n");
        printf("le token %s n'a pas ete trouve dans le mes %s, tag = %s\n",
               token, mes, tag);
        return -1;
    }

#ifdef DEBUG
    printf("apres : il reste info = %s\n", info);
#endif
    longueur = strlen(info);
    i = 1;

  /****Supression des espaces****/
    while (info[i] == ' ' && i < longueur)
    {
        n++;                    /* decalage */
        i++;                    /*  nbre de blancs +1;  */
    }

    if (i >= longueur)
        return -1;
    /*  Recherche de la fin du tag* */
    while (info[i] != ']' && i < longueur)
    {
        i++;
        if (i > strlen(mes))
            return -1;          /*pg erreur info a la place de mes? */
    }

    if (i >= longueur)
        return -1;
    /* recopie du tag */
    strncpy(tag, &info[n], i - 1);
    tag[i - 1] = '\0';

#ifdef DEBUG
    printf("tag trouve = %s \n", tag);
    printf("i=%d, n=%d \n", i, n);
#endif

    return i;                   /*PG: introduction ici la suite ne sert plus??? */


}
