/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Aibo.h>
#include <Joint.h>
#include <Camera.h>
#include <Sensor.h>
#include <tools.h>

#define	LA_VIE	42

void *aibo_recv_tagged_message(void *ptr)
{
   FILE *fsock = NULL;
   float timeup = 0;
   char tag[64];
   char buffer[128000]; /* Valeur arbitraire : voir si cest suffisant dans TOUS les cas (video, micro etc ...) */
   int nb_elem = 0;
   int sock = *((int *) ptr);
   char *ret = NULL;

   /* Gestion des signaux pour promethe */
   gestion_mask_signaux_hardware();

   /* On ouvre la socket en flux ... c'est plus simple pour le parsing :P */
   if ((fsock = fdopen(sock, "r")) == NULL)
   {
      fprintf(stderr, "aibo_recv_tagged_message : erreur d'ouverture de la socket en stream\n");
      exit(1);
   }

   while (LA_VIE)
   {
      memset(buffer, 0, 128000);
      memset(tag, 0, 64);
      /* On lit l'en tete d un ligne */
      if ((nb_elem = fscanf(fsock, "[%f:%s] *** %s << ", &timeup, tag, buffer)) < 2)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         fprintf(stderr, "aibo_recv_tagged_message : Erreur de reception d'un message\n\t\tmsg = %s", buffer);
         continue;
      }
      tag[strlen(tag) - 1] = 0; /* On retire le ] qui est lu dans tag */
#ifdef DEBUG
      printf("uptime : %f - %s = %s\n", timeup, tag, buffer);
#endif
      /* On recois une valeur */
      if (strncmp(tag, "walk", 4) == 0)
      {
         int ret = -1;

         ret = fgets(buffer, 128000 - 1, fsock);
         ret = atoi(buffer);
         if (ret != 0) fprintf(stderr, "PROMETHE NAO : walk error (code = %i)\n", ret);
      }
      else if (strncmp(tag, "val_", 4) == 0)
      {
         /* La valeur vien d'une articulation : proprioception */
         if (strncmp(tag + 4, "joint_", 6) == 0)
         {
            /* + 4 : val_ */
            /* + 6 : joint_ */
            Joint *serv = joint_get_serv_by_name(tag + 4 + 6);
            int proprio = 0;
            int i = 0;

            /* valeur recu d'Aibo */
            ret = fgets(buffer, 128000 - 1, fsock);
            while ((buffer[i] != '.') && (buffer[i] != '\0') && (buffer[i] != '\n'))
               i++;
            buffer[i] = '\0';
            proprio = atoi(buffer);

            /* Normalisation entre 0 et 1 */
            serv->proprio = ((float) (proprio - serv->start) / (serv->stop - serv->start));

            /* On signale que la valeur a ete mise a jour */
            serv->updated = 1;
            sem_post(&(serv->sem));
         }
         /* La valeur vien d'un capteur */
         else if (strncmp(tag + 4, "sensor_", 7) == 0)
         {
            /* + 4 : val_ */
            /* + 7 : sensor_ */
            Sensor *sens = sensor_get_sens_by_name(tag + 4 + 7);
            int val = 0;
            int i = 0;

            /* valeur recu d'Aibo */
            ret = fgets(buffer, 128000 - 1, fsock);
            while ((buffer[i] != '.') && (buffer[i] != '\0') && (buffer[i] != '\n'))
               i++;
            buffer[i] = '\0';
            val = atoi(buffer);

            /* Normalisation entre 0 et 1 */
            sens->valn = ((float) (val - sens->val_start) / (sens->val_stop - sens->val_start));

            /* On signale que la valeur a ete mise a jour */
            sens->updated = 1;
            sem_post(&(sens->sem));
         }
         /* La valeur vien de la camera (c'est une image) */
         else if (strncmp(tag + 4, "camera_", 7) == 0)
         {
            int size;
            char type[63];
            int sx, sy;
            int pos = 0;
            int nb = 0;
            /* + 4 : val_ */
            /* + 7 : camera_ */
            Camera *cam = camera_get_cam_by_name(tag + 4 + 7);

            /* On recois les parametres de l'image */
            if (fscanf(fsock, " BIN %d %63s %d %d\n", &size, type, &sx, &sy) < 4)
            {
               fprintf(stderr, "aibo_recv_tagged_message : erreur de reception des parametre de l'image a recevoir.\n");
               exit(-1);
            }
            if (cam->grab_buf == NULL)
            {
               /* On alloue la taille de l'image a recevoir */
               if ((cam->grab_buf = malloc(128000 * sizeof(unsigned char))) == NULL)
               {
                  fprintf(stderr, "aibo_recv_tagged_message : erreur d'allocation du buffer pour la reception d'une image.\n");
                  exit(-1);
               }
            }
            /* On recois l'image */
            memset(((char *) cam->grab_buf), 0, 128000 * sizeof(unsigned char));
            while (nb < size)
            {
               if ((pos = fread(((char *) cam->grab_buf) + nb, 1, size - nb, fsock)) < 0)
               {
                  fprintf(stderr, "aibo_recv_tagged_message : erreur de reception d'une image de la camera.\n");
                  exit(-1);
               }
               nb += pos;
            }
            /* On remplie la structure des parametres de l'image : resolution, taille */
            cam->width = sx;
            cam->height = sy;
            cam->size = size;
            cam->updated = 1;
            sem_post(&(cam->sem));
         }
         /* La valeur vien d'un micro (c'est un son) */
         else if (strncmp(tag + 4, "micro_", 6) == 0)
         {

         }
      }
      /* On recoi le retour de la marche */
      else if (strcmp(tag, "walk") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE NAO : Walk : %s\n", buffer);
      }
      /* On recoi l'identifiant */
      else if (strcmp(tag, "ident") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE AIBO : ident : %s\n", buffer);
      }
      /* On recoi un message d'initialisation */
      else if (strcmp(tag, "start") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE AIBO : start : %s\n", buffer);
      }
      /* On recoi un message de l'alimentation (battery) */
      else if (strcmp(tag, "power") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE AIBO : power : %s\n", buffer);
      }
      /* On recoi un message sans tag (origine non precise) */
      else if (strcmp(tag, "notag") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE AIBO : notag : %s\n", buffer);
      }
      /* On recoi un message d'erreur */
      else if (strcmp(tag, "error") == 0)
      {
         ret = fgets(buffer, 128000 - 1, fsock);
         printf("PROMETHE AIBO : error : %s\n", buffer);
      }
      else /* Sinon tag inconnu */
      {
         fprintf(stderr, "aibo_recv_tagged_message : tag %s inconnu\n", tag);
         ret = fgets(buffer, 128000 - 1, fsock);
      }
#ifdef DEBUG
      printf("PROMETHE AIBO : %s\n", buffer);
#endif
   }
   ret = NULL;
   free(ret);
   pthread_exit(NULL);
}
