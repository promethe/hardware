/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <Aibo.h>
#include <tools.h>
#include "tools/include/local_var.h"


int  connect_Aibo_Client(char *ip, char *port)
{

  int rc=-1;
  struct hostent *hen;		/* host-to-IP translation*/
  struct sockaddr_in sa;	/* Internet address struct*/
  int num_port=-1;
  int sock;

  num_port = atoi(port);
  printf("%s  \n",__FUNCTION__);
  printf("IP= %s port=%s\n",ip,port);
		
  memset(&sa, 0, sizeof(sa));
  sa.sin_family = AF_INET;
  sa.sin_port = htons(num_port);
  hen=gethostbyname(ip);
  if (!hen) 
    {	
      sa.sin_addr.s_addr = inet_addr(ip);
      if (sa.sin_addr.s_addr == INADDR_NONE) 
	{
	  printf("Problem resolving host  : %s\n",ip);
	  exit(-1);
	}
    }
  else    memcpy(&sa.sin_addr.s_addr, hen->h_addr_list[0], hen->h_length);
			
  /*Creation de la socket****/
  sock=socket(AF_INET, SOCK_STREAM, 0);
		
  /*  tentative de connectin au serveur***/
  rc=connect(sock, (struct sockaddr *) &sa, sizeof(sa));

  while(rc)
    { /***si echec attente 20ms***/
      usleep(200000);
      rc = connect(sock, (struct sockaddr *) &sa, sizeof(sa));
      perror("AIBOConnection error to ");
      printf(" IP : %s port: %s  \n waiting 2seconds before trying to reconnect",ip,port);
    }
  printf("Connection Granted to  %s : %s  \n Waiting for reply from server....\n",ip,port);
  /**attente de l'en-tete URBI***/
  
  usleep(100000);

  /**lecture et affichage de l'en-tete URBI***/
/*   while (pos==0)     pos = read(sock, reception, 1024); */

  return sock;         	
}


