/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/** ***********************************************************
 \file  f_recv_message.c
 \brief recoit et traite les messages

 Author: A.HIOLLE reecriture et correction de bugs P. Gaussier
 Created: 19/04/2005 modifie sept 205

 Comments:
 sur le Aibo urbi.ini penser a enlever le monitoring automatique de la batterie

 */


#include <stdlib.h>
#include <string.h>
#include <Aibo.h>
#include "tools/include/local_var.h"

/*#define DEBUG*/

/* cette fonction est appelee par f_update_aibo() */

#define message_size 256
#define reponse_size 512
#define tag_size     256
#define buffer_size 4000
#define URBI_BUFLEN  128000

/* cherche la fin de la ligne de texte ascii */
/* length correspond a la taille du buffer recu */
int end_of_line_position(char *recv_buffer, int length)
{
   int j;

   /* printf("recherche end_of_line %s \n",recv_buffer); */
   j = 0;
   while ((recv_buffer[j] != '\n') && (j < length))
   {
      if (recv_buffer[j] == '\0' && (j < length))
      {
#ifdef DEBUG
         printf("no_end j=%d \n", j);
#endif
         return (-1);
      }
      j++;
   }

   if (j >= length) return -1;
   return j;
}

/* attend une ligne se terminant par un retour a la ligne */
/* nbre_char_lu contient la taille de recv_buffer */
int recupere_ligne_complete(Aibo_Client * client, char *recv_buffer, int *nbre_char_lu)
{
   int rep;
   int cpt_echec = 0;
   int j = 0;
#ifdef DEBUG
   printf("On recupere une ligne complete\n");
#endif
   while (cpt_echec < 4)
   {
#ifdef DEBUG
      printf("A j=%d nbre_char_lu=%d\n", j, (*nbre_char_lu));
      printf("recv_buffer=%s\n", recv_buffer);
#endif

      j = end_of_line_position(recv_buffer, *nbre_char_lu);
      if (j > 0) return j;
#ifdef DEBUG
      printf("Ligne lue incomplete, on lit la suite ...\n");
#endif

      /* on doit au moins avoir la fin de la ligne de texte */

      rep = read(client->socket, &recv_buffer[(*nbre_char_lu)],
      buffer_size - (*nbre_char_lu) /* 2048 */);
      if (rep == -1)
      {
         perror(" Reading error in recupere_ligne_complete\n");
         exit(-1);
      }
      (*nbre_char_lu) = (*nbre_char_lu) + rep;
      /*recv_buffer[(* nbre_char_lu)]='\0';
       printf("X le nouveau buffer est :\n%s ;;;\n",recv_buffer); */

      cpt_echec++;
#ifdef DEBUG
      printf("nbre d'echecs = %d \n", cpt_echec);
#endif
   }
   printf("Echec leacture socket\n aibo_recv_message2.c\n");
   return -1;
}

/* renvoie le nombre de caracteres restant a traiter dans le buffer recv_buffer*/
/* Ces informations restant a traiter sont recopiees au debut de recv_buffer */
/* pos_fin_ligne contient la position du \n  de la premiere ligne de l'entete du message recus (caracteres) */
int remplit_buffer_binaire(Aibo_Client * client, char *reponse, char *recv_buffer, int pos_fin_ligne, int length_buffer)
{
   int length_data; /*taille des donnees a recevoir */
   int cpt_bin;
   int rep;
#ifdef DEBUG
   printf("\n\n remplit_buffer_binaire \n");
#endif
   pos_fin_ligne = pos_fin_ligne + 1;
   cpt_bin = length_buffer - pos_fin_ligne;
   memcpy(client->binary_buffer, &recv_buffer[pos_fin_ligne], cpt_bin);

   sscanf(reponse, "BIN %d ", &length_data);
#ifdef DEBUG
   printf("length=%d cpt= %d\n", length_data, cpt_bin);
#endif
   while (cpt_bin < length_data)
   {
      /*printf("attente de la fin du message binaire, cpt= %d\n",cpt_bin); */
      rep = read(client->socket, &(client->binary_buffer[cpt_bin]), length_data - cpt_bin);
      if (rep == -1)
      {
         perror(" Reading error in remplit_buffer_binaire\n");
         exit(-1);
      }
      cpt_bin += rep;
   }

   traite_Reponse_Binaire(reponse);

   if (cpt_bin > length_data) /* pb si c'est dans rev_buffer et pas client>binary_buffer... */
   {
#ifdef DEBUG
      printf("SUPPLEMENT=\n%s\n", &(client->binary_buffer[length_data]));
#endif
      memcpy(recv_buffer, &(client->binary_buffer[length_data]), cpt_bin - length_data);
#ifdef DEBUG
      printf("SUPPLEMENT garde=\n%s...\n", recv_buffer);
#endif
      return cpt_bin - length_data; /* nbre de caracteres restant a traiter dans recv_buffer */
   }

   return 0; /* pas de caracteres restant a traiter dans le recv_buffer */
}

/* Le message a traiter doit au moins correspondre a une ligne complete. */
/* S'il reste du texte a traite il a ete recopie au debut de recv_buffer et nbre_char_lu est sa taille*/
/* la fonction renvoie  0 si echec 1 si succes */

char reponse[reponse_size]; /****reponse extraite du message****/
int traite_message(Aibo_Client * client, char *tag, char *message, char *recv_buffer, int pos_fin_ligne, int *nbre_char_lu)
{

   int succes;

   if (tag[0] == '\0')
   {
      printf("Erreur dans hardware:Aibo:aibo_recv_message2 trait_message tag vide. message = %s \n", message);
      printf("recv_buffer = %s \n", recv_buffer);
      printf("\n");
      exit(1);
   }

#ifdef DEBUG
   printf("1 gottag : %s\n", tag);
#endif
   if (strncmp(tag, "power", 5) == 0)
   {
#ifdef DEBUG
      printf("Info batteries..........\n");
#endif
      *nbre_char_lu = *nbre_char_lu - pos_fin_ligne - 1;
      memcpy(recv_buffer, &recv_buffer[pos_fin_ligne + 1], *nbre_char_lu);
#ifdef DEBUG
      printf("a la fin de traite_message recv_buffer=%s\n", recv_buffer);
      printf("nbre_char_lu = %d \n", *nbre_char_lu);
#endif
      return 1;
   }
#ifdef DEBUG
   printf("2 gottag : %s\n", tag);
#endif
   succes = get_Reponse(message, reponse);
#ifdef DEBUG
   printf("3 gottag : %s\n", tag);
#endif
   if (succes == 0)
   {
      printf("Echec dans le traitement de la reponse\n");
      return 0;
   }
#ifdef DEBUG
   printf("gotreponse : %s \n", reponse);
#endif

   if (strncmp(reponse, "BIN", 3) == 0)
   {
      (*nbre_char_lu) = remplit_buffer_binaire(client, reponse, recv_buffer, pos_fin_ligne, *nbre_char_lu);
      /* le texte en trop a ete recopie au debut de recv_buffer et correspond a nbre_charlu */
      /* le buffer binaire de client contient les donnees binaires a traiter */
      return 1;
   }
#ifdef DEBUG
   printf("4 gottag : %s\n", tag);
#endif
   if (tag[0] == '\0')
   {
      printf("Erreur dans hardware:Aibo:aibo_recv_message2 tag vide. reponse = %s \n", reponse);
      printf("\n");
      exit(1);
   }
   succes = traite_Reponse(tag, reponse);
   if (succes == 0)
   {
      printf("Echec dans le traitement de la reponse\n");
      return 0;
   }

   /* mise a jour de recv_buffer et nbre_char_lu */
   (*nbre_char_lu) = (*nbre_char_lu) - pos_fin_ligne - 1;
   memcpy(recv_buffer, &recv_buffer[pos_fin_ligne + 1], *nbre_char_lu);
#ifdef DEBUG
   printf("a la fin de traite_message recv_buffer=%s\n", recv_buffer);
#endif
   return 1;
}

/* renvoie 0 si echec. Sinon le resultat est stocke dans les differentes structures globales Aibo */
char tag[tag_size]; /***tag extrait du message****/
char my_message[message_size];/***une ligne recu ***/
/*#define DEBUG*/
int aibo_recv_message()
{
   int succes;
   int nbre_char_lu = 0;
   int j = 0;
   /***iterarteurs**/

   char *recv_buffer;
   char *buffer_binaire;

   /*  char *tag;
    char  *my_message; */

#ifdef DEBUG
   struct timeval curtime;
#endif

   int pos_fin_ligne;


   Aibo_Client *client = get_Aibo_Client();

   (void) buffer_binaire;

   /*  tag=(char*)malloc(tag_size*sizeof(char));
    my_message=(char*)malloc(message_size*sizeof(char)); */

   if (client == NULL)
   {
      printf("No Aibo_Client found...Quitting from %s at %d\n", __FUNCTION__, __LINE__);
   }

   recv_buffer = client->recvbuffer;
   buffer_binaire = client->binary_buffer;

   set_Unupdate();
#ifdef DEBUG
   printf("Aibo_recv message  \n");
   curtime = get_CurrentTime();
#endif

   nbre_char_lu = 0;
   while (!get_Update())
   {
      while (nbre_char_lu == 0)
      {
         nbre_char_lu = read(client->socket, recv_buffer, buffer_size);
         if (nbre_char_lu == -1)
         {
            perror(" Reading error in aibo_recv_message\n");
            exit(-1);
         }
         /*  printf("in recv_buffer[%d]=%s\n", nbre_char_lu ,recv_buffer); */
      }

      /* tant que l'on n'a pas recupere une ligne de texte complete */
      pos_fin_ligne = recupere_ligne_complete(client, recv_buffer, &nbre_char_lu); /* info mises a la fin de  rcv_buffer */
      if (pos_fin_ligne < 0) return 0; /*echec lecture */

      strncpy(my_message, recv_buffer, pos_fin_ligne + 1);
      my_message[pos_fin_ligne + 1] = '\0';
#ifdef DEBUG
      printf("current message : %s  \n", my_message);
#endif
      j = get_Tag(my_message, tag); /* plantage ici avec lecture joint sans debug: message vide... */
#ifdef DEBUG
      printf("B j=%d pos_fin_ligne=%d, nbre_char_lu=%d\n", j, pos_fin_ligne,
            nbre_char_lu);
#endif
      if (j > 0)
      {
         /* printf("appel traite_message tag=%s\n",tag); */
         succes = traite_message(client, tag, my_message, recv_buffer, pos_fin_ligne, &nbre_char_lu);
         /* s'il reste du texte a traite il a ete recopie au debut de recv_buffer et nbre_char_lu est sa taille */
         recv_buffer[nbre_char_lu] = '\0';
#ifdef DEBUG
         printf("texte restant a traiter=%s\n", recv_buffer);
#endif
         if (succes == 0)
         {
            printf("Echec dans le traitement de la reponse\n");
            return 0;
         }
      }
      else /* pas de tag trouve */
      {
#ifdef DEBUG
         printf("Echec pour recuperer le tag (aibo_recv_message)\n");
#endif
         if (strncmp(my_message, "***", 3) == 0)
         {
#ifdef DEBUG
            printf("Info batteries..........\n");
#endif
            nbre_char_lu = nbre_char_lu - pos_fin_ligne;
            memcpy(recv_buffer, &recv_buffer[pos_fin_ligne], nbre_char_lu);
            nbre_char_lu = nbre_char_lu - 1;
#ifdef DEBUG
            printf("batterie: nbre_char_lu=%d \n", nbre_char_lu);
#endif
         }
         else return 0;
      }
   }
#ifdef DEBUG
   printf("All devices updated\n");
   printf("sortie recv message TIME ELAPSED ... T=%ld s%ld ms  \n",
         (get_CurrentTime().tv_sec - curtime.tv_sec),
         (get_CurrentTime().tv_usec - curtime.tv_usec) / 1000);
#endif
   return 1; /* tout a ete recu */
}
