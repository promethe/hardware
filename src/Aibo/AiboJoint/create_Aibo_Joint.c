/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tools.h>
#include <Aibo.h>
#include "tools/include/local_var.h"


Aibo_Joint *joint_aibo;

void create_Aibo_Joint(char *name, char *file)
{
    /*FILE * fh; */
    Aibo_Joint *first;
    (void)file;
    printf("Creation d'un senseur\n");
    /* get parameters from the hardware file */

/*	printf("file name is %s, aibo name is %s\n",file,name);
*/
    /*if((fh=fopen(file,"r"))==NULL){

       perror("Unable to open file...\n");
       printf("Quitting from %s at %d's line",__FUNCTION__,__LINE__);
       exit(-1); 
       }

       printf("file opened \n");
     */

  /***Initialisation de la liste chainee****/
    if (joint_aibo == NULL)
    {
        if ((joint_aibo = (Aibo_Joint *) malloc(sizeof(Aibo_Joint))) == NULL)
        {
            perror("Aibo_Joint allocation error : ");
            printf("in %s at %d \n", __FUNCTION__, __LINE__);
            exit(-1);
        }
        joint_aibo->first = joint_aibo;
        joint_aibo->num = 0;
    }
    else
    {/***Si ce 'est psa le premier senseur declare**/
        joint_aibo = joint_aibo->first;
        first = joint_aibo;
        while (joint_aibo->next != NULL)
            joint_aibo = joint_aibo->next;

        if ((joint_aibo->next =
             (Aibo_Joint *) malloc(sizeof(Aibo_Joint))) == NULL)
        {
            perror("Aibo_Joint allocation error : ");
            printf("in %s at %d \n", __FUNCTION__, __LINE__);
            exit(-1);
        }
        joint_aibo->next->num = joint_aibo->num + 1;
        joint_aibo = joint_aibo->next;
        joint_aibo->first = first;
    }

    strcpy(joint_aibo->name, name);
    joint_aibo->next = NULL;
    joint_aibo->valn = 0.;
    joint_aibo->updated = 0;
    printf("Joint No %d name : %s created...\n", joint_aibo->num,
           joint_aibo->name);

}
