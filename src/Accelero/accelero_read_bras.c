/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <Accelero.h>
#include <Serial.h>
#include <ClientHW.h>
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB_DATA 12 /* trame = 255 + 255 + 1 int (tps)+ 5 int + 5 int */

void accelero_read_bras(Accelero * accelero, float * accelero_value)
{
   unsigned char reponse[400];
   int i, n, o, p, fin, N;
   Serial *ptr;

   int serialport1;

   dprints("*** accelero_read_bras reading port = %s\n", accelero->port);

   ptr = serial_get_serial_by_name(accelero->port);
   if (ptr == NULL)
   {
      kprints("%s error no serial port %s \n", __FUNCTION__, accelero->port);
      exit(1);
   }
   serialport1 = (serial_get_serial_by_name(accelero->port))->desc;
   dprints("-----accelero serial_get_serial_by_name \n");

   dprints("+++++++++++++----------\n");

   /*	c=getchar(); */

   fin = 0;
   while (fin == 0)
   {

      o = write(serialport1, "A", 1);
      if (o == -1)
      {
         perror("write serial");
         exit(42);
      }

      p = 0;

      while (p < 2)
      {
         n = read(serialport1, &reponse[p], 2);
         p = p + n; /*printf("p=%d \n",p);*/
      }

      if (reponse[0] != (unsigned char) 255 && reponse[1] != (unsigned char) 255)
      {
         kprints("\n erreur de lecture accelerometre %s \n", __FUNCTION__);
         kprints(" v1= %d , v2= %d \n", (unsigned int) reponse[0], (unsigned int) reponse[1]);
         fin = 0;
         /* exit(1); */
      }
      else fin = 1;
   }
   /*	printf("ok attente trame \n");   */
   while (p < NB_DATA * 2 + 2)
   {
      n = read(serialport1, &reponse[p], 2);
      p = p + n;
   }


   /*  printf("\n");*/
   dprints("accelero conversion \n");

   N = ((unsigned int) reponse[2 * NB_DATA]) + ((unsigned int) reponse[2 * NB_DATA + 1] << 8); /* nombre de donnees dans la somme du filtre sur l'arduino*/
   dprints("N= %d \n", N);

   for (i = 1; i < NB_DATA; i++) /* on enleve 255|255  */
   {
      accelero_value[i - 1] = ((float) ((unsigned int) reponse[2 * i]) + ((unsigned int) reponse[2 * i + 1] << 8)) / N; /* reconversion int sur 2 octets vers int + suppression du debut de trame 255 | 255*/
      /*  printf("i=%d val=%d \n",i-1,accelero_value[i-1]); */
   }
   /*   printf("accelero fin lib \n"); */
}
