/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <Accelero.h>
#include <Serial.h>
#include <ClientHW.h>
#include <unistd.h>

#include <termios.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Basé sur accelero_read.c (~/simulateur/hardware/hardware_7.0/src/Accelero/) */

void accelero3D_read(Accelero * accelero, int * accelero_value)
{
    char reponse[400];

    int n, l, m;
    char lu;
    int serialport1;
    int ret;
    char cmd='A';

#ifdef ONLINE_DEBUG
    printf("\t accelero3D reading \n");
    printf("Port accel : %s \n", accelero->port);
#endif

    if(accelero->accelero_type == ACCEL_MAYA)
    {
	float x,y,z;
	serial_send_and_receive_block(accelero->port, &cmd, reponse,1,1000000);
	ret = sscanf(reponse, "-a-%f-%f-%f-", &x,&y,&z );

        if( ret == 3 )
	{
		accelero_value[0] = (int)(x*1000);	
		accelero_value[1] = (int)(y*1000);	
		accelero_value[2] = (int)(z*1000);	
	}
	else
	{
		accelero_value[0] = 0;	
		accelero_value[1] = 0;	
		accelero_value[2] = 0;	
	}
    }	
    else
    {
        serialport1 = (serial_get_serial_by_path(accelero->port))->desc;
    	tcdrain(serialport1);
    	tcflush(serialport1, TCIOFLUSH);

    	l = m = 0;

    	do
    	{
		/* Lecture de 1 octet sur le port série de l'accel */
        	n = read(serialport1, &lu, 1);

	#ifdef DEBUG
        	printf("n=%x\n", n);
	#endif

        	if (n)
        	{
            		if (lu > 2)
            		{
                		reponse[l] = lu;
                		l++;
            		}
        	}
        	else
        	{
            		lu = '\0';
            		m++;
        	}
    	}while (lu != 10);
    
    	l = m = 0;
    	do
    	{
       		n = read(serialport1, &lu, 1);
		#ifdef DEBUG
        	printf("n=%d\n", n);
		#endif
        	if (n)
        	{
            		if (lu > 2)
            		{
                		reponse[l] = lu;
			#ifdef DEBUG
                	printf("%c -> %d\n", reponse[l], reponse[l]);
			#endif
                	l++;
            		}
        	}
        	else
        	{
            		lu = '\0';
            		m++;
        	}
    	}
    	while ((lu != 10) && (l < 100) && (m < 10000));
    	n = l;
    	reponse[n - 1] = '\0';

	#ifdef ONLINE_DEBUG
    	printf("\n reponse : %s --\n", reponse);
	#endif
     	sscanf(reponse,"-a%d,%d,%d,%d-", &(accelero_value[0]),&(accelero_value[1]),&(accelero_value[2]),&(accelero_value[3]));
     	sscanf(reponse,"-a%d,%d,%d-", &(accelero_value[0]),&(accelero_value[1]),&(accelero_value[2]));
     	sscanf(reponse,"accel_x: %d accel_y: %d accel_z: %d", &(accelero_value[0]),&(accelero_value[1]),&(accelero_value[2]));
    }

}
