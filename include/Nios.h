/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_NIOS_H
#define _HARDWARE_NIOS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdint.h> /*les types avec leur taille*/
#include <net_message_debug_dist.h>

#define THETA_MAX 16
#define RHO_MAX 16

//#define REQUEST_GS 1
//#define REQUEST_GRAD 2
//#define REQUEST_GAUSS_1 3
//#define REQUEST_GAUSS_2 4
//#define REQUEST_DOG 5
//#define REQUEST_LOGPOL 6


#define NO_VIDEO 0x0
#define VIDEO_GS 0x1
#define VIDEO_GRAD 0x2
#define VIDEO_GAUSS_0 0x3
#define VIDEO_GAUSS_1 0x4
#define VIDEO_DOG 0x5
#define SEND_KP 0x8

#define VIDEO_MASK 0x7
#define KP_MASK 0x8
typedef uint8_t alt_u8;
typedef uint16_t alt_u16;
typedef uint32_t alt_u32;

/*struct keypoint received from the board on request*/
typedef struct Keypoint_t
{
	alt_u16 x;
	alt_u16 y;
	alt_u16 v;
	alt_u16 logpol[THETA_MAX*RHO_MAX];
}keypoint_t;

/*****Structure pour 1 point caracteristique********/
typedef struct NIOS_pt_carac
{
		uint16_t *image;								/* image de la dog*/
    int size;                   /* taille de l'image binaire recuperee */
    int sx;
    int sy;
    uint32_t posx;										/* position du pt dans image d'origine */
    uint32_t posy;
    float intensity; 						/* intensité du pt carac*/
} NIOS_pt_carac;


/*structure des keypoints d'une meme frame*/
typedef struct Nios_pt_carac
{
    uint32_t dim_image; /* le numero de la frame*/
    uint32_t nb_pt;
    NIOS_pt_carac* pts_carac; /* le tableau alloue pour les pts */
    sem_t sem; /* Semaphore pour l attente de la mise a jour de l'image */
} Nios_pt_carac;


extern Nios_pt_carac *cam_nios;
/***Client de communication avec le NIOS****/
typedef struct Nios_Client
{
      /* Nom du Client au cas ou plusieur existent */
    char name[50];        
 /***Port de connection***/
    char port[10];
          
   /***Ip du NIOS***/
    char IP[50];
      
/****Socket de connection*/
    int socket;

    /****order***/   
    char request;

  /***Taille du Buffer de reception des images  ***/
	alt_u32 dim[2];

  /****Buffer de reception des images  ***/
int16_t * buffer;

 /****image en float  ***/
  float *image;
  /***Liste chainee des keypoints extraits par frame  ***/
    Nios_pt_carac * nios_pt_carac;
             

} Nios_Client;

extern void create_nios_Client(char *name, char *file);

extern int connect_nios_Client(char *ip, char *port);



extern int nios_send_message(char *buffer, int size);

extern int nios_send_message_camera(void *cam, char *buffer, int size);

extern void *nios_recv_message(void *ptr);

extern int nios_Client_send(void);

extern void close_nios_Client(void);

extern void nios_apply_hardware_config(char *argv);

extern Nios_Client *get_nios_Client(void);





extern Nios_pt_carac *get_nios_Cam(void);

extern void create_nios_Cam(Nios_pt_carac* nios_pt_carac);
extern Nios_pt_carac * nioscam_grabimages(char order);


#endif
