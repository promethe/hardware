/*************************************************************
Author: Arnaud Blanchard
Created: 9/11/2014

Structure d'un bus, voir :Components/generic_fields.h et Components/bus_fields.h
************************************************************/
#ifndef COMPONENT_DATA_H
#define COMPONENT_DATA_H

#include "basic_tools.h"

#include <Components/generic.h>

typedef struct data type_data;

struct data
{
	/** Attention a l'ordre. Every variable you want to add must be inside bus_variables.h */
	#include <Components/generic_fields.h>
	#include <Components/data_fields.h>
};

#endif
