/*
 * publish_fields.h
 *
 *  Created on: 30 juil. 2014
 *      Author: sylvain
 */

void* (*channel_create)(Publish *self, const char* name, int reliable);
int (*channel_is_connected)(Publish *self, void* channelID);
void (*channel_write)(Publish *self,void* channelID, char *buffer, int bufferSize);
void (*channel_set_negociation_message)(Publish *self, void * channelID, char * buffer, int bufferSize);
void (*channel_register_receive_callback)(Publish *self, void * channelID, type_recv_cb cb, void *userData);
void (*destroy)(Publish *self);
