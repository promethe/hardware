/*************************************************************
Author: Arnaud Blanchard
Created: 30/01/2014

Champs d'un bus en plus des champs generiques (Components/generic_fields.h).
************************************************************/


pthread_mutex_t receive_mutex;
type_buffer send_buffer, receive_buffer;


void (*get_values)(type_data *data, float *values, int values_nb);
void (*set_values)(type_data *data, const float *values, int values_nb);
