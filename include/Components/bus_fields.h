/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
Author: Arnaud Blanchard
Created: 9/11/2009

Champs d'un bus en plus des champs generiques (Components/generic_fields.h).
************************************************************/

/* Ne pas mettre de ifndef car ca doit etre reinclu a chaque fois dans la structure du composant et des drivers */
const char *initialization, *finalization;
char *channel_id;
type_buffer send_buffer, receive_buffer;

int connected, channels_nb;

int (*there_is_data_to_receive)(type_bus *bus);
void (*bufferize)(type_bus *bus, const char *command, int length);
void (*bufferizef)(type_bus *bus, const char *format, ...);
int (*send_command)(type_bus *bus, const char *command, int length);
int (*send_command_block)(type_bus *bus, const char *command, int length);
int (*send_command_on_channel)(type_bus *bus, const char *data, int length, long unsigned int channel_id);

int (*sendf_command)(type_bus *bus, const char *format, ...);
void (*flush)(type_bus *bus);
int (*receive)(type_bus *bus, char *buffer, int length);
int (*receive_on_channel)(type_bus *bus, char *buffer, int length, long unsigned int channel_id);
int (*receive_block)(type_bus *bus, char *buffer, int length);
int (*send_and_receive)(type_bus *bus, const char *command, int lengthC, char *reply, int lengthR);
void (*lock)(type_bus *bus);
void (*unlock)(type_bus *bus);
char *(*check_recv_header)(type_bus *bus, char *buffer);

void (*get_values)(type_bus *bus, float *values);
void (*set_values)(type_bus *bus, const float *values);

void (*add_channel)(type_bus *bus, char *name, type_receive_callback* receive_callback, void *user_data);

type_channel_callback *channels;


