/*
 * publish.h
 *
 *  Created on: 30 juil. 2014
 *      Author: sylvain
 */

#ifndef PUBLISH_H_
#define PUBLISH_H_

#include <Components/generic.h>

typedef void (*type_recv_cb)(float* buffer, int bufferSize, void * userData);

typedef struct publish Publish;
struct publish{
	/** Attention a l'ordre. Ne pas rajouter de champs ici mais dans net_sensor_fields.h */
	#include <Components/generic_fields.h>
	#include <Components/publish_fields.h>
};


#endif /* PUBLISH_H_ */
