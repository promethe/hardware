/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _TOOLS_IOb_ROBOT_ALL_H
#define _TOOLS_IOb_ROBOT_ALL_H


#include <semaphore.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/time.h>

#ifdef Linux
#include <linux/videodev2.h>
#include <linux/joystick.h>
#include <linux/input.h>
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*Taille du buffer pour la structure du clientHW. A laisser car facile a changer si la tialle des msg augmente*/
#define MAXLINE 57600
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>

#ifdef PLAYER
#include <libplayerc/playerc.h>
#endif

/* #include <Biclops_interface.h> */

void libhardware_open(char*);
void libhardware_close();

void libhardware_start();

/********************************************************************************/
/***********************          Objet Joystick            *********************/
/********************************************************************************/

typedef struct Joystick
{
  /* Joystick information */
  char name[256];
  char port[256];
  int fd;

  pthread_mutex_t lock; /*Mutex pour la recopie dans le buffer de la structure*/

  int type;
  float sensitivity;

  int enabled;
  int autocheck;

  int *buttons;
  float *axes;

  int nb_buttons;
  int nb_axes;
  int use_emergency;
} Joystick;

extern void joystick_apply_hardware_config(char *argv);
extern void joystick_create_joystick(Joystick ** joystick, char *name, char *filename);
extern void joystick_create_joystick_table(Joystick ** list, int nb_pt);
extern void joystick_init(Joystick *joystick);

extern Joystick *joystick_get_joystick_by_name(char *name);
extern Joystick *joystick_get_first_joystick();
extern Joystick *joystick_get_joystick_by_number(int num);
extern int joystick_get_nb_joystick();

extern float joystick_get_axis_position(Joystick *joystick, int axis_nb);
extern int joystick_get_button_value(Joystick *joystick, int button_nb);

/********************************************************************************/
/***********************          Objet Mouse            *********************/
/********************************************************************************/

typedef struct Mouse
{
  /* Mouse information */
  char name[256];
  char port[256];
  int fd;

  pthread_mutex_t lock; /*Mutex pour la recopie dans le buffer de la structure*/

  /*int type;*/
  float sensitivity;

  int enabled;
  int autocheck;

  int *buttons;
  float *axes;

  int nb_buttons;
  int nb_axes;
  /* int use_emergency;*/
} Mouse;


extern void mouse_apply_hardware_config(char *argv);
extern void mouse_create_mouse(Mouse ** mouse, char *name, char *filename);
extern void mouse_create_mouse_table(Mouse ** mouse_table, int nb_mouse);
extern Mouse *mouse_get_mouse_by_name(char *name);
extern Mouse *mouse_get_first_mouse();
extern Mouse *mouse_get_mouse_by_number(int num);
extern int mouse_get_nb_mouse();
extern void mouse_init(Mouse *mouse);
extern void mouse_delete(Mouse *mouse);
extern void mouse_read_thread(Mouse *mouse);

extern float mouse_get_axis_position(Mouse *mouse, int axis_nb);
extern int mouse_get_button_value(Mouse *mouse, int button_nb);


/********************************************************************************/
/***********************          Objet Pressure          *********************/
/********************************************************************************/

typedef struct Pressure 
{
  /* Pressure  information */
  char name[256];
  char port[256];
  int fd;

  pthread_mutex_t lock; /*Mutex pour la recopie dans le buffer de la structure*/

  /*int type;*/
  float sensitivity;

  int enabled;
  int autocheck;

  int *buttons;
  float *axes;


  int nb_Pressure ;
  /* int use_emergency;*/
} Pressure;


extern void pressure_apply_hardware_config(char *argv);
extern void pressure_create_pressure(Pressure ** pressure, char *name, char *filename);
extern void pressure_create_pressure_table(Pressure ** pressure_table, int nb_pressure);
extern Pressure *pressure_get_pressure_by_name(char *name);
extern Pressure *pressure_get_first_pressure();
extern Pressure *pressure_get_pressure_by_number(int num);
extern int pressure_get_nb_pressure();
extern void pressure_init(Pressure *pressure);
extern void pressure_delete(Pressure *pressure);
extern float read_pressure(Pressure *pressure);

extern float pressure_get_axis_position(Pressure *pressure, int pressure_nb);


/********************************************************************************/
/***********************         	Objet Laser        		*********************/
/********************************************************************************/

#define USE_SICK_LMS 1
#define USE_URG_04LX 2

typedef struct Laser
{
  /*void*  this;*/
  char *name;                     /*Nom donne dans le .dev */
  int    laser_type;
  char *port;                     /*port relie a la Robubox */
  pthread_mutex_t mutex_laser_start;
  pthread_mutex_t mutex_laser_capture;
  int enabled;
  int fd;
  float seuil_max;

  float seuil_min;
  int nb_values;
  long  * values;                                        /* valeur en cm */

  int autocheck;
  /*laser URG*/
  void * urg;
  void * urg_parameters;

} Laser;

extern void laser_apply_hardware_config(char *argv);
extern void laser_create_laser(Laser ** laser, char *name,char *filename);
extern void laser_create_laser_table(Laser ** cam_table,int nb_camera);
extern Laser *laser_get_laser_by_name(char *name);
extern Laser *laser_get_first_laser(void);
extern long laser_get_intensity_by_degres(Laser *,float);
extern int laser_read(Laser * laser);

/* void laser_connect(Laser * laser);
 int readLMSdata(int fd, unsigned char* buf);
 int connectToLMS(int range_mode, int res_mode, int unit_mode, char * port, int baud_sel);*/

/********************************************************************************/
/***********************         Objet ServerRobubox        *********************/
/********************************************************************************/

typedef struct ServerRobubox
{
  /*void*  this;*/
  char *name;                     /*Nom donne dans le .dev */
  int port;                       /*port relie a la Robubox */
  int port_server;                /*port relie aux clientRobubox*/
  char *host;                     /*IP_host */
  pthread_mutex_t mutex_socket;
  int sockfd;                     /*socket de communication */
  struct sockaddr_in serv_addr;   /*structure du serveur */
  char buff[MAXLINE];             /*buffer de reception des message */
  /*int    serverRobubox_type; */

  int ping;
  long compteur;

} ServerRobubox;

extern void serverRobubox_apply_hardware_config(char *argv);
extern void serverRobubox_create_serverRobubox(ServerRobubox ** serverRobubox, char *name,char *filename);
extern void serverRobubox_create_serverRobubox_table(ServerRobubox ** cam_table,int nb_camera);
extern ServerRobubox *serverRobubox_get_serverRobubox_by_name(char *name);
extern ServerRobubox *serverRobubox_get_first_serverRobubox(void);

void serverRobubox_connect(ServerRobubox * chw);
long serverRobubox_ping(ServerRobubox * chw);
int serverRobubox_transmit(ServerRobubox * chw, char *commande);
int serverRobubox_transmit_receive(ServerRobubox * chw, char *commande, char *result);
int serverRobubox_init_connection(ServerRobubox * chw);
int serverRobubox_lecture_du_socket(ServerRobubox * chw);
int serverRobubox_ecriture_vers_socket(ServerRobubox * chw);
void serverRobubox_init_server(ServerRobubox * chw);



/********************************************************************************/
/***********************         Objet ClientRobubox        *********************/
/********************************************************************************/

typedef struct ClientRobubox
{
  /*void*  this;*/
  char *name;                     /*Nom donne dans le .dev */
  int port;                       /*port 6665 */
  char *host;                     /*IP_host */
  pthread_mutex_t mutex_socket;
  int sockfd;                     /*socket de communication */
  struct sockaddr_in serv_addr;   /*structure du serveur */
  char buff[MAXLINE];             /*buffer de reception des message */
  /*int    clientRobubox_type; */

  int ping;
  long compteur;

} ClientRobubox;

extern void clientRobubox_apply_hardware_config(char *argv);
extern void clientRobubox_create_clientRobubox(ClientRobubox ** clientRobubox, char *name,char *filename);
extern void clientRobubox_create_clientRobubox_table(ClientRobubox ** cam_table,int nb_camera);
extern ClientRobubox *clientRobubox_get_clientRobubox_by_name(char *name);
extern ClientRobubox *clientRobubox_get_first_clientRobubox(void);

void clientRobubox_connect(ClientRobubox * chw);
long clientRobubox_ping(ClientRobubox * chw);
int clientRobubox_transmit(ClientRobubox * chw, char *commande);
int clientRobubox_transmit_receive(ClientRobubox * chw, char *commande, char *result);
int clientRobubox_init_connection(ClientRobubox * chw);
int clientRobubox_lecture_du_socket(ClientRobubox * chw);
int clientRobubox_ecriture_vers_socket(ClientRobubox * chw);
void clientRobubox_init_watchdog(ClientRobubox * chw);


/********************************************************************************/
/***********************             Objet ClientHW           *********************/
/********************************************************************************/

/*Un clientHW est un client qui demandera l'execution des fonction hardware par un serverHW*/


typedef struct ClientHW
{
  /*void*  this;*/
  char *name;                 /*Nom donne dans le .dev */
  int port;                   /*port 6665 */
  char *host;                 /*IP_host */
  pthread_mutex_t mutex_socket;
  int sockfd;                 /*socket de communication */
  struct sockaddr_in serv_addr;   /*structure du serveur */
  char buff[MAXLINE];         /*buffer de reception des message */
  /*int    clientHW_type; */

} ClientHW;
/*Creation des objets definis dans le .dev*/
extern void clientHW_apply_hardware_config(char *argv);
/*private*/ extern void clientHW_create_clientHW(ClientHW ** clientHW,
    char *name, char *filename);
/*private*/ extern void clientHW_create_clientHW_table(ClientHW ** cam_table,
    int nb_camera);
/*private*/ void clientHW_connect(ClientHW * chw);
extern int clientHW_ping(ClientHW * chw);
/*private*/ extern ClientHW *clientHW_get_clientHW_by_name(char *name);
/*private*/ extern ClientHW *clientHW_get_first_clientHW(void);
/*private*/ int clientHW_transmit(ClientHW * chw, char *commande);
int clientHW_transmit_receive(ClientHW * chw, char *commande, char *result);
/*private*/ int clientHW_transmit_video(ClientHW * chw, char *commande,
    unsigned char *im_capture);




/********************************************************************************/
/***********************             Objet ClientPlayer  *********************/
/********************************************************************************/



typedef struct ClientPlayer
{
  /*void*  this;*/
  char *name;                 /*Nom donne dans le .dev */
  int port;                   /*port 6665 */
  char *host;                 /*IP_host */
  pthread_mutex_t mutex_socket;
  float rate;
#ifdef PLAYER
  playerc_client_t * player_client;
  playerc_sonar_t * player_sonar_proxy ;
  playerc_position2d_t * player_position_proxy;
#endif

} ClientPlayer;

extern void clientPlayer_apply_hardware_config(char *argv);
extern void clientPlayer_create_clientPlayer(ClientPlayer ** clientPlayer, char *name,
    char *filename);
extern void clientPlayer_create_clientPlayer_table(ClientPlayer ** cam_table,
    int nb_camera);
extern ClientPlayer *clientPlayer_get_clientPlayer_by_name(char *name);
extern ClientPlayer *clientPlayer_get_first_clientPlayer(void);
void clientPlayer_connect(ClientPlayer * chw);

/********************************************************************************/
/***********************             Objet Serial           *********************/
/********************************************************************************/

/*Port serie, certain hardware, presque tous, passent par le port serie, qu'il faut definir.*/

typedef struct Serial
{

  char *name;                 /* nom de l'objet dans le .dev ex: _ttyS0 */
  char path[128];             /* chemin du port '/dev/ttyS0' */

  int desc;                   /*  descripteur donne a l ouverture du port */

  /*parametre de configuration de l UART ex 9600 8 N 2 */
  char debit[128];
  char bit[128];
  char parite[128];
  char stop[128];
  int vmin;   /* parametres vmin et vtime , permet de choisir entre une lecture bloquante et n
on bloquante   */
  int vtime;
  int icanon;
  int onblock;
  pthread_mutex_t serial_mutex;

} Serial;

/* fonction de creation de l'objet */
extern void serial_apply_hardware_config(char *argv);
/*private*/ extern void serial_create_serial(Serial ** serial, char *name,
    char *file_hw);
/*private*/ extern void serial_create_serial_table(Serial ** list,
    int nb_rob);

/*fonction de modification des parametre de communication*/
extern int serial_setparam(char *port_name, char *speed, char *bits,
    char *par, char *stopb);

/*Fonction de recuperation des objets: private */
/*private */ extern Serial *serial_get_serial_by_path(char *path);
/*public */
/*private */ extern Serial *serial_get_serial_by_name(char *name);
/*public */

/*Fonction d'utilisation des objets: private */
extern int serial_send_and_receive(char *port_name,char
    *commande,
    char
    *retour,
    int
    lenght);
extern int serial_send_and_receive_nchar(char *port_name, char *commande, char *retour, int lenght,int length_out);  /*private */
/*private */ extern int serial_send(char *port_name, char *commande,
    int
    lenght);
/*public */
extern int serial_receive_block(char *port_name, char *retour,time_t timeout_usec);
extern int serial_send_and_receive_block(char *port_name, char *commande, char *retour,int lenght, time_t timeout_usec);
extern int serial_send_and_receive_line(char *port_name, char *commande, char *retour, int lenght);


/********************************************************************************/
/***********************             Objet Arm             **********************/
/********************************************************************************/
/** cf src/Arm - obsolete ?*/
void arm_wait(void);
void arm_print_position(void);
void commande_katana(char cmd, int *param, char *data);
int get_free(void);
int get_freezed(void);
int get_logical_dof(int i);
void set_logical_dof(int p_index, int value);
int get_maxdata(void);
int get_max_dof_katana_arm(void);
int posHigh(int motor_number, float teta);
int posLow(int motor_number, float teta);
void set_arm_rotation(float teta0, float teta1, float teta2, float teta3,float teta4);
void set_motor_velocity(int num, int maxppwm, int maxnpwm, int kP, int kD);

/********************************************************************************/
/***********************             Objet Joint           *********************/
/********************************************************************************/

#define USE_SERIAL 1
#define USE_CLIENTHW 3
#define USE_AIBO 4
#define USE_PLAYER 5
#define USE_BERENSON 6
#define USE_JOINT_WEBOTS 8

/* ce sojnt les servo moteur. Aujourd'hui, nous ne les controlons que par SSC (port serie 9600 8N2) ou par un clientHW */

typedef struct Joint
{

  char name[128];             /* nom de l'objet dans le fichier .dev ex: "Servo 'titi' is file.hws */
  char port_name[128];        /* nom du port utilise par le servo _ttyS0, doit avoir ete defini dans le .dev */
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  char clientRobubox[128];
  int type;
  int autocheck;

  float position;             /*position du servo : proprio */

  float proprio;

  char type_controlleur[128]; /*type de controller (ssc/pic...) */
  int ssc;                    /* n de la carte ssc */

  /* Integree pour Aibo : permet de dire si la valeur de proprioception a ete mise a jour */
  /* Car les recpetions de messages URBI-Aibo sont asynchrones */
  int updated;

  /* commande minimale et maximale pour le servo */
  int start;
  int stop;

  int init;                   /* valeur de la commande a laquelle il s'initialise */
  int zero;                   /* valeur de la commande a laquelle on considere qu'il est a 0 */

  int range;                  /* debattement authorise */

  int rotation;               /* sens de rotation du servo
                                   1: sens de rotation normale
                                   -1: sens de rotation inverse
   */

  int Vmax;                   /* vitesse maximale de rotation */
  int Vdefault;               /* vitesse par defaut de rotation */
  float Vmult;                /* coefficient multiplicatif de la vitesse */

  void *player_client;
  void *player_ptz_proxy;

  sem_t sem;  /* Semaphore pour l attente de la mise a jour d'une valeur (hors camera et micro) pour Aibo */
} Joint;


extern void joint_apply_hardware_config(char *argv);
/*private */ extern void create_joint(Joint ** serv, char *name, char *type);
/*private */ extern void joint_create_joint_table(Joint ** list, int nb_serv);
extern Joint *joint_get_joint_by_number(int number);
extern int joint_get_nb_joint(void);
/*fonction de recuperation de l'objet */
extern Joint *joint_get_serv_by_name(const char *name);

/*fonctoin d'information */
extern int joint_get_range_by_name(char *name);

/*foction de commande du servo */
extern void joint_servo_command(Joint * joint, float pct, int speed);

extern void joint_speedo_command(Joint *serv, float speed);
extern void joint_init(Joint *serv);

extern void joint_get_proprio(Joint *serv);
extern float read_biclops(Joint *serv);
extern void read_biclops_thread(Joint *serv);
extern void joint_delete(Joint * serv);

extern void joint_check_hardware(Joint * serv);

extern void joint_activate_servo(Joint *serv, float activ);

extern void arm_speed_init_command();
/********************************************************************************/
/* Objet Motor (redefinition de Joint mais sans les aberation de programmation  */
/********************************************************************************/

/* ce sojnt les motor moteur. Aujourd'hui, nous ne les controlons que par SSC (port serie 9600 8N2) ou par un clientHW */

#define USE_SERIAL_SSC2 1   /*SSC2*/
#define USE_SERIAL_SSC12 2   /*SSC12*/
#define USE_CLIENTHW 3
#define USE_AIBO 4
#define USE_PLAYER 5
#define USE_SERIAL_SSC32 6   /*SSC32*/
#define USE_MD23 7
#define USE_AX12_2POB 8
#define USE_AX12_USB2DYN 9

typedef struct Motor
{

  char name[128];             /* nom de l'objet dans le fichier .dev ex: "Motor 'titi' is file.hws */
  char port_name[128];        /* nom du port utilise par le motor _ttyS0, doit avoir ete defini dans le .dev */
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  int type;
  int autocheck;

  float position;             /*position du motor : proprio */

  float proprio;

  int adresse_bus;                    /* num du motor sur le bus de la carte ssc */




  /* Integree pour Aibo : permet de dire si la valeur de proprioception a ete mise a jour */
  /* Car les recpetions de messages URBI-Aibo sont asynchrones */
  int updated;

  /* commande minimale et maximale pour le motor */
  int start;
  int stop;

  int init;                   /* valeur de la commande a laquelle il s'initialise */
  int zero;                   /* valeur de la commande a laquelle on considere qu'il est a 0 */

  int range;                  /* debattement authorise */

  int rotation;               /* sens de rotation du motor
                                   1: sens de rotation normale
                                   -1: sens de rotation inverse
   */
  int vitesse;

  int Vmax;                   /* vitesse maximale de rotation */
  int Vdefault;               /* vitesse par defaut de rotation */
  float Vmult;                /* coefficient multiplicatif de la vitesse */

  void *player_client;
  void *player_ptz_proxy;

  sem_t sem;  /* Semaphore pour l attente de la mise a jour d'une valeur (hors camera et micro) pour Aibo */
} Motor;


extern void motor_apply_hardware_config(char *argv);
/*private */ extern void create_motor(Motor ** serv, char *name, char *type);
/*private */ extern void motor_create_motor_table(Motor ** list, int nb_serv);
extern void motor_init(Motor * motor);
extern Motor *motor_get_motor_by_number(int number);
extern int motor_get_nb_motor(void);
/*fonction de recuperation de l'objet */
extern Motor *motor_get_serv_by_name(char *name);

/*fonctoin d'information */
extern int motor_get_range_by_name(char *name);

/*foction de commande du motor */
extern void motor_command_position(Motor * motor, float pct, int speed);
extern void motor_command_position_wait_target(Motor * motor, float pct, int speed);

extern void motor_command_speed(Motor *serv, float speed);

extern int motor_is_moving(Motor *motor);
extern void motor_get_proprio(Motor *serv);

extern void motor_check_hardware(Motor * serv);



/********************************************************************************/
/***********************             Objet Robot           *********************/
/********************************************************************************/

#define USE_KOALA 1
#define USE_LABO3 2
#define USE_CLIENTHW 3
#define USE_SIMULATION 4
#define USE_PLAYER 5
#define USE_ROBUBOX 6
#define USE_MAYA 8
#define USE_ROBUBOX_V2 9

#define READ_US 0
#define READ_IR 1
#define READ_US_S 2

/*Robot, sur le port serie pour 1:koala et 2:Labo3.   3 correspond a un clientHW qu'il faut definir*/

typedef struct Robot
{
  /*void*  this;*/
  char *name;                 /*nom donne dans le .dev */
  char port[128];             /*port de communicaton ex _ttyS0 */
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  char clientRobubox[128];    /*nom du ClientRobubox a utiliser si il y en a un */
  char clientPlayer[128];    /*nom du ClientRobubox a utiliser si il y en a un */

  pthread_mutex_t lock;       // mutex pour la lecture threadee de l'odometrie => MAYA
  int enable;
  int robot_type;             /*le type: 0:a ne pas utiliser 1:koala 2:labo3 3:ClientHW 4:simulation 5: Player 6:ClientRobubox  */
  int autocheck;

  float angle90;              /*equivalant conteur de rout pour faire un angle droit */
  float largeur_robot;        /*largueur du robot pour integration de chemin */
  float nbt_encoder;        /*tic par tour*/
  float wheel_radius;        /*rayon de la roue du robot */
  float RS_max;
  float speed_max;

  /*Gestion des Sonars */
  int nb_ir;
  float angle_ir[16];         /*angle de laser ou sonar du premier au derneir en relatif */
  float IR[16];
  float seuil_ir;
  float actmax_ir;

  /*Gestion des Infra-Rouges */
  int nb_ir_sensor;
  float angle_ir_sensor[16];         /*angle de laser ou sonar du premier au derneir en relatif */
  float IR_SENSOR[16];
  float seuil_ir_sensor;
  float actmax_ir_sensor;

  void *player_client;

  /* Ajout Vincent */
  /* Valeurs d'odometrie (mises a jour par la fonction robot_go_by_speed_odometrie()) */
  double distance ;
  double angle ;

  double angle_rb2; //correction bug robubox v2

  int nb_clics_gauches ;
  int nb_clics_droits ;
  /* Nombre de clics a partir duquel on risque une remise a zero des compteurs non controlee */
  int nb_clics_max ;

  /* Valeurs de position */
  float posx_odo; /* Position calculee par odometrie par rapport a une position "reset"*/
  float posy_odo;
  float orientation_odo;

  float posx_odo_th; /* Position calculee par odometrie par rapport a une position "reset"*/
  float posy_odo_th;
  float orientation_odo_th;

  float posx_abs; /* Position dans un referentiel absolu */
  float posy_abs;
  float orientation_abs;

  float default_posx; /* Position servant de point de reference pour la position odometrie */
  float default_posy;
  float default_orientation;

  /* Valeurs pour le robot simule */
  float last_posx_abs; /* Derniere position pour simuler l'odometrie */
  float last_posy_abs;
  float last_orientation_abs;
  float last_orientation_abs_rb2; // correction bug robubox
  int current_landmark; /* Landmark courant */

  int mvt_sens;

  float current_divider; // current max for each motor
	
} Robot;

extern void robot_apply_hardware_config(char *argv);

extern void robot_create_robot(Robot ** robot, char *name, char *filename);
extern void robot_create_robot_table(Robot ** cam_table, int nb_camera);

extern Robot *robot_get_robot_by_name(char *name);
extern Robot *robot_get_first_robot(void);
extern Robot *robot_get_robot_by_number(int num);
extern int robot_get_nb_robot(void);
extern int robot_get_robot_type(Robot * robot);
/*movement*/

extern void robot_init_wheels_counter(Robot * robot);
extern void robot_read_wheels_counter(Robot * robot, int *DistL, int *DistR);
extern void robot_go_by_position(Robot * robot, float Left, float Right);
extern void robot_turn_angle(Robot * robot, float angle);   /*remplace tunr_left et turn_right */
extern void robot_go_by_speed(Robot * robot, float Left, float Right);
extern void robot_get_odometry(Robot * robot);
extern void robot_reset_odo(Robot * robot);
extern void robot_get_location_odo(Robot * robot);
extern void robot_reset_location_odo(Robot * robot);
extern void robot_set_location_abs(Robot * robot, float posx, float posy);
extern void robot_get_location_abs(Robot * robot);
extern void robot_go_forward(Robot * robot, float LR);
extern void robot_go_by_position_with_odo(Robot * robot, float Left,
    float Right);
extern void robot_set_velocity(Robot * robot, float speed, float sideway,
    float agular_speed, int state);

extern int robot_serial_write(Robot * robot, char *buff, char c,
    char *retour);
extern int robot_commande_koala(Robot * robot, char cmd, int *param,
    char *data);
extern int robot_precise_last_target_reached(Robot * robot);
extern int robot_last_target_reached(Robot * robot);

extern void robot_get_ir(Robot * r, float *ir);
extern void robot_get_ir_sensor(Robot * r, float *ir);
extern void robot_set_led(Robot * r,int led,int action);
/*Ancienne fonction a include*/
extern void robot_turn_right(Robot r, float angle);
extern void robot_turn_left(Robot r, float angle);
/* extern void robot_set_velocity(Robot * r, int vd,int vg); */
extern void robot_set_wheels_position(Robot * r, int pd, int pg);

extern int robot_check_hardware(Robot*r);
extern void robot_get_odometry_thread(Robot *robot);
extern void robot_get_motor_power(Robot * robot, float* left, float *right);

void robot_get_one_landmark(Robot * robot, int *landmark_id, float *azimuth, float *elevation);
void robot_reset_landmarks(Robot *robot);


/********************************************************************************/
/***********************             Objet Compass           *********************/
/********************************************************************************/

#define USE_HOLTZ 1
#define USE_STAMP 2
#define USE_CLIENTHW 3
#define USE_SIMULATION 4
#define USE_PLAYER 5
#define USE_GAZEBO 6
#define USE_COMPASS_ROBUBOX 8
#define USE_COMPASS_ROBUROC 9
#define USE_COMPASS_THREAD 10

/*Compass stamp 1:bobine holtz branche sur koala mais le code n'est plus dispo, 2: Stamp, 3: clientHW*/

typedef struct Compass
{
  /*void*  this;*/
  char *name;
  char *port;
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  char clientRobubox[128];
  int compass_type;           /*1: ancienne bobine holtz du koala 2: basic stamp 3: ClientHW */
  int autocheck;

  /*proxy pour le compass par player et par gazebo*/
  void *player_client;
  void *player_truth_proxy;
  float angle;/*peut aussi etre obtenu par la fonction read_compass*/
  float tilt; /*utilise par les compass compensee des roburocs*/
  float roll;/*utilise par les compass compensee des roburocs*/

} Compass;

/*fonction de creation de l'objet*/
extern void compass_apply_hardware_config(char *argv);
/*private*/ extern void compass_create_compass(Compass ** compass, char *name,
    char *filename);
/*private*/ extern void compass_create_compass_table(Compass ** cam_table,
    int nb_camera);

/*fonction de recuperation  de l'objet*/
extern Compass *compass_get_compass_by_name(char *name);
extern Compass *compass_get_first_compass(void);
extern Compass *compass_get_compass_by_number(int);
extern int compass_get_nb_compass(void);

/*fonction d utilisation de l objet*/
extern float compass_read(Compass * compass);

/*fonction hardware de lecture*/
/*private*/ extern int compass_server_read(Compass * compass);
/*private*/ extern int compass_stamp_read(Compass * compass);
/*private*/ extern int compass_holtz_read(Compass * compass);

extern void compass_init(Compass * compass);

extern void compass_delete(Compass *compass) ;

/********************************************************************************/
/***********************             Objet Accelero           *********************/
/********************************************************************************/

#define USE_4_on_plateforme 1

/*Compass stamp 1:bobine holtz branche sur koala mais le code n'est plus dispo, 2: Stamp, 3: clientHW*/

#define ACCEL_MAYA 4

typedef struct Accelero
{
  /*void*  this;*/
  char *name;
  char *port;
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  int accelero_type;           /*1: ancienne bobine holtz du koala 2: basic stamp 3: ClientHW */
  /*proxy pour le accelero par player et par gazebo*/
  /*1: ancienne bobine holtz du koala 2: basic stamp 3: ClientHW */
  float posx_min,posx_max,posy_min,posy_max;
  float ax,bx,ay,by;
  float eqax,eqapx,eqay,eqapy;

} Accelero;

/*fonction de creation de l'objet*/
extern void accelero_apply_hardware_config(char *argv);
/*private*/ extern void accelero_create_accelero(Accelero ** accelero, char *name,
    char *filename);
/*private*/ extern void accelero_create_accelero_table(Accelero ** cam_table,
    int nb_camera);

/*fonction de recuperation  de l'objet*/
extern Accelero *accelero_get_accelero_by_name(char *name);
extern Accelero *accelero_get_first_accelero(void);
extern Accelero *accelero_get_accelero_by_number(int);
extern int accelero_get_nb_accelero(void);

/*fonction d utilisation de l objet*/
extern void accelero_read(Accelero * accelero,int *);
/* Fonction ajoutee pour l'accel 3D - Erwan */
extern void accelero3D_read(Accelero * accelero,int *);
extern void accelero_read_bras(Accelero * accelero, float * accelero_value);



/********************************************************************************/
/***********************             Objet Gps           *********************/
/********************************************************************************/

#define USE_GARMIN 			1
#define USE_CLIENTHW 		3
#define USE_WEBOTS_GPS		4
#define USE_DSGPMS_OD		5
/*Gps 1:Garmin 3: clientHW 4: GPSwebots*/

typedef struct Gps_value
{
  float UTC_date;
  float day;
  float month;
  float year;
  float UTC_time;
  float hour;
  float minute;
  float second;
  float nord;
  float est;
  float altitude;
  float speed;
  float compass;
} Gps_value;

typedef struct Gps
{
  /*void*  this;*/
  char *name;
  char *port;
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  int gps_type;           /*Gps 1:Garmin 3: clientHW 4: GPSwebots*/
  /*proxy pour le gps par player et par gazebo*/
  void *player_client;
  void *player_truth_proxy;
  char *udphost ;				/* Added for gps_type USE_WEBOTS_GPS */
  int udpport;				    /* Added for gps_type USE_WEBOTS_GPS */
  int sockfd;					/* Added for gps_type USE_WEBOTS_GPS */
  struct sockaddr_in serv_addr;	/* Added for gps_type USE_WEBOTS_GPS */
  char buff[MAXLINE]; 		    /* Added for gps_type USE_WEBOTS_GPS */
  Gps_value *gps_value;
} Gps;


/*fonction de creation de l'objet*/
extern void gps_apply_hardware_config(char *argv);
/*private*/ extern void gps_create_gps(Gps ** gps, char *name, char *filename);
/*private*/ extern void gps_create_gps_table(Gps ** cam_table, int nb_camera);

/*fonction de recuperation  de l'objet*/
extern Gps *gps_get_gps_by_name(char *name);
extern Gps *gps_get_first_gps(void);
extern Gps *gps_get_gps_by_number(int);
extern int gps_get_nb_gps(void);

/*fonction d utilisation de l objet*/
extern void gps_read(Gps * gps);

/*fonction hardware de lecture*/
/*private*/ extern void gps_server_read(Gps * gps);
/*private*/ extern void gps_garmin_read_threaded(Gps * gps);
/*private*/ extern void gps_garmin_read(Gps * gps);
/*private*/ extern void gps_dsgpms_od_read(Gps * gps);
/*private*/ extern void gps_webots_read_threaded(Gps * gps);
/*private*/ extern void gps_webots_read(Gps * gps);




/********************************************************************************/
/***********************             Objet Camera           *********************/
/********************************************************************************/

/*PAL , composite et webcam*/
#define USE_CAMERA_CLASSIC 1
/*NTSC*/
#define USE_CAMPANO 2
/*client vers serverHW*/
#define USE_CLIENTHW 3
/*camera firewire*/
#define USE_FIREWIRE 4
/*player camera*/
#define USE_PLAYER 5
/* Aibo camera */
#define USE_CAM_AIBO 6
/* Video 4 Linux 2 camera */
#define USE_V4L2 7
/* Camera Robubox */
#define USE_CAM_WEBOTS 8

struct buffer {
  void *                  start;
  size_t                  length;
};

typedef struct Camera
{
  /*void*  this;*/
  char *port;                 /*port '/dev/video0' par exemple */
  char *name;                 /*object name in .dev file. ex: "Camera 'titi' is pal3x" */
  char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
  char clientRobubox[128];

  int desc;                   /*descriptor given by the open */
  int camera_type;

  int autocheck;

  int threading;
  int enabled;

  int color;

  int height_max;
  int width_max;
  int palette;                /*VIDEO_PALETTE_GREY if gray and VIDEO_PALETTE_RGB24 or VIDEO_PALETTE_YUV420P  if color */
  /*DC1394_COLOR_CODING_MONO8 && video_mode!= DC1394_COLOR_CODING_RGB8*/


  sem_t sem; /* Semaphore pour l attente de la mise a jour de l'image */
  pthread_mutex_t mutex_grab_image; /*Mutex pour la recopie dans le buffer de la structure*/

  unsigned char *img_buff;



  /*read in a hardware file whiche named is found in the dev file*/
  float teta_w;
  float teta_h;

  /*user defined variables*/
  int height;
  int width;
  float scale_factor;
  int norm;

  /* Camera parameters variables */
  int exposure;
  int brightness;
  int gain;
  int sharpness;
  int red_balance;
  int blue_balance;
  int hue;
  int saturation;
  int gamma;
  int shutter;
  int iris;

  /*************Pour V4L2 ****************/
#ifdef Linux
  int camera_type_v4l;            /*CAM_COMPOSITE ou CAM_WEBCAM */

  struct v4l2_capability v4l2_caps;
  struct v4l2_format v4l2_fmt;
  struct v4l2_requestbuffers req_buf;
  struct v4l2_buffer *v4l2_buffers;
  struct buffer *buffers;
  int exposure_mode;

#else
  /* pour Darwin ... */
#endif

  /* buffer de capture*/
  int nb_buffers;
  int flush_buffers;
  unsigned char **grab_buf;

  /****************dc1394_cameracapture********************/

  void *cam_firewire;
  void *converted_frame_firewire;

  int addr_bus_firewire;
  /*following param: see dc1394/types.h*/
  int video_mode;
  int framerate;
  int isospeed;

  /************ Camera Aibo ***********************/
  /* Integree pour Aibo : permet de dire si l'image de la camera a ete mise a jour */
  /* Car les recpetions de messages URBI-Aibo sont asynchrones */    
  int updated;
  int format; /* Format de l'image (ycbcr, jpeg ) */
  int size; /* taille de l'image ( != de la resolution, car compression possible )*/


  /****************Pour player********************/
  void *player_client;
  void *player_camera_proxy;

} Camera;

extern void camera_apply_hardware_config(char *argv);
extern Camera *camera_get_cam_by_name(char *name);
extern int camera_get_nb_camera(void);
extern Camera *camera_get_camera_by_number(int number);


extern void camera_grabimage(Camera * cam, unsigned char *im_capture,
    int color_wanted, float scale_factor,
    int hardscale);


extern int convertYCrCbtoRGB(char * sourceImage, int bufferSize, char * destinationImage);
extern int convertJPEGtoYCrCb(char *source, int sourcelen, char *dest, int *size);
extern int convertJPEGtoRGB(char *source, int sourcelen, char * dest, int *size);

extern float	camera_get_xfov(char *name);
extern float	camera_get_yfov(char *name);

extern int camera_set_exposure_value(Camera * cam, unsigned int value);
extern int camera_set_exposure_auto(Camera * cam);
extern int camera_set_brightness_value(Camera * cam, unsigned int value);
extern int camera_set_brightness_auto(Camera * cam);
extern int camera_set_gain_value(Camera * cam, unsigned int value);
extern int camera_set_gain_auto(Camera * cam);
extern int camera_set_sharpness_value(Camera * cam, unsigned int value);
extern int camera_set_sharpness_auto(Camera * cam);
extern int camera_set_white_balance_values(Camera * cam, unsigned int BU, unsigned int RV);
extern int camera_set_white_balance_auto(Camera * cam);
extern int camera_set_hue_value(Camera * cam, unsigned int value);
extern int camera_set_hue_auto(Camera * cam);
extern int camera_set_saturation_value(Camera * cam, unsigned int value);
extern int camera_set_saturation_auto(Camera * cam);
extern int camera_set_gamma_value(Camera * cam, unsigned int value);
extern int camera_set_gamma_auto(Camera * cam);
extern int camera_set_shutter_value(Camera * cam, unsigned int value);
extern int camera_set_shutter_auto(Camera * cam);
extern int camera_set_iris_value(Camera * cam, unsigned int value);
extern int camera_set_iris_auto(Camera * cam);

extern int camera_get_exposure_value(Camera * cam, unsigned int * value);
extern int camera_get_brightness_value(Camera * cam, unsigned int * value);
extern int camera_get_gain_value(Camera * cam, unsigned int * value);
extern int camera_get_sharpness_value(Camera * cam, unsigned int * value);
extern int camera_get_white_balance_values(Camera * cam, unsigned int* BU, unsigned int *RV);
extern int camera_get_hue_value(Camera * cam, unsigned int * value);
extern int camera_get_saturation_value(Camera * cam, unsigned int *value);
extern int camera_get_gamma_value(Camera * cam, unsigned int *value);
extern int camera_get_shutter_value(Camera * cam, unsigned int * value);
extern int camera_get_iris_value(Camera * cam, unsigned int* value);

extern int camera_get_exposure_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_gain_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_brightness_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_sharpness_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_white_balance_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_hue_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_saturation_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_gamma_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_shutter_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_iris_boundaries(Camera * cam, unsigned int *min, unsigned int *max);

extern void convert_border_bayer_line_to_bgr24( uint8_t* bayer, uint8_t* adjacent_bayer, uint8_t *bgr, int width, int start_with_green, int blue_line);

extern void bayer_to_rgbbgr24(uint8_t *bayer, uint8_t *bgr, int width, int height,int start_with_green, int blue_line);

extern void bayer_to_rgb24(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order);

extern void flip_bgr_image(unsigned char *bgr_buff, int width, int height);

/**********************************************************************/
/*****************objet AIBO *************************************/
/*********************************************************************/

/***Client de communication avec AIBO****/
typedef struct Aibo_Client
{

  char name[50];              /* Nom du Client au cas ou plusieur existent */
  char port[10];
  /***Port de connection***/
  char IP[50];
  /***Ip de l'AIBO***/

  int socket;
  /****Socket de connection*/

  char buffer[4000];
  /**Buffer d'envoi***/

  char request[4000];
  /****Contient toutes les demandes de valeurs de device(joint,sensor micro)*/

  char recvbuffer[4000];
  /****Buffer de recption***/

  char binary_buffer[128000];
  /***Buffer binaire pour images et son ***/

} Aibo_Client;

extern void create_Aibo_Client(char *name, char *file);

extern void connect_Aibo_Client(void);

extern int aibo_recv_message(void);

extern int Aibo_Client_send(void);

extern void close_Aibo_Client(void);

extern void aibo_apply_hardware_config(char *argv);

extern Aibo_Client *get_Aibo_Client(void);

extern int get_Tag(char *, char *);

extern int get_Reponse(char *, char *);

extern int traite_Reponse(char *tag, char *reponse);

extern int traite_Reponse_Binaire(char *reponse);

struct timeval get_CurrentTime(void);

int get_Update(void);

void set_Unupdate(void);

void add_to_buffer(char *);

void get_All_Val(void);

void add_request(char *name, char *field1, char *field2);

/*****Camera de l'AIBO********/
typedef struct Aibo_Cam
{
  int format;                 /* 0: raw , 1 : jpeg */
  char name[50];
  char *image;
  int size;                   /* taille de l'image binaire recuperee */
  int sx;
  int sy;
  char mode[20];
  int num;
  int updated;
} Aibo_Cam;

extern Aibo_Cam *get_Aibo_Cam(void);

extern void create_Aibo_Cam(char *name, char *file);

/****Micros de l'AIBO****/
typedef struct Aibo_Micro
{

  char name[50];

  char *trame;

  int updated;

} Aibo_Micro;

extern Aibo_Micro *get_Aibo_Micro(void);

extern void create_Aibo_Micro(char *name, char *file);


/*******Senseurs de l'AIBO**********/
typedef struct Aibo_Sensor
{

  struct Aibo_Sensor *first;

  struct Aibo_Sensor *next;

  char name[50];

  int num;

  float valn;

  int updated;

} Aibo_Sensor;

extern Aibo_Sensor *get_Aibo_Sensor(void);

extern void create_Aibo_Sensor(char *name, char *file);

extern Aibo_Sensor *get_Aibo_Sensor_by_name(char *name);

extern Aibo_Sensor *get_Aibo_Sensor_by_num(int num);

/*******Joint de l'AIBO*************/
typedef struct Aibo_Joint
{

  struct Aibo_Joint *first;

  struct Aibo_Joint *next;

  char name[50];

  int num;

  float valn;

  int updated;

} Aibo_Joint;

extern Aibo_Joint *get_Aibo_Joint(void);

extern void create_Aibo_Joint(char *name, char *file);

extern Aibo_Joint *get_Aibo_Joint_by_name(char *);

extern Aibo_Joint *get_Aibo_Joint_by_num(int num);

extern void set_valn(int num, float valn);


/********************************************************************************/
/***********************          Sensor	            *********************/
/********************************************************************************/

#define	TYPE_AIBO	1

typedef	struct		Sensor
{
  struct Sensor		*first;
  struct Sensor		*next;
  char			name[50];
  int			type;
  int			num;
  float			val_start;
  float			val_stop;
  float			val;
  float			valn;
  int			updated;
  float                 *vecteur_sensor;
  char                  client[256];
  int                   nb_sensor;
  char                  *port;
  int                   autocheck;
  int                   periode; /*periode en ms pour la boucle de lecture externe (si dispo)*/
  sem_t sem;  /* Semaphore pour l attente de la mise a jour d'une valeur (hors camera et micro)  */
} Sensor;


extern void sensor_apply_hardware_config(char *argv);
extern void sensor_create_sensor(Sensor **sens, char *name, char *type);
extern void sensor_create_sensor_table(Sensor **list,int nb_sensor);
extern Sensor	*sensor_get_sens_by_name(char * name);
extern float sensor_sens_command(Sensor * sens);
extern void sensor_init_sensor(Sensor *sens);

/********************************************************************************/
/***********************          ClientNios	            *********************/
/********************************************************************************/

#define	_HARDWARE_NIOS_H

#define THETA_MAX 16
#define RHO_MAX 16

//#define REQUEST_GS 1
//#define REQUEST_GRAD 2
//#define REQUEST_GAUSS_1 3
//#define REQUEST_GAUSS_2 4
//#define REQUEST_DOG 5
//#define REQUEST_LOGPOL 6

#define NO_VIDEO 0x0
#define VIDEO_GS 0x1
#define VIDEO_GRAD 0x2
#define VIDEO_GAUSS_0 0x3
#define VIDEO_GAUSS_1 0x4
#define VIDEO_DOG 0x5
#define SEND_KP 0x8

#define VIDEO_MASK 0x7
#define KP_MASK 0x8

typedef uint8_t alt_u8;
typedef uint16_t alt_u16;
typedef uint32_t alt_u32;


/*struct keypoint received from the board on request*/
typedef struct Keypoint_t
{
  alt_u16 x;
  alt_u16 y;
  alt_u16 v;
  alt_u16 logpol[THETA_MAX*RHO_MAX];
}keypoint_t;

/*****Structure pour 1 point caracteristique********/
typedef struct NIOS_pt_carac
{
  uint16_t *image;								/* image de la dog*/
  int size;                   /* taille de l'image binaire recuperee */
  int sx;
  int sy;
  uint32_t posx;										/* position du pt dans image d'origine */
  uint32_t posy;
  float intensity; 						/* intensite du pt carac*/
} NIOS_pt_carac;


/*structure des keypoints d'une meme frame*/
typedef struct Nios_pt_carac
{
  uint32_t dim_image; /* le numero de la frame*/
  uint32_t nb_pt;
  NIOS_pt_carac* pts_carac; /* le tableau alloue pour les pts */
  sem_t sem; /* Semaphore pour l attente de la mise a jour de l'image */
} Nios_pt_carac;


extern Nios_pt_carac *cam_nios;
/***Client de communication avec le NIOS****/
typedef struct Nios_Client
{
  /* Nom du Client au cas ou plusieur existent */
  char name[50];
  /***Port de connection***/
  char port[10];

  /***Ip du NIOS***/
  char IP[50];

  /****Socket de connection*/
  int socket;

  /****order***/
  char request;

  /***Taille du Buffer de reception des images  ***/
  alt_u32 dim[2];

  /****Buffer de reception des images  ***/
  int16_t * buffer;

  /****image en float  ***/
  float *image;
  /***Liste chainee des keypoints extraits par frame  ***/
  Nios_pt_carac * nios_pt_carac;


} Nios_Client;

extern void create_nios_Client(char *name, char *file);

extern int connect_nios_Client(char *ip, char *port);



extern int nios_send_message(char *buffer, int size);

extern int nios_send_message_camera(void *cam, char *buffer, int size);

extern void *nios_recv_message(void *ptr);

extern int nios_Client_send(void);

extern void close_nios_Client(void);

extern void nios_apply_hardware_config(char *argv);

extern Nios_Client *get_nios_Client(void);





extern Nios_pt_carac *get_nios_Cam(void);

extern void create_nios_Cam(Nios_pt_carac* nios_pt_carac);
extern Nios_pt_carac * nioscam_grabimages(char order);
/********************************************************************************/
/***********************          ClientKatana	            *********************/
/********************************************************************************/

typedef struct ClientKatana
{
  /*void*  this;*/
  char *name;                     /*Nom donne dans le .dev */
  int port;                       /*port 6665 */
  char *host;                     /*IP_host */
  pthread_mutex_t mutex_socket;
  int sockfd;                     /*socket de communication */
  struct sockaddr_in serv_addr;   /*structure du serveur */
  char buff[MAXLINE];             /*buffer de reception des message */
  /*int    clientKatana_type; */

  int ping;
  long compteur;

} ClientKatana;

extern void clientKatana_apply_hardware_config(char *argv);
extern void clientKatana_create_clientKatana(ClientKatana ** clientKatana, char *name,char *filename);
extern void clientKatana_create_clientKatana_table(ClientKatana ** cam_table,int nb_camera);
extern ClientKatana *clientKatana_get_clientKatana_by_name(char *name);
extern ClientKatana *clientKatana_get_first_clientKatana(void);

void clientKatana_connect(ClientKatana * chw);
long clientKatana_ping(ClientKatana * chw);
int clientKatana_transmit(ClientKatana * chw, char *commande);
int clientKatana_transmit_receive(ClientKatana * chw, char *commande, char *result);
int clientKatana_init_connection(ClientKatana * chw);
int clientKatana_lecture_du_socket(ClientKatana * chw);
int clientKatana_ecriture_vers_socket(ClientKatana * chw);
void clientKatana_init_watchdog(ClientKatana * chw);


/********************************************************************************/
/***********************          Objet World               *********************/
/********************************************************************************/

typedef struct Landmark
{
  int x;
  int y;
  int always_visible;

} Landmark;

typedef struct Obstacle
{
  int xa;
  int ya;
  int xb;
  int yb;
  int high;

} Obstacle;

typedef struct Door
{
   int xa;
   int ya;
   int xb;
   int yb;
   int direction;
   int high;

    
} Door;

typedef struct Resource
{
  float x;
  float y;
  float radius;
  int red;
  int blue;
  int green;

} Resource;

typedef struct World
{
  /* World information */
  char *name;
  float time_constant;
  float spatial_constant;
  int width;
  int height;

  /* Ostacles */
  int nb_obstacles;
  Obstacle *obstacles;

  /* Doors */
  int nb_doors;
  Door *doors;
  
  /* Landmarks */
  int nb_landmarks;
  Landmark *landmarks;

  /* Resources */
  int nb_resources;
  Resource *resources;

} World;

extern void world_apply_hardware_config(char *argv);

extern void world_create_world(World ** world, char *name, char *filename);
extern void world_create_world_table(World ** world_table, int nb_world);
extern World *world_get_world_by_name(char *name);
extern World *world_get_first_world();
extern World *world_get_world_by_number(int num);
extern int world_get_nb_world();



/********************************************************************************/
/***********************          Objet ColorDetector       *********************/
/********************************************************************************/

#define USE_ATMEGA_V1 1
#define USE_ATMEGA_V2 2
#define USE_WORLD 3
#define USE_COLORDETECT_WEBOTS 8

typedef struct ColorDetector
{
  char *name;
  char *port;
  char clientRobubox[128];
  int autocheck;
  int colordetector_type;
} ColorDetector;


extern int colordetector_check_hardware(ColorDetector * colordetector);
extern void colordetector_apply_hardware_config(char *argv);
extern void colordetector_create_colordetector(ColorDetector ** colordetector, char *name, char *filename);
extern void colordetector_create_colordetector_table(ColorDetector ** cam_table, int nb_camera);
extern ColorDetector * colordetector_get_colordetector_by_name(char *name);
extern ColorDetector * colordetector_get_first_colordetector(void);
extern ColorDetector * colordetector_get_colordetector_by_number(int data);
extern int colordetector_get_nb_colordetector(void);
extern void colordetector_read(ColorDetector * colordetector, int * colordetector_value);
extern void contrastdetector_read(ColorDetector * colordetector, int * contrast_detect);
extern int colordetector_server_read(ColorDetector * colordetector);


/********************************************************************************/
/***********************          Objet BatteryLevel       *********************/
/********************************************************************************/

#define USE_ATMEGA_V1 1
#define USE_ATMEGA_V2 2
#define USE_WORLD 3
#define USE_COLORDETECT_WEBOTS 8

typedef struct BatteryLevel
{
  char *name;
  char *port;
  char clientRobubox[128];
  int autocheck;
  int batterylevel_type;
} BatteryLevel;


extern int batterylevel_check_hardware(BatteryLevel * batterylevel);
extern void batterylevel_apply_hardware_config(char *argv);
extern void batterylevel_create_batterylevel(BatteryLevel ** batterylevel, char *name, char *filename);
extern void batterylevel_create_batterylevel_table(BatteryLevel ** cam_table, int nb_camera);
extern BatteryLevel * batterylevel_get_batterylevel_by_name(char *name);
extern BatteryLevel * batterylevel_get_first_batterylevel(void);
extern BatteryLevel * batterylevel_get_batterylevel_by_number(int data);
extern int batterylevel_get_nb_batterylevel(void);
extern void batterylevel_read(BatteryLevel * batterylevel, int * batterylevel_value);
extern int batterylevel_server_read(BatteryLevel * batterylevel);


/********************************************************************************/
/***********************          Objet PanTilt             *********************/
/********************************************************************************/

typedef struct Biclops
Biclops;

typedef struct PMDAxisControl
PMDAxisControl;

typedef struct PanTilt
{
  /* PanTilt information */
  char name[256];
  char config_file[256];
  int type;
  int autocheck;
  int debug;

  /* Biclops information */
  Biclops *biclops;

  /* Dynamic loading of the Biclops library */
  void *libBiclops_handle;
  int (*biclops_moveall)(Biclops *, long);
  int (*biclops_moveaxis)(Biclops *, const char *, int);
  double (*biclops_getaxisactualposition_rev)(Biclops *, const char *);
  void (*biclops_setaxisprofilepos_rev)(Biclops *, const char *, double);
} PanTilt;


extern void pantilt_apply_hardware_config(char *argv);
extern void pantilt_create_pantilt(PanTilt ** pantilt, char *name, char *filename);
extern void pantilt_create_pantilt_table(PanTilt ** pantilt_table, int nb_pantilt);
extern PanTilt *pantilt_get_pantilt_by_name(char *name);
extern PanTilt *pantilt_get_first_pantilt();
extern PanTilt *pantilt_get_pantilt_by_number(int num);
extern int pantilt_get_nb_pantilt();
extern void pantilt_delete(PanTilt *pantilt);
extern void pantilt_move_axis(PanTilt *pantilt, char *axis_name, int wait_done);
extern void pantilt_move_multiple(PanTilt *pantilt, long wait_period);
extern void pantilt_set_axis_position(PanTilt *pantilt, char *axis_name, float pos);
extern double pantilt_get_proprio(PanTilt *pantilt, char *axis_name);



/********************************************************************************/
/***********************       Objet IR_localisation        *********************/
/********************************************************************************/

typedef struct IR_localisation
{
  char *name;
  char *port;
  int autocheck;
  int IR_localisation_type;
} IR_localisation;


extern void IR_localisation_apply_hardware_config(char *argv);
extern IR_localisation * IR_localisation_get_IR_localisation_by_name(char *name); 
extern IR_localisation * IR_localisation_get_first_IR_localisation(void); 
extern IR_localisation * IR_localisation_get_IR_localisation_by_number(int data); 
extern int IR_localisation_get_nb_IR_localisation(void); 
extern void IR_localisation_read(IR_localisation * IR_localisation, float * IR_localisation_value);


/*******************************************************************************/
/***********************       Objet US_Serial        *********************/
/********************************************************************************/
#define MAX_US 13

typedef struct US_Serial
{
  pthread_mutex_t lock;
  int enabled;
  int autocheck;

  char *name;
  char *port;

  int nb_us;
  float US[MAX_US];
  float angle_us[MAX_US];
  float actmax_us;
  float seuil_us;

}US_Serial;

extern void us_serial_apply_hardware_config(char *argv);
extern int us_serial_check_hardware(US_Serial * us_serial);
extern void us_serial_create_us_serial( US_Serial ** us_serial, char *name, char *file_hw);
extern void us_serial_create_us_serial_table(US_Serial ** list, int nb_rob);
extern void us_serial_delete(US_Serial *us_serial);
extern US_Serial *us_serial_get_first_us_serial();
extern void us_serial_get_us(US_Serial * us_serial, float *US );
extern US_Serial *us_serial_get_us_serial_by_name(char *name );
extern US_Serial *us_serial_get_us_serial_by_number(int number);
extern void us_serial_init(US_Serial *us_serial);

/*******************************************************************************/
/***********************       Air Sensor        *********************/
/********************************************************************************/

typedef struct AirSensor
{
  /*void*  this;*/
  char *name;
  char *port;

  int type;
  int autocheck;
  int id;


  float val_min;
  float val_max;

  float value;

} AirSensor;


extern void air_sensor_apply_hardware_config(char *argv);
extern void air_sensor_create_air_sensor(AirSensor ** air_sensor, char *name,
    char *filename);
extern void air_sensor_create_air_sensor_table(AirSensor ** cam_table, int nb_camera);
extern AirSensor *air_sensor_get_air_sensor_by_name(char *name);
extern AirSensor *air_sensor_get_first_air_sensor(void);
extern AirSensor *air_sensor_get_air_sensor_by_number(int);
extern int air_sensor_get_nb_air_sensor(void);
extern float air_sensor_read(AirSensor * air_sensor);
extern char air_sensor_get_cmd(int type);
extern char air_sensor_get_head(int type);

/*******************************************************************************/
/***********************       Serial Led        *********************/
/********************************************************************************/

#define MINLED 0
#define MAXLED 255

typedef struct LED_Serial
{
  pthread_mutex_t lock;
  int enabled;
  int autocheck;

  char *name;
  char *port;

  unsigned char r;
  unsigned char g;
  unsigned char b;

}LED_Serial;

extern void led_serial_apply_hardware_config(char *argv);
extern int led_serial_check_hardware(LED_Serial * led_serial);
extern void led_serial_create_led_serial( LED_Serial ** led_serial, char *name, char *file_hw);
extern void led_serial_create_led_serial_table(LED_Serial ** list, int nb_rob);
extern void led_serial_delete(LED_Serial *led_serial);
extern LED_Serial *led_serial_get_first_led_serial();
extern LED_Serial *led_serial_get_led_serial_by_name(char *name );
extern LED_Serial *led_serial_get_led_serial_by_number(int number);
extern void led_serial_init(LED_Serial *led_serial);
extern void led_serial_get_led(LED_Serial * led_serial, float *R, float *G, float *B );
extern void led_serial_set_led(LED_Serial * led_serial, float *R, float *G, float *B );

extern void led_serial_read_thread(LED_Serial *serial);

/*******************************************************************************/
/***********************       Serial Command        *********************/
/********************************************************************************/

#define MINCMD 0
#define MAXCMD 255

typedef struct CMD_Serial
{
  pthread_mutex_t lock;
  int enabcmd;
  int autocheck;

  char *name;
  char *port;

  unsigned char l_value;
  unsigned char vr_value;
  unsigned char vp_value;

}CMD_Serial;


extern void cmd_serial_apply_hardware_config(char *argv);
extern int cmd_serial_check_hardware(CMD_Serial * cmd_serial);
extern void cmd_serial_create_cmd_serial( CMD_Serial ** cmd_serial, char *name, char *file_hw);
extern void cmd_serial_create_cmd_serial_table(CMD_Serial ** list, int nb_rob);
extern void cmd_serial_delete(CMD_Serial *cmd_serial);
extern CMD_Serial *cmd_serial_get_first_cmd_serial();
extern CMD_Serial *cmd_serial_get_cmd_serial_by_name(char *name );
extern CMD_Serial *cmd_serial_get_cmd_serial_by_number(int number);
extern void cmd_serial_init(CMD_Serial *cmd_serial);
extern void cmd_serial_get_cmd(CMD_Serial * cmd_serial, int *VR, int *VP);
extern void cmd_serial_set_cmd(CMD_Serial * cmd_serial, int *VR, int *VP );
extern void cmd_serial_set_cmd_reset(CMD_Serial * cmd_serial, int SSC);
extern void cmd_serial_read_thread(CMD_Serial *serial);





/********************************************************************************/
/***********************          Ancienne libhardware         *********************/
/********************************************************************************/


#ifndef _CAM_POS_H
#define _CAM_POS_H

/*! informations des positions relatives (par rapport au robot)
de la camera */

typedef struct cam_pos
{
  int servo_pan;              /*!<position du moteur horizontal [0-254] */
  int servo_tilt;             /*!<position du moteur vertical [0-254] */
} cam_pos;

#endif

#endif
