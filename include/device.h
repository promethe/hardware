/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*************************************************************
Author: Arnaud Blanchard
Created: 9/11/2009

Strucure et gestion d'un device.
Un device contient 1 ou plusieurs composants de meme type.
Il peut avoir un identifiant.
Il est toujours associe a un bus (sauf si c'est lui meme un bus).

Par exemple un katana peut etre un device avec id="katana" contenant 6 composant moteurs de type katana.
	void device_init(Device *this, Node *node): initie le device et cree et initie ses composants grace au donnees de la node.

************************************************************/
#ifndef DEVICE_H
#define DEVICE_H

#include "prom_tools/include/xml_tools.h"

#include <Components/generic.h>
#include <Components/bus.h>

#define HARDWARD_LIB_PATH "bin_leto_prom/Libraries/Hardware"

typedef struct device
{
	const char *id, *type_of_the_components, *type_of_the_drivers;
	type_bus **buses;
	int number_of_buses;
	struct generic_component **components;
	int number_of_components, total_elements_nb;
	Node *node;
	void *lib_handle_of_driver;
	void *lib_handle_of_component;

	void (*flush)(struct device *device);
}Device;

void device_init(Device *self, Node *node);
void device_start(Device *self);
void device_stop(Device *self);

#endif

