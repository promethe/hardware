/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_LASER_H
#define _HARDWARE_LASER_H

#include <pthread.h>
#include <dlfcn.h>
#include <semaphore.h>
#include <Laser/urg_ctrl.h>




#define USE_SICK_LMS 1
#define USE_URG_04LX 2

typedef struct Laser
{
	/*void*  this;*/
	char *name;                     /*Nom donne dans le .dev */
	int    laser_type; 
	char *port;                     /*port relie a la Robubox */
	pthread_mutex_t mutex_laser_start;
	pthread_mutex_t mutex_laser_capture;
	int enabled;
	int fd;
	float seuil_max;
	float seuil_min;
	int nb_values;
	long  * values;                                        /* valeur en cm */
	int autocheck;
/*laser URG*/
    	urg_t * urg;
	urg_parameter_t * urg_parameters;

} Laser;

extern void laser_apply_hardware_config(char *argv);
extern void laser_create_laser(Laser ** laser, char *name,char *filename);
extern void laser_create_laser_table(Laser ** cam_table,int nb_camera);
extern Laser *laser_get_laser_by_name(char *name);
extern Laser *laser_get_first_laser(void);
extern int laser_check_hardware(Laser *);
extern void laser_init(Laser *);
extern int laser_read(Laser *);
extern long laser_get_intensity_by_degres(Laser *,float);

/* void laser_get_values(Laser *laser);*/


/*Fonction pour le SICK_LMS*/
int readLMSdata(int fd, unsigned char* buf);
int connectToLMS(int range_mode, int res_mode, int unit_mode, char * port, int baud_sel);

/* Fonction pour le URG_04LX*/
extern void (*lh_urg_disconnect)(urg_t *urg);
extern const char * (*lh_urg_error)(urg_t *urg);
extern int (*lh_urg_connect)(urg_t *urg, const char *device, long baudrate);
extern int (*lh_urg_dataMax)(urg_t *urg);
extern int (*lh_urg_requestData)(urg_t *urg,urg_request_type request_type,int first_index,int last_index);
extern int (*lh_urg_receiveData)(urg_t *urg, long data[], int data_max);
extern long (*lh_urg_maxDistance)(urg_t *urg);
extern long (*lh_urg_minDistance)(urg_t *urg);
extern int (*lh_urg_deg2index)(urg_t *urg, int degree);
extern  int (*lh_urg_parameters)(urg_t*urg ,urg_parameter_t*param);


extern void urg_exit(urg_t *urg, const char *message);
extern int laser_capture_all_laser(Laser ** laser_table);
extern int laser_stop(Laser*laser);


#endif
