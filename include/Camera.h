/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_CAMERA_H
#define _HARDWARE_CAMERA_H

#include <sys/mman.h>
#include <sys/time.h>
#include <stdint.h>

#ifdef Linux
#include <linux/types.h>
#include <linux/videodev2.h>
#else
#endif


#include <semaphore.h>
#include <pthread.h>
#include <reseau.h>

#ifdef FIREWIRE
#include <dc1394/dc1394.h>
#endif

/*camera_type*/

/*PAL , composite et webcam*/
#define USE_CAMERA_CLASSIC 1
 /*NTSC*/
#define USE_CAMPANO 2
/*client vers serverHW*/
#define USE_CLIENTHW 3
/*camera firewire*/
#define USE_FIREWIRE 4
/*player camera*/
#define USE_PLAYER 5
/* Aibo camera */
#define USE_CAM_AIBO 6
/* Video 4 Linux 2 camera */
#define USE_V4L2 7
/* Camera Robubox */
#define USE_CAM_WEBOTS 8
/* Camera NIOS */
#define USE_CAM_NIOS 9

/*mode thraeding*/

#define PAL 0
#define NTSC 1

#define NOCAMTHREAD 0
#define CAMTHREAD_NORMAL 1



#define CAMERA_WEBOTS_BLACK_WHITE 0 
#define CAMERA_WEBOTS_COLOR 1

struct buffer {
        void *                  start;
        size_t                  length;
};

typedef struct Camera
{
/*void*  this;*/
    char *port;                 /*port '/dev/video0' par exemple */
    char *name;                 /*object name in .dev file. ex: "Camera 'titi' is pal3x" */
    char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
    char clientRobubox[128];

    int desc;                   /*descriptor given by the open */
    int camera_type;

    int autocheck;

    int threading;
    int enabled;

    int color;

    int height_max;
    int width_max;
    int palette;                /*VIDEO_PALETTE_GREY if gray and VIDEO_PALETTE_RGB24 or VIDEO_PALETTE_YUV420P  if color */
				/*DC1394_COLOR_CODING_MONO8 && video_mode!= DC1394_COLOR_CODING_RGB8*/


  sem_t sem; /* Semaphore pour l attente de la mise a jour de l'image */
  pthread_mutex_t mutex_grab_image; /*Mutex pour la recopie dans le buffer de la structure*/
  
  unsigned char *img_buff;



/*read in a hardware file whiche named is found in the dev file*/
    float teta_w;
    float teta_h;

/*user defined variables*/
    int height;
    int width;
    float scale_factor;
    int norm;

/* Camera parameters variables */
    int exposure;
    int brightness;
    int gain;
    int sharpness;
    int red_balance;
    int blue_balance;
    int hue;
    int saturation;
    int gamma;
    int shutter;
    int iris;      

/*************Pour V4L2 ****************/
#ifdef Linux
    int camera_type_v4l;            /*CAM_COMPOSITE ou CAM_WEBCAM */

    struct v4l2_capability v4l2_caps;
    struct v4l2_format v4l2_fmt;
    struct v4l2_requestbuffers req_buf;
    struct v4l2_buffer *v4l2_buffers;
    struct buffer *buffers;

    int exposure_mode;

#else
/* pour Darwin ... */
#endif

/* buffer de capture*/
    int nb_buffers;
    int flush_buffers;
    unsigned char **grab_buf;

/****************dc1394_cameracapture********************/ 

    void *cam_firewire;
    void *converted_frame_firewire;
    
    int addr_bus_firewire;
    /*following param: see dc1394/types.h*/
    int video_mode;
    int framerate;
    int isospeed;

/************ Camera Aibo ***********************/
  /* Integree pour Aibo : permet de dire si l'image de la camera a ete mise a jour */
  /* Car les recpetions de messages URBI-Aibo sont asynchrones */    
  int updated;
  int format; /* Format de l'image (ycbcr, jpeg ) */
  int size; /* taille de l'image ( != de la resolution, car compression possible )*/


/****************Pour player********************/ 
    void *player_client;
    void *player_camera_proxy;

} Camera;

extern void camera_apply_hardware_config(char *argv);
extern void camera_create_camera(Camera ** cam, char *name, char *file_hw);

extern void camera_create_cam_table(Camera ** cam_table, int nb_camera);
extern Camera *camera_get_cam_by_name(char *name);
extern int camera_get_nb_camera(void);
extern Camera *camera_get_camera_by_number(int number);


extern void camera_grabimage(Camera * cam, unsigned char *im_capture,
                             int color_wanted, float scale_factor,
                             int hardscale);
extern int camera_check_hardware(Camera * cam);


extern void camera_set_scale_factor(Camera * cam, float scale);
extern void camera_set_image_size(Camera * cam, int width, int height);
extern int camera_init_capture(Camera * cam);
extern void camera_stop(Camera * cam);
extern int camera_grabimage_thread(Camera * cam);


extern int convertYCrCbtoRGB(char * sourceImage, int bufferSize, char * destinationImage);
extern int convertJPEGtoYCrCb(char *source, int sourcelen, char *dest, int *size);
extern int convertJPEGtoRGB(char *source, int sourcelen, char * dest, int *size);
extern void yuv422_to_rgb24(unsigned char *img, int width, int height);
extern void yuv420p_to_rgb24(unsigned char *img, int width, int height);
extern void switch_rb(unsigned char *img, int width, int height);

extern float	camera_get_xfov(char *name);
extern float	camera_get_yfov(char *name);

#ifdef FIREWIRE
void cleanup_and_exit(dc1394camera_t *camera);
#endif

extern int camera_set_exposure_value(Camera * cam, unsigned int value);
extern int camera_set_exposure_auto(Camera * cam);
extern int camera_set_brightness_value(Camera * cam, unsigned int value);
extern int camera_set_brightness_auto(Camera * cam);
extern int camera_set_gain_value(Camera * cam, unsigned int value);
extern int camera_set_gain_auto(Camera * cam);
extern int camera_set_sharpness_value(Camera * cam, unsigned int value);
extern int camera_set_sharpness_auto(Camera * cam);
extern int camera_set_white_balance_values(Camera * cam, unsigned int BU, unsigned int RV);
extern int camera_set_white_balance_auto(Camera * cam);
extern int camera_set_hue_value(Camera * cam, unsigned int value);
extern int camera_set_hue_auto(Camera * cam);
extern int camera_set_saturation_value(Camera * cam, unsigned int value);
extern int camera_set_saturation_auto(Camera * cam);
extern int camera_set_gamma_value(Camera * cam, unsigned int value);
extern int camera_set_gamma_auto(Camera * cam);
extern int camera_set_shutter_value(Camera * cam, unsigned int value);
extern int camera_set_shutter_auto(Camera * cam);
extern int camera_set_iris_value(Camera * cam, unsigned int value);
extern int camera_set_iris_auto(Camera * cam);

extern int camera_get_exposure_value(Camera * cam, unsigned int * value);
extern int camera_get_brightness_value(Camera * cam, unsigned int * value);
extern int camera_get_gain_value(Camera * cam, unsigned int * value);
extern int camera_get_sharpness_value(Camera * cam, unsigned int * value);
extern int camera_get_white_balance_values(Camera * cam, unsigned int* BU, unsigned int *RV);
extern int camera_get_hue_value(Camera * cam, unsigned int * value);
extern int camera_get_saturation_value(Camera * cam, unsigned int *value);
extern int camera_get_gamma_value(Camera * cam, unsigned int *value);
extern int camera_get_shutter_value(Camera * cam, unsigned int * value);
extern int camera_get_iris_value(Camera * cam, unsigned int* value);

extern int camera_get_exposure_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_gain_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_brightness_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_sharpness_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_white_balance_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_hue_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_saturation_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_gamma_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_shutter_boundaries(Camera * cam, unsigned int *min, unsigned int *max);
extern int camera_get_iris_boundaries(Camera * cam, unsigned int *min, unsigned int *max);

extern void convert_border_bayer_line_to_bgr24( uint8_t* bayer, uint8_t* adjacent_bayer, uint8_t *bgr, int width, int start_with_green, int blue_line);

extern void bayer_to_rgbbgr24(uint8_t *bayer, uint8_t *bgr, int width, int height,int start_with_green, int blue_line);

extern void bayer_to_rgb24(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order);

extern void flip_bgr_image(unsigned char *bgr_buff, int width, int height);
#endif
