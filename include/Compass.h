/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_COMPASS_H
#define _HARDWARE_COMPASS_H

#define USE_HOLTZ 1
#define USE_STAMP 2
#define USE_CLIENTHW 3
#define USE_SIMULATION 4
#define USE_PLAYER 5
#define USE_GAZEBO 6
#define USE_COMPASS_ROBUBOX 8
#define USE_COMPASS_ROBUROC 9
#define USE_COMPASS_THREAD 10

#include <TypeDef/boolean.h>
#include <net_message_debug_dist.h>

typedef struct Compass
{
/*void*  this;*/
    char *name;
    char *port;
    char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
	char clientRobubox[128];         /* Client Robubox simulé par webots */
    int compass_type;           /*1: ancienne bobine holtz du koala 2: basic stamp 3: ClientHW */
    int autocheck;
/*proxy pour le compass par player et par gazebo*/
    void *player_client;
    void *player_truth_proxy;
    float angle;/*peut aussi etre obtenu par la fonction read_compass*/
    float tilt; /*utilise par les compass compensee des roburocs*/
    float roll;/*utilise par les compass compensee des roburocs*/

    int compass_thread_value;
    int enabled;
} Compass;

extern void compass_apply_hardware_config(char *argv);
extern void compass_create_compass(Compass ** compass, char *name,
                                   char *filename);
extern void compass_create_compass_table(Compass ** cam_table, int nb_camera);
extern Compass *compass_get_compass_by_name(char *name
                                            /*, Camera ** tableau,int size */
    );
extern Compass *compass_get_first_compass(void);
extern Compass *compass_get_compass_by_number(int);
extern int compass_get_nb_compass(void);
extern float compass_read(Compass * compass);
extern int compass_server_read(Compass * compass);
extern int compass_stamp_read(Compass * compass);
extern int compass_stamp_query(Compass * compass);
extern int compass_holtz_read(Compass * compass);
extern int compass_check_hardware(Compass * compass);
extern int compass_robubox_query(Compass * compass);
extern int compass_robuboc_query(Compass * compass);
extern int compass_thread_query(Compass *compass) ;
extern void compass_init(Compass * compass);

extern void compass_read_thread(Compass *compass);

extern void compass_delete(Compass *compass) ;

#endif
