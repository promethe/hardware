/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_ROBOT_H
#define _HARDWARE_ROBOT_H

#include <pthread.h> 

#define USE_KOALA 1
#define USE_LABO3 2
#define USE_CLIENTHW 3
#define USE_SIMULATION 4
#define USE_PLAYER 5
#define USE_ROBUBOX 6
#define USE_NAO 7
#define USE_MAYA 8
#define USE_ROBUBOX_V2 9

#define READ_US 0
#define READ_IR 1

typedef struct Robot
{
    /*void*  this;*/
    char *name;                 /*nom donne dans le .dev */
    char port[128];             /*port de communicaton ex _ttyS0 */
    char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
    char clientRobubox[128];    /*nom du ClientRobubox a utiliser si il y en a un */
    char clientPlayer[128];    /*nom du ClientRobubox a utiliser si il y en a un */
    
    pthread_mutex_t lock;	// mutex pour la lecture thread��e de l'odometrie => MAYA
    int enable;
    int robot_type;             /*le type: 0:a ne pas utiliser 1:koala 2:labo3 3:ClientHW 4:simulation 5: Player 6:ClientRobubox  */
    int autocheck;   

    float angle90;              /*equivalant conteur de rout pour faire un angle droit */
    float largeur_robot;        /*largueur du robot pour integration de chemin */
    float nbt_encoder;        /*tic par tour*/
    float wheel_radius;        /*rayon de la roue du robot */
    float RS_max;
    float speed_max;

	/*Gestion des Sonars */
    int nb_ir;
    float angle_ir[16];         /*angle de laser ou sonar du premier au derneir en relatif */
    float IR[16];
    float seuil_ir;
    float actmax_ir;
	
	/*Gestion des Infra-Rouges */
    int nb_ir_sensor;
    float angle_ir_sensor[16];         /*angle de laser ou sonar du premier au derneir en relatif */
    float IR_SENSOR[16];
    float seuil_ir_sensor;
    float actmax_ir_sensor;

    void *player_client;
   
    /* Ajout Vincent */
    /* Valeurs d'odometrie (mises a jour par la fonction robot_get_odometry()) */
    double distance ;
    double angle ;

    double angle_rb2; //correction bug robubox v2
    
    int nb_clics_gauches ;
    int nb_clics_droits ;
    /* Nombre de clics a partir duquel on risque une remise a zero des compteurs non controlee */
    int nb_clics_max ; 

    /* Valeurs de position */ 
    float posx_odo; /* Position calculee par odometrie par rapport a une position "reset"*/
    float posy_odo;
    float orientation_odo;
    
    float posx_odo_th; /* Position calculee par odometrie par rapport a une position "reset"*/
    float posy_odo_th;
    float orientation_odo_th;

    float posx_abs; /* Position dans un referentiel absolu */
    float posy_abs;
    float orientation_abs;

    float default_posx; /* Position servant de point de reference pour la position odometrie */
    float default_posy;
    float default_orientation;

    /* Valeurs pour le robot simule */
    float last_posx_abs; /* Derniere position pour simuler l'odometrie */
    float last_posy_abs;
    float last_orientation_abs;
    float last_orientation_abs_rb2; // correction bug robubox
    int current_landmark; /* Landmark courant */
    int mvt_sens;

    float current_divider; // current max for each motor

} Robot;

extern void robot_apply_hardware_config(char *argv);
extern void robot_create_robot(Robot ** robot, char *name, char *filename);
extern void robot_create_robot_table(Robot ** cam_table, int nb_camera);
void robot_delete(Robot *robot);


extern Robot *robot_get_robot_by_name(char *name
                                      /*, Camera ** tableau,int size */ );
extern Robot *robot_get_first_robot(void);
extern Robot *robot_get_robot_by_number(int num);
extern int robot_get_nb_robot(void);
extern int robot_get_robot_type(Robot * robot);
/*movement*/

extern void robot_init_wheels_counter(Robot * robot);
extern void robot_read_wheels_counter(Robot * robot, int *DistL, int *DistR);
extern void robot_go_by_position(Robot * robot, float Left, float Right);
extern void robot_turn_angle(Robot * robot, float angle);   /*remplace tunr_left et turn_right */
extern void robot_go_by_speed(Robot * robot, float Left, float Right);
extern void robot_get_odometry(Robot * robot); 
extern void robot_reset_odo(Robot * robot);
extern void robot_get_location_odo(Robot * robot);
extern void robot_reset_location_odo(Robot * robot);
extern void robot_set_location_abs(Robot * robot, float posx, float posy);
extern void robot_get_location_abs(Robot * robot);
extern void robot_go_forward(Robot * robot, float LR);
extern void robot_go_by_position_with_odo(Robot * robot, float Left,
                                          float Right);
extern void robot_set_velocity(Robot * robot, float speed, float sideway,
                               float agular_speed, int state);

extern int robot_serial_write(Robot * robot, char *buff, char c,
                              char *retour);
extern int robot_commande_koala(Robot * robot, char cmd, int *param,
                                    char *data);
extern int robot_precise_last_target_reached(Robot * robot);
extern int robot_last_target_reached(Robot * robot);

extern void robot_get_ir(Robot * r, float *ir); /* recupere les UltraSon => le nom est mal choisi  */
extern void robot_get_ir_sensor(Robot * r, float *ir); /* ajouter pour lire la valeur des InfraRed Robubox v2 */
extern void robot_set_led(Robot * r,int led,int action);
/*Ancienne fonction a include*/
extern void robot_turn_right(Robot r, float angle);
extern void robot_turn_left(Robot r, float angle);
 /* extern void robot_set_velocity(Robot * r, int vd,int vg); */
extern void robot_set_wheels_position(Robot * r, int pd, int pg);

extern int robot_check_hardware(Robot*r);
extern void robot_get_odometry_thread(Robot *robot);
extern void robot_get_motor_power(Robot * robot, float* left, float *right);

void robot_get_one_landmark(Robot * robot, int *landmark_id, float *azimuth, float *elevation);
void robot_reset_landmarks(Robot *robot);



#define TIMEOUT 20000
#define COM_A 'A'               /* Configure                            */
            /*--------------------------------------*/
#define COM_B 'B'               /* Read software version                */
            /*--------------------------------------*/
#define COM_C 'C'               /* Set a position to be reached         */
            /*--------------------------------------*/
#define COM_D 'D'               /* Set speed                            */
            /*--------------------------------------*/
#define	COM_E 'E'               /* Read speed                           */
            /*--------------------------------------*/
#define COM_F 'F'               /* Configure the position PID controller */
            /*--------------------------------------*/
#define COM_G 'G'               /* Set position to the position counter */
            /*--------------------------------------*/
#define COM_H 'H'               /* Read position                        */
            /*--------------------------------------*/
#define COM_I 'I'               /* Read I/A input                       */
            /*--------------------------------------*/
#define COM_J 'J'               /* Configure the speed profile controller */
            /*--------------------------------------*/
#define COM_K 'K'               /* Read the status of the mottion controller */
            /*--------------------------------------*/
#define COM_L 'L'               /* Change LED state                     */
            /*--------------------------------------*/
#define COM_N 'N'               /* Read the proximity sensors           */
            /*--------------------------------------*/
#define COM_O 'O'               /* Read ambient light sensors           */
            /*--------------------------------------*/
#define COM_T 'T'               /* Send a message to an additional module */
            /*--------------------------------------*/
#define COM_R 'R'               /* Read a byte on the extension bus     */
            /*--------------------------------------*/
#define COM_W 'W'               /* Write a byte on the extension bus    */
            /*--------------------------------------*/
#define COM_Q 'Q'               /* Write a bit on a output              */
            /*--------------------------------------*/
#define COM_P 'P'               /* Set the desired PWM amplitude        */
            /*--------------------------------------*/
#define COM_Y 'Y'               /* Read the digital inputs              */
            /*--------------------------------------*/
#define COM_Z 'Z'               /* User Defined     */
            /*--------------------------------------*/


/* Protocole de com MAYA  */

#define SYNC_BYTE 0x00
#define GET_ENCODERS 0x25
#define GET_VI 0x2C
#define SET_SPEED_1 0x31
#define SET_SPEED_2 0x32
#define SET_TURN 0x32
#define SET_MODE  0x34
#define RESET_ENCODERS 0x35
#define SET_ACCELERATION 0x33

#define DACCELERATION 5 

#define ZSPEED 0x80
#define MPSPEED 0xFF
#define MNSPEED 0x00

#define MODE_TURN 0x02
#define MODE_STRAIGHT 0x00

#endif
