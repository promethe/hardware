/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_WORLD_H
#define _HARDWARE_WORLD_H

typedef struct Landmark
{
   int x;
   int y;
   int always_visible;
    
} Landmark;

typedef struct Obstacle
{
   int xa;
   int ya;
   int xb;
   int yb;
   int high;
    
} Obstacle;

typedef struct Door
{
   int xa;
   int ya;
   int xb;
   int yb;
   int direction;
   int high;

    
} Door;

typedef struct Resource
{
   float x;
   float y;
   float radius;
   int red;
   int blue;
   int green;
    
} Resource;

typedef struct World
{
      /* World information */
      char *name;
      float time_constant;
      float spatial_constant;
      int width;
      int height;

      /* Ostacles */
      int nb_obstacles;
      Obstacle *obstacles;

	  /* Doors */ /* A door is an obstacle that can be crossed only in one direction: 1 is north to south, 2 is east to west, 3 is south to north and 4 is west to east*/
      int nb_doors;
      Door *doors;
	  
      /* Landmarks */
      int nb_landmarks;
      Landmark *landmarks;

      /* Resources */
      int nb_resources;
      Resource *resources;

} World;

extern void world_apply_hardware_config(char *argv);
extern void world_create_world(World ** world, char *name, char *filename);
extern void world_create_world_table(World ** world_table, int nb_world);
extern World *world_get_world_by_name(char *name);
extern World *world_get_first_world();
extern World *world_get_world_by_number(int num);
extern int world_get_nb_world();
extern void world_get_floor_color(World *world, float pos_x, float pos_y, int *colors);

#endif
