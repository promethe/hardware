/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_CLIENTROBUBOX_H
#define _HARDWARE_CLIENTROBUBOX_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

/* TODO: tests taille de buffer et taille image dans Webots */
#define MAXLINE 57600

typedef struct ClientRobubox
{
	/*void*  this;*/
	char *name;                     /*Nom donne dans le .dev */
	int port;                       /*port 6665 */
	char *host;                     /*IP_host */
	
	int timeout_delay;
	
	pthread_mutex_t mutex_socket;
	int sockfd;                     /*socket de communication */
	struct sockaddr_in serv_addr;   /*structure du serveur */
	char buff[MAXLINE];             /*buffer de reception des message */
	/*int    clientRobubox_type; */
	
	int ping;
	long compteur;

} ClientRobubox;

extern void clientRobubox_apply_hardware_config(char *argv);
extern void clientRobubox_create_clientRobubox(ClientRobubox ** clientRobubox, char *name,char *filename);
extern void clientRobubox_create_clientRobubox_table(ClientRobubox ** cam_table,int nb_camera);
extern ClientRobubox *clientRobubox_get_clientRobubox_by_name(char *name);
extern ClientRobubox *clientRobubox_get_first_clientRobubox(void);

void clientRobubox_connect(ClientRobubox * chw);
long clientRobubox_ping(ClientRobubox * chw);
int clientRobubox_transmit(ClientRobubox * chw, char *commande);
int clientRobubox_transmit_receive(ClientRobubox * chw, char *commande, char *result);
int clientRobubox_init_connection(ClientRobubox * chw);
int clientRobubox_lecture_du_socket(ClientRobubox * chw);
int clientRobubox_ecriture_vers_socket(ClientRobubox * chw);
void clientRobubox_init_watchdog(ClientRobubox * chw);
#endif
