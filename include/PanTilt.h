/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_PANTILT_H
#define _HARDWARE_PANTILT_H

#include <dlfcn.h>
#include <net_message_debug_dist.h>

typedef struct Biclops
Biclops;

typedef struct PMDAxisControl
PMDAxisControl;

typedef struct PanTilt
{
      /* PanTilt information */
      char name[256];
      char config_file[256];
      int type;
      int autocheck;
      int debug;

      /* Biclops information */
      Biclops *biclops;

      /* Dynamic loading of the Biclops library */
      void *libBiclops_handle;
      int (*biclops_moveall)(Biclops *, long);
      int (*biclops_moveaxis)(Biclops *, const char *, int);
      double (*biclops_getaxisactualposition_rev)(Biclops *, const char *);
      void (*biclops_setaxisprofilepos_rev)(Biclops *, const char *, double);
} PanTilt;


extern void pantilt_apply_hardware_config(char *argv);
extern void pantilt_create_pantilt(PanTilt ** pantilt, char *name, char *filename);
extern void pantilt_create_pantilt_table(PanTilt ** pantilt_table, int nb_pantilt);
extern PanTilt *pantilt_get_pantilt_by_name(char *name);
extern PanTilt *pantilt_get_first_pantilt();
extern PanTilt *pantilt_get_pantilt_by_number(int num);
extern int pantilt_get_nb_pantilt();
extern void pantilt_delete(PanTilt *pantilt);
extern void pantilt_move_axis(PanTilt *pantilt, char *axis_name, int wait_done);
extern void pantilt_move_multiple(PanTilt *pantilt, long wait_period);
extern void pantilt_set_axis_position(PanTilt *pantilt, char *axis_name, float pos);
extern double pantilt_get_proprio(PanTilt *pantilt, char *axis_name);

#endif
