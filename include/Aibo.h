/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_AIBO_H
#define _HARDWARE_AIBO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>

/***Client de communication avec AIBO****/
typedef struct Aibo_Client
{

    char name[50];              /* Nom du Client au cas ou plusieur existent */

    char port[10];
           /***Port de connection***/

    char IP[50];
         /***Ip de l'AIBO***/

    int socket;
       /****Socket de connection*/

    char buffer[4000];
          /**Buffer d'envoi***/

    char request[4000];
           /****Contient toutes les demandes de valeurs de device(joint,sensor micro)*/

    char recvbuffer[4000];
              /****Buffer de recption***/

    char binary_buffer[128000];
               /***Buffer binaire pour images et son ***/

} Aibo_Client;

extern void create_Aibo_Client(char *name, char *file);

extern int connect_Aibo_Client(char *ip, char *port);

extern int aibo_recv_message(void);

extern int aibo_send_message(char *buffer, int size);

extern int aibo_send_message_camera(void *cam, char *buffer, int size);

extern void *aibo_recv_tagged_message(void *ptr);

extern int Aibo_Client_send(void);

extern void close_Aibo_Client(void);

extern void aibo_apply_hardware_config(char *argv);

extern Aibo_Client *get_Aibo_Client(void);

extern int get_Tag(char *, char *);

extern int get_Reponse(char *, char *);

extern int traite_Reponse(char *tag, char *reponse);

extern int traite_Reponse_Binaire(char *reponse);

struct timeval get_CurrentTime(void);

int get_Update(void);

void set_Unupdate(void);

void add_to_buffer(char *);

void get_All_Val(void);

void add_request(char *name, char *field1, char *field2);

/*****Camera de l'AIBO********/
typedef struct Aibo_Cam
{
    int format;                 /* 0: raw , 1 : jpeg */
    char name[50];
    char *image;
    int size;                   /* taille de l'image binaire recuperee */
    int sx;
    int sy;
    char mode[20];
    int num;
    int updated;
} Aibo_Cam;

extern Aibo_Cam *get_Aibo_Cam(void);

extern void create_Aibo_Cam(char *name, char *file);

/****Micros de l'AIBO****/
typedef struct Aibo_Micro
{

    char name[50];

    char *trame;

    int updated;

} Aibo_Micro;

extern Aibo_Micro *get_Aibo_Micro(void);

extern void create_Aibo_Micro(char *name, char *file);


/*******Senseurs de l'AIBO**********/
typedef struct Aibo_Sensor
{

    struct Aibo_Sensor *first;

    struct Aibo_Sensor *next;

    char name[50];

    int num;

    float valn;

    int updated;

} Aibo_Sensor;

extern Aibo_Sensor *get_Aibo_Sensor(void);

extern void create_Aibo_Sensor(char *name, char *file);

extern Aibo_Sensor *get_Aibo_Sensor_by_name(char *name);

extern Aibo_Sensor *get_Aibo_Sensor_by_num(int num);

/*******Joint de l'AIBO*************/
typedef struct Aibo_Joint
{

    struct Aibo_Joint *first;

    struct Aibo_Joint *next;

    char name[50];

    int num;

    float valn;

    int updated;

} Aibo_Joint;

extern Aibo_Joint *get_Aibo_Joint(void);

extern void create_Aibo_Joint(char *name, char *file);

extern Aibo_Joint *get_Aibo_Joint_by_name(char *);

extern Aibo_Joint *get_Aibo_Joint_by_num(int num);

extern void set_valn(int num, float valn);
#endif
