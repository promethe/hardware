/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_MOUSE_H
#define _HARDWARE_MOUSE_H

#include <pthread.h>

#ifdef Linux
#include <linux/input.h>
#endif

#include <net_message_debug_dist.h>

/*#define MOUSE_MAX_VALUE 32767

#define MOUSE_TYPE1_NAME "Saitek Cyborg USB Stick"
#define MOUSE_TYPE2_NAME "Logitech Logitech Freedom 2.4"*/

typedef struct Mouse
{
      /* Mouse information */
      char name[256];
      char port[256];
      int fd;

      pthread_mutex_t lock; /*Mutex pour la recopie dans le buffer de la structure*/

      /*int type;*/
      float sensitivity;

      int enabled;
      int autocheck;

      int *buttons;
      float *axes;

      int nb_buttons;
      int nb_axes;
     /* int use_emergency;*/
} Mouse;


extern void mouse_apply_hardware_config(char *argv);
extern void mouse_create_mouse(Mouse ** mouse, char *name, char *filename);
extern void mouse_create_mouse_table(Mouse ** mouse_table, int nb_mouse);
extern Mouse *mouse_get_mouse_by_name(char *name);
extern Mouse *mouse_get_first_mouse();
extern Mouse *mouse_get_mouse_by_number(int num);
extern int mouse_get_nb_mouse();
extern void mouse_init(Mouse *mouse);
extern void mouse_delete(Mouse *mouse);
extern void mouse_read_thread(Mouse *mouse);

extern float mouse_get_axis_position(Mouse *mouse, int axis_nb);
extern int mouse_get_button_value(Mouse *mouse, int button_nb);

#endif
