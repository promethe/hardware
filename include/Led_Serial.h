/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_LED_SERIAL_H
#define _HARDWARE_LED_SERIAL_H

#include <pthread.h>
#include <net_message_debug_dist.h>

#define MINLED 0
#define MAXLED 255

typedef struct LED_Serial
{
	pthread_mutex_t lock;
	int enabled;
	int autocheck;

	char *name;
	char *port;

	unsigned char r;
	unsigned char g;
	unsigned char b;

}LED_Serial;

extern void led_serial_apply_hardware_config(char *argv);
extern int led_serial_check_hardware(LED_Serial * led_serial);
extern void led_serial_create_led_serial( LED_Serial ** led_serial, char *name, char *file_hw);
extern void led_serial_create_led_serial_table(LED_Serial ** list, int nb_rob);
extern void led_serial_delete(LED_Serial *led_serial);
extern LED_Serial *led_serial_get_first_led_serial();
extern LED_Serial *led_serial_get_led_serial_by_name(char *name );
extern LED_Serial *led_serial_get_led_serial_by_number(int number);
extern void led_serial_init(LED_Serial *led_serial);
extern void led_serial_get_led(LED_Serial * led_serial, float *R, float *G, float *B );
extern void led_serial_set_led(LED_Serial * led_serial, float *R, float *G, float *B );

extern void led_serial_read_thread(LED_Serial *serial);

#endif
