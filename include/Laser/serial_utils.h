/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef SERIAL_UTILS_H
#define SERIAL_UTILS_H

/*
  \file
  \brief  Supplementary functions of serial sending and receiving

  \author Satofumi KAMIMURA

  $Id: serial_utils.h 143 2008-08-05 18:39:57Z satofumi $

  \todo Const is to be add to the input argument.
*/

#include "serial_t.h"


/*
  \brief The line feed code is returned.

  \retval true If character is LF or CR
  \retval false If character is not LF or CR
*/
extern int serial_isLF(const char ch);


/*
  \brief Skip the receive data

  ConnectionInterface::clear() �Ƃ́A�^�C���A�E�g���Ԃ��w�肵�ēǂݔ�΂���_���قȂ�

  \param[in,out] serial Structure of serial control
  \param[in] total_timeout Upper bound value of timeout  [msec]
  \param[in] each_timeout Upper bound value of timeout occur between reception of each data [msec]
*/
extern void serial_skip(serial_t *serial, int total_timeout,
                        int each_timeout);


/*
  \brief Read upto line feed

  Add "\\0" at the end of character array and return

  \param[in,out] serial Structure of serial control
  \param[in] data Buffer to store received data
  \param[in] data_size_max Maximum size of receive buffer
  \param[in] timeout Time out [msec]

  \return Number of received bytes
*/
extern int serial_getLine(serial_t *serial,
                          char *data, int data_size_max, int timeout);

#endif /* !SERIAL_UTILS_H */
