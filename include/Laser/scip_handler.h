/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef C_SCIP_HANDLER_H
#define C_SCIP_HANDLER_H

/*!
  \file
  \brief  Process SKIP commands

  \author Satofumi KAMIMURA

  $Id: scip_handler.h 542 2009-02-03 07:35:52Z satofumi $
*/

#include "urg_parameter_t.h"
#include "serial_t.h"


enum {
  ScipNoWaitReply = 0,       /*!< Dont wait for reply */
  ScipWaitReply = 1,          /*!< Wait for reply */

  ScipLineWidth = 64 + 1 + 1  /*!< Maximum length of one line */
};


/*!
  \brief Send command

  \param[out] serial Structure of serial control
  \param[in] send_command Command to be sent

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_send(serial_t *serial, const char *send_command);


/*!
  \brief Receive response of the command.

  Store the response of the command if "ret" value is zero.\n
  �܂��Aexpected_ret �ɂ́A����Ƃ݂Ȃ��Ă悢�R�}���h�������A�I�[�� -1 �̔z��Œ�`�ł���B�R�}���h������ expected_ret �Ɋ܂܂��ꍇ�A���̊֐��̖߂�l�̓[��(����)�ƂȂ�B\n
  expected_ret �����݂���̂́A���݂̏�Ԃɐݒ肷��悤�ɃR�}���h���w�������ꍇ�ɁA�R�}���h�����Ƃ��Ă̓[���ȊO���Ԃ���Ă��܂��̂��A����Ƃ݂Ȃ��������߁B
  \param[out] serial Structure of serial control
  \param[out] command_first
  \param[out] return_code Return value
  \param[in] expected_ret Return value considered to be normal
  \param[in] timeout Time out [msec]

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_recv(serial_t *serial, const char *command_first,
                     int* return_code, int expected_ret[],
                     int timeout);


/*!
  \brief Transit to SCIP2.0 mode

  Return 0(Normal) when changed to SCIP2.0 mode

  \param[in,out] serial Structure of serial control

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_scip20(serial_t *serial);


/*!
  \brief Stop measurement and turn off the laser.

  If the purpose is to stop MD, then send QT command without waiting for the response from MD command.
  Process the response of QT in urg_receiveData()

  \param[in,out] serial Structure of serial control
  \param[in] return_code Response from QT command
  \param[in] wait_reply ScipNoWaitReply when response is not waited.
                        ScipWaitReply when response is waited.

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_qt(serial_t *serial, int *return_code, int wait_reply);


/*!
  \brief Get Parameter information

  \param[in,out] serial Structure of serial control
  \param[out] parameters urg_parameter_t Structure member

  \retval 0 Normal
  \retval < 0 Error

*/
extern int scip_pp(serial_t *serial, urg_parameter_t *parameters);


/*!
  \brief Get version information

  \param[in,out] serial Structure of serial control
  \param[out] lines Storage location of characters containing version information.
  \param[in] lines_max Maximum number of character string

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_vv(serial_t *serial, char *lines[], int lines_max);


/*!
  \brief Change baudrate

  \param[in,out] serial Structure of serial control
  \param[in] baudrate Baudrate

  \retval 0 Normal
  \retval < 0 Error
*/
extern int scip_ss(serial_t *serial, long baudrate);

#endif /* !C_SCIP_HANDLER_H */
