/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef C_SERIAL_CTRL_H
#define C_SERIAL_CTRL_H

/*!
  \file
  \brief Serial communication

  Serial Communication Interface 

  \author Satofumi KAMIMURA

  $Id: serial_ctrl.h 143 2008-08-05 18:39:57Z satofumi $
*/

#include <Laser/serial_t.h>


/*!
  \brief Connection

  \param[in,out] serial Struct of serial handle
  \param[in] device Connected device
  \param[in] baudrate Connected baud rate

  \retval 0 Normal
  \retval < 0 Error
*/
extern int serial_connect(serial_t *serial, const char *device, long baudrate);


/*!
  \brief Disconnect

  \param[in,out] serial Struct of serial handle
*/
extern void serial_disconnect(serial_t *serial);


/*!
  \brief Checks whether device is connected or not and returns the result.

  \param[in,out] serial Struct of serial handle

  \retval 1 Connected
  \retval 0 Disconnected
*/
extern int serial_isConnected(serial_t *serial);


/*!
  \brief Change baudrate

  \param[in,out] serial Struct of serial handle
  \param[in] baudrate Baud rate

  \retval 0 Normal
  \retval < 0 Error
*/
extern int serial_setBaudrate(serial_t *serial, long baudrate);


/*!
  \brief Communication

  \param[in,out] serial Struct of serial handle
  \param[in] data Data to be transmitted.
  \param[in] data_size Data size

  \retval >= 0 Transmitted data size
  \retval < 0 Error
*/
extern int urg_serial_send(serial_t *serial, const char *data, int data_size);


/*!
  \brief Reception

  \param[in,out] serial Struct of serial handle
  \param[in] data Buffer to store received data
  \param[in] data_size_max Maximum size of receive buffer
  \param[in] timeout Time out [msec]

  \retval >= 0 Received data size
  \retval < 0 Error
*/
extern int serial_recv(serial_t *serial,
                       char *data, int data_size_max, int timeout);


/*!
  \brief pushes a read byte back to  buffer

  \param[in,out] serial Struct of serial handle
  \param[in] ch pushed back character

  \attention Should NOT push the character back to buffer without reading the buffer.
*/
extern void serial_ungetc(serial_t *serial, char ch);


#endif /* !C_SERIAL_CTRL_H */
