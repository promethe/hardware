/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_JOINT_H
#define _HARDWARE_JOINT_H

#include <semaphore.h>

#define USE_SERIAL_SSC2 1   /*SSC2*/
#define USE_SERIAL_SSC12 2   /*SSC12*/
#define USE_CLIENTHW 3
#define USE_AIBO 4
#define USE_PLAYER 5
#define USE_SERIAL_SSC32 6   /*SSC32*/
#define USE_MD23 7
#define USE_AX12_2POB 8
#define USE_AX12_USB2DYN 9

typedef struct Motor
{

    char name[128];             /* nom de l'objet dans le fichier .dev ex: "Motor 'titi' is file.hws */
    char port_name[128];        /* nom du port utilise par le motor _ttyS0, doit avoir ete defini dans le .dev */
    char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
    int type;
    int autocheck;

    float position;             /*position du motor : proprio */

    float proprio;

    int adress_bus;                    /* num du motor sur le bus de la carte ssc */




    /* Integree pour Aibo : permet de dire si la valeur de proprioception a ete mise a jour */
    /* Car les recpetions de messages URBI-Aibo sont asynchrones */    
    int updated;

    /* commande minimale et maximale pour le motor */
    int start;
    int stop;

    int init;                   /* valeur de la commande a laquelle il s'initialise */
    int zero;                   /* valeur de la commande a laquelle on considere qu'il est a 0� */

    int range;                  /* debattement authorise */

    int rotation;               /* sens de rotation du motor
                                   1: sens de rotation normale
                                   -1: sens de rotation inverse
                                 */

    int vitesse;                   /* vitesse maximale de rotation */
    
    int Vmax;
    int Vdefault;               /* vitesse par defaut de rotation */
    float Vmult;                /* coefficient multiplicatif de la vitesse */

    void *player_client;
    void *player_ptz_proxy;

    sem_t sem;  /* Semaphore pour l attente de la mise a jour d'une valeur (hors camera et micro) pour Aibo */
} Motor;


extern void motor_apply_hardware_config(char *argv);
extern void motor_create_motor(Motor ** serv, char *name, char *type);
extern void motor_create_motor_table(Motor ** list, int nb_serv);
extern void motor_init(Motor * motor);
extern Motor *motor_get_serv_by_name(char *name);
extern Motor *motor_get_motor_by_number(int number);
extern int motor_get_nb_motor(void);

extern int motor_get_range_by_name(char *name);
extern void motor_command_position(Motor * motor, float pct, int speed);
extern void motor_command_position_wait_target(Motor * motor, float pct, int speed);

extern int motor_is_moving(Motor *motor);
extern void motor_get_proprio(Motor *serv);
extern void motor_command_speed(Motor *serv, float speed);

extern void motor_check_hardware(Motor * serv);

 
#endif
