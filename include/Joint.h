/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_JOINT_H
#define _HARDWARE_JOINT_H

#include <semaphore.h>
#include <net_message_debug_dist.h>

#define USE_SERIAL 1
#define USE_CLIENTHW 3
#define USE_AIBO 4
#define USE_PLAYER 5
#define USE_BERENSON 6
#define USE_JOINT_WEBOTS 8

typedef struct Joint
{

    char name[128];             /* nom de l'objet dans le fichier .dev ex: "Servo 'titi' is file.hws */
    char port_name[128];        /* nom du port utilise par le servo _ttyS0, doit avoir ete defini dans le .dev */
    char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
	char clientRobubox[128];
    int type;

    int autocheck;

    float position;
    float proprio;
    char type_controlleur[128]; /*type de controller (ssc/pic...) */
    int ssc;                    /* n� de la carte ssc */

    /* Integree pour Aibo : permet de dire si la valeur de proprioception a ete mise a jour */
    /* Car les recpetions de messages URBI-Aibo sont asynchrones */    
    int updated;
    int enabled;

    /* commande minimale et maximale pour le servo */
    int start;
    int stop;

    int init;                   /* valeur de la commande a laquelle il s'initialise */
    int zero;                   /* valeur de la commande a laquelle on considere qu'il est a 0� */

    int range;                  /* debattement authorise */

    int rotation;               /* sens de rotation du servo
                                   1: sens de rotation normale
                                   -1: sens de rotation inverse
                                 */

    int Vmax;                   /* vitesse maximale de rotation */
    int Vdefault;               /* vitesse par defaut de rotation */
    float Vmult;                /* coefficient multiplicatif de la vitesse */
    
    /*Proxy pour avoir le ptz via player */
    void *player_client;
    void *player_ptz_proxy;

    sem_t sem;  /* Semaphore pour l attente de la mise a jour d'une valeur (hors camera et micro) pour Aibo */
} Joint;


extern void joint_apply_hardware_config(char *argv);
extern void joint_create_joint(Joint ** serv, char *name, char *type);
extern void joint_create_joint_table(Joint ** list, int nb_serv);
extern Joint *joint_get_serv_by_name(const char *name);
extern Joint *joint_get_joint_by_number(int number);
extern int joint_get_nb_joint(void);

extern int joint_get_range_by_name(char *name);
extern void joint_servo_command(Joint * joint, float pct, int speed);
extern void joint_init(Joint *serv);
extern void	joint_get_proprio(Joint *serv);
extern void joint_speedo_command(Joint *serv, float speed); 
extern float read_biclops(Joint *serv);
extern void read_biclops_thread(Joint *serv);
extern void joint_delete(Joint * serv);


extern void joint_check_hardware(Joint * serv);

extern void arm_speed_init_command();

extern void joint_activate_servo(Joint *serv, float activ);

#endif
