/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_SERIAL_H
#define _HARDWARE_SERIAL_H

#include <pthread.h>
#include <net_message_debug_dist.h>

typedef struct Serial
{

    char *name;                 /* nom de l'objet dans le .dev ex: _ttyS0 */
    char path[128];             /* chemin du port '/dev/ttyS0' */

    int desc;                   /*  descripteur donne a l ouverture du port */

    /*parametre de configuration de l UART ex 9600 8 N 2 */
    char debit[128];
    char bit[128];
    char parite[128];
    char stop[128];
    int vmin;	/* paramètres vmin et vtime , permet de choisir entre une lecture bloquante et non bloquante   */
    int vtime;	
    int icanon;	
    int onblock;	
    pthread_mutex_t mutex_serial;   /*evite d'envoyer 2 ordres en meme temps... */

} Serial;


  /*Fonction de creation objet */
extern void serial_apply_hardware_config(char *argv);   /*public */
extern void serial_create_serial(Serial ** serial, char *name, char *file_hw);  /*private */
extern void serial_create_serial_table(Serial ** list, int nb_rob); /*private */
  /*deprecated, remplacer par serial_create_serail
     extern char * serial_init(char *name, char *path);
   */

  /*Fonction de recuperation des objets */
extern Serial *serial_get_serial_by_path(char *path);   /*private */
extern Serial *serial_get_serial_by_name(char *name);   /*private */

  /*Fonction d'utilisation des objets */
extern int serial_send_and_receive(char *port_name, char *commande, char *retour, int lenght_in);  /*private */
extern int serial_send_and_receive_block(char *port_name, char *commande, char *retour,int lenght, time_t timeout_usec);
extern int serial_receive(char *port_name, char *retour);
extern int serial_receive_block(char *port_name, char *retour,time_t timeout_usec);
extern int serial_send_and_receive_nchar(char *port_name, char *commande, char *retour, int lenght_in,int length_out);  /*private */
extern int serial_send_and_receive_line(char *port_name, char *commande, char *retour, int lenght);
extern int serial_send(char *port_name, char *commande, int lenght_in);    /*private */
extern char serial_send_and_readchar(char *port_name, char *commande, int lenght_in);
extern int serial_send_and_receive_ax12(char *port_name, unsigned char *commande, unsigned char *retour, unsigned char lenght_in, unsigned char * length_out);

  /*  Fonction private */
extern int serial_setparam(char *port_name, char *speed, char *bits, char *par, char *stopb);   /*private */
#endif
