/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef COMPONENT_TOOLS_H
#define COMPONENT_TOOLS_H

#include <dlfcn.h>

/** Lie le pointeur de fonction du composant passe en parametre avec la fonction du driver du meme nom. S'il n'existe pas, la fonction pointe vers missing_name_of_fonction qui elle aurra été crée par CREATE_MISSING */
#define LINK_DRIVER(this, name_of_function)																\
this->name_of_function = NULL;                                            \
try_to_link_function_with_library(&(this->name_of_function), this->device->lib_handle_of_driver, #name_of_function);		\
if (this->name_of_function == NULL) this->name_of_function = (typeof(this->name_of_function))missing_##name_of_function;


/** Cree les fonctions par des composants manquantes dans les drivers. Elles genereront le bon message d'alerte si elles sont appellees. */
#define CREATE_MISSING(name_of_function)	\
void missing_##name_of_function(type_generic_component *component)	\
{															\
	EXIT_ON_ERROR("%s has not been implemented in the driver '%s' for the component of type '%s'.", #name_of_function, component->device->type_of_the_drivers, component->device->type_of_the_components); \
}

#endif

