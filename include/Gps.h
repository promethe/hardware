/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _HARDWARE_ROBOT_H
#define _HARDWARE_ROBOT_H

#define USE_GARMIN 			1
#define USE_CLIENTHW		3
#define USE_WEBOTS_GPS		4
#define USE_DSGPMS_OD		5

#include <TypeDef/boolean.h>

#include <sys/socket.h>
#include <netinet/in.h>

#define MAXLINE 57600  /* Value taken from ClientRobubox.h that also handles socket connection with webots */

typedef struct Gps_value
{
   float UTC_date;
   float day;
   float month;
   float year;
   float UTC_time;
   float hour;
   float minute;
   float second;
   float nord;
   float est;
   float altitude;
   float speed;
   float compass;
} Gps_value;

typedef struct Gps
{
   /*void*  this;*/
   char *name;
   char *port;
   char clientHW[128];         /*nom du ClientHW a utiliser si il y en a un */
   int gps_type;           /*1: garmin 3: ClientHW 4: GPSwebots*/
   /*proxy pour le gps par player et par gazebo*/
   void *player_client;
   void *player_truth_proxy;
   char *udphost ;				/* Added for gps_type USE_WEBOTS_GPS */
   int udpport;				/* Added for gps_type USE_WEBOTS_GPS */
   int sockfd;					/* Added for gps_type USE_WEBOTS_GPS */
   struct sockaddr_in serv_addr;	/* Added for gps_type USE_WEBOTS_GPS */
   char buff[MAXLINE]; 		/* Added for gps_type USE_WEBOTS_GPS */
   Gps_value *gps_value;
} Gps;


/*fonction de creation de l'objet*/
extern void gps_apply_hardware_config(char *argv);
/*private*/ extern void gps_create_gps(Gps ** gps, char *name, char *filename);
/*private*/ extern void gps_create_gps_table(Gps ** cam_table, int nb_camera);

/*fonction de recuperation  de l'objet*/
extern Gps *gps_get_gps_by_name(char *name /*, Camera ** tableau,int size */ );
extern Gps *gps_get_first_gps(void);
extern Gps *gps_get_gps_by_number(int);
extern int gps_get_nb_gps(void);

/*fonction d utilisation de l objet*/
extern void gps_read(Gps * gps);

/*fonction hardware de lecture*/
/*private*/ extern void gps_server_read(Gps * gps);
/*private*/ extern void gps_garmin_read_threaded(Gps * gps);
/*private*/ extern void gps_garmin_read(Gps * gps);
/*private*/ extern void gps_dsgpms_od_read(Gps * gps);
/*private*/ extern void gps_webots_read_threaded(Gps * gps);
/*private*/ extern void gps_webots_read(Gps * gps);

#endif
